<?php 
defined('IN_YYS')or exit('Access Denied.');
System::DOWN_App_class('base','member','no');
System::DOWN_App_fun('my','index');
System::DOWN_App_fun('user','index');
System::DOWN_sys_fun('send');
System::DOWN_sys_fun('user');
class ajax_goods extends base {
	
	public function __construct(){
		parent::__construct();
		$this->db = System::DOWN_sys_class('model');
	}
	
	public function ajax_index_goods(){
		$datatype = $_REQUEST['datatype'];
		//if($_REQUEST['lng'] && !_getcookie('lng')){
			//_setcookie('lat',$_REQUEST['lat']);
			//_setcookie('lng',$_REQUEST['lng']);
		//}else{
		//	$_REQUEST['lng'] = _getcookie('lng');
		//	$_REQUEST['lat'] = _getcookie('lat');
		//}
		$result = $this->$datatype();
		if($result){
			$return['status'] = 1;
		}else{
			$return['status'] = 0;
		}
		$return['list'] = $result;
		if($this->page){
			$return['page'] = $this->page;
		}
		if($this->count){
			$return['count'] = $this->count;
		}

		echo json_encode($return);
		exit();
	}
	
	/*所有商品*/
	private function all(){
		$uid = $this->userinfo['uid'];
		$where = " zg.`isShelve` = '1' and su.`status` = '3' and su.`isCanSearch` = '1' and zg.`isfreepost` = '1'";
		$locationId = $this->userinfo['locationId'];
		$location = $this->db->YOne("select * from `@#_yonghu_location` where `id`='$locationId' ");
		if($location){
			$dcode = array();
			$dcode['provinceCode'] = $location['sheng'];
			$dcode['cityCode'] = $location['shi'];
			$dcode['districtCode'] = $location['xian'];
		}else{
			$dcode = $this->getGoodsLngLat($_REQUEST['lng'],$_REQUEST['lat']);
		}
		$where .= " and su.`province` = '$dcode[provinceCode]' and su.`city` = '$dcode[cityCode]' and su.`country` = '$dcode[districtCode]' ";
		$keyword = htmlspecialchars($_REQUEST['keyword']);
		if($keyword){
			$where .= " and zg.`title` like '%$keyword%'";
		}
		$cateid = intval($_REQUEST['cateid']);
		if($cateid){
			$where .= " and (zg.`cateid` = '$cateid' or zg.`catesubid` = '$cateid') ";
		}
		$count = $this->db->YCount("select zg.* from `@#_zg_shangpin` as zg left join `@#_supplier` as su on zg.supplierId = su.id where $where");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_REQUEST['page'])){$fenyenum=$_REQUEST['page'];}else{$fenyenum=1;}	
		$fenye->config($count,100000,$fenyenum,"0");		
		$list = $this->db->YPage("select zg.* from `@#_zg_shangpin` as zg left join `@#_supplier` as su on zg.supplierId = su.id where $where ORDER BY su.`credibility` DESC ,zg.`qishu` DESC",array("num"=>100000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$count = $this->db->YCount("select * from `@#_zg_yonghu_yys_record` where `shopid` = '$one[id]'");
			$collection = $this->db->YCount("select * from `@#_collection` where `uid` = '$uid' and `collection_type` = 'goods' and `collection_id` = '$one[id]'");
			if($collection['count'] >0){
				$list[$key]['collection'] = 1;
			}else{
				$list[$key]['collection'] = 0;
			}
			$list[$key]['buyCount'] = $count['count'];
			$list[$key]['supplierArea'] = getCityName(getSupplierInfo($one['supplierId'],'city'));
			$list[$key]['supplierName'] = getSupplierInfo($one['supplierId'],'name');
		}
		if(ceil($count/($fenyenum * 100000))){
			$this->page = $fenyenum + 1;
		}
		$this->count = $count;
		return $list;
	}
	
	/*即将揭晓*/
	private function being(){
		$uid = $this->userinfo['uid'];
		$where = " zg.`isShelve` = '1' and su.`status` = '3' and su.`isCanSearch` = '1' and zg.`isfreepost` = '1'";
		$cateid = intval($_REQUEST['cateid']);
		if($cateid){
			$where .= " and (zg.`cateid` = '$cateid' or zg.`catesubid` = '$cateid') ";
		}
		$locationId = $this->userinfo['locationId'];
		$location = $this->db->YOne("select * from `@#_yonghu_location` where `id`='$locationId' ");
		if($location){
			$dcode = array();
			$dcode['provinceCode'] = $location['sheng'];
			$dcode['cityCode'] = $location['shi'];
			$dcode['districtCode'] = $location['xian'];
		}else{
			$dcode = $this->getGoodsLngLat($_REQUEST['lng'],$_REQUEST['lat']);
		}
		$where .= " and su.`province` = '$dcode[provinceCode]' and su.`city` = '$dcode[cityCode]' and su.`country` = '$dcode[districtCode]' ";
		$count = $this->db->YCount("select zg.* from `@#_zg_shangpin` where `shenyurenshu` > 0 and `isCoupon` = '0'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,1000,$fenyenum,"0");		
		$list = $this->db->YPage("select zg.* from `@#_zg_shangpin` where `shenyurenshu` > 0 and `isCoupon` = '0' ORDER BY `shenyurenshu` ASC, id desc",array("num"=>1000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$count = $this->db->YCount("select * from `@#_zg_yonghu_yys_record` where `shopid` = '$one[id]'");
			$collection = $this->db->YCount("select * from `@#_collection` where `uid` = '$uid' and `collection_type` = 'goods' and `collection_id` = '$one[id]'");
			if($collection['count'] >0){
				$list[$key]['collection'] = 1;
			}else{
				$list[$key]['collection'] = 0;
			}
			$list[$key]['buyCount'] = $count['count'];
			$list[$key]['supplierArea'] = getCityName(getSupplierInfo($one['supplierId'],'city'));
			$list[$key]['supplierName'] = getSupplierInfo($one['supplierId'],'name');
		}
		return $list;
	}
	
	/*人气*/
	private function hot(){
		$count = $this->db->YCount("select zg.* from `@#_zg_shangpin` where `shenyurenshu` > 0 and `isCoupon` = '0'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,1000,$fenyenum,"0");		
		$list = $this->db->YPage("select zg.* from `@#_zg_shangpin` where `shenyurenshu` > 0 and `isCoupon` = '0'ORDER BY `canyurenshu` DESC",array("num"=>1000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$count = $this->db->YCount("select * from `@#_zg_yonghu_yys_record` where `shopid` = '$one[id]'");
			$collection = $this->db->YCount("select * from `@#_collection` where `uid` = '$uid' and `collection_type` = 'goods' and `collection_id` = '$one[id]'");
			if($collection['count'] >0){
				$list[$key]['collection'] = 1;
			}else{
				$list[$key]['collection'] = 0;
			}
			$list[$key]['buyCount'] = $count['count'];
			$list[$key]['supplierArea'] = getCityName(getSupplierInfo($one['supplierId'],'city'));
			$list[$key]['supplierName'] = getSupplierInfo($one['supplierId'],'name');
		}
		return $list;
	}
	
	/*最新*/
	private function news(){
		$uid = $this->userinfo['uid'];
		$count = $this->db->YCount("select * from `@#_zg_shangpin` where `shenyurenshu` > 0 and `isCoupon` = '0'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,1000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_zg_shangpin` where `shenyurenshu` > 0 and `isCoupon` = '0'ORDER BY `time` DESC",array("num"=>1000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$count = $this->db->YCount("select * from `@#_zg_yonghu_yys_record` where `shopid` = '$one[id]'");
			$collection = $this->db->YCount("select * from `@#_collection` where `uid` = '$uid' and `collection_type` = 'goods' and `collection_id` = '$one[id]'");
			if($collection['count'] >0){
				$list[$key]['collection'] = 1;
			}else{
				$list[$key]['collection'] = 0;
			}
			$list[$key]['buyCount'] = $count['count'];
			$list[$key]['supplierArea'] = getCityName(getSupplierInfo($one['supplierId'],'city'));
			$list[$key]['supplierName'] = getSupplierInfo($one['supplierId'],'name');
		}
		return $list;
	}

	/*推荐*/
	private function command(){
		$uid = $this->userinfo['uid'];
		$where = " zg.`isShelve` = '1' and su.`status` = '3' and su.`isCanSearch` = '1' and zg.`isfreepost` = '1'";
		$cateid = intval($_REQUEST['cateid']);
		if($cateid){
			$where .= " and (zg.`cateid` = '$cateid' or zg.`catesubid` = '$cateid') ";
		}
		$keyword = htmlspecialchars($_REQUEST['keyword']);
		if($keyword){
			$where .= " and zg.`title` like '%$keyword%'";
		}
		$locationId = $this->userinfo['locationId'];
		$location = $this->db->YOne("select * from `@#_yonghu_location` where `id`='$locationId' ");
		if($location){
			$dcode = array();
			$dcode['provinceCode'] = $location['sheng'];
			$dcode['cityCode'] = $location['shi'];
			$dcode['districtCode'] = $location['xian'];
		}else{
			$dcode = $this->getGoodsLngLat($_REQUEST['lng'],$_REQUEST['lat']);
		}
		$where .= " and su.`province` = '$dcode[provinceCode]' and su.`city` = '$dcode[cityCode]' and su.`country` = '$dcode[districtCode]' ";
		$count = $this->db->YCount("select zg.* from `@#_zg_shangpin` as zg left join `@#_supplier` as su on zg.supplierId = su.id where $where");
		$this->count = $count;
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,1000,$fenyenum,"0");		
		$list = $this->db->YPage("select zg.* from `@#_zg_shangpin` as zg left join `@#_supplier` as su on zg.supplierId = su.id where $where ORDER BY zg.`qishu` DESC",array("num"=>1000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$count = $this->db->YCount("select * from `@#_zg_yonghu_yys_record` where `shopid` = '$one[id]'");
			$collection = $this->db->YCount("select * from `@#_collection` where `uid` = '$uid' and `collection_type` = 'goods' and `collection_id` = '$one[id]'");
			if($collection['count'] >0){
				$list[$key]['collection'] = 1;
			}else{
				$list[$key]['collection'] = 0;
			}
			$list[$key]['buyCount'] = $count['count'];
			$list[$key]['supplierArea'] = getCityName(getSupplierInfo($one['supplierId'],'city'));
			$list[$key]['supplierName'] = getSupplierInfo($one['supplierId'],'name');
		}
		return $list;
	}
	
	/*价格*/
	private function priceUp(){
		$uid = $this->userinfo['uid'];
		$where = " zg.`isShelve` = '1' and su.`status` = '3' and su.`isCanSearch` = '1' and zg.`isfreepost` = '1'";
		$cateid = intval($_REQUEST['cateid']);
		if($cateid){
			$where .= " and (zg.`cateid` = '$cateid' or zg.`catesubid` = '$cateid') ";
		}
		$locationId = $this->userinfo['locationId'];
		$location = $this->db->YOne("select * from `@#_yonghu_location` where `id`='$locationId' ");
		if($location){
			$dcode = array();
			$dcode['provinceCode'] = $location['sheng'];
			$dcode['cityCode'] = $location['shi'];
			$dcode['districtCode'] = $location['xian'];
		}else{
			$dcode = $this->getGoodsLngLat($_REQUEST['lng'],$_REQUEST['lat']);
		}
		$where .= " and su.`province` = '$dcode[provinceCode]' and su.`city` = '$dcode[cityCode]' and su.`country` = '$dcode[districtCode]' ";
		$keyword = htmlspecialchars($_REQUEST['keyword']);
		if($keyword){
			$where .= " and zg.`title` like '%$keyword%'";
		}
		$count = $this->db->YCount("select zg.* from `@#_zg_shangpin` as zg left join `@#_supplier` as su on zg.supplierId = su.id where $where");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,1000,$fenyenum,"0");		
		$list = $this->db->YPage("select zg.* from `@#_zg_shangpin` as zg left join `@#_supplier` as su on zg.supplierId = su.id where $where ORDER BY zg.`yunjiage` ASC",array("num"=>1000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$count = $this->db->YCount("select * from `@#_zg_yonghu_yys_record` where `shopid` = '$one[id]'");
			$collection = $this->db->YCount("select * from `@#_collection` where `uid` = '$uid' and `collection_type` = 'goods' and `collection_id` = '$one[id]'");
			if($collection['count'] >0){
				$list[$key]['collection'] = 1;
			}else{
				$list[$key]['collection'] = 0;
			}
			$list[$key]['buyCount'] = $count['count'];
			$list[$key]['supplierArea'] = getCityName(getSupplierInfo($one['supplierId'],'city'));
			$list[$key]['supplierName'] = getSupplierInfo($one['supplierId'],'name');
		}
		return $list;
	}
	
	private function priceDown(){
		$uid = $this->userinfo['uid'];
		$where = " zg.`isShelve` = '1' and su.`status` = '3' and su.`isCanSearch` = '1' and zg.`isfreepost` = '1'";
		$cateid = intval($_REQUEST['cateid']);
		if($cateid){
			$where .= " and (zg.`cateid` = '$cateid' or zg.`catesubid` = '$cateid') ";
		}
		$locationId = $this->userinfo['locationId'];
		$location = $this->db->YOne("select * from `@#_yonghu_location` where `id`='$locationId' ");
		if($location){
			$dcode = array();
			$dcode['provinceCode'] = $location['sheng'];
			$dcode['cityCode'] = $location['shi'];
			$dcode['districtCode'] = $location['xian'];
		}else{
			$dcode = $this->getGoodsLngLat($_REQUEST['lng'],$_REQUEST['lat']);
		}
		$where .= " and su.`province` = '$dcode[provinceCode]' and su.`city` = '$dcode[cityCode]' and su.`country` = '$dcode[districtCode]' ";
		$keyword = htmlspecialchars($_REQUEST['keyword']);
		if($keyword){
			$where .= " and zg.`title` like '%$keyword%'";
		}
		$count = $this->db->YCount("select zg.* from `@#_zg_shangpin` as zg left join `@#_supplier` as su on zg.supplierId = su.id where $where");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,1000,$fenyenum,"0");		
		$list = $this->db->YPage("select zg.* from `@#_zg_shangpin` as zg left join `@#_supplier` as su on zg.supplierId = su.id where $where ORDER BY zg.`yunjiage` DESC",array("num"=>1000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$count = $this->db->YCount("select * from `@#_zg_yonghu_yys_record` where `shopid` = '$one[id]'");
			$collection = $this->db->YCount("select * from `@#_collection` where `uid` = '$uid' and `collection_type` = 'goods' and `collection_id` = '$one[id]'");
			if($collection['count'] >0){
				$list[$key]['collection'] = 1;
			}else{
				$list[$key]['collection'] = 0;
			}
			$list[$key]['buyCount'] = $count['count'];
			$list[$key]['supplierArea'] = getCityName(getSupplierInfo($one['supplierId'],'city'));
			$list[$key]['supplierName'] = getSupplierInfo($one['supplierId'],'name');
		}
		return $list;
	}
	
	/*分类采集*/
	private function brand(){
		$uid = $this->userinfo['uid'];
		$where = " zg.`isShelve` = '1' and su.`status` = '3' and su.`isCanSearch` = '1' and zg.`isfreepost` = '1'";
		$cateid = intval($_REQUEST['cateid']);
		if($cateid){
			$where .= " and (zg.`cateid` = '$cateid' or zg.`catesubid` = '$cateid') ";
		}
		$keyword = htmlspecialchars($_REQUEST['keyword']);
		if($keyword){
			$where .= " and zg.`title` like '%$keyword%'";
		}
		$locationId = $this->userinfo['locationId'];
		$location = $this->db->YOne("select * from `@#_yonghu_location` where `id`='$locationId' ");
		if($location){
			$dcode = array();
			$dcode['provinceCode'] = $location['sheng'];
			$dcode['cityCode'] = $location['shi'];
			$dcode['districtCode'] = $location['xian'];
		}else{
			$dcode = $this->getGoodsLngLat($_REQUEST['lng'],$_REQUEST['lat']);
		}
		$where .= " and su.`province` = '$dcode[provinceCode]' and su.`city` = '$dcode[cityCode]' and su.`country` = '$dcode[districtCode]' ";
		$count = $this->db->YCount("select zg.* from `@#_zg_shangpin` as zg left join `@#_supplier` as su on zg.supplierId = su.id where $where");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,1000,$fenyenum,"0");		
		$list = $this->db->YPage("select zg.* from `@#_zg_shangpin` as zg left join `@#_supplier` as su on zg.supplierId = su.id where $where ORDER BY su.`credibility` DESC ,zg.`qishu` DESC",array("num"=>1000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$count = $this->db->YCount("select * from `@#_zg_yonghu_yys_record` where `shopid` = '$one[id]'");
			$collection = $this->db->YCount("select * from `@#_collection` where `uid` = '$uid' and `collection_type` = 'goods' and `collection_id` = '$one[id]'");
			if($collection['count'] >0){
				$list[$key]['collection'] = 1;
			}else{
				$list[$key]['collection'] = 0;
			}
			$list[$key]['buyCount'] = $count['count'];
			$list[$key]['supplierArea'] = getCityName(getSupplierInfo($one['supplierId'],'city'));
			$list[$key]['supplierName'] = getSupplierInfo($one['supplierId'],'name');
		}
		return $list;
	}
	
	/*最新揭晓*/
	public function newLottery($page=1,$num=1000){
		$count = $this->db->YCount("select * from `@#_zg_shangpin` where `shenyurenshu` = 0 and `q_uid` IS NOT NULL");
		$fenye = System::DOWN_sys_class('page');
		if(isset($page)){
			$fenyenum = $page;
		}else{
			$fenyenum=1;
		}	
		$fenye->config($count,$num,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_zg_shangpin` where `shenyurenshu` = 0 and `q_uid` IS NOT NULL ORDER BY `q_end_time` DESC",array("num"=>$num,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $val){
			$userInfo = unserialize($val['q_user']);
			$list[$key]['username'] = $userInfo['username'];
			$list[$key]['q_end_time'] = date('Y-m-d H:i:s',$val['q_end_time']);
			$list[$key]['img'] = $userInfo['img'];
			$gounumber = $this->db->YOne("select sum(`gonumber`) as `gonumber` from `@#_yonghu_yys_record` where `shopid` = '$val[id]'");
			$list[$key]['gonumber'] = $gounumber['gonumber'];
			$list[$key]['supplierArea'] = getCityName(getSupplierInfo($one['supplierId'],'city'));
			$list[$key]['supplierName'] = getSupplierInfo($one['supplierId'],'name');
			$count = $this->db->YCount("select * from `@#_zg_yonghu_yys_record` where `shopid` = '$one[id]'");
			$list[$key]['buyCount'] = $count['count'];
		}
		return $list;
	}
	
	
	/*推荐宝贝*/
	private function collect_recommend(){
		$count = $this->db->YCount("select * from `@#_zg_shangpin` where `shenyurenshu` > 0");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,1000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_zg_shangpin` where `shenyurenshu` > 0 ORDER BY `money` DESC",array("num"=>1000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		return $list;
	}
	
	/*收藏宝贝*/
	private function collect_goods(){
		$uid = $this->userinfo['uid'];
		$count = $this->db->YCount("select * from `@#_collection` where `uid` = '$uid' and `status` = '1' and `collection_type` = 'goods'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}
		$fenye->config($count,1000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_collection` where `uid` = '$uid' and`uid` = '$uid' and `status` = '1' and `collection_type` = 'goods' ORDER BY `create_time` DESC",array("num"=>1000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$goods = $this->db->YOne("select * from `@#_zg_shangpin` where `id` = '$one[collection_id]'");
			$supplier = $this->db->YOne("select * from `@#_supplier` where `id` = '$goods[supplierId]'");
			$list[$key]['address'] = getCityName($supplier['country']);
			$list[$key]['supplier_name'] = $supplier['name'];
			$list[$key]['thumb'] = $goods['thumb'];
			$list[$key]['money'] = $goods['money'];
			$list[$key]['title'] = $goods['title'];
			$list[$key]['yunjiage'] = $goods['yunjiage'];
			$list[$key]['isfreepost'] = $goods['isfreepost'];
			$list[$key]['goods_id'] = $goods['id'];
			$list[$key]['cid'] = $one['id'];
			$list[$key]['supplierPercent'] = $goods['supplierPercent'];
		}
		return $list;
	}
	
	/*收藏商铺*/
	private function collect_supplier(){
		$uid = $this->userinfo['uid'];
		$count = $this->db->YCount("select * from `@#_collection` where `uid` = '$uid' and `status` = '1' and `collection_type` = 'supplier'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,1000,$fenyenum,"0");		
		$lists = $this->db->YPage("select * from `@#_collection` where `uid` = '$uid' and `status` = '1' and `collection_type` = 'supplier' ORDER BY `create_time` DESC",array("num"=>1000,"page"=>$fenyenum,"type"=>1,"cache"=>0));

		$list = array();
		foreach($lists as $key => $one){
			$supplier = $this->db->YOne("select * from `@#_supplier` where `id` = '$one[collection_id]'");
			$supplier['address'] = /*getCityName($supplier['province']).*/getCityName($supplier['city']).getCityName($supplier['country']).getCityName($supplier['street']);/*.$supplier['address'];*/
			$supplier['collectId'] = $one['id'];
			$list[] = $supplier;
		}
		return $list;
	}
	
	/*收藏卡券*/
	private function collect_coupon(){
		$count = $this->db->YCount("select * from `@#_collection` where `status` = '1' and `collection_type` = '0'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,1000,$fenyenum,"0");		
		$lists = $this->db->YPage("select * from `@#_collection` where `status` = '1' and `collection_type` = '0' ORDER BY `create_time` DESC",array("num"=>1000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		$list = array();
		foreach($lists as $key => $one){
			$list[] = $this->db->YOne("select * from `@#_zg_shangpin` where `id` = '$one[collection_id]'");
		}
		return $list;
	}
	
	/*失效宝贝*/
	private function collect_invild(){
		$count = $this->db->YCount("select * from `@#_collection` where `status` = '1' and `collection_type` = '0'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,1000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_collection` where `status` = '1' and `collection_type` = '0' ORDER BY `create_time` DESC",array("num"=>1000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		$list['collect'] = array();
		foreach($list as $key => $one){
			$list['collect'][] = $this->db->YOne("select * from `@#_zg_shangpin` where `id` = '$one[collection_id]'");
		}
		return $list;
	}
	
	
	/*分页加载*/
	public function getAjaxPage(){
		$functionName = $_REQUEST['functionName'];
		if($functionName){
			$page = intval($_POST['page']);
			if(!$page || $page <= 0){
				$page = 1;
			}
			$num = intval($_POST['num']);
			if(!$num || $num <= 0){
				$num = 1000;
			}
			$return['list'] = $this->$functionName($page,$num);
			$return['status'] = 1;
		}else{
			$return = array(
				'status' => 0,
				'msg' => '内部错误',
			);
		}
		echo json_encode($return);
	}
	
	/*收藏*/
	public function doCollection(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$id = intval($_REQUEST['id']);
		$type = $_REQUEST['type'];
		if(!$id || !$type){
			$status = 0;
			$msg = '缺少必要参数';
		}else{
			$collection = $this->db->YOne("select * from `@#_collection` where `uid` = '$uid' and `collection_id` = '$id' and `collection_type` = '$type'");
			if(!$collection){
				$data = array(
					'uid' => $uid,
					'collection_id' => $id,
					'create_time' => time(),
					'status' => 1,
					'collection_type' => $type
				);
				$res = insertInto($data,'collection');
				if($res){
					$status = 1;
					$msg = '收藏成功';
				}else{
					$status = 0;
					$msg = '收藏失败，请稍后再试！';
				}
			}else{
				$status = 0;
				$msg = '已经收藏过了';
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg));
	}
	
	public function delCollection(){
		$id = intval($_REQUEST['id']);
		$collection = $this->db->YOne("select * from `@#_collection` where `id` = '$id'");
		if(!$id || !$collection){
			$status = 0;
		}else{
			$res = deleteInfo($id,'collection');
			if($res){
				$status = 1;
			}
		}
		echo json_encode(array('status'=>$status));
	}
	
	private function getGoodsLngLat($lng,$lat){
		$url = "http://api.map.baidu.com/geocoder/v2/?location=$lat,$lng&output=json&pois=0&ak=HrdIKAFcRojIa5MCysQwX4ux";
		$result = httpGet($url);
		$result = json_decode($result,true);
		$province = str_replace('省','',$result['result']['addressComponent']['province']);
		$district = str_replace('市','',$result['result']['addressComponent']['district']);
		$city = str_replace('市','',$result['result']['addressComponent']['city']);
		$provinceCode = $this->db->YOne("select * from `@#_area` where `name` like '%$province%' and `level` = '1'");
		$provinceCode = $provinceCode['code'];
		$cityCode = $this->db->YOne("select * from `@#_area` where `parentId` = '$provinceCode' and `name` like '%$city%' and `level` = '2'");
		$cityCode = $cityCode['code'];
		$districtCode = $this->db->YOne("select * from `@#_area` where `parentId` = '$cityCode' and`name` like '%$districtCode%' and `level` = '3'");
		$districtCode = $districtCode['code'];
		$districtCode = $result['result']['addressComponent']['adcode'];
		return array('provinceCode'=>$provinceCode,'cityCode'=>$cityCode,'districtCode'=>$districtCode);
	}
}