<?php 
defined('IN_YYS')or exit('Access Denied.');
System::DOWN_App_class('base','member','no');
System::DOWN_App_fun('user','index');
class cart extends base {
	private $gouwuchelist;
	
	public function __construct(){
		parent::__construct();
		$cartZgList = _getcookie('cartZgList');
		$cartZgList = json_decode(stripslashes($cartZgList),true);
		
		$goods_ids = $goods_ids_str = $_REQUEST['goods_ids'];
		if($goods_ids){
			$goods_ids = explode(',',$goods_ids);
			$goods_ids = array_filter($goods_ids);
			foreach($cartZgList as $key => $one){
				foreach($one as $k => $o){
					if(!in_array($o['shangpinId'],$goods_ids)){
						unset($cartZgList[$key][$k]);
					}
				}
			}
		}
		$this->goods_ids = $goods_ids_str;
		$this->coupon_ids = $_REQUEST['coupon_ids'];
		$this->cartZgList = $cartZgList;
		$this->Cartlistcoupon = _getcookie('Cartlistcoupon');
		$this->db = System::DOWN_sys_class("model");
		if(!$this->userinfo){
			header("location: ".LOCAL_PATH."/mobile/user/login");	
		}
		
		$this->notice_list = $this->db->YList("select * from `@#_notice` order by `sort` desc ,`create_time` desc limit 4");

		$this->nav = $this->segment(2);
	}
	
	public function cartlist(){
		$biaoti = '购物车';
		$index="i3";
		$cartZgList = $this->cartZgList;
		$Mcartlist = array();
		$totalNum = 0;
		$totalMoney = 0;
		//清掉众筹商品
		foreach($cartZgList as $key => $one){
			$supplier_num = 0;
			$supplier_total = 0;
			$coupon = 0;
			//unset($one['supplier_total']);
			//unset($one['supplier_num']);
			$supplier_total = 0;
			$supplier_num = 0;
			foreach($one as $k => $o){
				if(intval($o['isZhongChou']) == 1){
					unset($one[$k]);
					unset($cartZgList[$key][$k]);
					$haveZhongChou = 1;
				}
			}
		}
		$this->cartZgList = $cartZgList;
		_setcookie('cartZgList', json_encode($this->cartZgList));
		//开始正常循环
		foreach($cartZgList as $key => $one){
			$supplier_num = 0;
			$supplier_total = 0;
			$coupon = 0;
			//unset($one['supplier_total']);
			//unset($one['supplier_num']);
			$supplier_total = 0;
			$supplier_num = 0;
			foreach($one as $k => $o){
				if(intval($o['isZhongChou']) == 1){
					unset($one[$k]);
					unset($cartZgList[$key][$k]);
					$haveZhongChou = 1;
				}else{
					$haveZhongChou = 0;
					$shangpin = $this->db->YOne("select * from `@#_zg_shangpin` where `id` = '$o[shangpinId]'");
					$supplier = $this->db->YOne("select * from `@#_supplier` where `id` = '$shangpin[supplierId]'");
					$shangpin['now_num'] = $o['num'];
					$shangpin['maxnum'] = $shangpin['maxqishu'];
					$shangpin['now_price'] = number_format($o['num'] * $shangpin['yunjiage'],2);
					$shangpin['supplierName'] = $supplier['name'];
					$shangpin['color'] = $o['color'];
					$shangpin['type'] = $o['type'];
					$totalNum += $o['num'];
					$totalMoney += $o['num'] * $shangpin['yunjiage'];
					$one[$k] = $shangpin;
					$supplier_num += $o['num'];
					$supplier_total += $o['num'] * $shangpin['yunjiage'];
					$coupon += $shangpin['supplierPercent'] * $o['num'];
				}
			}
			$supplier['supplier_num'] = number_format($supplier_num,2);
			$supplier['supplier_total'] = number_format($supplier_total,2);
			$supplier['coupon'] = number_format($coupon,2);
			$Mcartlist[$key]['supplier'] = $supplier;
			$Mcartlist[$key]['goods'] = $one;
		}
		
		// echo "<pre>";
		// print_r($Mcartlist);
		// die();
		
		$totalMoney = number_format($totalMoney,2);
		$notice = $this->notice_list;
		include templates("mobile/cart","cartlist");
	}
	
	public function pay(){ 
		$biaoti = "支付结算";
		$index="i3";
        $webname=$this->_yys['web_name'];	
		parent::__construct();	
		if(!$huiyuan=$this->userinfo){
			header("location: ".LOCAL_PATH."/mobile/user/login");	
		}

		$ajax_order = 

		//如果是众筹结算
		$isZhongChou = 0;
		if(isset($_REQUEST['jiesuansubmit'])){
			$isZhongChou = 1;
			$shangpinid = $_REQUEST['id'];
			$sid = $_REQUEST['sid'];
			$item_num = $_REQUEST['item_num'];
			$this->cartZgList = array($sid => array(0=>array("shangpinId"=>$shangpinid,"num"=>$item_num,"type"=>null,"color"=>null,"isZhongChou"=>1)));
			_setcookie('cartZgList', json_encode($this->cartZgList));
		}
		$cartZgList = $this->cartZgList;
	
		if($cartZgList){
			if(isset(current($cartZgList)[0]['isZhongChou']) && intval(current($cartZgList)[0]['isZhongChou'])==1){
				$isZhongChou = 1;
			}
			$Mcartlist = array();
			$totalNum = 0;
			$totalMoney = 0;
			foreach($cartZgList as $key => $one){
				unset($one['supplier_total']);
				unset($one['supplier_num']);
				foreach($one as $k => $o){
					if($isZhongChou == 0){
						$shangpin = $this->db->YOne("select * from `@#_zg_shangpin` where `id` = '$o[shangpinId]'");
					}else{
						$shangpin = $this->db->YOne("select * from `@#_shangpin` where `id` = '$o[shangpinId]'");
					}
					$supplier = $this->db->YOne("select * from `@#_supplier` where `id` = '$shangpin[supplierId]'");
					$coupon_0 = $this->db->YList("select * from `@#_yonghu_coupon` where `uid` = '$huiyuan[uid]' and `sid` = '$shangpin[supplierId]' and `status` = '0' and `type` = '0'");
					$coupon_0_count = $this->db->Yone("select COALESCE(sum(`money`),0) as `money` from `@#_yonghu_coupon` where `uid` = '$huiyuan[uid]' and `sid` = '$shangpin[supplierId]' and `status` = '0' and `type` = '0'");
					$coupon_0_count = $coupon_0_count['money'];
					$coupon_1 = $this->db->YList("select * from `@#_yonghu_coupon` where `uid` = '$huiyuan[uid]' and `sid` = '$shangpin[supplierId]' and `status` = '0' and `type` = '1'");
					$coupon_1_count = $this->db->Yone("select COALESCE(sum(`money`),0) as `money` from `@#_yonghu_coupon` where `uid` = '$huiyuan[uid]' and `sid` = '$shangpin[supplierId]' and `status` = '0' and `type` = '1'");
					$coupon_1_count = $coupon_1_count['money'];
					$shangpin['now_num'] = $o['num'];
					$shangpin['maxnum'] = $shangpin['maxqishu'];
					$shangpin['now_price'] = $o['num'] * $shangpin['yunjiage'];
					$shangpin['supplierName'] = $supplier['name'];
					$shangpin['color'] = $o['color'];
					$shangpin['type'] = $o['type'];
					$totalNum += $o['num'];
					$totalMoney += $shangpin['now_price'];
					$one[$k] = $shangpin;
				}
				if($one){
					$Mcartlist[$key]['coupon_0'] = $coupon_0;
					$Mcartlist[$key]['coupon_1'] = $coupon_1;
					$Mcartlist[$key]['coupon_0_count'] = $coupon_0_count;
					$Mcartlist[$key]['coupon_1_count'] = $coupon_1_count;
					$Mcartlist[$key]['supplier'] = $supplier;
					$Mcartlist[$key]['goods'] = $one;
				}
			}
		}
		$Mcartlist = array_filter($Mcartlist);
		$payMoney = floatval($totalMoney);
		$totalMoney = floatval($totalMoney);
		//echo("商品总价：".$totalMoney);
		//echo "用户余额：".$huiyuan['money'];
		$Money = floatval($huiyuan['money']);
		$Money1 = $huiyuan['money1'];
		$yue = floatval($huiyuan['money']) - floatval($totalMoney);
		if($yue >= 0){
			//echo "能支付的起".$yue;
			$yue = 1;
		}else{
			//echo "付不起，要显示第三方支付";
			$yue = 0;
		}
		//exit();
		$gold = $Money1 - $totalMoney;
		if($gold >= 0){
			$gold = 1;
		}else{
			$gold = 0;
		}
		
		$paylist = $this->db->Ylist("select * from `@#_payment` where `pay_start` = '1' and `mobile` = '1'");
		$addressId = _getcookie('addressId');
		if($addressId){
			$dizhi = $this->db->YOne("select * from `@#_yonghu_dizhi` where `id` = '$addressId' and `uid` = '$huiyuan[uid]'");
		}else{
			$dizhi = $this->db->YOne("select * from `@#_yonghu_dizhi` where `uid` = '$huiyuan[uid]' and `default` = 'Y'");
			_setcookie('addressId',$dizhi['id']);
		}
		if($dizhi){
			$sheng = getCityName($dizhi['sheng']);
			$shi = getCityName($dizhi['shi']);
			$xian = getCityName($dizhi['xian']);
			$jiedao = getCityName($dizhi['jiedao']);
			$dizhi['address'] = $sheng.$shi.$xian.$jiedao.$dizhi['address'];
		}
		$goods_ids = $this->goods_ids;
		$coupon_ids = $this->coupon_ids;
		include templates("mobile/cart","payment");
	}

	private function unicode_decode($name)  
	{  
		// 转换编码，将Unicode编码转换成可以浏览的utf-8编码  
		$pattern = '/([\w]+)|(\\\u([\w]{4}))/i';  
		preg_match_all($pattern, $name, $matches);  
		if (!empty($matches))  
		{  
			$name = '';  
			for ($j = 0; $j < count($matches[0]); $j++)  
			{  
				$str = $matches[0][$j];  
				if (strpos($str, '\\u') === 0)  
				{  
					$code = base_convert(substr($str, 2, 2), 16, 10);  
					$code2 = base_convert(substr($str, 4), 16, 10);  
					$c = chr($code).chr($code2);  
					$c = iconv('UCS-2', 'UTF-8', $c);  
					$name .= $c;  
				}  
				else  
				{  
					$name .= $str;  
				}  
			}  
		}  
		return $name;  
	}  
	
	//开始支付
	public function paysubmit(){
		$index="i3";
		$webname = $this->_yys['web_name'];
		header("Cache-control: private");
		parent::__construct();	
		if(!$this->userinfo){
			header("location: ".LOCAL_PATH."/mobile/user/login");
			exit;
		}
		$pay_checkbox=false;//默认不采用账户余额支付
		$uid = $this->userinfo['uid'];
		$payment_id = intval($_POST['payment']); //支付方式id。 微信：8，支付宝：10。余额：0
		$payment = $this->db->YOne("select * from `@#_payment` where `pay_id` = '$payment_id' and `pay_start`='1' ");//支付方式详情
		$pay = System::DOWN_App_class('pay','pay');
		$pay->fufen = 0;
		
		$yue = $_REQUEST['yue'];//余额
		$gold = $_REQUEST['gold'];//货款
		
		if(!empty($payment)){//微信 或 支付宝
			$pay_type_bank = $payment['pay_class'];//微信: weixin. 支付宝：malipay
		}
		
		//banktype已经作废，所以pay_type_id就是支付方式id
		if($banktype != 'nobank'){
			$pay_type_id = $payment_id;
		}
		
		if($payment_id == '0'){ //非 微信 和 支付宝 支付
			$pay_checkbox=true;//余额 支付
		}
		
		if($yue){//余额充足必须余额支付
			$pay_checkbox = true;
		}
		
		/*
			优惠券
			处理是否`uid` = '$huiyuan[uid]' and `sid` = '$shangpin[supplierId]' and `status` = '0' and `type` = '1'");

		*/
		$usable_coupon = true;
		$coupon_1 = $_REQUEST['coupon_1'];
		$is_coupon_1 = intval($_REQUEST['is_coupon_1']);
		if($is_coupon_1 == 0){
			$coupon_1 = null;
		}
		//echo("所有可用的优惠券：");
		//print_r($coupon_1);
		foreach($coupon_1 as $key => $one){//遍历优惠券
			$coupon = $this->db->YOne("select * from `@#_yonghu_coupon` where `id` = '$one[0]' and `status` = '0' and `type` = '1'");
			if($coupon['status'] != 0 || !$coupon){//如果状态不是0，或者不存在此优惠券
				$usable_coupon = false;//只要存在无效优惠券就报错
			}
		}
		if(!$usable_coupon){
			_notemobile("不存在此优惠券或优惠券不可用");
		}
		/* 优惠券处理结束 */
		
		/*
			汇券
			处理是否`uid` = '$huiyuan[uid]' and `sid` = '$shangpin[supplierId]' and `status` = '0' and `type` = '1'");
		*/
		$usable_coupon_other = true;
		$coupon_0 = $_REQUEST['coupon_0'];
		$is_coupon_0 = intval($_REQUEST['is_coupon_0']);
		if($is_coupon_0 == 0){
			$coupon_0 = null;
		}
		foreach($coupon_0 as $key => $one){//遍历汇券
			$coupon = $this->db->YOne("select * from `@#_yonghu_coupon` where `id` = '$one[0]' and `status` = '0' and `type` = '0'");
			if($coupon['status'] != 0 || !$coupon){
				$usable_coupon_other = false;
			}
		}
		//echo("所有可用的汇券：");
		//print_r($coupon_0);
		if(!$usable_coupon_other){
			_notemobile("不存在此汇奖券或汇奖券不可用");
		}
		/* 汇券处理结束 */

		$this->coupon_0= $coupon_0;//所有汇券
		$pay->coupon_0 = $coupon_0;
		$se_coupon_0 = serialize($coupon_0);

		$this->coupon_1= $coupon_1;//所有优惠券
		$pay->coupon_1 = $coupon_1;
		$se_coupon_1 = serialize($coupon_1);

		$pay->pay_type_bank = $pay_type_bank;//weixin/malipay/undefined
		$pay->goods_ids = $_REQUEST['goods_ids'];//所有商品id
		$pay->coupon_ids = $_REQUEST['coupon_ids'];
		$pay->addressId = _getcookie('addressId');
		$pay->cartZgList = json_decode(stripslashes(_getcookie('cartZgList')),true);
		$pay->cartZgList = serialize($pay->cartZgList);
		$cartZgList = unserialize($pay->cartZgList);
		if(current($cartZgList)[0]['isZhongChou'] == 1){
			$pay->isZhongChou = 1;
		}
		$pay->uid = $uid;
		/* 修复coupon_ids、addressId、goods_ids、cartZgList、coupon_0、coupon_1不能随cookie保存 */
		$this->db->query("update `@#_yonghu` set `cartZgList` = '$pay->cartZgList',`coupon_ids` = '$pay->coupon_ids',`addressId` = '$pay->addressId',`goods_ids` = '$pay->goods_ids',`coupon_0` = '$se_coupon_0',`coupon_1` = '$se_coupon_1'  where `uid` = '$uid'");
		/* 修复完毕 */
		//初始化支付接口 （用户id，支付方式id，买商品）
		$ok = $pay->init($uid,$pay_type_id,'go_record');
		if($ok != 'ok'){
			_setcookie('cartList',NULL);		
			_notemobile("出错啦：".$ok."<br>请<a href='".LOCAL_PATH."/mobile/cart/cartlist' style='color:#22AAFF'>返回购物车</a>查看");
		}
		//支付，pay_checkbox:是否用余额支付
		$check = $pay->start_pay($pay_checkbox);
		if(!$check && is_array($check)){
			_notemobile($check.",请<a href='".LOCAL_PATH."/mobile/cart/cartlist' style='color:#22AAFF'>返回购物车</a>查看");
		}
		if($check){
			//成功
			if($pay->isZhongChou == 1){
				//标记订单已完成
				$res = updateSet(array('status'=>4),'order_list',$pay->dingdancode,'order_sn');
				if(!$this->yaojiang($pay->dingdancode) || !$res){
					_setcookie('cartList',NULL);
					_notemobile("出错啦！抽奖失败。<br>请<a href='".LOCAL_PATH."/mobile/cart/cartlist' style='color:#22AAFF'>返回购物车</a>查看");
				}
				header("location: ".LOCAL_PATH."/mobile/cart/paysuccesszc");
			}else{
				header("location: ".LOCAL_PATH."/mobile/cart/paysuccess");
			}
		}else{
			//失败	
			_setcookie('cartList',NULL);
			header("location: ".LOCAL_PATH."/mobile/mobile");
		}
		exit;
	}
	
	//成功页面
	public function paysuccess(){
		$index="i3";
		$uid = intval(_encrypt(_getcookie("uid"),'DECODE'));
		$this->userinfo=$this->db->YOne("SELECT * from `@#_yonghu` where `uid` = '$uid'");
		$userInfo = $this->userinfo;
		$mobile = $this->userinfo['mobile'];
	
		$Mcartlist=json_decode(stripslashes($this->Cartlist),true);
		$shopids='';	
		if(is_array($Mcartlist)){			
			foreach($Mcartlist as $key => $val){
				$shopids.=intval($key).',';				
			}
			$shopids=str_replace(',0','',$shopids);
			$shopids=trim($shopids,',');
		}	 
		$yyslist=array();
		if($shopids!=NULL){
			$yyslist=$this->db->Ylist("SELECT * FROM `@#_shangpin` where `id` in($shopids)",array("key"=>"id"));
		}
		foreach($yyslist as $k=>$v){
			push_weixin_msg(1,$this->userinfo['openid'],$this->userinfo['username'],$this->userinfo['uid'],$v['title'],$v['qishu']);
		}
		$webname=$this->_yys['web_name'];
		/* 手动清理购物车 */
		//当前付款的商品列表 deal_id_array
		$goods_ids = $this->userinfo['goods_ids'];
		$deal_id_array=explode(',',$goods_ids);
		$gouwuchelist = _getcookie('cartZgList');
		$gouwuchelist = json_decode(stripslashes($gouwuchelist),true);
		foreach($gouwuchelist as $key => $one){
			foreach($one as $k => $o){
				if(in_array($o['shangpinId'],$deal_id_array)){
					unset($gouwuchelist[$key][$k]);
				}
			}
			if(count($gouwuchelist[$key]) <= 0){
				unset($gouwuchelist[$key]);
			}
		}
		$gouwuchelist = json_encode($gouwuchelist);
		if($gouwuchelist){
			_setcookie('cartZgList',$gouwuchelist);
		}else{
			_setcookie('cartZgList',null);
		}
		//清除缓存

		/* end清理购物车 */
		_setcookie('cartList',NULL);
		include templates("mobile/cart","paysuccess");
	}

	public function yaojiang($order_sn){
		$order = $this->db->YOne("select * from `@#_order_list` where `order_sn` = '$order_sn' and `is_randcoupon` = '0'");
		if(!$order){
			return false;
		}else{
			return $this->makeRandCoupon($order_sn);
		}
	}
	
	public function makeRandCoupon($order_sn){
		/*查询有几单*/
		$order_item = $this->db->YList("select * from `@#_order_item` where `order_sn` = '$order_sn'");
		$total_num = 0;
		foreach($order_item as $key => $one){
			$total_num += $one['supplierPercent'];
		}
		$pay = System::DOWN_App_class('pay','pay');
		$pay->makeRandCoupon($order_sn,$total_num);
		$this->db->query("update `@#_order_list` set `is_randcoupon` = '1',`is_ok` = '1' where `order_sn` = '$order_sn'");
		return true;
	}

	//充值
	public function addmoney(){
		parent::__construct();	
		$webname = $this->_yys['web_name'];
		$index="i3";
		$money = $this->segment(4);
		$banktype = $this->segment(5); 		 
		if(!$this->userinfo){
			header("location: ".LOCAL_PATH."/mobile/user/login");
			exit;
		}
		
		if($money <= 0 || !$money){
			_notemobile("请填写正确的充值金额！");
		}
		
		$payment = $this->db->YOne("select * from `@#_payment` where `pay_id` = '$banktype'");
		if(!$payment){
			_notemobile("没有可用的支付方式！");
		}	

		if(!empty($zhifutype)){
			$pay_type_bank = $payment['pay_class'];
		}
		$pay_type_id = $banktype;
		$uid = $this->userinfo['uid'];
		$pay=System::DOWN_App_class('pay','pay');
		$pay->pay_type_bank = $pay_type_bank;
		$pay->init($uid,$pay_type_id,'addmoney_record',$money);
	}
	
	public function coupon(){
		$couponId = intval($this->segment(4));
		$coupon = $this->db->YOne("select * from `@#_coupon` where `id` = '$couponId'");
		$coupon['cart_gorenci'] = '1';
		$coupon['money'] = $coupon['discountPrice'];
		$coupon['num'] = 1;
		$yyslist[$couponId] = $coupon;

		if($yyslist){
			$shop = 1;
		}else{
			$shop = 0;
		}
		_setcookie('Cartlistcoupon',json_encode($yyslist));
		$Mcartlist=json_decode(stripslashes($this->Cartlistcoupon),true);
		
		include templates("mobile/cart","coupon");
	}
	
	public function paysuccesscoupon(){
		$code = $this->segment(4);
		$couponOrder 	= $this->db->YOne("select * from `@#_coupon_yonghu_yys_record` where `code` = '$code'");
		$coupon 		= $this->db->YOne("select * from `@#_coupon` where `id` = '$couponOrder[couponId]'");
		$couponCode 	= $this->db->YOne("select * from `@#_coupon_code` where `id` = '$couponOrder[cardId]'");
		$_COOKIE['Cartlistcoupon'] = NULL;
		_setcookie("Cartlistcoupon",null);
	
		include_once((dirname(dirname(dirname(dirname(__FILE__)))) . '/phpqrcode/phpqrcode.php'));
		$value = $coupon['couponUrl'];//二维码内容
		$errorCorrectionLevel = 'H';//容错级别
		$matrixPointSize = 6;//生成图片大小
		
		$huiyuan = $this->userinfo;
		//生成二维码图片
		$imgName = uniqid().'.png';
		$img = dirname(dirname(dirname(dirname(__FILE__)))).'/statics/uploads/couponImg/'.$imgName;
		
		QRcode::png($value, $img, $errorCorrectionLevel, $matrixPointSize, 2);
		$logo = YYS_UPLOADS_PATH . '/' . $coupon['thumb'];//准备好的logo图片
		$QR = '/statics/uploads/couponImg/'.$imgName;
		
		include templates("mobile/cart","paysuccesscoupon");		
	}
	
	public function paycoupon(){
		parent::__construct();	
		if(!$huiyuan=$this->userinfo)$this->HeaderLogin();
		$couponId = intval($this->segment(4));
		$coupon = $this->db->YOne("select * from `@#_coupon` where `id` = '$couponId'");
		$MoenyCount = $coupon['discountPrice'];
		$Money = $huiyuan['money'];
		$shopnum = 1;
		$yyslist[] = $coupon;
		
		
		include templates("mobile/cart","paycoupon");
	}

	//众筹成功页面
	public function paysuccesszc(){
		$biaoti = '众筹成功页';
		$_COOKIE['Cartlistzg'] = NULL;
			_setcookie("Cartlistzg",null);
		include templates("mobile/cart","paysuccesszc");
	}

	public function paysubmitcoupon(){
		//同时登陆补丁
		parent::__construct();
		
		if(!$this->userinfo){
		  header("location: ".LOCAL_PATH."/mobile/user/login");
		  exit;
		}	
		//同时登陆补丁结束
		$couponId = intval($this->segment(4));

		parent::__construct();	
		if(!$this->userinfo)$this->HeaderLogin();
		$uid = $this->userinfo['uid'];
	
		//$pay_checkbox=isset($_POST['moneycheckbox']) ? true : false;
		$pay_checkbox = true;
		$pay_type_bank=isset($_POST['pay_bank']) ? $_POST['pay_bank'] : false;
		$pay_type_id=isset($_POST['account']) ? $_POST['account'] : false;
						
		if(isset($_POST['shop_score'])){
			$fufen_yys = System::DOWN_App_config("user_fufen",'','member');	
			$fufen = intval($_POST['shop_score_num']);			
			if($fufen_yys['fufen_yuan']){				
				$fufen = intval($fufen / $fufen_yys['fufen_yuan']);
				$fufen = $fufen * $fufen_yys['fufen_yuan'];
			}			
		}else{
			$fufen = 0;
		}		
		/*************
			start
		*************/
	
		$pay=System::DOWN_App_class('pay','pay');

		$pay->fufen = $fufen;
		$pay->pay_type_bank = $pay_type_bank;
		$ok = $pay->init($uid,$pay_type_id,'go_record',null,'coupon');	//惠券商品	

		if($ok !== 'ok'){
			$_COOKIE['Cartlistcoupon'] = NULL;
			_setcookie("Cartlistcoupon",null);
			_note($ok,YYS_LOCAL_PATH);
		}
		
		$check = $pay->start_pay($pay_checkbox,'coupon');
		
		if($check === 'not_pay'){
			_note('未选择支付平台!',LOCAL_PATH.'/mobile/cart/coupon/'.$couponId);
		}
		if(!$check){
			_note("商品支付失败!",LOCAL_PATH.'/mobile/cart/coupon/'.$couponId);
		}
		if($check){
			//成功
			header("location: ".LOCAL_PATH."/mobile/cart/paysuccesscoupon/".$check);
		}else{
			//失败	
			$_COOKIE['Cartlistzg'] = NULL;
			_setcookie("Cartlistzg",null);
			header("location: ".LOCAL_PATH);
		}		
		exit;
	}
}


?>