<?php defined('G_IN_ADMIN')or exit('Access Denied.'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>商家货款明细</title>
<link rel="stylesheet" href="/statics/plugin/mstyle/css/style.css" type="text/css"><!--页面CSS-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/animate.min.css" type="text/css"><!--CSS3动画-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/timer.css" type="text/css"><!--CSS3动画-->
<script type="text/javascript" src="/statics/plugin/mstyle/js/jquery.min.js"></script><script type="text/javascript" charset="utf-8" src="<?php echo YYS_PLUGIN_PATH; ?>/calendar/calendar.js"></script><!--jQuery库-->
</head>
<body>
	<div class="container min-wid">
		<div class="path">
			<i><a href="#"><img src="/statics/plugin/mstyle/img/ico_1.png" alt=""></a></i>
			<p>
				当前位置：<a href="#">财务管理</a> > <a href="#">商家货款明细</a> >
			</p>
			<div class="push">
				<a href="<?php echo YYS_MODULE_PATH; ?>/index/Tdefault">欢迎界面</a>
				<a href="javascript:void(0)" onclick="location=location">刷新框架</a>
				<a href="<?php echo YYS_MODULE_PATH; ?>/cache/init">清理缓存</a>
			</div>
		</div><!-- path -->
		<div class="consumption wid1">
			<div class="pict">
				<img src="/statics/plugin/mstyle/img/pic_02.png" alt="">
			</div>
			<div class="fl-box">
			<form method="POST" action="#">
				<?php 
				if($supplierId){
					echo '<var>商家ID</var>
					<input type="text" value="'.$supplier["id"].'" disabled>
					<var>店铺名称</var>
					<input class="lar" type="text" value="'.$supplier["name"].'" disabled="">';
				}else if($_REQUEST['id']){
					echo '<var>会员ID</var>
					<input type="text" name="id" value="'.$supplier["id"].'">
					<var>店铺名称</var>
					<input class="lar" type="text" name="name" value="'.$supplier["name"].'">
					<button type="submit" name="namesubmit" value="筛选">筛选</button>';
				}else{
					echo '<var>会员ID</var>
					<input type="text" name="id" value="">
					<var>店铺名称</var>
					<input class="lar" name="name" type="text" value="">
					<button type="submit" name="namesubmit" value="筛选">筛选</button>';
				}
				?>
				<div class="grate-date">
					查询日期
					<input id="in1" type="text" name="start_time" value='<?php if(isset($_POST["start_time"])) echo $_POST["start_time"]; else echo "2018-1-1"; ?>' class="date_picker">
					至
					<input id="in2" type="text" name="end_time" value='<?php if(isset($_POST["end_time"])) echo $_POST['end_time']; else echo '2018-2-1'; ?>'  class="date_picker">
				</div>
				<button type="submit" name="sososubmit" value="筛选">筛选</button>
			</form>
			</div>
			<div class="fr-box">
				<span>查询货款<i><?php echo $summoeny['sum_money'];?></i>元</span>
				<!--span>每页显示:
					<select name="" id="">
						<option value="">10</option>
						<option value="">20</option>
						<option value="">30</option>
					</select>
				</span-->
			</div>
		</div>
		<div class="consumption-table dt wid1">
			<ul>
				<li class="head">
					<span>会员ID</span>
					<span>会员昵称</span>
					<span>消费商家</span>
					<span>商品名称</span>
					<span>商品价格</span>
					<span>数量</span>
					<span>返券金额</span>
					<span>记账时间</span>
					<span>货款</span>
				</li>
    	<?php 
		//foreach($gold as $v){ 
		for($j=0;$j<count($gold);$j++){
			$goods_id = $gold[$j]['goods_id'];
			$supplier = $this->db->Query("SELECT * FROM `yys_supplier` WHERE `id` in ( SELECT `supplierId` FROM `yys_zg_shangpin` WHERE `id` = '$goods_id' ) LIMIT 1");
			$supplier = $supplier->fetch_row();
			//print_r($supplier);
		?>
				<li>
					<span><?php echo $gold[$j]['uid']; ?></span>
					<span><?php  echo $huiyuans[$j];?></span>
					<span><?php echo $supplier[5]; ?></span>
					<span><a target="_blank" href="/mobile/mobile/goods/<?php echo $gold[$j]['goods_id']; ?>"><?php echo $gold[$j]['goods_name']; ?></a></span>
					<span><?php echo $gold[$j]['goods_price']; ?></span>
					<span><?php echo $gold[$j]['number']; ?></span>
					<span><?php echo $gold[$j]['supplier_percent']; ?></span>
					<span><?php echo date("Y-m-d H:i:s",$gold[$j]['time']); ?></span>
					<span><?php echo $gold[$j]['money']; ?></span>
				</li>
<?php }  ?>
			</ul>
		</div>
		<div class="pages">
			<span>
				<?php echo $fenye->show('new')['html']; ?>
			</span>
			<span>共 <?php echo ceil($zongji/$num); ?> 页</span>
		
			<span>到第<input type="text" id="page" value="">页</span>
			<span><a class="page-but" id="gopage" href="javascript:void(0);">确定</a></span>
		</div>
	</div><!-- container -->
</body>
<script src="/statics/plugin/mstyle/js/jquery.date_input.pack.js"></script>
<script src="/statics/plugin/mstyle/js/library-3.28.js"></script><!--自定义封装函数-->
<script src="/statics/plugin/mstyle/js/scrollanim.min.js"></script><!--动画效果库-->
<script>
	var date=new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var date = date.getDate();
	$("#in1").val(year+"-"+(month-1)+"-"+date);
	$("#in2").val(year+"-"+month+"-"+date);
	$("#gopage").click(function(){
		var page = $("#page").val();
		var allpage = "<?php echo ceil($zongji/$num); ?>";
		if(page == '' || allpage < page){
			var page = 1;
		}
		location.href = "<?php echo $fenye->show('new')['url']; ?>"+page;
	});
	
</script>
</html>
