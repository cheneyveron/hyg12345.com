<?php 

class tocode {

	private $go_list;
	public $go_code;
	public $go_content;
	public $cyrs;
	public $shop;
	public $count_time='';
	
	
	public function __construct() {
		//$myfile = fopen("ctest.txt", "a");
		//fwrite($myfile, "当前时间".microtime(true)." 进入tocode -> __construct\n");
		$this->db = System::DOWN_sys_class("model");
		//fwrite($myfile, "当前时间".microtime(true)." 构造完db\n");
		$url = "http://f.apiplus.net/cqssc.json";
		$curl = curl_init();
		//fwrite($myfile, "当前时间".microtime(true)." 创建完curl\n");
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($curl, CURLOPT_TIMEOUT,0.5);
		curl_setopt($curl, CURLOPT_HEADER,0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		//fwrite($myfile, "当前时间".microtime(true)." 设置完curl\n");
		$result = curl_exec($curl);
		//fwrite($myfile, "当前时间".microtime(true)." 抓取完url\n");
		curl_close($curl);
		$result = json_decode($result,true);
		
		$result = $result['data'];
		sort($result);
		//fwrite($myfile, "当前时间".microtime(true)." 整理完result\n");
		foreach($result as $key => $one){
			$info = $this->db->YOne("SELECT * FROM `@#_lottery_code` WHERE `expect` = '".$one['expect']."'");
			if(!$info){
				$one['opentime'] = strtotime($one['opentime']);
				$sql = "INSERT INTO `@#_lottery_code` (`expect`,`opencode`,`opentime`,`opentimestamp`) VALUES ('$one[expect]','$one[opencode]','$one[opentime]','$one[opentimestamp]')";
				$this->db->Query($sql);
			}
		}
		//fwrite($myfile, "当前时间".microtime(true)." 准备退出__construct\n");
		//fclose($myfile);
	}	

	public function config($shop=null,$type=null){
		$this->shop = $shop;		
	}
	
	public function get_run_code(){
		
	}
	
	public function returns(){
	
	
	}
	
	
	public function yunxing_shop(&$time='',$num=100,$cyrs='233'){
		if(empty($time))return false;
		if(empty($num))return false;
		if(empty($cyrs))return false;
		$this->times = $time;
		$this->num = $num;
		$this->cyrs = $cyrs;
		$this->get_code_user_html();
		$this->get_user_go_code();
	}

	private function get_code_dabai(){
		$go_list = $this->go_list;
		$html=array();
		$count_time = 0;
		foreach($go_list as $key=>$v){
			$html[$key]['time'] = $v['time'];	
			$html[$key]['username'] = $v['username'];	
			$html[$key]['uid'] = $v['uid'];
			$html[$key]['shopid'] = $v['shopid'];	
			$html[$key]['shopname'] = $v['shopname'];	
			$html[$key]['shopqishu'] = $v['shopqishu'];
			$html[$key]['gonumber'] = $v['gonumber'];			
			$h=abs(date("H",$v['time']));
			$i=date("i",$v['time']);
			$s=date("s",$v['time']);	
			list($time,$ms) = explode(".",$v['time']);
			$time = $h.$i.$s.$ms;
			$html[$key]['time_add'] = $time;
			$count_time += $time;			
		}
		$this->go_content = serialize($html);
		$this->count_time=$count_time;
		
		$cqssc = $this->db->YList("select * from `@#_lottery_code` order by `expect` desc limit 1");
		$cqssc = $cqssc['0'];
		$opencode = explode(',',$cqssc['opencode']);
		$goucode = '';
		foreach($opencode as $key => $one){
			$goucode .= (int)$one;
		}
		$this->go_sscid = $cqssc['id'];
		$this->go_code = 10000001+fmod(($count_time+$goucode),$this->cyrs);			
	}
	
	private function get_code_yibai(){		
		$time = $this->times;
		$cyrs = $this->cyrs;		
		$h=abs(date("H",$time));
		$i=date("i",$time);
		$s=date("s",$time);		
		$w=substr($time,11,3);
		$num= $h.$i.$s.$w;
		if(!$cyrs)$cyrs=1;
		
		$cqssc = $this->db->YList("select * from `@#_lottery_code` order by `expect` desc limit 1");
		$cqssc = $cqssc['0'];
		$opencode = explode(',',$cqssc['opencode']);
		$goucode = '';
		foreach($opencode as $key => $one){
			$goucode .= (int)$one;
		}
		
		$this->go_sscid = $cqssc['id'];
		$this->go_code = 10000001+fmod(($num*100+$goucode),$cyrs);
		$this->go_content = false;
	}
	
	private function get_user_go_code(){
		
		if(file_exists(YYS_MANAGE.'class/goodspecify/control/'.'itocode.class.php')):
			$itocode = System::DOWN_App_class("itocode","goodspecify");
			$itocode->go_itocode($this->shop,$this->go_code,$this->go_content,$this->count_time);
		endif;
		$this->get_code_user_html();
	}
		
	private function get_code_user_html(){		
		$time = $this->times;
		$num  = $this->num;
		$this->go_list = $this->db->Ylist("select * from `@#_yonghu_yys_record` where `time` < '$time' order by `id` DESC limit 0,$num");
		if($this->go_list  && count($this->go_list) >= $this->num){
			$this->get_code_dabai();
		}else{
			$this->get_code_yibai();
		}
	}
	
}