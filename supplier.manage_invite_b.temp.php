<?php defined('G_IN_ADMIN')or exit('Access Denied.'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>商户邀请店铺</title>
<link rel="stylesheet" href="/statics/plugin/mstyle/css/style.css" type="text/css"><!--页面CSS-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/animate.min.css" type="text/css"><!--CSS3动画-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/timer.css" type="text/css"><!--CSS3动画-->
<script type="text/javascript" src="/statics/plugin/mstyle/js/jquery.min.js"></script><!--jQuery库-->
</head>
<body>
	<div class="container min-wid">
		<div class="path">
			<i><a href="#"><img src="/statics/plugin/mstyle/img/ico_1.png" alt=""></a></i>
			<p>
				当前位置：<a href="#">商户管理</a> > <a href="#">商户邀请店铺</a> >
			</p>
			<div class="push">
				<a href="<?php echo YYS_MODULE_PATH; ?>/index/Tdefault">欢迎界面</a>
				<a href="javascript:void(0)" onClick="location=location">刷新框架</a>
				<a href="<?php echo YYS_MODULE_PATH; ?>/cache/init">清理缓存</a>
			</div>
		</div><!-- path -->
		<div class="invitation wid1">
			<div class="pict">
				<img src="/statics/plugin/mstyle/img/pic_02.png" alt="">
			</div>
			<div class="fl-box">
			<form method="POST" action="#">
				<?php 
				if($shanghuid){
					echo '<var>会员ID</var>
					<input type="text" value="'.$supplier["id"].'" disabled>
					<var>会员昵称</var>
					<input class="lar" type="text" value="'.$supplier["name"].'" disabled="">';
				}else if($_REQUEST['id']){
					echo '<var>会员ID</var>
					<input type="text" name="id" value="'.$supplier["id"].'">
					<var>会员昵称</var>
					<input class="lar" type="text" name="name" value="'.$supplier["name"].'">
					<button type="submit" name="namesubmit" value="筛选">筛选</button>';
				}else{
					echo '<var>会员ID</var>
					<input type="text" name="id" value="">
					<var>会员昵称</var>
					<input class="lar" name="name" type="text" value="">
					<button type="submit" name="namesubmit" value="筛选">筛选</button>';
				}
				?>
				<div class="grate-date">
					邀请日期
					<input id="in1" type="text" name="start_time" value='<?php if(isset($_POST["start_time"])) echo $_POST["start_time"]; else echo "2018-1-1"; ?>' class="date_picker">
					至
					<input id="in2" type="text" name="end_time" value='<?php if(isset($_POST["end_time"])) echo $_POST['end_time']; else echo '2018-2-1'; ?>'  class="date_picker">
				</div>
				<button type="submit" name="sososubmit" value="筛选">筛选</button>
			</form>
			</div>
			<div class="fr-box">
				<span>邀店铺数：<i><?php echo $zongji; ?></i></span>
			</div>
		</div>
		<div class="invitation-table dt wid1">
			<ul>
				<li class="head">
					<span>邀请商户ID</span>
					<span>邀请商户</span>
					<span>邀请商户店名</span>
					<span>邀请商户手机</span>
					<span>邀请商户店主</span>
					<span>邀请商户地址</span>
					<span>行业分类</span>
					<span>内部店铺</span>
					<span>邀请时间</span>
					<span>查看</span>
				</li>
				<?php
				foreach($yyslist as $value){
					echo "<li><span>".$value['id']."</span>";
					echo "<span><img style='max-width: 25px;
					max-height: 25px;
					border-radius: 50%;' src='/statics/uploads/".$value['supplier_logo']."'></span>";
					echo "<span>".$value['name']."</span>";
					echo "<span>".$value['mobile']."</span>";
					echo "<span>".$value['realname']."</span>";
					$provinceId = $value['province'];
					$province = $this->db->YOne("select * from `@#_area` where `code` = '$provinceId'");
					$cityId = $value['city'];
					$city = $this->db->YOne("select * from `@#_area` where `code` = '$cityId'");
					$countryId = $value['country'];
					$country = $this->db->YOne("select * from `@#_area` where `code` = '$countryId'");
					$streetId = $value['street'];
					$street = $this->db->YOne("select * from `@#_area` where `code` = '$streetId'");
					echo "<span><var>".$province['name'].$city['name'].$country['name'].$country['name'].$street['name'].$value['address']."</var></span>";
					$cateid = $value['cateid'];
					$cate = $this->db->YOne("select * from `@#_fenlei` where `cateid` = '$cateid'");
					$catesubid = $value['catesubid'];
					$catesub = $this->db->YOne("select * from `@#_fenlei` where `cateid` = '$catesubid'");
					echo "<span>".$cate['name']."</span>";
					if($value['inside'] == 0){
						echo "<span>否</span>";
					}else if($value['inside'] == 1){
						echo "<span>是</span>";
					}
					echo "<span>".date("Y-m-d H:i:s",$value['create_time'])."</span>";
					echo "<span><a target='_blank' href='/mobile/supplier/index/".$value['id']."' class='ico-look'></a></span>";
				}
				?>

			</ul>
		</div>
		<div class="pages">
			<span>
				<?php echo $fenye->show('new')['html']; ?>
			</span>
			<span>共 <?php echo ceil($zongji/$num); ?> 页</span>
		
			<span>到第<input type="text" id="page" value="">页</span>
			<span><a class="page-but" id="gopage" href="javascript:void(0);">确定</a></span>
		</div>
	</div><!-- container -->
</body>
<script src="/statics/plugin/mstyle/js/jquery.date_input.pack.js"></script>
<script src="/statics/plugin/mstyle/js/library-3.28.js"></script><!--自定义封装函数-->
<script src="/statics/plugin/mstyle/js/scrollanim.min.js"></script><!--动画效果库-->
<script>
	$("#gopage").click(function(){
		var page = $("#page").val();
		var allpage = "<?php echo ceil($zongji/$num); ?>";
		if(page == '' || allpage < page){
			var page = 1;
		}
		location.href = "<?php echo $fenye->show('new')['url']; ?>"+page;
	});
</script>
</html>
