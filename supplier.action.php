<?php 
defined('IN_YYS')or exit('Access Denied.');
System::DOWN_App_class('base','member','no');
System::DOWN_App_fun('my','index');
System::DOWN_App_fun('user','index');
System::DOWN_sys_fun('send');
System::DOWN_sys_fun('user');
class supplier extends base {
	public function __construct(){ 
		parent::__construct();
		$this->db=System::DOWN_sys_class('model');
		
		if(DOWN_A != 'userphotoup' && DOWN_A !='singphotoup' && DOWN_A != 'index' && DOWN_A != 'map' && DOWN_A != 'show' && DOWN_A != 'showchat'){
			if(!$this->userinfo)_notemobile("请登录",LOCAL_PATH."/mobile/user/login",3);
		}
	}
	/*
	*	加盟商首页
	*/
	public function index(){
		$index="i0";
		
		/*邀请佣金 微信分享部分*/
		//邀请关系
        session_start();
		if(isset($_GET['yaoqing']) && $_GET['yaoqing'] != null){
			$_SESSION['uu'] = $_GET['yaoqing'];
		}
        if(isset($_GET['fid']) && $_GET['fid'] != null){
            $_SESSION['fid'] = $_GET['fid'];
            $_SESSION['uu'] = $_GET['fid'];
        }

		$webname=$this->_yys['web_name'];
		$biaoti=""._yys("web_name");
		$index="i0";
		
		//佣金部分开始
		$deng=$this->userinfo;
		if($_GET['yaoqing']){
			$uu=$_GET['yaoqing'];
			session_start();
			$_SESSION['uu']=$uu;
		}

		if($_GET['yaoqing2']){
			$yaoqing2=$_GET['yaoqing2'];
			session_start();
			$_SESSION['yaoqing2']=$yaoqing2;
		}

		if($_GET['yaoqing3']){
			$yaoqing3=$_GET['yaoqing3'];
			session_start();
			$_SESSION['yaoqing3']=$yaoqing3;
		}
		//var_dump($_SESSION['yaoqing2']);
		require_once("yungousys/class/mobile/jssdk.php");
		$yaoqing = System::DOWN_App_config("user_fufen");
	
      
	   $appid = $yaoqing[appid];
	   $secret = $yaoqing[secret];
		$jssdk = new JSSDK($appid, $secret);
		$signPackage = $jssdk->GetSignPackage();
		//exit;
/*分享增加结束*/
		
		$id = intval($this->segment(4));
		if(!$id){
			_notemobile("该店铺未通过审核或不存在",LOCAL_PATH."/mobile/local/cate",3);
		}else{
			$supplier = $this->db->Yone("select * from `@#_supplier` where `id` = '$id'"); 
			$biaoti=$supplier['name'];
			if(!$supplier){
				_notemobile("该店铺未通过审核或不存在",LOCAL_PATH."/mobile/local/cate",3);
			}else{
				$this->db->Query("update `@#_supplier` set `hit` = `hit` + 1 where `id` = '$id'");
				$supplier['hit']++; 
				$supplier['address'] = getCityName($supplier['street']).$supplier['address'];
				$cate_list = getCateGoods($supplier['id'],1);
				//获取信誉等级
				$huiyuandj=$this->db->Ylist("select * from `@#_yonghu_group` where `type` = '商户等级'");
				$jingyan=$supplier['credibility'];
				if(!empty($huiyuandj)){
					foreach($huiyuandj as $key=>$val){
						if($jingyan>=$val['jingyan_start'] && $jingyan<=$val['jingyan_end']){
						$huiyuan['level']=$val['name'];
						$huiyuan['icon']=$val['icon'];
						}
					}
				}
				/*信誉等级结束*/			
				include templates("mobile/supplier","index");
			}
		}
	}
	
	/*
	*	申请开店
	*/
	public function submit(){
		$biaoti="申请开店";
		$huiyuan = $this->userinfo;
		$uid = $huiyuan['uid'];
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$uid'"); 
		if($supplier['status'] == 0 && $supplier){
			_notemobile("系统24小时内审核回复，请耐心等待！",LOCAL_PATH."/mobile/home",3);
		}
	
		if($supplier && $supplier['status'] == 1){
			$url = LOCAL_PATH."/mobile/supplier/coupon";
			header("location:".$url);
		}
		if($supplier && $supplier['status'] == 2){
			_notemobile("系统24小时内审核回复，请耐心等待！",LOCAL_PATH."/mobile/home",3);
		}
		/*	按要求重新改下跳转	if($supplier && $supplier['status'] == 3){
			$url = LOCAL_PATH."/mobile/supplier/submitStepTwo";
			header("location:".$url);
		} */
		if($supplier && $supplier['status'] == 3){
			_notemobile("恭喜，您已正式开店！",LOCAL_PATH."/mobile/supplier/my",3);
		}
		$provinceList = $this->db->Ylist("select * from `@#_area` where `parentId` = '0'");
		$cityList = $this->db->Ylist("select * from `@#_area` where `parentId` = '$supplier[province]'");
		$countryList = $this->db->Ylist("select * from `@#_area` where `parentId` = '$supplier[city]'");
		$streetList = $this->db->Ylist("select * from `@#_area` where `parentId` = '$supplier[country]'");
		
		$supplierTypeList = $this->db->Ylist("select * from `@#_supplier_type` order by `sort` desc , id desc");
		$supplierBodyList = $this->db->Ylist("select * from `@#_supplier_body` order by `sort` desc , id desc");
		$supplierConfig = System::DOWN_sys_config("supplier");
		$defaultInviteId = $supplierConfig['defaultSupplierInviteId'];
		$fenleis=$this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE `model` = '3' order by `parentid` ASC,`cateid` ASC",array('key'=>'cateid'));
		$tree=System::DOWN_sys_class('tree');
		$tree->icon = array('│ ','├─ ','└─ ');
		$tree->nbsp = '&nbsp;';
		$fenleishtml="<option value='\$cateid'>\$spacer\$name</option>";
		$tree->init($fenleis);
		$fenleishtml=$tree->get_tree(0,$fenleishtml);
		$fenlei_lists = $this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE `parentid` = '0' and `model` = '3' order by `parentid` ASC,`cateid` ASC",array('key'=>'cateid'));
		$fenlei_list = $this->db->Ylist("select * from `@#_fenlei` where `parentid` = '$supplier[cateid]'");
		$header = '最简单的开店模式，欢迎您加入'._yys("web_name");
		include templates("mobile/supplier","submit");
	}
	
	/*
	*	申请开店，第二步
	*/
	public function submitStepTwo(){
		$biaoti="信息管理";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'"); 
		
		if(!$supplier){
			$url = LOCAL_PATH."/mobile/supplier/submit";
			header("location:".$url);
		}
		
		$address  = getCityName($supplier['province']).getCityName($supplier['city']).getCityName($supplier['country']);
		$address .= getCityName($supplier['street']).$supplier['address'];
		$supplierType = getSupplierType($supplier['supplier_type']);
		$header = '恭喜您加盟'._yys("web_name").'，专员协助你全程开店。';
		include templates("mobile/supplier","submitStepTwo");
	}
	
	/*
	*	申请开店，海报
	*/
	public function posters(){
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'"); 
		$biaoti="更换店招";
/*		if($supplier){
			if($supplier['status'] == 1){
				$url = LOCAL_PATH."/mobile/supplier/coupon";
			}elseif($supplier['status'] == 2){
				$url = LOCAL_PATH."/mobile/supplier/coupon";
			}elseif($supplier['step'] == 0){
				_notemobile("系统24小时内审核回复，请耐心等待！",LOCAL_PATH."/mobile/home",3);
			}
			header("location:".$url);
		}else{
			$url = LOCAL_PATH."/mobile/supplier/submit";
			header("location:".$url);
		}
		*/
		
		$header = '上传海报和店铺图片，专员全程协助开店。';
		include templates("mobile/supplier","posters");
	}

	/*
	*	卡券样式
	*/
	public function coupon(){
		$biaoti="卡券样式";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'"); 

		if(!$supplier || $supplier['status'] != 1){
			$url = LOCAL_PATH."/mobile/supplier/submit";
			header("location:".$url);
		}
		
		/*卡券的分类*/
		$couponeCateList = $this->db->Ylist("select * from `@#_supplier_coupon_cate` order by `sort` DESC");
		
		$header = '填写合适的卡券面值，选择你喜欢的样式。';
		include templates("mobile/supplier","coupon");
	}

	/*
	*	店铺管理
	*/
	public function manage(){
		$biaoti="店铺管理";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		if(!$supplier){
			_notemobile("还没有店铺，请先申请吧！",LOCAL_PATH."/mobile/supplier/submit",3);
		}elseif($supplier['status'] != '3'){
			_notemobile("店铺尚未通过审核！",LOCAL_PATH."/mobile/home",3);
		}
		$header = "轻松管理店铺，其他我们为你服务<br/>店铺编号：".$supplier['supplier_sn'];

//获取等级信誉
	$huiyuandj=$this->db->Ylist("select * from `@#_yonghu_group` where `type` = '商户等级'");
	$jingyan=$supplier['credibility'];
	  if(!empty($huiyuandj)){
	     foreach($huiyuandj as $key=>$val){
		    if($jingyan>=$val['jingyan_start'] && $jingyan<=$val['jingyan_end']){
			   $huiyuan['level']=$val['name'];
			   $huiyuan['icon']=$val['icon'];
			}
		 }
	  }
		include templates("mobile/supplier","manage");
	}
	/*

	*	商户审核消息提示*/
	public function message(){
		$biaoti="商户审核";
		$huiyuan = $this->userinfo;

		include templates("mobile/supplier","message");
	}
	
	/*
	*	卡券管理*/

	public function cardmanage(){
		$biaoti="卡券管理";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		if(!$supplier){
			_notemobile("还没有店铺，请先申请吧！",LOCAL_PATH."/mobile/supplier/submit",3);
		}elseif($supplier['status'] != '3'){
			_notemobile("店铺尚未通过审核！",LOCAL_PATH."/mobile/home",3);
		}
		$header = "轻松管理店铺，其他我们为你服务<br/>店铺编号：".$supplier['supplier_sn'];
		include templates("mobile/supplier","cardmanage");
	}
		
	/*
	*	我的店铺
	*/
	public function my(){
		$index="i0";
		$huiyuan = $this->userinfo;
		$uid = $huiyuan['uid'];
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		if(!$supplier){
			_notemobile("还没有店铺，请先申请吧！",LOCAL_PATH."/mobile/supplier/submit",3);
		}elseif($supplier['status'] == '2'){
			$this->db->query("update `@#_supplier` set `step` = '0',`status` = '0' where `uid` = '$huiyuan[uid]'");
			_notemobile("店铺未通过审核，重新填写资料吧！",LOCAL_PATH."/mobile/supplier/submit",3);
		}elseif($supplier['status'] != '3'){
			_notemobile("店铺尚未通过审核！",LOCAL_PATH."/mobile/home",3);
		}
		$supplierImg = $this->db->Ylist("select * from `@#_supplier_img` where `type` = '0' and `supplier_id` = '$supplier[id]'");
		$address = getCityName($supplier['street']).$supplier['address'];
		$country = getCityName($supplier['country']);
		$street = getCityName($supplier['street']);
		$cate_list = getCateGoods($supplier['id'],1);
		$collection = $this->db->YCount("select * from `@#_collection` where `uid` = '$uid' and `collection_type` = 'goods' and `collection_id` = '$supplier[id]'");
		if($collection['count'] >0){
			$supplier['collection'] = 1;
		}else{
			$supplier['collection'] = 0;
		}
		$supplier_type = $this->db->Yone("select * from `@#_supplier_type` where `id` = '$supplier[supplier_type]'");
		$header = '恭喜您加盟'._yys("web_name").'<br/>专员协助你全程开店';	
		$biaoti="$supplier[name]";
		//佣金部分开始
		$deng=$this->userinfo;
		if($_GET['yaoqing']){
			$uu=$_GET['yaoqing'];
			session_start();
			$_SESSION['uu']=$uu;
		}

		if($_GET['yaoqing2']){
			$yaoqing2=$_GET['yaoqing2'];
			session_start();
			$_SESSION['yaoqing2']=$yaoqing2;
		}

		if($_GET['yaoqing3']){
			$yaoqing3=$_GET['yaoqing3'];
			session_start();
			$_SESSION['yaoqing3']=$yaoqing3;
		}
		//var_dump($_SESSION['yaoqing2']);
		require_once("yungousys/class/mobile/jssdk.php");
		$yaoqing = System::DOWN_App_config("user_fufen");
	
      
	   $appid = $yaoqing[appid];
	   $secret = $yaoqing[secret];
		$jssdk = new JSSDK($appid, $secret);
		$signPackage = $jssdk->GetSignPackage();
		//exit;
/*分享增加结束*/
				//获取信誉等级
				$huiyuandj=$this->db->Ylist("select * from `@#_yonghu_group` where `type` = '商户等级'");
				$jingyan=$supplier['credibility'];
				if(!empty($huiyuandj)){
					foreach($huiyuandj as $key=>$val){
						if($jingyan>=$val['jingyan_start'] && $jingyan<=$val['jingyan_end']){
						$huiyuan['level']=$val['name'];
						$huiyuan['icon']=$val['icon'];
						}
					}
				}
				/*信誉等级结束*/	
		
		include templates("mobile/supplier","my");
	}
	
	/*
	*	我的店铺
	*/
	public function detail(){
		$biaoti="我的店铺";
		$index="i2";
		$supplierId = $this->userinfo['supplierId'];
		if(!$supplierId){
			_notemobile("没有找到这个店铺",LOCAL_PATH."/mobile/home/",3);
		}
		$supplier = $this->db->Yone("select * from `@#_supplier` where `id` = '$supplierId'");
		$address  = getCityName($supplier['province']).getCityName($supplier['city']).getCityName($supplier['country']);
		$address .= getCityName($supplier['street']).$supplier['address'];
		include templates("mobile/supplier","show");
	}
	
	/*
	*	分享赚钱
	*/
	public function share(){
		$biaoti="分享赚钱";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		$address  = getCityName($supplier['province']).getCityName($supplier['city']).getCityName($supplier['country']);
		$address .= getCityName($supplier['street']).$supplier['address'];
		include templates("mobile/supplier","share");
	}
	
	/*
	*	收藏店铺
	*/
	public function collection(){
		$biaoti="收藏店铺";
		$huiyuan = $this->userinfo;
		$header = '恭喜您加盟'._yys("web_name").'<br/>专员协助你全程开店';
		include templates("mobile/supplier","collection");
	}
	
	/*
	*	到店看看
	*/
	public function map(){
		$biaoti="到店看看";
//		$huiyuan = $this->userinfo;
		$supplierId = $this->segment(4);
		if($supplierId){
			$supplier = $this->db->Yone("select * from `@#_supplier` where `id` = '$supplierId'");
		}else{
			$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		}
		$address  = getCityName($supplier['province']).getCityName($supplier['city']).getCityName($supplier['country']);
		$address .= getCityName($supplier['street']).$supplier['address'];
		

		/*邀请佣金 微信分享部分*/
		$deng=$this->userinfo;
		if($_GET['yaoqing']){
			$uu=$_GET['yaoqing'];
			session_start();
			$_SESSION['uu']=$uu;
		}

		if($_GET['yaoqing2']){
			$yaoqing2=$_GET['yaoqing2'];
			session_start();
			$_SESSION['yaoqing2']=$yaoqing2;
		}

		if($_GET['yaoqing3']){
			$yaoqing3=$_GET['yaoqing3'];
			session_start();
			$_SESSION['yaoqing3']=$yaoqing3;
		}
		//var_dump($_SESSION['yaoqing2']);
		require_once("yungousys/class/mobile/jssdk.php");
		$yaoqing = System::DOWN_App_config("user_fufen");
	
      
	   $appid = $yaoqing[appid];
	   $secret = $yaoqing[secret];
		$jssdk = new JSSDK($appid, $secret);
		$signPackage = $jssdk->GetSignPackage();
		//exit;
		
		include templates("mobile/supplier","map");
	}
	
	/*
	*	信息管理
	*/
	public function supplierEdit(){
		$biaoti="信息管理";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		$header = '恭喜您加盟'._yys("web_name").'<br/>专员协助你全程开店';
		include templates("mobile/supplier","supplierEdit");
	}
	/*
	*	货款明细
	*/
	public function settlement(){
		$biaoti="货款明细";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		$header = '交易结算的货款：（售价-惠券）X数量=货款';
		include templates("mobile/supplier","settlement");
	}

	/*
	*	卡券消费
	*/
	public function couponCost(){
		$biaoti="卡券消费";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		$header = '恭喜您加盟'._yys("web_name").'<br/>专员协助你全程开店';
		include templates("mobile/supplier","couponCost");
	}
	
	/*发货管理*/
	public function sendManage(){
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		$header = '恭喜您加盟'._yys("web_name").'<br/>专员协助你全程开店';
		include templates("mobile/supplier","sendManage");
	}
	
	/*口碑评论*/
	public function comment(){
		$biaoti="口碑评论";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		$supplierImg = $this->db->Ylist("select * from `@#_supplier_img` where `type` = '0' and `supplier_id` = '$supplier[id]'");
		$address  = getCityName($supplier['province']).getCityName($supplier['city']).getCityName($supplier['country']);
		$address .= getCityName($supplier['street']).$supplier['address'];
		$header = '恭喜您加盟'._yys("web_name").'<br/>专员协助你全程开店';
		include templates("mobile/supplier","comment");
	}
	
	/*区域设置*/
	public function setArea(){
		$biaoti="区域设置";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		include templates("mobile/supplier","setArea");
	}
	
	/*周边店铺*/
	public function nearSupplier(){
		$biaoti="周边店铺";
		$index="i2";
		include templates("mobile/supplier","near");
	}
	
	/*拍图管理*/
	public function showImage(){
		$biaoti="拍图管理";
		$index="i2";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		include templates("mobile/supplier","showImage");
	}
	
	/*变更审核*/
	public function editSubmit(){
		$biaoti="变更审核";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		include templates("mobile/supplier","editSubmit");
	}
	
	/*
	*	我的互动拍图
	*/
	public function myshow(){
		$biaoti="互动拍图";
		$index="i1";
		
		$huiyuan = $this->userinfo;
		$uid = $huiyuan['uid'];
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		if(!$supplier){
			_notemobile("还没有店铺，请先申请吧！",LOCAL_PATH."/mobile/supplier/submit",3);
		}elseif($supplier['status'] == '2'){
			$this->db->query("update `@#_supplier` set `step` = '0',`status` = '0' where `uid` = '$huiyuan[uid]'");
			_notemobile("店铺未通过审核，重新填写资料吧！",LOCAL_PATH."/mobile/supplier/submit",3);
		}elseif($supplier['status'] != '3'){
			_notemobile("店铺尚未通过审核！",LOCAL_PATH."/mobile/home",3);
		}
		
	$supplierId = $supplier[id];

		$address = getCityName($supplier['street']).$supplier['address'];
		$country = getCityName($supplier['country']);
		$street = getCityName($supplier['street']);		
		include templates("mobile/supplier","show");
	}
	
	
	
	/*我的拍图*/
	public function show(){
		$index="i1";
		$biaoti="店铺拍图";
		$supplierId = intval($this->segment(4));
		if(!$supplierId){
			_notemobile("没有找到这个店铺",LOCAL_PATH."/mobile/home/",3);
		}
		$supplier = $this->db->Yone("select * from `@#_supplier` where `id` = '$supplierId'");
		$address  = getCityName($supplier['province']).getCityName($supplier['city']).getCityName($supplier['country']);
		$address .= getCityName($supplier['street']).$supplier['address'];
		
		/*最新一期*/
		$go = $this->db->YOne("select * from `@#_shangpin` where `supplierId` = '$supplierId' and `q_uid` IS NOT NULL order by id desc limit 1");
		$username = getUserInfo($go['q_uid'],'uid','username');
		
		
		
		//分享接口部分开始
		$deng=$this->userinfo;
		if($_GET['yaoqing']){
			$uu=$_GET['yaoqing'];
			session_start();
			$_SESSION['uu']=$uu;
		}

		if($_GET['yaoqing2']){
			$yaoqing2=$_GET['yaoqing2'];
			session_start();
			$_SESSION['yaoqing2']=$yaoqing2;
		}

		if($_GET['yaoqing3']){
			$yaoqing3=$_GET['yaoqing3'];
			session_start();
			$_SESSION['yaoqing3']=$yaoqing3;
		}
		//var_dump($_SESSION['yaoqing2']);
		require_once("yungousys/class/mobile/jssdk.php");
		$yaoqing = System::DOWN_App_config("user_fufen");
	
      
	   $appid = $yaoqing[appid];
	   $secret = $yaoqing[secret];
		$jssdk = new JSSDK($appid, $secret);
		$signPackage = $jssdk->GetSignPackage();
		//exit;
		
		include templates("mobile/supplier","show");
	}
	/*拍图互动*/
	public function showchat(){
		$index="i1";
		$id = intval($this->segment(4));
		$supplier_pat = $this->db->YOne("select * from `@#_supplier_pat` where `id` = '$id'");
		if(!$id || !$supplier_pat){
			_notemobile("这个页面可能去火星了~",LOCAL_PATH."/mobile/home/",3);
		}
		$supplier_pat['comment'] = getSupplierPatComment($id);
		updateSet(array('hit'=>($supplier_pat['hit']+1)),'supplier_pat',$id);
		
		
		$supplierId = $supplier_pat['sid'];
		if(!$supplierId){
			_notemobile("没有找到这个店铺",LOCAL_PATH."/mobile/home/",3);
		}
		$supplier = $this->db->Yone("select * from `@#_supplier` where `id` = '$supplierId'");
		$address  = getCityName($supplier['province']).getCityName($supplier['city']).getCityName($supplier['country']);
		$address .= getCityName($supplier['street']).$supplier['address'];
		$biaoti=$supplier_pat['notice'];
		//分享接口部分开始
		$deng=$this->userinfo;
		if($_GET['yaoqing']){
			$uu=$_GET['yaoqing'];
			session_start();
			$_SESSION['uu']=$uu;
		}

		if($_GET['yaoqing2']){
			$yaoqing2=$_GET['yaoqing2'];
			session_start();
			$_SESSION['yaoqing2']=$yaoqing2;
		}

		if($_GET['yaoqing3']){
			$yaoqing3=$_GET['yaoqing3'];
			session_start();
			$_SESSION['yaoqing3']=$yaoqing3;
		}
		//var_dump($_SESSION['yaoqing2']);
		require_once("yungousys/class/mobile/jssdk.php");
		$yaoqing = System::DOWN_App_config("user_fufen");
	
      
	   $appid = $yaoqing[appid];
	   $secret = $yaoqing[secret];
		$jssdk = new JSSDK($appid, $secret);
		$signPackage = $jssdk->GetSignPackage();
		//exit;
		
		
		
		
		include templates("mobile/supplier","showchat");
	}
	/*众筹惠券*/
	public function card(){
		$biaoti="众筹惠券";
		$index="i2";
		$supplierId = intval($this->segment(4));
		if(!$supplierId){
			_notemobile("该店铺未开通该功能",LOCAL_PATH."/mobile/home/",3);
		}
		$supplier = $this->db->Yone("select * from `@#_supplier` where `id` = '$supplierId'");
		$address  = getCityName($supplier['province']).getCityName($supplier['city']).getCityName($supplier['country']);
		$address .= getCityName($supplier['street']).$supplier['address'];
		
		/*最新一期*/
		$go = $this->db->YOne("select * from `@#_shangpin` where `supplierId` = '$supplierId' and `q_uid` IS NOT NULL order by id desc limit 1");
		$username = getUserInfo($go['q_uid'],'uid','username');
		include templates("mobile/supplier","card");
	}
	/*paddy 20170830新增商户模块改到一起*/
	/*发布商品*/
	public function goods_add(){
		$biaoti="发布商品";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
        $huiyuan = $this->userinfo;
        $uid = $huiyuan['uid'];
        $mysql_model=System::DOWN_sys_class('model');
		$biaoti="发布商品";
		$gradeConfig = System::DOWN_App_config("user_gyszg","","admin");
		$yonghuzzg = $mysql_model->YOne("select * from `@#_yonghu_zzzg` where `uid` = '$uid'");
		$goodNum = $mysql_model->YOne("select count(*) as count from `@#_zg_shangpin` where `uid` = '$uid'");
	
		//if($gradeConfig[$yonghuzzg['supplyGrade'].'GoodsNum'] < $goodNum['count']){
		//	_note('发布数量已到最大个数');
		//}elseif(time() > $yonghuzzg['supplyGradeTime']){
		//	_note('您的会员已到期，请及时续费');
		//}
		
//增加产品二级分类
		$fenleis=$this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE `model` = '1' order by `parentid` desc,`cateid` desc",array('key'=>'cateid'));
		$tree=System::DOWN_sys_class('tree');
		$tree->icon = array('│ ','├─ ','└─ ');
		$tree->nbsp = '&nbsp;';
		$fenleishtml="<option value='\$cateid'>\$spacer\$name</option>";
		$tree->init($fenleis);
		$fenleishtml=$tree->get_tree(0,$fenleishtml);
		$fenlei_lists = $this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE `parentid` = '0' and `model` = '1' order by `order` desc,`cateid` desc",array('key'=>'cateid'));
		$fenlei_list = $this->db->Ylist("select * from `@#_fenlei` where `parentid` = '$supplier[cateid]'");
//分类结束
		

		if(isset($_POST['title'])){
			$huiyuan = $this->userinfo;
			$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
			$data['supplierId'] = $supplier['id'];
			$data['title'] = ($_POST['title']);
			$data['title2'] = ($_POST['title2']);
			$data['yunjiage'] = intval($_POST['yunjiage']);
			$data['supplierPercent'] = intval($_POST['supplierPercent']);
			$data['maxqishu'] = intval($_POST['maxqishu']);
			$data['isfreepost'] = _htmtguolv($_POST['isfreepost']);
			$data['isShelve'] = isset($_POST['isShelve']) ? 1 : 0;
			$data['cateid'] = intval($_POST['cateid']);
			$data['catesubid'] = intval($_POST['catesubid']);
			$data['order'] = intval($_POST['order']);
			$data['thumb'] = _htmtguolv($_POST['thumb']);
			$data['img1'] = _htmtguolv($_POST['img1']);
			$data['img2'] = _htmtguolv($_POST['img2']);
			$data['img3'] = _htmtguolv($_POST['img3']);
			$data['img4'] = _htmtguolv($_POST['img4']);
			
			if($_POST['yunjiage'] <= $_POST['supplierPercent']){
				_notemobile("返利价不得大于商品价");
			}
			
			$data['time'] = time();
			$query_1 = insertInto($data,'zg_shangpin');
			if($query_1){
				$mysql_model->tijiao_commit();
				_notemobile("提交成功",LOCAL_PATH."/mobile/supplier/goods_on",3);
			}else{
				$mysql_model->tijiao_rollback();
				_notemobile("提交失败，请重试或联系管理员",LOCAL_PATH."/mobile/supplier/goods_add",3);
			}
		}else {
			$this->categoryszg=$this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE `model` = '1' order by `parentid` ASC,`cateid` ASC",array('key'=>'cateid'));
			$cateid=intval($this->segment(4));		
			$fenleis=$this->categoryszg;
			$tree=System::DOWN_sys_class('tree');
			$tree->icon = array('│ ','├─ ','└─ ');
			$tree->nbsp = '&nbsp;';
			$fenleishtml="<option value='\$cateid'>\$spacer\$name</option>";
			$tree->init($fenleis);	
			$fenleishtml=$tree->get_tree(0,$fenleishtml);
			$fenleishtml='<option value="0">≡ 请选择栏目 ≡</option>'.$fenleishtml;
			if($cateid){			
				$cateinfo=$this->db->YOne("SELECT * FROM `@#_fenlei` WHERE `cateid` = '$cateid' LIMIT 1");
				if(!$cateinfo)_notemobile("参数不正确,没有这个栏目",G_ADMIN_PATH.'/'.DOWN_C.'/addarticle');
				$fenleishtml.='<option value="'.$cateinfo['cateid'].'" selected="true">'.$cateinfo['name'].'</option>';			
			}		

			  
            $this->zzfb = $mysql_model->YOne("SELECT * from `@#_yonghu_zzzg` where `uid` = '$uid'");
            $zzfb = $this->zzfb;
		include templates("mobile/supplier","goods_add");
	}
    }
/*修改商品*/
	public function goods_edit(){
		$biaoti="修改商品";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");

        $uid = $huiyuan['uid'];
        $mysql_model=System::DOWN_sys_class('model');
		$biaoti="发布商品";
		$gradeConfig = System::DOWN_App_config("user_gyszg","","admin");
		$yonghuzzg = $mysql_model->YOne("select * from `@#_yonghu_zzzg` where `uid` = '$uid'");
		$goodNum = $mysql_model->YOne("select count(*) as count from `@#_zg_shangpin` where `uid` = '$uid'");
	
		//if($gradeConfig[$yonghuzzg['supplyGrade'].'GoodsNum'] < $goodNum['count']){
		//	_note('发布数量已到最大个数');
		//}elseif(time() > $yonghuzzg['supplyGradeTime']){
		//	_note('您的会员已到期，请及时续费');
		//}
				$id = intval($this->segment(4));
		$shangpin = $this->db->YOne("select * from `@#_zg_shangpin` where `id` = '$id'");
//增加产品二级分类
		$fenleis=$this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE `model` = '1' order by `parentid` desc,`cateid` desc",array('key'=>'cateid'));
		$tree=System::DOWN_sys_class('tree');
		$tree->icon = array('│ ','├─ ','└─ ');
		$tree->nbsp = '&nbsp;';
		$fenleishtml="<option value='\$cateid'>\$spacer\$name</option>";
		$tree->init($fenleis);
		$fenleishtml=$tree->get_tree(0,$fenleishtml);
		$fenlei_lists = $this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE `parentid` = '0' and `model` = '1' order by `order` desc,`cateid` desc",array('key'=>'cateid'));
		$fenlei_list = $this->db->Ylist("select * from `@#_fenlei` where `parentid` = '$shangpin[cateid]'");
//分类结束


		if(isset($_POST['title'])){
			$huiyuan = $this->userinfo;
			$id = intval($_POST['id']);
			$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
			$data['supplierId'] = $supplier['id'];
			$data['title'] = _htmtguolv($_POST['title']);
			$data['title2'] = _htmtguolv($_POST['title2']);
			$data['yunjiage'] = intval($_POST['yunjiage']);
			$data['supplierPercent'] = intval($_POST['supplierPercent']);
			$data['maxqishu'] = intval($_POST['maxqishu']);
			$data['isfreepost'] = _htmtguolv($_POST['isfreepost']);
			$data['isShelve'] = isset($_POST['isShelve']) ? 1 : 0;
			$data['cateid'] = intval($_POST['cateid']);
			$data['catesubid'] = intval($_POST['catesubid']);
			$data['order'] = intval($_POST['order']);
			$data['thumb'] = _htmtguolv($_POST['thumb']);
			$data['img1'] = _htmtguolv($_POST['img1']);
			$data['img2'] = _htmtguolv($_POST['img2']);
			$data['img3'] = _htmtguolv($_POST['img3']);
			$data['img4'] = _htmtguolv($_POST['img4']);
			$data['time'] = time();
			$query_1 = updateSet($data,'zg_shangpin',$id);
			if($query_1){
				$mysql_model->tijiao_commit();
				_notemobile("提交成功",LOCAL_PATH."/mobile/supplier/goods_on",3);
			}else{
				$mysql_model->tijiao_rollback();
				_notemobile("提交失败，请重试或联系管理员",LOCAL_PATH."/mobile/supplier/goods_add",3);
			}
		}

		include templates("mobile/supplier","goods_edit");

	}
/*在售商品*/
	public function goods_on(){
		$biaoti="在售商品";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		$cate_list = getCateGoods($supplier['id'],1);
		include templates("mobile/supplier","goods_on");
	}
/*商品规格*/
	public function goods_newTypes(){
		$id = intval($this->segment(4));
		$biaoti="商品规格";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		$zg_shangpin = $this->db->YOne("select * from `@#_zg_shangpin` where `id` = '$id'");
		$new_types = unserialize($zg_shangpin['newTypes']);
		arsort($new_types);
		$count = count($new_types);
		include templates("mobile/supplier","goods_newTypes");
	}
	public function goods_color(){
		$id = intval($this->segment(4));
		$biaoti="商品颜色";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		$zg_shangpin = $this->db->YOne("select * from `@#_zg_shangpin` where `id` = '$id'");
		$colors = unserialize($zg_shangpin['color']);
		arsort($colors);
		$count = count($colors);
		include templates("mobile/supplier","goods_color");
	}
	
/*下架商品*/
	public function goods_off(){
		$biaoti="下架商品";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		$cate_list = getCateGoods($supplier['id'],0);
		include templates("mobile/supplier","goods_off");
	}
/*网购发货*/
	public function goods_send(){
		$biaoti="网购发货";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		include templates("mobile/supplier","goods_send");
	}
/*店内发货-指面对面实体购物或虚拟购物商家快速发货*/
	public function shop_send(){
		$biaoti="店内发货";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		include templates("mobile/supplier","shop_send");
	}
/*截图配货-内容调用显示即可*/
	public function goods_order(){
		$biaoti="截图配货";
		$huiyuan = $this->userinfo;
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$huiyuan[uid]'");
		$order_sn = $this->segment(4);
		$order_list = $this->db->YOne("select * from `@#_order_list` where `order_sn` = '$order_sn' and `supplier_id` = '$supplier[id]'");
		$order_item = $this->db->YList("select * from `@#_order_item` where `order_sn` = '$order_sn'");
		$address = $this->db->YOne("select * from `@#_yonghu_dizhi` where `id` = '$order_list[address_id]'");
		$province = $this->db->YOne("select * from `@#_area` where `code` = '$address[sheng]'");
		$city = $this->db->YOne("select * from `@#_area` where `code` = '$address[shi]'");
		$distinct = $this->db->YOne("select * from `@#_area` where `code` = '$address[xian]'");
		$street = $this->db->YOne("select * from `@#_area` where `code` = '$address[jiedao]'");
		$address['address'] = $province['name'].$city['name'].$distinct['name'].$street['name'].$address['address'];
		$order_list['create_time'] = date('Y-m-d H:i:s',$order_list['create_time']);
		$express = $this->db->YOne("select * from `@#_express_list` where `id` = '$order_list[express_id]'");
		$total_num = 0;
		foreach($order_item as $key => $one){
			$order_item[$key]['goods_name'] = html_entity_decode($one['goods_name']);
			$total_num += $one['number'];
		}
		include templates("mobile/supplier","goods_order");
	}
	
  /*生成商家带logo店铺二维码，调用方法 <img src=/?url=http://?url=&logo=>*/
public function supplierQrcode(){
include_once((dirname(dirname(dirname(dirname(__FILE__)))) . '/phpqrcode/phpqrcode.php'));
$value = $_GET['url'];//二维码内容 
$errorCorrectionLevel = 'H';//容错级别 
$matrixPointSize = 6;//生成图片大小 
//生成二维码图片 
QRcode::png($value, 'qrcode.png', $errorCorrectionLevel, $matrixPointSize, 2); 
$logo = $_GET['logo'];//准备好的logo图片 
$QR = 'qrcode.png';//已经生成的原始二维码图 
 
if ($logo !== FALSE) { 
 $QR = imagecreatefromstring(file_get_contents($QR)); 
 $logo = imagecreatefromstring(file_get_contents($logo)); 
 $QR_width = imagesx($QR);//二维码图片宽度 
 $QR_height = imagesy($QR);//二维码图片高度 
 $logo_width = imagesx($logo);//logo图片宽度 
 $logo_height = imagesy($logo);//logo图片高度 
 $logo_qr_width = $QR_width / 5; 
 $scale = $logo_width/$logo_qr_width; 
 $logo_qr_height = $logo_height/$scale; 
 $from_width = ($QR_width - $logo_qr_width) / 2; 
 //重新组合图片并调整大小 
 imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width, 
 $logo_qr_height, $logo_width, $logo_height); 
} 
//输出图片 
Header("Content-type: image/png");
ImagePng($QR);

	}

	/*内部店铺详情*/
}