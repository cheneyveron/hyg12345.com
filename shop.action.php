<?php 
defined('IN_YYS')or exit('no');
System::DOWN_App_class('admin',G_ADMIN_DIR,'no');
System::DOWN_App_fun('global',G_ADMIN_DIR);
System::DOWN_sys_fun('user');
class shop extends admin {
	private $db;
	public function __construct(){		
		parent::__construct();		
		$this->db=System::DOWN_sys_class('model');
		$this->ment=array();
		$this->categorys=$this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE 1 order by `parentid` ASC,`cateid` ASC",array('key'=>'cateid'));
		$this->models=$this->db->Ylist("SELECT * FROM `@#_moxing` WHERE 1",array('key'=>'modelid'));
	}

	
	//模型
	public function model(){
		$models=$this->models;	
		include $this->dwt(DOWN_M,'content.model');
	}
	
	public function lists(){
		$fenleis=$this->categorys;
		$models=$this->models;
		$tree=System::DOWN_sys_class('tree');
		$tree->icon = array('│ ','├─ ','└─ ');
		$tree->nbsp = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		foreach($fenleis as $v){
			$v['typename']=cattype($v['model']);		
			if($v['model']==-1 || $v['model']==-2){
				$v['seecontent']=G_ADMIN_PATH.'/shop/editcate/';
				$v['addcontent']=G_ADMIN_PATH.'/shop/editcate/';
			}			
			if($v['model']>0){
				$v['seecontent']=G_ADMIN_PATH.'/'.DOWN_C.'/get/';	
				$v['addcontent']=G_ADMIN_PATH.'/'.DOWN_C.'/add/';
				$v['model']=$models[$v['model']]['name'];
			}else{
				$v['model']='';
			}
			$fenleis[$v['cateid']]=$v;
		}
		$html=<<<HTML
			<tr>
			<td align='center'>\$cateid</td>
            <td align='left'>\$spacer\$name</th>
            <td align='center'>\$typename</td>
            <td align='center'>\$model</td>
            <td align='center'></td>
			<td align='center'>               
				<a href='\$addcontent\$cateid\'>添加内容</a><span class='span_fenge lr5'>|</span>
				 <a href='\$seecontent\$cateid'>查看内容</a><span class='span_fenge lr5'></span>   
            </td>
          </tr>
HTML;

		$tree->init($fenleis);
		$html=$tree->get_tree(0,$html);	
		include $this->dwt(DOWN_M,'shop.list');
	}
	//商品列表	
	public function goods_lists(){
		$this->ment=array(
			array("lists","商品管理",DOWN_M.'/'.DOWN_C."/goods_list"),
			array("add","添加商品",DOWN_M.'/'.DOWN_C."/goods_add"),
			array("renqi","人气商品",DOWN_M.'/'.DOWN_C."/goods_list/isShelverenqi"),
			array("xsjx","限时揭晓商品",DOWN_M.'/'.DOWN_C."/goods_list/xianshi"),
			array("qishu","期数倒序",DOWN_M.'/'.DOWN_C."/goods_list/qishu"),
			array("danjia","单价倒序",DOWN_M.'/'.DOWN_C."/goods_list/danjia"),
			array("money","商品价格倒序",DOWN_M.'/'.DOWN_C."/goods_list/money"),
			array("money","已揭晓",DOWN_M.'/'.DOWN_C."/goods_list/jiexiaook"),
			array("money","<font color='#f00'>期数已满商品</font>",DOWN_M.'/'.DOWN_C."/goods_list/maxqishu"),
		);	
	    $cateid=$this->segment(4);
		$list_where = '';
		if($cateid){
			$supplierId = $this->segment(5);
			if($supplierId){
				$list_where .= " `supplierId` = '$supplierId' and ";
			}
			if($cateid=='isfreepost'){
				$list_where .= "`isfreepost` = '1' and ";
			}
			if($cateid=='isfreepostzt'){
				$list_where .= "`isfreepost` = '0' and ";
			}			
			if($cateid=='isSince'){
				$list_where .= "`isSince` = '1' and ";
			}			
			if($cateid=='isstop'){
				$list_where .= "`isstop` = '1' and ";
			}			
			
			
			if($cateid=='maxqishu'){
				$list_where .= "`qishu` = `maxqishu`  and `q_end_time` is not null and ";
			}			
			if($cateid=='renqi'){
				$list_where .= "`renqi` = '1' and ";
			}
			if($cateid=='isShelve'){
				$list_where .= "`isShelve` = '0' and ";
			}
			if($cateid=='isShelve2'){
				$list_where .= "`isShelve` = '1' and ";
			}
			if($cateid=='xianshi'){
				$list_where .= "`xsjx_time` != '0' and ";
			}
			
			
			if($cateid=='qishu'){
				$list_where .= " order by `qishu` DESC";
				$this->ment[4][1]="期数正序";
				$this->ment[4][2]=DOWN_M.'/'.DOWN_C."/goods_list/qishuasc";
			}
			if($cateid=='qishuasc'){
				$list_where .= " order by `qishu` ASC";
				$this->ment[4][1]="期数倒序";
				$this->ment[4][2]=DOWN_M.'/'.DOWN_C."/goods_list/qishu";
			}
			if($cateid=='danjia'){
				$list_where .= " order by `yunjiage` DESC";
				$this->ment[5][1]="单价正序";
				$this->ment[5][2]=DOWN_M.'/'.DOWN_C."/goods_list/danjiaasc";
			}
			if($cateid=='danjiaasc'){
				$list_where .= " order by `yunjiage` ASC";
				$this->ment[5][1]="单价倒序";
				$this->ment[5][2]=DOWN_M.'/'.DOWN_C."/goods_list/danjia";
			}
			if($cateid=='money'){
				$list_where .= " order by `money` DESC";
				$this->ment[6][1]="商品价格正序";
				$this->ment[6][2]=DOWN_M.'/'.DOWN_C."/goods_list/moneyasc";
			}
			if($cateid=='moneyasc'){
				$list_where .= " order by `money` ASC";
				$this->ment[6][1]="商品价格倒序";
				$this->ment[6][2]=DOWN_M.'/'.DOWN_C."/goods_list/money";
			}
			if($cateid==''){
				$list_where .= "`q_uid` is null and ";
			}
		}else{
			$supplierId = $this->segment(5);
			if($supplierId){
				$list_where .= " `supplierId` = '$supplierId' and `q_uid` is null and ";
				$supplier = $this->db->YOne("select * from `@#_supplier` where `id` = '$supplierId'");
			}else{
				$list_where .= " `q_uid` is null and ";
			}
		}

		
		if(isset($_POST['sososubmit'])){
			$cate = $_POST['cateid'];
			if(intval($cate) && $cate != -1){
				$list_where .= " `cateid` = '$cate'  and ";
			}
			$posttime1 = !empty($_POST['start_time']) ? strtotime($_POST['start_time']) : NULL;
			$posttime2 = !empty($_POST['end_time']) ? strtotime($_POST['end_time']) : NULL;			
			if($posttime1 && $posttime2){
				if($posttime2 < $posttime1)_note("结束时间不能小于开始时间");
				$list_where .= " `time` > '$posttime1' AND `time` < '$posttime2' and ";
			}
			if($posttime1 && empty($posttime2)){				
				$list_where .= " `time` > '$posttime1' and ";
			}
			if($posttime2 && empty($posttime1)){				
				$list_where .= " `time` < '$posttime2' and ";
			}
			if($tetle && empty($tetle)){				
				$list_where .= "`tetle` like '%$title%' and ";
			}
		}
		if(isset($_REQUEST['namesubmit'])){/*按名称检索 */		
			$sotype = $_REQUEST['sotype'];
			$name = $_REQUEST['name'];
			$sosotext = $_REQUEST['sosotext'];			
			if($sotype == 'cateid'){
				$sosotext = intval($sosotext);
				if($list_where)
					$list_where .= "`cateid` = '$sosotext' and ";
				else
					$list_where .= "`cateid` = '$sosotext' and ";
			}
			if($sotype == 'catename'){
				$sosotext = htmlspecialchars($sosotext);
				$info = $this->db->YOne("SELECT * FROM `@#_fenlei` where `name` = '$sosotext' LIMIT 1");
				
				if($info) $list_where .= "`cateid` = '$info[cateid]' and ";
			}
			if($sotype == 'title'){
				$sosotext = htmlspecialchars($sosotext);
				$list_where .= "`title` like '%$sosotext%' and ";
			}
			if($sotype == 'id'){
				$sosotext = intval($sosotext);
				$list_where .= "`id` = '$sosotext' and ";
			}
			if($sotype == 'mobile'){
				$list_where .= "`mobile` = '$sosotext' and ";
			}
		}
		//print_r($list_where);

		$num=13;
		$zongji=$this->db->Query("SELECT COUNT(*) FROM `@#_zg_shangpin` WHERE $list_where 1 order by `id` DESC ");
		$row = $zongji->fetch_row();
		$zongji = $row[0];
		$fenye=System::DOWN_sys_class('page');

		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($zongji,$num,$fenyenum,"0");
		$yyslist=$this->db->YPage("SELECT * FROM `@#_zg_shangpin` WHERE $list_where 1 order by `id` DESC",array("num"=>$num,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		$fenlei_lists = $this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE `parentid` = '0' and `model` = '1' order by `order` desc,`cateid` ASC",array('key'=>'cateid'));
		include $this->dwt(DOWN_M,'shop.goods_lists');
	}
	
	/* 单个商品的购买详细 */
	public function goods_go_one(){
		$yonghuid = intval($this->segment(4));
		$ginfo = $this->db->YOne("select * from `@#_zg_shangpin` where `id` = '$yonghuid' limit 1");
		if(!$ginfo)_note("没有找到这个商品");
		
		$zongji=$this->db->YCount("select * from `@#_zg_yonghu_yys_record` where `shopid` = '$yonghuid' order by `id` DESC");
		$fenye=System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($zongji,20,$fenyenum,"0");		
		$go_list = $this->db->YPage("select * from `@#_zg_yonghu_yys_record` where `shopid` = '$yonghuid' order by `id` DESC",array("num"=>20,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		include $this->dwt(DOWN_M,'shop.go_list');		
	}

	
	//商品回收站
	public function goods_del_list(){
		$this->ment=array(
						array("lists","返回商品列表",DOWN_M.'/'.DOWN_C."/goods_list"),
						array("add","添加商品",DOWN_M.'/'.DOWN_C."/goods_add"),
		);
		
		$num=20;
		$zongji=$this->db->YCount("SELECT COUNT(*) FROM `@#_zg_shangpin_del` WHERE 1"); 
		$fenye=System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($zongji,$num,$fenyenum,"0");
		$yyslist=$this->db->YPage("SELECT * FROM `@#_zg_shangpin_del` WHERE 1",array("num"=>$num,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		
		
		include $this->dwt(DOWN_M,'shop.del');
	}
	
	//编辑商品
	public function goods_edit(){
		$this->db->tijiao_start();
		$shopid=intval($this->segment(4));		
		$shopinfo=$this->db->YOne("SELECT * FROM `@#_zg_shangpin` WHERE `id` = '$shopid' order by `qishu` DESC LIMIT 1 for update");	
		//if($shopinfo['q_end_time'])_note("该商品已经揭晓,不能修改!",YYS_MODULE_PATH.'/shop/goods_list');
		if(!$shopinfo)_note("参数不正确");	
		$typesList = $this->db->YList("select * from `@#_zg_types`");
		
		if($shopinfo['newTypes']){
			$newTypes = unserialize($shopinfo['newTypes']);
			$html = '';
			foreach($newTypes as $key => $one){
				
				$html .= '<tr class="sinwlTr" id="newType_'.$key.'"><td align="right" style="width:120px"><font color="red">*</font>属性名称：</td><td>';
				$html .= '<input type="text" name="mainWord['.$one['ids'].']" dataId="'.$one['ids'].'" id="mainWord" value="'.$one['name'].'"/>';
				$html .= '属性类型：<select name="wordType[]">';
				if($one['type'] == 'select'){
					$html .= '<option value="select" selected>下拉列表</option>';
					$html .= '<option value="radio">单选</option>';
					$html .= '<option value="checkbox">多选</option>';
				}elseif($one['type'] == 'radio'){
					$html .= '<option value="select">下拉列表</option>';
					$html .= '<option value="radio" selected>单选</option>';
					$html .= '<option value="checkbox">多选</option>';
				}elseif($one['type'] == 'checkbox'){
					$html .= '<option value="select">下拉列表</option>';
					$html .= '<option value="radio">单选</option>';
					$html .= '<option value="checkbox" selected>多选</option>';
				}
				$html .= '</select>';
				$html .= '<input type="button" class="subWord button" dataId="'.$one['ids'].'" value="添加二级属性及价格"/>';
				$html .= '<a href="javascript:void(0);" class="del" dataId="'.$one['ids'].'">删除</a></td></tr>';
				
				foreach($one['sub'] as $k => $o){
					$html .= '<tr id="newTypeId_'.$k.'"><td align="right" style="width:120px"><font color="red">*</font>'.$one['name'].'：</td>';
					$html .= '<td>属性名称：<input type="text" class="sinwlSubInput_'.$one['ids'].'" name="sub_'.$one['ids'].'['.$k.'][subname]" value="'.$o['subname'].'"/> 价格：<input type="text" name="sub_'.$one['ids'].'['.$k.'][price]" value="'.$o['price'].'"/>';
					$html .= '<a href="javascript:void(0);" class="del" dataId="'.$k.'">删除</a>';
					$html .= '</td></tr>';
				}
			}
		}
		/*if($shopinfo['newTypes']){
			$newType = unserialize($shopinfo['newTypes']);

			foreach($newType as $key => $one){
				$newTypeInfo = $this->db->YOne("select * from `@#_zg_types` where `id` = '$one'");

				$type = $newTypeInfo['type'];
				$typePart = explode(',',$newTypeInfo['content']);

				if($type == 'text'){
					$html  = '<tr id="newType_'.$one.'"><td align="right" style="width:120px"><font color="red">*</font>'.$newTypeInfo['title'].'：</td><td>';
					$html .= '<input type="text" />';
					$html .= '<a href="javascript:void(0);" class="del" dataId="'.$one.'">删除</a></tr>';
				}elseif($type == 'select'){
					$html  = '<tr id="newType_'.$one.'"><td align="right" style="width:120px"><font color="red">*</font>'.$newTypeInfo['title'].'：</td><td>';
					$html .= '<select name="newType">';
					foreach($typePart as $tkey => $tone){
						$html .= '<option value="'.$one.'_'.$tone.'">'.$tone.'</option>';
					}
					$html .= '</select><a href="javascript:void(0);" class="del" dataId="'.$one.'">删除</a></td></tr>';
				}elseif($type == 'radio'){
					$html  = '<tr id="newType_'.$one.'"><td align="right" style="width:120px"><font color="red">*</font>'.$newTypeInfo['title'].'：</td><td>';
					foreach($typePart as $tkey => $tone){
						$html .= '<label class="sinwlLabel"><input type="radio" value="'.$one.'_'.$tone.'"/>'.$tone.'</label>';
					}
					$html .= '<a href="javascript:void(0);" class="del" dataId="'.$one.'">删除</a></td></tr>';
				}elseif($type == 'checkbox'){
					$html  = '<tr id="newType_'.$one.'"><td align="right" style="width:120px"><font color="red">*</font>'.$newTypeInfo['title'].'：</td><td>';
					foreach($typePart as $tkey => $tone){
						$html .= '<label class="sinwlLabel"><input type="checkbox" value="'.$one.'_'.$tone.'"/>'.$tone.'</label>';
					}
					$html .= '<a href="javascript:void(0);" class="del" dataId="'.$one.'">删除</a></td></tr>';
				}
				$html .= '<input type="hidden" id="newTypeId_'.$one.'" name="newTypeId[]" value="'.$one.'"/>';
				$htmls[] = $html;
			}
		}*/

		if(isset($_POST['dosubmit'])){		
		
			$cateid = intval($_POST['cateid']);
			$catesubid = intval($_POST['catesubid']);
			$pinpaiid = intval($_POST['brand']);
			$biaoti = htmlspecialchars($_POST['title']);
			$biaoti_color = htmlspecialchars($_POST['title_style_color']);
			$biaoti_bold = htmlspecialchars($_POST['title_style_bold']);
			$biaoti2 = htmlspecialchars($_POST['title2']);
			$money = htmlspecialchars($_POST['money']);
			$yunjiage = htmlspecialchars($_POST['yunjiage']);
			$supplierPercent = htmlspecialchars($_POST['supplierPercent']);
			$guanjianzi = htmlspecialchars($_POST['keywords']);
			$miaoshu = htmlspecialchars($_POST['description']);
			$content = editor_safe_replace(stripslashes($_POST['content']));			
			$thumb = trim(htmlspecialchars($_POST['thumb']));		
			
			$img1 = trim(htmlspecialchars($_POST['img1']));
			$img2 = htmlspecialchars($_POST['img2']);
			$img3 = htmlspecialchars($_POST['img3']);
			$img4 = htmlspecialchars($_POST['img4']);
			$isfreepost = ($_POST['isfreepost']);
			$isSince = ($_POST['isSince']);
			$isshop = ($_POST['isshop']);
			$isShelve = intval($_POST['isShelve']);
			$isstop = ($_POST['isstop']);
			$order = ($_POST['order']);
			$maxqishu = intval($_POST['maxqishu']) ? intval($_POST['maxqishu']) : 1;	
			$shangpinss_key_pos = isset($_POST['goods_key']['pos']) ? 1 : 0;
			$shangpinss_key_renqi = isset($_POST['goods_key']['renqi']) ? 1 : 0;		
			$shangpinss_key_renqi1 = isset($_POST['goods_key']['renqi1']) ? 1 : 0;
			//fxi by dabin
			$goods_key_leixing = $_POST['goods_key']['leixing'];
			$expressFee = ($_POST['expressFee']);
			$uid = ($_POST['uid']);
			$cardId1 = mysql_escape_string($_POST['cardId1']);
			$cardId2 = mysql_escape_string($_POST['cardId2']);
			$cardId = mysql_escape_string($_POST['cardId']);
			$cardPwd = mysql_escape_string($_POST['cardPwd']);

			$supplierId = intval($_POST['supplierId']);
			$costPrice	= $_POST['costPrice'];
			$sendUrl 	= $_POST['sendUrl'];
			$taobaoUrl 	= $_POST['taobaoUrl'];
			
			//修正新属性
			
			$mainWord = $_POST['mainWord'];
			$wordType = $_POST['wordType'];

			$subWord = array();
			foreach($mainWord as $key => $one){
				$subWord[$key]['ids'] = $key;
				$subWord[$key]['name'] = $one;
				$subWord[$key]['type'] = $wordType[$key];
				$subWord[$key]['sub'] = $_POST["sub_$key"];
			}
			
			$newTypes = serialize($subWord);
	
			$quyu_begin = intval($_POST['quyu_begin']) ? intval($_POST['quyu_begin']) : null;
			$quyu_end = intval($_POST['quyu_end']) ? intval($_POST['quyu_end']) : null;
			if(!$quyu_begin || !$quyu_end){
				$max_quyu = max($quyu_begin,$quyu_end);
				$quyu_begin = $quyu_begin==$max_quyu ? $quyu_end : $quyu_begin;
				$quyu_end = $max_quyu;
			}


			if(!$cateid)_note("请选择栏目");
			//			if(!$pinpaiid)_note("请选择品牌");
			if(!$biaoti)_note("标题不能为空");
			if(!$thumb)_note("缩略图不能为空");
			
			$biaoti_style='';
			if($biaoti_color){
				$biaoti_style.='color:'.$biaoti_color.';';
			}
			if($biaoti_bold){
				$biaoti_style.='font-weight:'.$biaoti_bold.';';
			}
			if(isset($_POST['uppicarr'])){
				$picarr = serialize($_POST['uppicarr']);
			}else{
				$picarr = serialize(array());
			}			
			if($_POST['xsjx_time'] != ''){			
				$xsjx_time = strtotime($_POST['xsjx_time']) ? strtotime($_POST['xsjx_time']) : time();
				$xsjx_time_h = intval($_POST['xsjx_time_h']) ? $_POST['xsjx_time_h'] : 36000;
				$xsjx_time += $xsjx_time_h;
			}else{
				$xsjx_time = '0';
			}
			
			if($maxqishu > 65535){
				_note("最大期数不能超过65535期");
			}	
			if($maxqishu < $shopinfo['qishu']){
				_note("最期数不能小于当前期数！");
			}	
				
			$sql="UPDATE `@#_zg_shangpin` SET `cateid` = '$cateid',
											`catesubid` = '$catesubid',
										   `brandid` = '$pinpaiid',
										   `title` = '$biaoti',
										   `title_style` = '$biaoti_style',
										   `title2` = '$biaoti2',
										   `money` = '$money',
										   `yunjiage` = '$yunjiage',
										   `supplierPercent` = '$supplierPercent',
										   `keywords`='$guanjianzi',
										   `description`='$miaoshu',
										   `thumb` = '$thumb',
										   
										   `img1` = '$img1',
										   `img2` = '$img2',
										   `img3` = '$img3',
										   `img4` = '$img4',
										   `isfreepost` = '$isfreepost',
										   `isSince` = '$isSince',
										   `isshop` = '$isshop',
										   `isShelve` = '$isShelve',
										   `isstop` = '$isstop',
										   `order` = '$order',

										   `picarr` = '$picarr',
										   `content` = '$content',	
										   `maxqishu` = '$maxqishu',
										   `renqi` = '$shangpinss_key_renqi',
										   `renqi1` = '$shangpinss_key_renqi1',
										   `leixing` = '$goods_key_leixing',
										   `xsjx_time` = '$xsjx_time',
										  `quyu_begin` = '$quyu_begin' ,
											`quyu_end` = '$quyu_end' ,
										   `ka`='".htmlspecialchars($_POST['kahao'])."',
										   `mi`='".htmlspecialchars($_POST['mima'])."',
										   `pos` = '$shangpinss_key_pos',
										   `uid` = '$uid',
										   `cardId1` = '$cardId1',
											`yuanjia` = '$cardId2',
										   `cardId` = '$cardId',
										   `cardPwd` = '$cardPwd',
											`expressFee` = '$expressFee',
											`supplierId` = '$supplierId',
											`costPrice` = '$costPrice',
											`sendUrl` = '$sendUrl',
											`taobaoUrl` = '$taobaoUrl',
											`newTypes` = '$newTypes'
											WHERE `id`='$shopid'
											
			";				
			$s_sid = $shopinfo['sid'];
			$this->db->Query("UPDATE `@#_zg_shangpin` SET `maxqishu` = '$maxqishu' where `sid` = '$s_sid'");			
			if($this->db->Query($sql)){			
				$this->db->tijiao_commit();	
				_note("修改成功!");
			}else{	
				$this->db->tijiao_rollback();
				_note("修改失败!");
			}			
		}	
		$this->ment=array(
						array("lists","商品管理",DOWN_M.'/'.DOWN_C."/goods_list"),
						array("insert","添加商品",DOWN_M.'/'.DOWN_C."/goods_add"),
		);			
		
		$cateinfo=$this->db->YOne("SELECT * FROM `@#_fenlei` WHERE `cateid` = '$shopinfo[cateid]' LIMIT 1");
		//增加子分类调用
		$fenleis=$this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE `model` = '1' order by `order` desc,`cateid` ASC",array('key'=>'cateid'));
		$fenlei_lists = $this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE `parentid` = '0' and `model` = '1' order by `order` desc,`cateid` ASC",array('key'=>'cateid'));
		//print_r($fenlei_lists);
		$fenlei_list = $this->db->Ylist("select * from `@#_fenlei` where `parentid` = '$shopinfo[cateid]'");
		
		$pinpaiList=$this->db->Ylist("SELECT * FROM `@#_pinpai` where 1",array("key"=>'id'));
				
		if($shopinfo['title_style']){		
			if(stripos($shopinfo['title_style'],"font-weight:")!==false){
				$biaoti_bold = 'bold';
			}else{
				$biaoti_bold = '';
			}
			if(stripos($shopinfo['title_style'],"color:")!==false){
				$biaoti_color = explode(';',$shopinfo['title_style']); 
				$biaoti_color = explode(':',$biaoti_color[0]); 
				$biaoti_color = $biaoti_color[1];
			}else{
				$biaoti_color = '';
			}				
		}else{
			$biaoti_color='';
			$biaoti_bold = '';
		}	
		
		$shopinfo['picarr'] = unserialize($shopinfo['picarr']);
				
		if($shopinfo['xsjx_time']){
			$shopinfo['xsjx_time_1'] = date("Y-m-d",$shopinfo['xsjx_time']);
			$shopinfo['xsjx_time_h'] = $shopinfo['xsjx_time'] - strtotime($shopinfo['xsjx_time_1']);
		    $shopinfo['xsjx_time'] = $shopinfo['xsjx_time_1'];
		}else{
			$shopinfo['xsjx_time']='';
			$shopinfo['xsjx_time_h']=79200;
		}	
		
	
		include $this->dwt(DOWN_M,'shop.goods_edit');
	}
	
	
	//添加商品
	public function goods_add(){		
	
		$typesList = $this->db->YList("select * from `@#_zg_types`");

		if(isset($_POST['dosubmit'])){
		
			$cateid = intval($_POST['cateid']);
			$catesubid = intval($_POST['catesubid']);
			$order = intval($_POST['order']);
			$pinpaiid = intval($_POST['brand']);
			$biaoti = _htmtguolv($_POST['title']);
			$biaoti_color = htmlspecialchars($_POST['title_style_color']);
			$biaoti_bold = htmlspecialchars($_POST['title_style_bold']);
			$biaoti2 = _htmtguolv($_POST['title2']);
						
			$guanjianzi = htmlspecialchars($_POST['keywords']);
			$miaoshu = htmlspecialchars($_POST['description']);
			$content = editor_safe_replace(stripslashes($_POST['content']));
			$money = intval($_POST['money']);
			$yunjiage = intval($_POST['yunjiage']);
			$yuanjia = intval($_POST['yuanjia']);
			$thumb = htmlspecialchars($_POST['thumb']);	

			$img1 = trim(htmlspecialchars($_POST['img1']));
			$img2 = trim(htmlspecialchars($_POST['img2']));
			$img3 = trim(htmlspecialchars($_POST['img3']));
			$img4 = trim(htmlspecialchars($_POST['img4']));
			$isfreepost = ($_POST['isfreepost']);
			$isSince = ($_POST['isSince']);
			$isshop = ($_POST['isshop']);
			$isShelve = ($_POST['isShelve']);
			$isstop = ($_POST['isstop']);
			
			$maxqishu = intval($_POST['maxqishu']);			
			$canyurenshu = 0;		
			$shangpinss_key_pos = isset($_POST['goods_key']['pos']) ? 1 : 0;
			$shangpinss_key_renqi = isset($_POST['goods_key']['renqi']) ? 1 : 0;
			$shangpinss_key_renqi1 = isset($_POST['goods_key']['renqi1']) ? 1 : 0;
			
			$supplierId 	= floatval($_REQUEST['supplierId']);
			$costPrice	= $_POST['costPrice'];
			$sendUrl 	= $_POST['sendUrl'];
			$taobaoUrl 	= $_POST['taobaoUrl'];
			$supplierPercent = floatval($_POST['supplierPercent']);
			$uid = ($_POST['uid']);
			
			$mainWord = $_POST['mainWord'];
			$wordType = $_POST['wordType'];

			$subWord = array();
			foreach($mainWord as $key => $one){
				$subWord[$key]['ids'] = $key;
				$subWord[$key]['name'] = $one;
				$subWord[$key]['type'] = $wordType[$key];
				$subWord[$key]['sub'] = $_POST["sub_$key"];
			}
			$newTypes = serialize($subWord);

            if($_POST['zzfb'] > 0){
                $id = $_POST['zzfb'];
                $detail=$this->db->YOne("SELECT * FROM `@#_sqfb` WHERE id = $id and stuats = 2 and goods_id = 0");
                if(!$detail)
                {
                    _note("请检查是否审核通过或是已存在商品",G_ADMIN_PATH.'/zzfb/addlist');
                }
                $uid = $_POST['uid'];
            }
			//            else{
			//                $uid = 0;
 			//           }


			//fix by dabin
			$goods_key_leixing = $_POST['goods_key']['leixing'];
			
			//roce:获取卡密卡号
			$cardId1 = mysql_escape_string($_POST['cardId1']);
			$cardId2 = mysql_escape_string($_POST['cardId2']);
			$cardId = mysql_escape_string($_POST['cardId']);
			$cardPwd = mysql_escape_string($_POST['cardPwd']);


			$quyu_begin = intval($_POST['quyu_begin']) ? intval($_POST['quyu_begin']) : null;
			$quyu_end = intval($_POST['quyu_end']) ? intval($_POST['quyu_end']) : null;
			if(!$quyu_begin || !$quyu_end){
				$max_quyu = max($quyu_begin,$quyu_end);
				$quyu_begin = $quyu_begin==$max_quyu ? $quyu_end : $quyu_begin;
				$quyu_end = $max_quyu;
			}
			if(!$cateid)_note("请选择栏目");
			//			if(!$pinpaiid)_note("请选择品牌");
			if(!$biaoti)_note("标题不能为空");
			if(!$thumb)_note("缩略图不能为空");
			$expressFee = ($_POST['expressFee']);
			
			
			$biaoti_style='';
			if($biaoti_color){
				$biaoti_style.='color:'.$biaoti_color.';';
			}
			if($biaoti_bold){
				$biaoti_style.='font-weight:'.$biaoti_bold.';';
			}
			if(isset($_POST['uppicarr'])){
				$picarr = serialize($_POST['uppicarr']);
			}else{
				$picarr = serialize(array());
			}
		
		
		
			
			if($_POST['xsjx_time'] != ''){			
				$xsjx_time = strtotime($_POST['xsjx_time']) ? strtotime($_POST['xsjx_time']) : time();
				$xsjx_time_h = intval($_POST['xsjx_time_h']) ? $_POST['xsjx_time_h'] : 36000;
				$xsjx_time += $xsjx_time_h;	
			}else{
				$xsjx_time = '0';		
			}	
		
		
			if($maxqishu > 65535){
				_note("最大期数不能超过65535期");
			}			
							
								
			//			if($money < $yunjiage) _note("商品价格不能小于购买价格");					
			$zongrenshu = ceil($money/$yunjiage);
			$codes_len = ceil($zongrenshu/3000);
			$shenyurenshu = $zongrenshu-$canyurenshu;
			if($zongrenshu==100000 || ($zongrenshu-$canyurenshu)==100000){
				_note("价格不正确");
			}

					
			$time=time();	//商品添加时间		
			$this->db->tijiao_start();
					
			$query_1 = $this->db->Query("INSERT INTO `@#_zg_shangpin` (`quyu_begin`,`quyu_end`,`ka`,`mi`,`cateid`,`catesubid`,`order`, `brandid`, `title`, `title_style`, `title2`, `keywords`, `description`, `money`, `yunjiage`, `zongrenshu`, `canyurenshu`,`shenyurenshu`, `qishu`,`maxqishu`,`thumb`, `img1`,`img2`,`img3`,`img4`,`isfreepost`,`isSince`,`isshop`,`isShelve`,`isstop`,`picarr`, `content`,`xsjx_time`,`renqi`,`renqi1`,`pos`, `time`, `cardId`, `cardId1`, `cardPwd`, `leixing`,`yuanjia`,`uid`,`expressFee`,`supplierId`,`costPrice`,`sendUrl`,`taobaoUrl`,`newTypes`,`supplierPercent`) VALUES ( '$quyu_begin','$quyu_end','".htmlspecialchars($_POST['kahao'])."','".htmlspecialchars($_POST['mima'])."','$cateid','$catesubid','$order', '$pinpaiid', '$biaoti', '$biaoti_style', '$biaoti2', '$guanjianzi', '$miaoshu', '$money', '$yunjiage', '1', '$canyurenshu','1', '0','$maxqishu', '$thumb', '$img1','$img2','$img3','$img4','$isfreepost','$isSince','$isshop','$isShelve','$isstop','$picarr', '$content','$xsjx_time','$shangpinss_key_renqi','$shangpinss_key_renqi1', '$shangpinss_key_pos','$time','$cardId','$cardId1','$cardPwd','$goods_key_leixing','$cardId2','$uid','$expressFee','$supplierId','$costPrice','$sendUrl','$taobaoUrl','$newTypes','$supplierPercent')");
			$shopid = $this->db->last_id();
			System::DOWN_App_fun("content");	
			
			$query_table = content_get_codes_table();
			if(!$query_table){
				$this->db->tijiao_rollback();
				_note("众筹码仓库不正确!");
			}
			$query_2 = content_huode_go_codes($zongrenshu,3000,$shopid);
			$query_3 = $this->db->Query("UPDATE `@#_zg_shangpin` SET `codes_table` = '$query_table',`sid` = '$shopid',`def_renshu` = '$canyurenshu' where `id` = '$shopid'");
			
					
			if($query_1 && $query_2 && $query_3){
				$this->db->tijiao_commit();
                if($_POST['zzfb'] > 0){
                    $id = $_POST['zzfb'];
                    $this->db->Query("UPDATE `@#_sqfb` set goods_id = $shopid WHERE id = $id");



                    include_once((dirname(dirname(dirname(dirname(__FILE__)))).'/phpqrcode/phpqrcode.php'));
                    QRcode::png(LOCAL_PATH."/products/".$shopid.'.html','zizhu/'.$shopid.'.png','L',8);


                    _note("自助商品添加成功并已生成链接!",G_ADMIN_PATH.'/zzfb/goodslist');
                }
				_note("商品添加成功!");
			}else{		
			
				$this->db->tijiao_rollback();
				_note("商品添加失败!");
			}	
			
			header("Cache-control: private");
		}
		
		
		$temp=intval($this->segment(4));
        if($temp == 'zzfb'){
            $id = intval($this->segment(5));
        }
        else{
            $cateid=intval($this->segment(4));
        }
		
		$supplierId=  $_REQUEST['supplierId'];
		
		
		//增加子分类调用
		$fenleis=$this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE `model` = '1' order by `order` desc,`cateid` desc",array('key'=>'cateid'));
		$fenlei_lists = $this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE `parentid` = '0' and `model` = '1' order by `order` desc,`cateid` desc",array('key'=>'cateid'));
		$fenlei_list = $this->db->Ylist("select * from `@#_fenlei` where `parentid` = '$shopinfo[cateid]'");
		
		$tree=System::DOWN_sys_class('tree');
		$tree->icon = array('│ ','├─ ','└─ ');
		$tree->nbsp = '&nbsp;';
		$fenleishtml="<option value='\$cateid'>\$spacer\$name</option>";
		$tree->init($fenleis);	
		$fenleishtml=$tree->get_tree(0,$fenleishtml);
		$fenleishtml='<option value="0">≡ 请选择栏目 ≡</option>'.$fenleishtml;
		if($cateid){			
			$cateinfo=$this->db->YOne("SELECT * FROM `@#_fenlei` WHERE `cateid` = '$cateid' LIMIT 1");
			if(!$cateinfo)_note("参数不正确,没有这个栏目",G_ADMIN_PATH.'/'.DOWN_C.'/addarticle');
			$fenleishtml.='<option value="'.$cateinfo['cateid'].'" selected="true">'.$cateinfo['name'].'</option>';
			$pinpaiList=$this->db->Ylist("SELECT * FROM `@#_pinpai` where `cateid`='$cateid'",array("key"=>"id"));
		}else{
			$pinpaiList=$this->db->Ylist("SELECT * FROM `@#_pinpai` where 1",array("key"=>"id"));
		}	
		
		$this->ment=array(
			array("lists","商品管理",DOWN_M.'/'.DOWN_C."/goods_list"),
			array("insert","添加商品",DOWN_M.'/'.DOWN_C."/goods_add"),
		);
        if($id){
            $detail=$this->db->YOne("SELECT * FROM `@#_sqfb` WHERE id = $id and stuats = 2 and goods_id = 0");
			if($detail['newTypes']){
				$newTypes = unserialize($detail['newTypes']);
				$html = '';
				foreach($newTypes as $key => $one){
					
					$html .= '<tr class="sinwlTr"><td align="right" style="width:120px"><font color="red">*</font>属性名称：</td><td>';
					$html .= '<input type="text" name="mainWord['.$one['ids'].']" dataId="'.$one['ids'].'" id="mainWord" value="'.$one['name'].'"/>';
					$html .= '属性类型：<select name="wordType['.$one['ids'].']">';
					if($one['type'] == 'select'){
						$html .= '<option value="select" selected>下拉列表</option>';
					}elseif($one['type'] == 'radio'){
						$html .= '<option value="radio" selected>单选</option>';
					}elseif($one['type'] == 'checkbox'){
						$html .= '<option value="checkbox" selected>多选</option>';
					}
					$html .= '</select>';
					$html .= '<input type="button" class="subWord button" dataId="'.$one['ids'].'" value="添加二级属性及价格"/></td></tr>';
					
					foreach($one['sub'] as $k => $o){
						$html .= '<tr><td align="right" style="width:120px"><font color="red">*</font>'.$one['name'].'：</td>';
						$html .= '<td>属性名称：<input type="text" class="sinwlSubInput_'.$one['ids'].'" name="sub_'.$one['ids'].'['.$k.'][subname]" value="'.$o['subname'].'"/> 价格：<input type="text" name="sub_'.$one['ids'].'['.$k.'][price]" value="'.$o['price'].'"/>';
						$html .= '</td></tr>';
					}
				}
			}
            if(!$detail)
            {
                _note("请检查是否审核通过或是已存在商品",G_ADMIN_PATH.'/zzfb/addlist');
            }
        }
		include $this->dwt(DOWN_M,'shop.goods_add');
			
	}
	
	
	//商品设置
	public function goods_set(){
		$p_key = $this->segment(4);
		$p_val = intval($this->segment(5));
		
		if(empty($p_key) || empty($p_val)){
			_note("设置失败");
		}
		$ss = $this->db->YOne("SELECT * FROM `@#_shai` where `sd_shopid` = '$p_val'");
		//var_dump($ss[sd_userid]);
		//exit;
		$query = true;
		switch($p_key){
			case 'renqi':
				$query = $this->db->Query("UPDATE `@#_zg_shangpin` SET `renqi` = '1' where `id` = '$p_val'");				
				break;	
				case 'renqi1':
				$query = $this->db->Query("UPDATE `@#_zg_shangpin` SET `renqi1` = '1' where `id` = '$p_val'");				
				break;	
				case 'pos':
				$query = $this->db->Query("UPDATE `@#_zg_shangpin` SET `pos` = '1' where `id` = '$p_val'");				
				break;	
				case 'shenhe':
				$query = $this->db->Query("UPDATE `@#_shai` SET `shenhe` = '1' where `sd_shopid` = '$p_val'");	
				$this->db->Query("UPDATE `@#_yonghu` SET `score` = score+'1000' where `uid` = '$ss[sd_userid]'");
				break;	
				case 'shenhe1':
				$query = $this->db->Query("UPDATE `@#_shai` SET `shenhe` = '0' where `sd_shopid` = '$p_val'");				
				break;	
				case 'fahuo':
				$query = $this->db->Query("UPDATE `@#_zg_shangpin` SET `fahuo` = '0' where `id` = '$p_val'");				
				break;
				case 'fahuo1':
				$query = $this->db->Query("UPDATE `@#_zg_shangpin` SET `fahuo` = '1' where `id` = '$p_val'");				
				break;
				case 'huiyuan':
				$query = $this->db->Query("UPDATE `@#_yonghu` SET `huiyuan` = '1' where `uid` = '$p_val'");
				break;	
				case 'huiyuan1':
				$query = $this->db->Query("UPDATE `@#_yonghu` SET `huiyuan` = '0' where `uid` = '$p_val'");
				break;	
		}
		
		if($query){
			_note("设置成功");
		}else{
			_note("设置失败");
		}
		
		
	}
	
	//ajax 删除文章
	public function article_del(){
		$id=intval($this->segment(4));		
		$this->db->Query("DELETE FROM `@#_wenzhang` WHERE (`id`='$id') LIMIT 1");
			if($this->db->affected_rows()){			
				_note("文章删除成功",LOCAL_PATH."/admin/shop/article_list");
			}else{
				echo "no";
			}	
	}
	
	//ajax 删除商品
	public function goods_del(){
		$this->db=System::DOWN_App_model('admin_model',G_ADMIN_DIR);
		if($uid && $ashell){
			$CheckId = _encrypt($uid,'DECODE');
			$CheckAshell =  _encrypt($ashell,'DECODE');
		}else{			
			$CheckId=_encrypt(_getcookie("AID"),'DECODE');
			$CheckAshell=_encrypt(_getcookie("ASHELL"),'DECODE');
		}
		$info=$this->db->YOne("SELECT * FROM `@#_manage` WHERE `uid` = '$CheckId'");
		//老的权限删除

		if($info[xianzhi]=='1'){
				_note("测试帐号无修改权限!<br>旺旺号:澳安达 QQ：320023815");
			}
			
		$shopid=intval($this->segment(4));		
		$info = $this->db->YOne("SELECT codes_table FROM `@#_zg_shangpin` WHERE `id` = '$shopid'");
		$table = $info['codes_table'];
		
		$this->db->tijiao_start();	
		//$q1 = $this->db->Query("INSERT INTO `@#_zg_shangpin_del` select * from `@#_zg_shangpin` where `id` = '$shopid'");
		$q1 = $this->db->Query("DELETE FROM `@#_{$table}` WHERE `s_id` = '$shopid'");
		$q2 = $this->db->Query("DELETE FROM `@#_zg_shangpin` WHERE `id` = '$shopid' LIMIT 1");	
		//	$q3 = $this->db->Query("DELETE FROM `@#_zg_yonghu_yys_record` WHERE `shopid` = '$shopid'");	
		if($q2){					
			$this->db->tijiao_commit();
			_note("商品删除成功",LOCAL_PATH."/admin/shop/goods_list");
		}else{
			$this->db->tijiao_rollback();
			echo "no";
		}
		exit;
	}	
	
	
	// 撤销删除
	public function goods_del_key(){
		$shopid=intval($this->segment(4));
		$key=$this->segment(5);
		//撤销	
		if($key=='yes'){			
			$this->db->tijiao_start();
			$q1 = $this->db->Query("INSERT INTO `@#_zg_shangpin` select * from `@#_zg_shangpin_del` where `id` = '$shopid' LIMIT 1");
			$q2 = $this->db->Query("DELETE FROM `@#_zg_shangpin_del` WHERE `id` = '$shopid' LIMIT 1");
			if(!$q1 || !$q2){
				$this->db->tijiao_rollback();
				_note("操作失败");			
			}else{
				$this->db->tijiao_commit();
				_note("操作成功");
			}
		}
		//从数据库删除
		if($key=='no'){
			$this->db->Query("DELETE FROM `@#_zg_shangpin_del` WHERE `id` = '$shopid' LIMIT 1");
			_note("操作成功");
		}
	}	
	
	//清空回收站
	public function goods_del_all(){
		$this->db->Query("TRUNCATE TABLE  `@#_zg_shangpin_del`");
		_note("清空成功");
	}
	
	
	/*
	*	商品排序
	*/	
	public function goods_listorder(){		
		if($this->segment(4)=='dosubmit'){
			foreach($_POST['listorders'] as $id => $listorder){
				$id = intval($id);
				$listorder = intval($listorder);
				$this->db->Query("UPDATE `@#_zg_shangpin` SET `order` = '$listorder' where `id` = '$id'");		
			}				
			_note("排序更新成功");
		}else{
			_note("请排序");
		}		
	}//
	
	/*
	*	文章排序
	*/	
	public function article_listorder(){		
		if($this->segment(4)=='dosubmit'){
			foreach($_POST['listorders'] as $id => $listorder){
				$id = intval($id);
				$listorder = intval($listorder);
				$this->db->Query("UPDATE `@#_wenzhang` SET `order` = '$listorder' where `id` = '$id'");		
			}				
			_note("排序更新成功");
		}else{
			_note("请排序");
		}		
	}//
		
	/*
	*	商品最大期数修改
	*	ajax
	*/	
	public function goods_max_qishu(){
		if($this->segment(4)!='dosubmit'){
			echo json_encode(array("meg"=>"not key","err"=>"-1"));			
			exit;
		}		
		
		if(!isset($_POST['gid']) || !isset($_POST['qishu']) || !isset($_POST['money']) || !isset($_POST['onemoney'])){
			echo json_encode(array("meg"=>"参数不正确!","err"=>"-1"));
			exit;
		}
				
		$yonghuid = abs(intval($_POST['gid']));
		$qishu = abs(intval($_POST['qishu']));
		$money = abs(intval($_POST['money']));
		$onemoney = abs(intval($_POST['onemoney']));
		
		
		if(!$yonghuid || !$qishu){
			echo json_encode(array("meg"=>"参数不正确!","err"=>"-1"));
			exit;
		}
		/*if($money < $onemoney){
			echo json_encode(array("meg"=>" 总价不能小于众筹价!","err"=>"-1"));
			exit;
		}*/
		
		$info = $this->db->YOne("SELECT * FROM `@#_zg_shangpin` where `id` = '$yonghuid'");
		
		if(!$info){
			echo json_encode(array("meg"=>"没有该商品或还有剩余期数!","err"=>"-1"));
			exit;
		}
		
		if($qishu <= $info['qishu']){
			echo json_encode(array("meg"=>"期数不正确!","err"=>"-1"));
			exit;
		}		
		$ret = $this->db->Query("UPDATE `@#_zg_shangpin` SET `maxqishu` = '$qishu' where `sid` = '$info[sid]'");
		if(!$ret){
			echo json_encode(array("meg"=>"库存更新失败!","err"=>"-1"));
			exit;
		}else{
			echo json_encode(array("meg"=>"库存更新成功!","err"=>"1"));
			exit;
		}
		$info['maxqishu'] = $qishu;
		$info['money'] = $money;
		$info['yunjiage'] = $onemoney;
		$info['zongrenshu'] = ceil($money/$onemoney);		
		System::DOWN_App_fun("content");
		//$ret = content_add_shop_install($info);
		if($ret){
			echo json_encode(array("meg"=>"商品库存更新成功!","err"=>"1"));
			exit;
		}else{
			echo json_encode(array("meg"=>"库存更新失败!","err"=>"-1"));
			exit;		
		}		
	
		
	}//
	
	/**
	*	重置商品价格
	**/
	public function goods_set_money(){		

		$this->ment=array(
			array("lists","商品管理",DOWN_M.'/'.DOWN_C."/goods_list"),
			array("add","添加商品",DOWN_M.'/'.DOWN_C."/goods_add"),
			array("renqi","人气商品",DOWN_M.'/'.DOWN_C."/goods_list/renqi"),
			array("money","商品价格倒序",DOWN_M.'/'.DOWN_C."/goods_list/money"),

		);	
		$this->db->tijiao_start();	
		$yonghuid = abs(intval($this->segment(4)));
		$shopinfo = $this->db->YOne("SELECT * FROM `@#_zg_shangpin` where `id` = '$yonghuid' for update");		
		if(!$shopinfo || !empty($shopinfo['q_uid'])){
			_note("参数不正确!");exit;
		}
		
		if(isset($_POST['money']) || isset($_POST['yunjiage'])){
			$new_money = abs(intval($_POST['money']));
			$new_one_m = abs(intval($_POST['yunjiage']));
			
	
			if($new_one_m > $new_money){
				_note("单件购买价格不能大于商品总价格!");
			}
			if($new_one_m < 0 || $new_money < 0){
				_note("价格填写错误!");
			}
			if(($new_one_m == $shopinfo['yunjiage']) && ($new_money == $shopinfo['money'])){
				_note("价格没有改变!");
			}
			
			System::DOWN_App_fun("content");
						
			$table = $shopinfo['codes_table'];
			$this->db->tijiao_start();	
			$q1 = $this->db->Query("DELETE FROM `@#_zg_yonghu_yys_record` WHERE `shopid` = '$yonghuid'");
			$q2 = $this->db->Query("DELETE FROM `@#_{$table}` WHERE `s_id` = '$yonghuid'");			
			$zongrenshu = ceil($new_money/$new_one_m);		
			
			$q3 = content_huode_go_codes($zongrenshu,3000,$yonghuid);
			$q4 = $this->db->Query("UPDATE `@#_zg_shangpin` SET 
			`canyurenshu` = '0',
			`zongrenshu` = '$zongrenshu', 
			`money` = '$new_money', 
			`yunjiage` = '$new_one_m', 
			`shenyurenshu` = `zongrenshu`
			where `id` = '$yonghuid'");
			
			
			
			if($q1 && $q2 && $q3 && $q4){
					$this->db->tijiao_commit();
					_note("更新成功!");
			}else{
				$this->db->tijiao_rollback();
				_note("更新失败!");				
			}
		}
				
		include $this->dwt(DOWN_M,'shop.set_money');
	}
	
	
	public function settings(){
		
		$this->ment=array(
			array("lists","商品管理",DOWN_M.'/'.DOWN_C."/goods_list"),
			array("add","添加商品",DOWN_M.'/'.DOWN_C."/goods_add"),
			array("renqi","人气商品",DOWN_M.'/'.DOWN_C."/goods_list/renqi"),
			array("xsjx","限时揭晓商品",DOWN_M.'/'.DOWN_C."/goods_list/xianshi"),
			array("qishu","期数倒序",DOWN_M.'/'.DOWN_C."/goods_list/qishu"),
			array("danjia","单价倒序",DOWN_M.'/'.DOWN_C."/goods_list/danjia"),
			array("money","商品价格倒序",DOWN_M.'/'.DOWN_C."/goods_list/money"),
			array("money","已揭晓",DOWN_M.'/'.DOWN_C."/goods_list/jiexiaook"),
			array("money","<font color='#f00'>期数已满商品</font>",DOWN_M.'/'.DOWN_C."/goods_list/maxqishu"),
		);	
		
		if(isset($_POST['submit'])){
			$path =  dirname(__FILE__).'/control/commision.ini.php';			
			if(!is_writable($path)) _note('Please chmod  commision.ini.php  to 0777 !');
			$commision=intval(trim($_POST['commision']));
			$commisionDay=intval(trim($_POST['commisionDay']));

			if($commision<=0){
				_note('请输入正确的返还比例');
			}
			if($commisionDay<1){
				_note('请输入正确的');
			}

			$html=<<<HTML
<?php 
return array (	
	'commision' => '$commision',
	'commisionDay' => '$commisionDay',
);
?>
HTML;

			file_put_contents($path,$html);
				_note("修改成功!");
			}
			
			$config = System::DOWN_App_config("commision");	
		
		include $this->dwt(DOWN_M,'shop.zg_settings');
	}
	
	public function types_list(){

		$this->ment=array(
			array("lists","属性字段列表",DOWN_M.'/'.DOWN_C."/types_list"),
			array("add","添加属性字段",DOWN_M.'/'.DOWN_C."/types_add"),
		);	
		
		$types = array(
			'text' => '文本框',
			'select' => '下拉列表',
			'radio' => '单选',
			'checkbox' => '多选',
		);
		
		$num=20;
		$zongji=$this->db->YCount("SELECT COUNT(*) FROM `@#_zg_types`");
		$fenye=System::DOWN_sys_class('page');

		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($zongji,$num,$fenyenum,"0");
		$yyslist=$this->db->YPage("SELECT * FROM `@#_zg_types` order by id desc",array("num"=>$num,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		include $this->dwt(DOWN_M,'shop.types');
	}
	
	public function types_add(){
		$this->ment=array(
			array("lists","属性字段列表",DOWN_M.'/'.DOWN_C."/types_list"),
			array("add","添加属性字段",DOWN_M.'/'.DOWN_C."/types_add"),
		);	
		
		if(isset($_POST['dosubmit'])){
			
			$title = $_POST['title'];
			$type = $_POST['type'];
			$content = explode("\r\n",trim($_POST['content']));
			$content = implode(',',$content);
			$addTime = time();
			
			if(!$title){
				_note("请输入字段标题!");
			}
			if(!$type){
				_note("请选择正确的字段类型!");
			}
			
			if($type != 'text' && $content == ''){
				_note("当字段类型为单选、多选、下拉列表时内容不可为空");
			}
			
			$sql="INSERT INTO `@#_zg_types` (`title`,`type`,`content`,`addTime`) VALUES ('$title', '$type','$content','$addTime')";			
			$this->db->Query($sql);
			if($this->db->affected_rows()){			
				_note("添加成功!",LOCAL_PATH.'/'.DOWN_M.'/'.DOWN_C.'/types_list');
			}else{
				_note("添加失败!");
			}
		}
		include $this->dwt(DOWN_M,'shop.types_add');
	}
	
	
	public function types_edit(){
		$this->ment=array(
			array("lists","属性字段列表",DOWN_M.'/'.DOWN_C."/types_list"),
			array("add","添加属性字段",DOWN_M.'/'.DOWN_C."/types_add"),
		);	
		
		$id = $this->segment(4);
		$types = $this->db->YOne("select * from `@#_zg_types` where `id` = '$id'");
		if(!$types){
			_note("不存在这个类型");
		}else{
			$types['content'] = str_replace(",","\r\n",$types['content']);
		}
		if(isset($_POST['dosubmit'])){
			$id = intval($_POST['id']);
			$title = $_POST['title'];
			$type = $_POST['type'];
			$content = explode("\r\n",trim($_POST['content']));
			$content = implode(',',$content);
			$addTime = time();
			
			if(!$title){
				_note("请输入字段标题!");
			}
			if(!$type){
				_note("请选择正确的字段类型!");
			}
			
			if(($type != 'text' || $type != 'areatext' ) && !$content){
				_note("当字段类型为单选、多选、下拉列表时内容不可为空");
			}
			
			$sql="UPDATE `@#_zg_types` SET `title` = '$title',`type` = '$type',`content` = '$content',`addTime` = '$addTime' WHERE `id` = '$id'";	
			$this->db->Query($sql);
			if($this->db->affected_rows()){			
				_note("修改成功!",LOCAL_PATH.'/'.DOWN_M.'/'.DOWN_C.'/types_list');
			}else{
				_note("修改失败!");
			}
		}
		include $this->dwt(DOWN_M,'shop.types_edit');
	}	
	
	public function types_del(){
		$id = $this->segment(4);
		if(!$id){
			_note("请输入字段标题!");
		}else{
			$res = $this->db->Query("delete from `@#_zg_types` where `id` = '$id'");
			if($res){
				_note("删除成功",LOCAL_PATH.'/'.DOWN_M.'/'.DOWN_C.'/types_list');
			}else{
				_note("删除失败!");
			}
		}
	}
	
	public function serializeType(){
		$typeId = intval($_POST['typeId']);
		$types = $this->db->YOne("select * from `@#_zg_types` where `id` = '$typeId'");

		if(!$types){
			$status = 0;
			$msg = '不存在此属性';
		}else{
			$type = $types['type'];
			$typePart = explode(',',$types['content']);
			$status = 1;
		}
		echo json_encode(array('status'=>$status,'html'=>$html));
	}
	
	public function setshelve(){
		$id = $this->segment(4);
		if(!$id){
			_note("不存在这个商城商品!");
		}else{
			$info = $this->db->YOne("select `isShelve` from `@#_zg_shangpin` where `id` = '$id'");
			if($info['isShelve'] == 1){
				$isShelve = 0;
				$msg = '下架成功';
			}else{
				$isShelve = 1;
				$msg = '上架成功';
			}
			$sql = "UPDATE `@#_zg_shangpin` SET `isShelve` = '$isShelve' WHERE `id` = '$id'";
			$res = $this->db->Query($sql);
			if($res){
				_note($msg);
			}else{
				_note('操作失败');
			}
		}
	}
/*商城结束*/
	
	
	
	
	
	


	//商品图片上传到磁盘

	public function shop_img_insert(){

	

		if(is_array($_FILES['Filedata'])){		

			System::DOWN_sys_class('upload','sys','no');

			upload::upload_config(array('png','jpg','jpeg','gif'),500000,'shopimg');

			upload::go_upload($_FILES['Filedata']);

			$msg=array();

			if(!upload::$ok){

				$msg['ok']='no';

				$msg['text']=upload::$error;

			}else{

				$msg['ok']='yes';

				$msg['text']=upload::$filedir."/".upload::$filename;				

			}	

			echo json_encode($msg);	

		}

		

	}

	//商品展示图片从磁盘删除

	public function shop_img_del(){		

		$action=isset($_GET['action']) ? $_GET['action'] : null; 

		$filename=isset($_GET['filename']) ? $_GET['filename'] : null;

		if($action=='del' && !empty($filename)){

			$filename=YYS_UPLOADS.'shopimg/'.$filename;			

			$size=getimagesize($filename);			

			$filetype=explode('/',$size['mime']);			

			if($filetype[0]!='image'){

				return false;

				exit;

			}

			unlink($filename);

			exit;

		}

	}

}



?>