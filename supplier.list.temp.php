<?php defined('G_IN_ADMIN')or exit('Access Denied.'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>商户审核开店</title>
<link rel="stylesheet" href="/statics/plugin/mstyle/css/style.css" type="text/css"><!--页面CSS-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/animate.min.css" type="text/css"><!--CSS3动画-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/timer.css" type="text/css"><!--CSS3动画-->
<script type="text/javascript" src="/statics/plugin/mstyle/js/jquery.min.js"></script><!--jQuery库-->
<script type="text/javascript" charset="utf-8" src="<?php echo YYS_PLUGIN_PATH; ?>/calendar/calendar.js"></script>
<script src="<?php echo YYS_GLOBAL_STYLE; ?>/global/js/jquery-1.8.3.min.js"></script>
</head>
<body>
	<div class="container min-wid">
		<div class="path">
			<i><a href="#"><img src="/statics/plugin/mstyle/img/ico_1.png" alt=""></a></i>
			<p>
				当前位置：<a href="#">商户开店</a> > <a href="#">商户审核开店</a> >
			</p>
			<div class="push">
				<a href="<?php echo YYS_MODULE_PATH; ?>/index/Tdefault">欢迎界面</a>
				<a href="javascript:void(0)" onclick="location=location">刷新框架</a>
				<a href="<?php echo YYS_MODULE_PATH; ?>/cache/init">清理缓存</a>
			</div>
		</div><!-- path -->
<form action="#" method="GET">
		<div class="nuclear wid1">
			<div class="fl-box">
				<div class="grate-date">
					日期
					<input name="posttime1" type="text" id="posttime1" value="<?php if(isset($_REQUEST["posttime1"])) echo $_REQUEST["posttime1"]; else echo "2018-1-1"; ?>" class="date_picker">
					至
					<input name="posttime2" type="text" id="posttime2" value="<?php if(isset($_REQUEST["posttime2"])) echo $_REQUEST["posttime2"]; else echo "2018-1-1"; ?>"class="date_picker">
				</div>
				<button type="submit" name="sososubmit">筛选</button>
				<var>省市</var>
				<select name="province" id="province">
					<option value="0">省市名称</option>
					<?php
						foreach($provinceList as $key => $one){
							$select = '';
							if($one['code'] == $_REQUEST['province']){
								$select = "selected";
							}
							echo '<option '.$select.' value="'.$one['code'].'">'.$one['name'].'</option>';
						}
					?>
				</select>
				<var>城市</var>
				<select name="city" id="city">
					<option value="0">地级市</option>
				</select> 
				<var>区县</var>
				<select name="country" id="country">
					<option value="0">区或县级市</option>
				</select>
				<button type="submit" name="areasubmit">筛选</button>
				<select name="sotype">
<option value="name">商户名称</option>
<option value="id">商户编号</option>
<option value="mobile">手机号码</option>
				</select>
				<input type="text" name="sosotext" placeholder="输入关键词查询">
				<button type="submit" name="namesubmit">筛选</button>
			</div></form>
			<div class="fr-box">
				<span>
				</span>
			</div>
		</div><!-- readyshop -->

	<div class="nuclear-table wid1">
			<ul>
				<li class="head">
					<span>店铺ID</span>
					<span>店铺名称</span>
					<span>区县</span>
					<span>行业服务</span>

					<span>负责人</span>
					<span>手机号</span>
					<span>身份证号</span>
					<span>身份证照</span>
					<span>实体店景</span>
					<span>内部</span>
					<span>审核选项</span>
					<span>保存</span>
					<span>编辑</span>
					<span>删除</span>
					<?php
						if($cateid == 's2'){
					?>
					<span>开卡</span>
					<?php
						}
					?>
				</li>
<?php foreach($notice_list AS $v) { ?>
				<li>
					<span><?php echo $v['id'];?></span>
					<span><?php echo _strcut($v['name'],0,25);?></span>
					<span>					<?php 
						$address  = getCityName($v['country']);
						$address .= getCityName($v['street']);
						echo $address;
					?></span>
					<span><?php echo $this->categorys[$v['cateid']]['name']; ?></span>
					<span><?php echo $v['realname']; ?></span>
					<span><?php echo $v['mobile']; ?></span>
					<span><?php echo $v['idcard']; ?></span>
					<span>
						<i>
							<img src="/statics/uploads/<?php echo $v['idimg']; ?>" alt="">
						</i>
					</span>
					<span>
						<i>
							<img src="/statics/uploads/<?php echo $v['supplier_img']; ?>" alt="">
						</i>
					</span>
					<span><?php echo ($v['inside']=='1') ? '是' : '否'; ?></span>
					<span>
<select name="status" id="status_<?php echo $v['id'];?>">
<option value="0" <?php if($v['status'] == 0) echo "selected";?>>申请开店</option>
<option value="1" <?php if($v['status'] == 1) echo "selected";?>>审核开店</option>
<option value="2" <?php if($v['status'] == 2) echo "selected";?>>准备开店</option>
<option value="3" <?php if($v['status'] == 3) echo "selected";?>>正式开店</option>
<option value="4" <?php if($v['status'] == 4) echo "selected";?>>申请驳回</option>
<option value="5" <?php if($v['status'] == 5) echo "selected";?>>店铺停业</option>
<option value="6" <?php if($v['status'] == 6) echo "selected";?>>账户冻结</option>
</select>
					</span>
					<span><a class="ico-add" href="javascript:void(0);" onclick="doChangeStatus('<?php echo $v['id'];?>');"></a></span>
					<span><a class="ico-edit" href="<?php echo G_ADMIN_PATH; ?>/supplier/edit/<?php echo $v['id'];?>"></a></span>
					<span><a class="ico-del" href="<?php echo G_ADMIN_PATH.'/supplier/del/'.$v['id'];?>" onclick="return confirm('确认删除这个店铺吗？');"></a></span>
					<span>
						<?php
							if($v['status']==2){
								echo '<a class="ico-rotop" href="'.G_ADMIN_PATH.'/supplier/coupon/'.$v['id'].'"></a>';
							}else{
								echo " ";
							}
?></span>
				
				</li>
				<?php } ?>
			

			</ul>
		</div></form>
		<div class="pages">
			<span>
				<?php echo $fenye->show('new')['html']; ?>
			</span>
			<span>共 <?php echo ceil($zongji/$num); ?> 页</span>
		
			<span>到第<input type="text" id="page" value="">页</span>
			<span><a class="page-but" id="gopage" href="javascript:void(0);">确定</a></span>
		</div>
	</div><!-- container -->
</body>
<script src="/statics/plugin/mstyle/js/jquery.date_input.pack.js"></script>
<script src="/statics/plugin/mstyle/js/library-3.28.js"></script><!--自定义封装函数-->
<script src="/statics/plugin/mstyle/js/scrollanim.min.js"></script><!--动画效果库-->
<script>
	function doChangeStatus(id){
		var status = $("#status_"+id).val();
		if(confirm("确定要改变吗？")){
			$.ajax({
				type: "POST",
				url: "/admin/supplier/doChangeStatus",
				data: {status:status, id:id},
				dataType: "json",
				success: function(data){
					if(data.status == 1){
						
					}else{
						
					}
					alert(data.msg);
					return false;
				}
			});
		}
	}
	$("#gopage").click(function(){
		var page = $("#page").val();
		var allpage = "<?php echo ceil($zongji/$num); ?>";
		if(page == '' || allpage < page){
			var page = 1;
		}
		location.href = "<?php echo $fenye->show('new')['url']; ?>"+page;
	});


	
	/*地区联动*/
	$("select[name='province']").change(function(){
		var code = $(this).val();
		ajaxGetArea(code,'2');
	});
	
	$("select[name='city']").change(function(){
		var code = $(this).val();
		ajaxGetArea(code,'3');
	});
	
	$("select[name='country']").change(function(){
		var code = $(this).val();
		ajaxGetArea(code,'4');
	});
	
	$("select[name='street']").change(function(){
		var code = $(this).val();
		ajaxGetArea(code,'5');
	});

	function ajaxGetArea(code,level){
		$.ajax({
			type: "POST",
			url: "/mobile/ajax_user/ajax_add_data/",
			dataType:'json',
			data:{code:code,level:level},
			success: function(data){
				if(data.status == 1){
					var html = '<option value="0">请选择</option>';
					$.each(data.list,function (index,obj){
						html += '<option value="'+obj.code+'">'+obj.name+'</option>';
					});

					if(level == '2'){
						$("select[name='city']").html(html);
						$("select[name='country']").html(html);
						$("select[name='area']").html(html);
						$("select[name='street']").html(html);
					}else if(level == '3'){
						$("select[name='country']").html(html);
						$("select[name='area']").html(html);
						$("select[name='street']").html(html);
					}else if(level == '4'){
						$("select[name='area']").html(html);
						$("select[name='street']").html(html);
					}else if(level == '5'){
						$("select[name='area']").html(html);
					}
				}
			}
		}); 
	}
</script>
</html>
