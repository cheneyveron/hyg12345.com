document.write('<script language=javascript src="' + YYS_TEMPLATES_JS + '/mobile/jquery.touchSlider.js"></script>');
(function($) {
    $.extend({
        myTime: {
            CurTime: function() {
                return Date.parse(new Date()) / 1000
            },
            DateToUnix: function(string) {
                var f = string.split(' ', 2);
                var d = (f[0] ? f[0] : '').split('-', 3);
                var t = (f[1] ? f[1] : '').split(':', 3);
                return (new Date(parseInt(d[0], 10) || null, (parseInt(d[1], 10) || 1) - 1, parseInt(d[2], 10) || null, parseInt(t[0], 10) || null, parseInt(t[1], 10) || null, parseInt(t[2], 10) || null)).getTime() / 1000
            },
            UnixToDate: function(unixTime, isFull, timeZone) {
                if (typeof(timeZone) == 'number') {
                    unixTime = parseInt(unixTime) + parseInt(timeZone) * 60 * 60
                }
                var time = new Date(unixTime * 1000);
                var ymdhis = "";
                ymdhis += time.getUTCFullYear() + "-";
                ymdhis += (time.getUTCMonth() + 1) + "-";
                ymdhis += time.getUTCDate();
                if (isFull === true) {
                    ymdhis += " " + time.getUTCHours() + ":";
                    ymdhis += time.getUTCMinutes() + ":";
                    ymdhis += time.getUTCSeconds()
                }
                return ymdhis
            }
        }
    })
})(jQuery);
$(document).ready(function() {
    $(".dtdw").unbind('click').click(function() {
        var province = $("select[name='province']").find('option:selected').text();
        var city = $("select[name='city']").find('option:selected').text();
        var country = $("select[name='country']").find('option:selected').text();
        var street = $("select[name='street']").find('option:selected').text();
        var address = $("#address").val();
        if (!address || address == '') {
            showMsg('请先填写具体地址')
        } else {
            var myGeo = new BMap.Geocoder();
            myGeo.getPoint(province + city + country + street + address, function(point) {
                if (point) {
                    map.clearOverlays();
                    map.centerAndZoom(point, 16);
                    map.addOverlay(new BMap.Marker(point));
                    $("#lat").val(point.lat);
                    $("#lng").val(point.lng)
                } else {
                    showMsg('没有找到这个地址，请手动选择')
                }
            }, city)
        }
    });
    $(".tjsq").click(function() {
        var data = $("form[name='supplierForm']").serialize();
        $.ajax({
            type: "POST",
            url: LOCAL_PATH + "/mobile/ajax_supplier/ajax_supplier_data/",
            dataType: 'json',
            data: {
                datatype: 'supplierSubmitOne',
                data: data
            },
            success: function(data) {
                if (data.status == 1) {
                    showMsg(data.msg, '温馨提示', LOCAL_PATH + "/mobile/home/")
                } else {
                    showMsg(data.msg, '错误提示');
                    return false
                }
            }
        })
    });
    $("select[name='province']").change(function() {
        var code = $(this).val();
        ajaxGetArea(code, '2')
    });
    $("select[name='city']").change(function() {
        var code = $(this).val();
        ajaxGetArea(code, '3')
    });
    $("select[name='country']").change(function() {
        var code = $(this).val();
        ajaxGetArea(code, '4')
    });
    $("select[name='street']").change(function() {
        var code = $(this).val();
        ajaxGetArea(code, '5')
    });

    function ajaxGetArea(code, level) {
        $.ajax({
            type: "POST",
            url: "/mobile/ajax_user/ajax_add_data/",
            dataType: 'json',
            data: {
                code: code,
                level: level
            },
            success: function(data) {
                if (data.status == 1) {
                    var html = '<option value="0">请选择</option>';
                    $.each(data.list, function(index, obj) {
                        html += '<option value="' + obj.code + '">' + obj.name + '</option>'
                    });
                    if (level == '2') {
                        $("select[name='city']").html(html)
                    } else if (level == '3') {
                        $("select[name='country']").html(html)
                    } else if (level == '4') {
                        $("select[name='street']").html(html)
                    } else if (level == '5') {
                        $("select[name='area']").html(html)
                    }
                }
            }
        })
    }
    var idimglength = $("#idimg").length;
    if (idimglength >= 1) {
        $('#idimg').diyUpload({
            url: LOCAL_PATH + "/mobile/ajax_upload/upload/",
            success: function(data) {
                if (data.status == 1) {
                    var html = '<input type="hidden" value="' + data.src + '" name="idimg"/>';
                    $("#imgList").append(html);
                    $("#idimgDown").html('已上传');
                    showTips(data.msg);
                    return false
                } else {
                    showTips(data.msg);
                    return false
                }
            },
            error: function(err) {
                console.info(err)
            },
            fileNumLimit: 1,
        });
        $('#supplier_img').diyUpload({
            url: LOCAL_PATH + "/mobile/ajax_upload/upload/",
            success: function(data) {
                if (data.status == 1) {
                    var html = '<input type="hidden" value="' + data.src + '" name="supplier_img"/>';
                    $("#imgList").append(html);
                    $("#supplier_imgDown").html('已上传');
                    showTips(data.msg);
                    return false
                } else {
                    showTips(data.msg);
                    return false
                }
            },
            error: function(err) {
                console.info(err)
            },
            fileNumLimit: 1,
        })
    }
    $(".sinwlSubmitStepTwo").unbind('click').click(function() {
        var datatype = $(this).attr('datatype');
        var data = $("form[name='stepTwo']").serialize();
        $.ajax({
            type: "POST",
            url: LOCAL_PATH + "/mobile/ajax_supplier/ajax_supplier_data/",
            dataType: 'json',
            data: {
                datatype: 'supplierSubmitTwo',
                data: data
            },
            success: function(data) {
                if (data.status == 1) {
                    if (datatype == 'upload_posters') {
                        layer.open({
                            content: data.msg,
                            style: 'color:blue;font-size:24px;',
                            btn: '确定',
                            yes: function(index) {
                                location.href = LOCAL_PATH + "/mobile/supplier/submitStepTwo/";
                                layer.close(index)
                            }
                        })
                    } else if (datatype == 'submit') {
                        layer.open({
                            content: data.msg,
                            style: 'color:blue;font-size:24px;',
                            btn: ['确定'],
                            yes: function(index) {
                                location.href = LOCAL_PATH + "/mobile/supplier/submitStepTwo/"
                            },
                            no: function(index) {
                                location.href = LOCAL_PATH + "/mobile/supplier/submitStepTwo"
                            }
                        });
                        layer.close(index)
                    } else if (datatype == 'coupon') {
                        layer.open({
                            content: data.msg,
                            style: 'color:blue;font-size:24px;',
                            btn: '前往商品',
                            yes: function(index) {
                                location.href = LOCAL_PATH + "/mobile/supplier/submitStepTwo/";
                                layer.close(index)
                            }
                        })
                    }
                } else {
                    showMsg(data.msg);
                    return false
                }
            }
        })
    });
    var supplier_logolength = $("#supplier_logo").length;
    if (supplier_logolength >= 1) {
        $('#supplier_logo').diyUpload({
            url: LOCAL_PATH + "/mobile/ajax_upload/upload/",
            success: function(data) {
                if (data.status == 1) {
                    var html = '<input type="hidden" value="' + data.src + '" name="supplier_logo"/>';
                    $("#imgList").append(html);
                    layer.open({
                        content: data.msg,
                        skin: 'msg',
                        time: 2
                    });
                    return false
                } else {
                    layer.open({
                        content: data.msg,
                        skin: 'msg',
                        time: 2
                    });
                    return false
                }
            },
            error: function(err) {
                console.info(err)
            },
            fileNumLimit: 1,
        })
    }
    $(".selectCouponId").on('click', function() {
        var couponId = $(this).attr('dataId');
        $("input[name='couponId']").val(couponId)
    });
    $("#couponSubmit").click(function() {
        layer.open({
            type: 2,
            shade: 'background-color: rgba(0,0,0,.3)',
            content: '加载中...'
        });
        var couponId = $("input[name='couponId']").val();
        var data = $("form[name='kqForm']").serialize();
        if (couponId == '' || !couponId) {
            showTips('请选择商品样式')
        } else {
            $.ajax({
                type: "POST",
                url: LOCAL_PATH + "/mobile/ajax_supplier/ajax_supplier_data/",
                dataType: 'json',
                data: {
                    datatype: 'insertSupplierSelect',
                    couponId: couponId,
                    data: data
                },
                success: function(data) {
                    if (data.status == 1) {
                        layer.open({
                            content: data.msg,
                            style: 'color:blue;font-size:24px;',
                            btn: '会员中心',
                            yes: function(index) {
                                layer.closeAll();
                                location.href = LOCAL_PATH + "/mobile/home/"
                            }
                        })
                    } else {
                        showMsg(data.msg)
                    }
                    return false
                },
            })
        }
    });
    $(".supplier_manage").find('li').on('click', function() {
        $(this).addClass('cur2').siblings().removeClass('cur2');
        var datatype = $(this).attr("datatype");
        if (datatype == 'informationManage') {
            location.href = LOCAL_PATH + "/mobile/supplier/supplierEdit/";
            return false
        } else if (datatype == 'transactionRecord') {
            location.href = LOCAL_PATH + "/mobile/supplier/transactionRecord/";
            return false
        } else if (datatype == 'manage') {
            location.href = LOCAL_PATH + "/mobile/supplier/manage/";
            return false
        } else if (datatype == 'sendManage') {
            location.href = LOCAL_PATH + "/mobile/supplier/sendManage/";
            return false
        } else {
            supplier_manage(datatype)
        }
    });

    function supplier_manage(datatype) {
        layer.open({
            type: 2,
            shade: 'background-color: rgba(0,0,0,.3)',
            content: '加载中...'
        });
        $.ajax({
            type: "POST",
            url: "/mobile/ajax_supplier/ajax_supplier_data/",
            dataType: 'json',
            data: {
                datatype: datatype
            },
            success: function(data) {
                if (data.status == 1) {
                    var html = '<ul>';
                    $.each(data.list, function(index, obj) {
                        html += '<li>';
                        html += '	<div class="gdp" style="width:85%">';
                        html += '		<div class="g_bt">';
                        html += '			<img src="' + YYS_UPLOADS_PATH + '/' + obj.supplierLogo + '">';
                        html += '		</div>';
                        html += '		<div class="g_nr">';
                        html += '			<span>' + obj.supplierName + '</span><br>';
                        html += '			' + obj.goodsType + '<br>';
                        html += obj.address;
                        html += ' 		</div>';
                        html += ' 		<div class="g_jd">';
                        html += '			<img src="' + YYS_TEMPLATES_CSS + '/images/dj.jpg">';
                        html += '			<img src="' + YYS_TEMPLATES_CSS + '/images/dj.jpg">';
                        html += '			<img src="' + YYS_TEMPLATES_CSS + '/images/dj.jpg"><br>';
                        html += '			<span class="jd"><a href="#">进店</a></span>';
                        html += '  		</div>';
                        html += '	</div>';
                        if (datatype != 'failCollection') {
                            html += '	<div class="s_sc1"><img src="' + YYS_TEMPLATES_CSS + '/images/fx.png"></div>'
                        } else {
                            html += '	<div class="s_sc1"><img src="' + YYS_TEMPLATES_CSS + '/images/shc.png"></div>'
                        }
                        html += '	<div class="dp_tp">';
                        html += obj.notice;
                        html += '	</div>';
                        html += '</li>'
                    });
                    html += '</ul>'
                } else {
                    var html = '<ul>';
                    html += '<li>暂无推荐店铺</li>';
                    html += '</ul>'
                }
                $(".sgoods1").find('ul').html(html);
                layer.closeAll();
                return false
            }
        })
    }
    $(".modify_supplier").click(function() {
        layer.open({
            type: 2,
            shade: 'background-color: rgba(0,0,0,.3)',
            content: '加载中...'
        });
        var appointment_mobile = $("#appointment_mobile").val();
        var id = $("#id").val();
        $.ajax({
            type: "POST",
            url: "/mobile/ajax_supplier/ajax_supplier_data/",
            dataType: 'json',
            data: {
                id: id,
                appointment_mobile: appointment_mobile,
                datatype: 'modifySupplier'
            },
            success: function(data) {
                if (data.status == 1) {
                    showMsg(data.msg)
                } else {
                    showTips(data.msg);
                    layer.closeAll()
                }
                return false
            }
        })
    });
    sinwlSendManage('noAddress');
    $(".sinwlSendManage").find('li').on('click', function() {
        $(this).addClass('cur2').siblings().removeClass('cur2');
        var datatype = $(this).attr("datatype");
        sinwlSendManage(datatype)
    });

    function sinwlSendManage(datatype) {
        layer.open({
            type: 2,
            shade: 'background-color: rgba(0,0,0,.3)',
            content: '加载中...'
        });
        $.ajax({
            type: "POST",
            url: "/mobile/ajax_supplier/ajax_supplier_data/",
            dataType: 'json',
            data: {
                datatype: datatype
            },
            success: function(data) {
                if (data.status == 1) {
                    var html = '<ul>';
                    $.each(data.list, function(index, obj) {
                        html += '<li>';
                        html += '	<div class="k_top">';
                        html += '		<div class="k_left">';
                        html += '			<div class="k_bt">';
                        html += '				<a href="#">(第' + obj.shopqishu + '期)' + obj.shopname + '</a>';
                        html += '			</div>';
                        html += '			<div class="k_nr">';
                        html += '				揭晓时间：2016-12-19 21:34:55<br>';
                        html += '				幸运号码：10000023    购买次数：29人次<br>';
                        html += '				订单编号：23148464    用户ID号：565551<br>';
                        html += '				消费时间：2016-12-11 21:45:37<br>';
                        html += '				<b>状态说明：用户尚未完善收货地址</b>';
                        html += '			</div>';
                        html += '		</div>';
                        html += '		<div class="k_right">';
                        html += '			<img src="images/lq.png"><br>状态：已消费<br>';
                        html += '			<div class="ann1"><input type="bottom" value="提交发货"></div>';
                        html += '		</div>';
                        html += '		<div class="k_bottom">';
                        html += '			<div class="km2">';
                        html += '				物流公司：<input type="text" placeholder="选择物流" class="mika">';
                        html += '				快递单号：<input type="text" placeholder="LKSDJF465456" class="mika">';
                        html += '			</div>';
                        html += '		</div>';
                        html += '	</div>';
                        html += '</li>'
                    });
                    html += '</ul>'
                } else {
                    var html = '<ul>';
                    html += '<li>暂无记录</li>';
                    html += '</ul>'
                }
                $(".supplierManageShow").html(html);
                layer.closeAll();
                return false
            }
        })
    }
    sinwlCouponManage('couponCost');
    $(".sinwlCouponManage").find('li').on('click', function() {
        $(this).addClass('cur2').siblings().removeClass('cur2');
        var datatype = $(this).attr("datatype");
        sinwlCouponManage(datatype)
    });

    function sinwlCouponManage(datatype) {
        layer.open({
            type: 2,
            shade: 'background-color: rgba(0,0,0,.3)',
            content: '加载中...'
        });
        $.ajax({
            type: "POST",
            url: "/mobile/ajax_supplier/ajax_supplier_data/",
            dataType: 'json',
            data: {
                datatype: datatype
            },
            success: function(data) {
                if (data.status == 1) {
                    var html = '<ul>';
                    $.each(data.list, function(index, obj) {
                        html += '<li>';
                        html += '	<div class="k_top">';
                        html += '		<div class="k_left">';
                        html += '			<div class="k_bt">';
                        html += '				<a href="#">(第' + obj.shopqishu + '期)' + obj.shopname + '</a>';
                        html += '			</div>';
                        html += '			<div class="k_nr">';
                        html += '				揭晓时间：2016-12-19 21:34:55<br>';
                        html += '				幸运号码：10000023    购买次数：29人次<br>';
                        html += '				订单编号：23148464    用户ID号：565551<br>';
                        html += '				消费时间：2016-12-11 21:45:37<br>';
                        html += '				<b>状态说明：用户尚未完善收货地址</b>';
                        html += '			</div>';
                        html += '		</div>';
                        html += '		<div class="k_right">';
                        html += '			<img src="images/lq.png"><br>状态：已消费<br>';
                        html += '			<div class="ann1"><input type="bottom" value="提交发货"></div>';
                        html += '		</div>';
                        html += '		<div class="k_bottom">';
                        html += '			<div class="km2">';
                        html += '				物流公司：<input type="text" placeholder="选择物流" class="mika">';
                        html += '				快递单号：<input type="text" placeholder="LKSDJF465456" class="mika">';
                        html += '			</div>';
                        html += '		</div>';
                        html += '	</div>';
                        html += '</li>'
                    });
                    html += '</ul>'
                } else {
                    var html = '<ul>';
                    html += '<li>暂无记录</li>';
                    html += '</ul>'
                }
                $(".supplierManageShow").html(html);
                layer.closeAll();
                return false
            }
        })
    }
    $('.collectionBut').on('click', function() {
        var datatype = $(this).attr("datatype");
        var dataId = $(this).attr("dataId");
        var type = $(this).attr('type');
        $.ajax({
            type: "POST",
            url: "/mobile/ajax_collection/ajax_collection/",
            dataType: 'json',
            data: {
                dataId: dataId,
                type: type,
                datatype: datatype
            },
            success: function(data) {
                if (data.status == 1) {
                    showTips(data.msg)
                } else if (data.status == 2) {
                    showTips(data.msg);
                    location.href = LOCAL_PATH + "/mobile/user/login/"
                } else {
                    showTips(data.msg)
                }
                return false
            }
        })
    });
    collectFunction('recommand');
    $(".collectionSupplier").find('li').on('click', function() {
        $(this).addClass('cur2').siblings().removeClass('cur2');
        var datatype = $(this).attr("datatype");
        collectFunction(datatype)
    });

    function collectFunction(datatype) {
        layer.open({
            type: 2,
            shade: 'background-color: rgba(0,0,0,.3)',
            content: '加载中...'
        });
        $.ajax({
            type: "POST",
            url: "/mobile/ajax_collection/ajax_collection/",
            dataType: 'json',
            data: {
                datatype: datatype
            },
            success: function(data) {
                if (data.status == 1) {
                    var html = '<ul>';
                    $.each(data.list, function(index, obj) {
                        html += '<li>';
                        html += '	<div class="gdp" style="width:85%">';
                        html += '		<div class="g_bt">';
                        html += '			<img src="' + YYS_UPLOADS_PATH + '/' + obj.supplierLogo + '">';
                        html += '		</div>';
                        html += '		<div class="g_nr">';
                        html += '			<span>' + obj.supplierName + '</span><br>';
                        html += '			' + obj.goodsType + '<br>';
                        html += obj.address;
                        html += ' 		</div>';
                        html += ' 		<div class="g_jd">';
                        html += '			<img src="' + YYS_TEMPLATES_CSS + '/images/dj.jpg">';
                        html += '			<img src="' + YYS_TEMPLATES_CSS + '/images/dj.jpg">';
                        html += '			<img src="' + YYS_TEMPLATES_CSS + '/images/dj.jpg"><br>';
                        html += '			<span class="jd"><a href="#">进店</a></span>';
                        html += '  		</div>';
                        html += '	</div>';
                        if (datatype != 'failCollection') {
                            html += '	<div class="s_sc1"><img src="' + YYS_TEMPLATES_CSS + '/images/fx.png"></div>'
                        } else {
                            html += '	<div class="s_sc1"><img src="' + YYS_TEMPLATES_CSS + '/images/shc.png"></div>'
                        }
                        html += '	<div class="dp_tp">';
                        html += obj.notice;
                        html += '	</div>';
                        html += '</li>'
                    });
                    html += '</ul>'
                } else {
                    var html = '<ul>';
                    html += '<li>暂无推荐店铺</li>';
                    html += '</ul>'
                }
                $(".sgoods1").find('ul').html(html);
                layer.closeAll();
                return false
            }
        })
    }
    $(".main_visual").hover(function() {
        $("#btn_prev,#btn_next").fadeIn()
    }, function() {
        $("#btn_prev,#btn_next").fadeOut()
    });
    d = false;
    $(".main_image").touchSlider({
        flexible: true,
        speed: 200,
        btn_prev: $("#btn_prev"),
        btn_next: $("#btn_next"),
        paging: $(".flicking_con a"),
        counter: function(e) {
            $(".flicking_con a").removeClass("on").eq(e.current - 1).addClass("on")
        }
    });
    $(".main_image").bind("mousedown", function() {
        d = false
    });
    $(".main_image").bind("dragstart", function() {
        d = true
    });
    $(".main_image a").click(function() {
        if (d) {
            return false
        }
    });
    timer = setInterval(function() {
        $("#btn_next").click()
    }, 5000);
    $(".main_visual").hover(function() {
        clearInterval(timer)
    }, function() {
        timer = setInterval(function() {
            $("#btn_next").click()
        }, 5000)
    });
    $(".main_image").bind("touchstart", function() {
        clearInterval(timer)
    }).bind("touchend", function() {
        timer = setInterval(function() {
            $("#btn_next").click()
        }, 5000)
    })
});

function loadTransactionRecords() {
    layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '加载中...'
    });
    $.ajax({
        type: "POST",
        url: "/mobile/ajax_supplier/ajax_supplier_data/",
        dataType: 'json',
        data: {
            datatype: 'transactionRecord'
        },
        success: function(data) {
            if (data.status == 1) {
                var html = '<ul>';
                $.each(data.list, function(index, obj) {
                    html += '<li>';
                    html += '	<div class="jl_tp">';
                    html += '		<img src="images/lq.png">';
                    html += '	</div>';
                    html += '	<div class="jl_nr">';
                    html += '		<div class="jl_bt">(第16期)100元购物商品...</div>';
                    html += '		2016-12-25  21:45:25';
                    html += '	</div>';
                    html += '	<div class="jg">+100</div>';
                    html += '</li>'
                });
                html += '</ul>'
            } else {
                var html = '<ul>';
                html += '<li>暂无交易记录</li>';
                html += '</ul>'
            }
            $(".jy_jl").html(html);
            layer.closeAll();
            return false
        }
    })
}

function ajaxGetSupplierList() {
    layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '加载中...'
    });
    $.ajax({
        type: "POST",
        url: "/mobile/ajax_supplier/ajax_supplier_data/",
        dataType: 'json',
        data: {
            datatype: 'transactionRecord'
        },
        success: function(data) {
            if (data.status == 1) {
                var html = '<ul>';
                $.each(data.list, function(index, obj) {
                    html += '<li>';
                    html += '	<div class="jl_tp">';
                    html += '		<img src="images/lq.png">';
                    html += '	</div>';
                    html += '	<div class="jl_nr">';
                    html += '		<div class="jl_bt">(第16期)100元购物商品...</div>';
                    html += '		2016-12-25  21:45:25';
                    html += '	</div>';
                    html += '	<div class="jg">+100</div>';
                    html += '</li>'
                });
                html += '</ul>'
            } else {
                var html = '<ul>';
                html += '<li>暂无交易记录</li>';
                html += '</ul>'
            }
            $(".jy_jl").html(html);
            layer.closeAll();
            return false
        }
    })
}

function setSupplierArea(id, isCanSearch) {
    $.ajax({
        type: "POST",
        url: "/mobile/ajax_supplier/setSupplierArea/",
        dataType: 'json',
        data: {
            id: id,
            isCanSearch: isCanSearch
        },
        success: function(data) {
            showTips(data.msg)
        },
        error: function() {}
    })
}

function getSupplierComment(supplierId) {
    layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '加载中...'
    });
    $.ajax({
        type: "POST",
        url: "/mobile/ajax_supplier/ajax_supplier_data/",
        dataType: 'json',
        data: {
            datatype: 'supplierComment',
			supplierId: supplierId
		},
        success: function(data) {
            if (data.status == 1) {
                var html = '';
                $.each(data.list, function(index, obj) {
                    html += '<li>';
                    html += '    <h4>' + obj.content + '</h4>';
                    html += '   <p>';
                    html += '       <i class="ico-comm"></i>';
                    html += '         <span class="fl">点赞<var id="zan_'+obj.id+'">' + obj.zan + '</var>次</span>';
                    html += '         <span class="fr">' + $.myTime.UnixToDate(obj.time, true);
					html += ' 			<i class="ico-zan" onclick="doZan(\''+obj.id+'\');"></i></span>';
                    html += '    </p>';
                    html += ' </li>'
                });
            } else {
                var html = '';
                html += '<li style="text-align:center;width:100%;">暂时没有评论</li>';
            }
            $("#comment ul").html(html);
            layer.closeAll();
            return false
        }
    })
}

function getSupplierService(supplierId) {
    layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '加载中...'
    });
    $.ajax({
        type: "POST",
        url: "/mobile/ajax_supplier/ajax_supplier_data/",
        dataType: 'json',
        data: {
            datatype: 'supplierService',
            id: supplierId
        },
        success: function(data) {
            if (data.status == 1) {
                var html = '<div class="service dt"><ul>';
                html += '	<li>';
                html += '		<span>店名</span>';
                html += '		<div class="fl">';
                html += '   	 	<p>' + data.supplier.name + '</p>';
                html += ' 		</div>';
                html += '  	</li>';
                html += '	<li>';
                html += '		<span>地址</span>';
                html += '		<div class="fl">';
                html += '			<p>' + data.supplier.address + '</p>';
                html += '		</div>';
                html += '	</li>';
                html += '	<li>';
                html += '		<span>服务</span>';
                html += '		<div class="fl">';
                html += '  		 	<p>' + data.supplier.service + '</p>';
                html += '		</div>';
                html += '	</li>';
                html += '	<li>';
                html += '		<span>主营</span>';
                html += '		<div class="fl">';
                html += '			<p>' + data.supplier.goods_type + '</p>';
                html += '		</div>';
                html += '	</li>';
                html += '	<li>';
                html += '		<span>行业</span>';
                html += '		<div class="fl">';
                html += '			<p>' + data.supplier.supplier_type + '</p>';
                html += '		</div>';
                html += '	</li>';
                html += '	<li>';
                html += '		<span>店号</span>';
                html += '		<div class="fl">';
                html += '			<p>' + data.supplier.id + '</p>';
                html += '		</div>';
                html += '	</li>';
                html += '</ul></div>'
            } else {
                var html = '<div class="service dt"><ul>';
                html += '<li style="text-align:center;width:100%;">' + data.msg + '</li>';
                html += '</ul></div>'
            }
            $(".two-more").hide();
            $("#comment ul").html(html);
            layer.closeAll();
            return false
        }
    })
}

function getSupplierCoupon(supplierId, dataId, isfreepost) {
    layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '加载中...'
    });
    $.ajax({
        type: "POST",
        url: "/mobile/ajax_supplier/ajax_supplier_data/",
        dataType: 'json',
        data: {
            datatype: 'getSupplierCoupon',
            id: supplierId,
            dataId: dataId,
            isfreepost: isfreepost
        },
        success: function(data) {
            if (data.status == 1) {
                var html = '<div class="coupons1 dt"><ul>';
                $.each(data.list, function(index, obj) {
                    html += '<li>';
                    html += '	<a href="' + LOCAL_PATH + '/mobile/mobile/goods/' + obj.id + '">';
                    html += '		<div class="pict">';
                    html += '			<img src="' + YYS_UPLOADS_PATH + '/' + obj.thumb + '" alt="">';
                    html += '		</div>';
                    html += '		<h3>' + obj.title + '</h3>';
                    html += '	</a>';
                    html += '	<p>售价：￥' + obj.yunjiage;
                    html += ' 		<span class="fr">';
                    if (obj.isfreepost == 1) {
                        html += '包邮'
                    } else {
                        html += '自提'
                    }
                    html += '		</span>';
                    html += '	</p>';
                    html += '	<p>惠券：￥' + obj.supplierPercent + '</p>';
                    html += '	<p>付款：' + obj.qishu + '人</p>';
                    html += '	<a class="join" href="' + LOCAL_PATH + '/mobile/mobile/goods/' + obj.id + '"><img src="' + YYS_TEMPLATES_CSS + '/images/img43.png" alt=""></a>';
                    html += '</li>'
                });
                html += '</ul></div>'
            } else {
                var html = '<ul>';
                html += '<li id="nodata">暂无商品</li>';
                html += '</ul>'
            }
            $(".two-more").show();
            $("#supplierIntroduction").hide();
            $("#comment ul").html(html);
            layer.closeAll();
            return false
        }
    })
}

function getSupplierCouponDetail(supplierId) {
    layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '加载中...'
    });
    $.ajax({
        type: "POST",
        url: "/mobile/ajax_supplier/ajax_supplier_data/",
        dataType: 'json',
        data: {
            datatype: 'getSupplierCouponDetail',
            id: supplierId
        },
        success: function(data) {
            if (data.status == 1) {
                var html = '<div class="cash-coupon dt">';
                html += '	<ul>';
                html += '		<li>';
                html += '			<div class="pict"><a title="'+data.list.title+'" href="' + LOCAL_PATH + '/mobile/mobile/item/'+data.list.id+'">';
                html += '				<img src="' + YYS_UPLOADS_PATH +'/'+data.list.thumb+'" alt="">';
                html += '			</a></div>';
                html += '			<div class="text">';
                html += '				<p>'+data.list.canyurenshu+'<var>'+data.list.zongrenshu+'</var></p>';
                html += '				<span><i style="width:'+data.list.percent+'%"></i></span>';
                html += '			</div>';
                html += '		</li>';
                html += '	</ul>';
                html += '	<div class="boxdow"><img src="' + YYS_TEMPLATES_CSS + '/images/pic_19.png" alt=""></div>';
                html += '</div>';
                html += '<div class="card-coupons">';
                html += '<p>1、幸运者获得购物卡券，只能在本店消费购物，全网不通用。</p>';
                html += '<p>2、下次购买商品时选择抵消你所获得的【惠券购物券】，商品金额必须大于卡券面值。</p>';
                html += '<p>3、卡券没有期限，消费时可以选择网购抵消或实体店抵消两种方式';
                html += '。实体店抵消方式请确认是否是本店卡券，消费后自动抵消。</p>';
                html += '<p>4、可以对本店的服务和商品评价，好评加1分，中评不加分，差评减1分。您的评价对本店很重要，会影响本店的口碑和搜索排序。</p>';
                html += '<p>5、谢谢你的好评，鼓励我们更好的提供服务和产品质量。</p>';
                html += '</div>'
            } else {
                var html = '<ul>';
                html += '<li id="nodata">暂无购物惠券</li>';
                html += '</ul>'
            }
            $(".two-more").hide();
            $("#supplierIntroduction").hide();
            $("#comment ul").html(html);
            layer.closeAll();
            return false
        }
    })
}

function getCouponStyle(id) {
    layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '加载中...'
    });
    $.ajax({
        type: "POST",
        url: "/mobile/ajax_supplier/ajax_supplier_data/",
        dataType: 'json',
        data: {
            datatype: 'getCouponStyle',
            id: id
        },
        success: function(data) {
            if (data.status == 1) {
                var html = '<div class="coupons dt"><ul>';
                $.each(data.list, function(index, obj) {
                    html += '<li onclick="setCouponCode(\'' + obj.title + '\');">';
                    html += '	<div class="pict">';
                    html += '    	<img class="lazy" src="' + YYS_TEMPLATES_CSS + '/images/loading.gif" data-original="' + YYS_UPLOADS_PATH + '/' + obj.thumb + '"/>';
                    html += '	</div>';
                    html += '	<p>编号：<i>' + obj.title + '</i></p>';
                    html += '</li>'
                });
                html += '</ul></div>'
            } else {
                var html = '<ul>';
                html += '<li>暂无商品</li>';
                html += '</ul>'
            }
            $(".par-number").html(html);
            $("img.lazy").lazyload({
                threshold: 200,
                effect: "fadeIn"
            });
            layer.closeAll();
            return false
        }
    })
}

function getSupplierPhoto(id) {
    layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '加载中...'
    });
    $.ajax({
        type: "POST",
        url: "/mobile/ajax_supplier/ajax_supplier_data/",
        dataType: 'json',
        data: {
            datatype: 'getSupplierPhoto',
            id: id
        },
        success: function(data) {
            if (data.status == 1) {
                var html = '<div class="coupons dt"><ul>';
                $.each(data.list, function(index, obj) {
                    html += '<li onclick="setCouponCode(' + obj.title + ');">';
                    html += '	<div class="pict">';
                    html += '    	<img class="lazy" src="' + YYS_TEMPLATES_CSS + '/images/loading.gif" data-original="' + YYS_UPLOADS_PATH + '/' + obj.thumb + '"/>';
                    html += '	</div>';
                    html += '	<p>编号：<i>' + obj.title + '</i></p>';
                    html += '</li>'
                });
                html += '</ul></div>'
            } else {
                var html = '<ul>';
                html += '<li>暂无商品</li>';
                html += '</ul>'
            }
            $(".par-number").html(html);
            $("img.lazy").lazyload({
                threshold: 200,
                effect: "fadeIn"
            });
            layer.closeAll();
            return false
        }
    })
}

function setCouponCode(title) {
    $("#couponCode").val(title);
	return false;
}

function insertSupplierSelect() {
    layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '加载中...'
    });
    var money = $("#money").val();
    var couponCode = $("#couponCode").val();
    if (!money || money == '') {
        showMsg('请按顺序输入金额', '错误提示')
    } else if (couponCode == '' || !couponCode) {
        showMsg('请选择商品编号', '错误提示')
    } else {
        var data = $("#form1").serialize();
        $.ajax({
            type: "POST",
            url: "/mobile/ajax_supplier/insertSupplierSelect/",
            dataType: 'json',
            data: {
                money: money,
                couponCode: couponCode
            },
            success: function(data) {
                if (data.status == 1) {
                    showMsg(data.msg, '温馨提示', data.url)
                } else {
                    showMsg(data.msg, '错误提示')
                }
                return false
            }
        })
    }
}

function getNoSpendList(obj) {
    $(".consumption").find('li').removeClass('acti');
    obj.addClass('acti');
    layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '加载中...'
    });
    $.ajax({
        type: "POST",
        url: "/mobile/ajax_supplier/getNoSpendList/",
        dataType: 'json',
        success: function(data) {
            if (data.status == 1) {
                var html = '<ul>';
                $.each(data.list, function(index, obj) {
                    html += '<li>';
                    html += '	<div class="pict">';
                    html += '		<a href="' + LOCAL_PATH + '/mobile/supplier/index/' + obj.sid + '">';
                    html += '			<img src="' + YYS_UPLOADS_PATH + '/' + obj.thumb + '" alt=""/>';
                    html += '			<p>'+obj.status+'</p>';
                    html += '		</a>';
                    html += '	</div>';
                    html += '	<div class="text">';
                    html += '		<div class="text-line">';
                    html += '			<p>卡券面值：' + obj.money + '</p>';
                    html += '			<p>发券时间：' + $.myTime.UnixToDate(obj.create_time, true) + '</p>';
                    html += '			<p>持有用户：' + obj.username + '</p>';
                    html += '			<p>使用时间：' + obj.use_time + '</p>';
                    html += '		</div>';
                    html += '		<div class="pict0">';
                    html += '			<img src="' + YYS_UPLOADS_PATH + '/' + obj.avatar + '" alt=""/>';
                    html += '		</div>';
                    html += '	</div>';
                    html += '</li>'
                });
                html += '</ul>'
            } else {
                var html = '<ul>';
                html += '<li style="text-align:center;">暂无未抵消的商品</li>';
                html += '</ul>'
            }
            $(".offset").find('ul').html(html);
            layer.closeAll();
            return false
        }
    })
}

function getSpendList(obj) {
    $(".consumption").find('li').removeClass('acti');
    obj.addClass('acti');
    layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '加载中...'
    });
    $.ajax({
        type: "POST",
        url: "/mobile/ajax_supplier/getSpendList/",
        dataType: 'json',
        success: function(data) {
            if (data.status == 1) {
                var html = '<ul>';
                $.each(data.list, function(index, obj) {
                    html += '<li>';
                    html += '	<div class="pict">';
                    html += '		<a href="' + LOCAL_PATH + '/mobile/mobile/item/' + obj.goods_id + '">';
                    html += '			<img src="' + YYS_UPLOADS_PATH + '/' + obj.thumb + '" alt=""/>';
                    html += '			<p>已消费</p>';
                    html += ' 		</a>';
                    html += '	</div>';
                    html += '	<div class="text">';
					html += '		<a href="' + LOCAL_PATH + '/mobile/mobile/item/' + obj.goods_id + '">';
                    html += ' 			<b>(' + obj.qishu + '期)' + obj.title + '…</b>';
                    html += ' 			<div class="text-line">';
                    html += '   			<p>揭晓时间：' + obj.q_end_time + '</p>';
                    html += '   			<p>幸运号码：' + obj.q_user_code + '</p>';
                    html += '   			<p>订单号：' + obj.order_sn + '</p>';
                    html += '    			<p>抵消时间：' + obj.use_time + '</p>';
                    html += '  			</div>';
                    html += ' 			<div class="pict0">';
                    html += '     			<img src="' + YYS_UPLOADS_PATH + '/' + obj.avatar + '" alt="">';
                    html += ' 			</div>';
					html += '		</a>';
                    html += ' 	</div>';
                    html += ' </li>'
                });
                html += '</ul>'
            } else {
                var html = '<ul>';
                html += '<li style="text-align:center;">暂无已抵消的商品</li>';
                html += '</ul>'
            }
            $(".offset").find('ul').html(html);
            layer.closeAll();
            return false
        }
    })
}

function getSupplierSpendList(obj) {
    $(".consumption").find('li').removeClass('acti');
    obj.addClass('acti');
    layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '加载中...'
    });
    $.ajax({
        type: "POST",
        url: "/mobile/ajax_supplier/getSupplierSpendList/",
        dataType: 'json',
        success: function(data) {
            if (data.status == 1) {
                var html = '<ul>';
                $.each(data.list, function(index, obj) {
                    html += '<li>';
                    html += '	<div class="pict">';
                    html += '		<a href="' + LOCAL_PATH + '/mobile/mobile/item/' + obj.shopid + '">';
					html += '			<img src="' + YYS_UPLOADS_PATH + '/' + obj.thumb + '" alt=""/>';
                    html += '			<p>'+obj.status+'</p>';
                    html += ' 		</a>';
                    html += '	</div>';
                    html += '	<div class="text">';
					html += '		<a href="' + LOCAL_PATH + '/mobile/mobile/item/' + obj.shopid + '">';
                    html += ' 			<b>(' + obj.qishu + '期)' + obj.title + '…</b>';
                    html += ' 			<div class="text-line">';
                    html += '   			<p>发放时间：' + obj.create_time + '</p>';
//                    html += '   			<p>幸运号码：' + obj.q_user_code + '</p>';
                    html += '   			<p>持有用户：' + obj.username + '</p>';
                    html += '    			<p>使用时间：' + obj.use_time + '</p>';
                    html += '  			</div>';
                    html += ' 			<div class="pict0">';
                    html += '     			<img src="' + YYS_UPLOADS_PATH + '/' + obj.avatar + '" alt="">';
                    html += ' 			</div>';
					html += '		</a>';
                    html += ' 	</div>';
                    html += ' </li>'
                });
                html += '</ul>'
            } else {
                var html = '<ul>';
                html += '<li style="text-align:center;">暂无已抵消的商品</li>';
                html += '</ul>'
            }
            $(".offset").find('ul').html(html);
            layer.closeAll();
            return false
        }
    })
}

function costCoupon(code) {
    var secret = $("#secret_" + code).val();
    if (secret == '' || !secret) {
        showMsg('请输入商品密码')
    } else {
        layer.open({
            content: '您确定核销此商品吗？',
            btn: ['是的', '不要'],
            yes: function(index) {
                layer.open({
                    type: 2,
                    shade: 'background-color: rgba(0,0,0,.3)',
                    content: '加载中...'
                });
                $.ajax({
                    type: "POST",
                    url: "/mobile/ajax_coupon/costCoupon/",
                    data: {
                        code: code,
                        secret: secret
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == 1) {
                            showMsg(data.msg, LOCAL_PATH + '/mobile/supplier/manage')
                        } else {
                            showMsg(data.msg)
                        }
                        return false
                    }
                })
            }
        })
    }
}

function doSupplierNear() {
    layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '定位中...'
    });
    var map = new BMap.Map("allmap");
    var geolocation = new BMap.Geolocation();
    var geoc = new BMap.Geocoder();
    geolocation.getCurrentPosition(function(r) {
        if (this.getStatus() == BMAP_STATUS_SUCCESS) {
            var pt = r.point;
            geoc.getLocation(pt, function(rs) {
                var addComp = rs.addressComponents;
                $(".position var").html(addComp.city + addComp.district);
                layer.open({
                    type: 2,
                    shade: 'background-color: rgba(0,0,0,.3)',
                    content: '加载中...'
                });
                $.ajax({
                    type: "POST",
                    url: "/mobile/ajax_supplier/getNearSupplier/",
                    dataType: 'json',
                    data: {
                        lng: r.point.lng,
                        lat: r.point.lat
                    },
                    success: function(data) {
                        if (data.status == 1) {
                            var html = '<ul>';
                            $.each(data.list, function(index, obj) {
                                html += '<li>';
                                html += '	<div class="pict">';
                                html += '		<a href="' + LOCAL_PATH + '/mobile/supplier/index/' + obj.id + '"><img src="' + YYS_UPLOADS_PATH + '/' + obj.supplier_logo + '" alt=""></a>';
                                html += '	</div>';
                                html += '	<div class="text">';
                                html += '		<h2>' + obj.name + '</h2>';
                                html += '		<p>' + obj.service + '</p>';
                                html += '		<p>' + obj.address + '</p>';
                                html += '		<span class="fl">';
                                html += '			<i></i>';
                                html += '			<i></i>';
                                html += '			<i></i>';
                                html += '			<i></i>';
                                html += '		</span>';
                                html += '		<span class="fr">';
                                html += '			<a href="' + LOCAL_PATH + '/mobile/supplier/index/' + obj.id + '"><img src="' + YYS_TEMPLATES_CSS + '/images/img24.png" alt=""/></a>';
                                html += '			<a href="' + LOCAL_PATH + '/mobile/supplier/map/' + obj.id + '"><img src="' + YYS_TEMPLATES_CSS + '/images/img25.png" alt=""/></a>';
                                html += '			<a href="javascript:void(0);" onclick="doCollection(' + obj.id + ',\'supplier\');"><img src="' + YYS_TEMPLATES_CSS + '/images/img26.png" alt=""/></a>';
                                html += '		</span>';
                                html += '	</div>';
                                html += '</li>'
                            });
                            html += '</ul>'
                        } else {
                            var html = '<ul>';
                            html += '<li style="text-align:center;">暂无附近的商户</li>';
                            html += '</ul>'
                        }
                        $(".shop-list").find('ul').html(html);
                        layer.closeAll();
                        return false
                    }
                })
            })
        } else {
            alert('failed' + this.getStatus())
        }
    }, {
        enableHighAccuracy: true
    })
}

function patfigure(supplier_id) {
    var notice = $("#notice").val();
    if (!notice || notice == '') {
        layer.open({
            content: '请填写一句话',
            skin: 'msg',
            time: 2
        });
        return false
    }
    var index = layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '玩命提交中...'
    });
    var supplier_pat = $("#supplier_pat").val();
            layer.open({
                type: 2,
                shade: 'background-color: rgba(0,0,0,.3)',
                content: '加载中...'
            });
            $.ajax({
                type: "POST",
                url: "/mobile/ajax_upload/supplier_pat/",
                data: {
                    supplier_id: supplier_id,
                    notice: notice,
                    supplier_pat: supplier_pat
                },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        layer.open({
                            content: data.msg,
                            skin: 'msg',
                            time: 2,
                            end: function(data) {
                                window.location.reload();
                                return false
                            }
                        });
                        return false
                    } else {
                        layer.open({
                            content: data.msg,
                            skin: 'msg',
                            time: 2
                        });
                        layer.close(index);
                        return false
                    }
                },
                error: function(data, status, e) {
                    layer.open({
                        content: '上传出错',
                        skin: 'msg',
                        time: 2
                    });
                    return false
                }
            })
}

function getSupplierPat(id) {
    layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '加载中...'
    });
    $.ajax({
        type: "POST",
        url: "/mobile/ajax_supplier/ajax_supplier_data/getSupplierPat/",
        dataType: 'json',
        data: {
            datatype: 'getSupplierPat',id:id
        },
        success: function(data) {
            if (data.status == 1) {
                var html = '';
                $.each(data.list, function(index, obj) {
                    html += '<li id="supplier_pat_' + obj.id + '">';
                    html += '	<div class="pict">';
                    html += '		<img src="' + YYS_UPLOADS_PATH + '/' + obj.img + '" alt=""/>';
                    html += '	</div>';
                    html += '	<div class="text">';
                    html += '		<p>' + obj.notice + '</p>';
                    html += '		<span>' + $.myTime.UnixToDate(obj.time, true) + ' <var>';
                    html += '		<a href="javascript:void(0);" class="ico-top" onclick="sendToUp(' + obj.id + ')"></a>';
                    html += '		<a href="javascript:void(0);" class="ico-del" onclick="delSupplierPat(' + obj.id + ')"></a></var></span>';
                    html += '	</div>';
                    html += '</li>'
                })
            } else {
                var html = '';
                html += '<li style="text-align:center;">暂无拍图</li>'
            }
            $(".list-manage").find('ul').html(html);
            $("img.lazy").lazyload({
                threshold: 200,
                effect: "fadeIn"
            });
            layer.closeAll();
            return false
        }
    })
}

function sendToUp(id) {
    layer.open({
        content: '您确定要将此图置顶吗？',
        title: '操作提示',
        btn: ['是的', '不要'],
        yes: function(index) {
            layer.open({
                type: 2,
                shade: 'background-color: rgba(0,0,0,.3)',
                content: '加载中...'
            });
            $.ajax({
                type: "POST",
                url: "/mobile/ajax_supplier/sendSupplierPatToUp/",
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        getSupplierPat()
                    } else {
                        showMsg(data.msg)
                    }
                    layer.closeAll();
                    location.reload();
                }
            })
        }
    })
}

function delSupplierPat(id) {
    layer.open({
        content: '您确定删除此此商品吗？',
        title: '操作提示',
        btn: ['是的', '不要'],
        yes: function(index) {
            layer.open({
                type: 2,
                shade: 'background-color: rgba(0,0,0,.3)',
                content: '加载中...'
            });
            $.ajax({
                type: "POST",
                url: "/mobile/ajax_supplier/delSupplierPat/",
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        $("#supplier_pat_" + id).remove()
                    } else {
                        showMsg(data.msg)
                    }
                    layer.closeAll();
                    return false
                }
            })
        }
    })
}

function getGoodsShelves(isShelve, dataId, isfreepost) {
    layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '加载中...'
    });
    $.ajax({
        type: "POST",
        url: LOCAL_PATH + "/mobile/ajax_supplier/ajax_supplier_data/getGoodsShelves/",
        dataType: 'json',
        data: {
            datatype: 'getGoodsShelves',
            isShelve: isShelve,
            dataId: dataId,
            isfreepost: isfreepost
        },
        success: function(data) {
            if (data.status == 1) {
                var html = '';
                $.each(data.list, function(index, obj) {
                    html += '<li id="shangpin_' + obj.id + '">';
                    html += '	<a href="' + LOCAL_PATH + '/mobile/mobile/goods/' + obj.id + '">';
                    html += '		<div class="pict-fl-box">';
                    html += ' 		<div class="pict">';
                    html += '   		<img src="' + YYS_UPLOADS_PATH + '/' + obj.thumb + '" alt=""/>';
                    html += '		</div>';
                    html += '       <var>销量：' + obj.qishu + '</var>';
                    html += '	</div>';
                    html += '	</a>';
                    html += '		<div class="text">';
                    html += '	<a href="' + LOCAL_PATH + '/mobile/mobile/goods/' + obj.id + '">';
                    html += '    		<b><var>';
                    if (obj.isfreepost == 1) {
                        html += '包邮'
                    } else {
                        html += '自提'
                    }
                    html += '	</var>' + obj.title + '</b>';
                    html += '	</a>';
                    html += '   		<strong>';
                    html += '      	 		<span class="color">￥<i class="pice">' + obj.yunjiage + '</i></span>';
                    html += '      			<span class="sp0">优惠券：￥<i class="fan">' + obj.supplierPercent + '</i></span>';
                    html += '       		<span class="sp1">库存：' + obj.maxqishu + '</span>';
                    html += '    		</strong>';
                    html += '    		<strong>';
                    html += '        		<span class="sp1">';
                    html += '					<a href="' + LOCAL_PATH + '/mobile/supplier/goods_edit/' + obj.id + '">编辑</a>';
                    html += '					<a href="' + LOCAL_PATH + '/mobile/supplier/goods_color/' + obj.id + '">颜色</a>';
                    html += '					<a href="' + LOCAL_PATH + '/mobile/supplier/goods_newTypes/' + obj.id + '">规格</a>';
                    if (isShelve == 1) {
                        html += '					<a href="javascript:void(0);" onclick="doOffShelve(' + obj.id + ');">下架</a>'
                    } else {
                        html += '					<a href="javascript:void(0);" onclick="doOnShelve(' + obj.id + ');">上架</a>'
                    }
                    html += '					<a href="javascript:void(0);" onclick="delSupplierGoods(' + obj.id + ');" class="gai">删除</a>';
                    html += '       		</span>';
                    html += '   		 </strong>';
                    html += '   		<div class="box">';
                    html += '       		<span>售价</span>';
                    html += '       		<input class="pice" type="text" value="14780">';
                    html += '      	 		<span>优惠券</span>';
                    html += '       		<input class="fan" type="text" value="1000">';
                    html += '        		<a href="#">确<br>定</a>';
                    html += '   		</div>';
                    html += '  		</div>';
                    html += '</li>'
                })
            } else {
                var html = '';
                html += '<li style="text-align:center;">暂无商品</li>'
            }
            $(".shelves").find('ul').html(html);
            layer.closeAll();
            return false
        }
    })
}

function delSupplierGoods(id) {
    layer.open({
        content: '确定要删除此商品吗？',
        title: '操作提示',
        btn: ['是的', '不要'],
        yes: function(index) {
            layer.open({
                type: 2,
                shade: 'background-color: rgba(0,0,0,.3)',
                content: '加载中...'
            });
            $.ajax({
                type: "POST",
                url: "/mobile/ajax_supplier/delSupplierGoods/",
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        $("#shangpin_" + id).remove()
                    } else {
                        showMsg(data.msg)
                    }
                    layer.closeAll();
                    return false
                }
            })
        }
    })
}

function doOffShelve(id) {
    layer.open({
        content: '确定要下架此商品吗？',
        title: '操作提示',
        btn: ['是的', '不要'],
        yes: function(index) {
            layer.open({
                type: 2,
                shade: 'background-color: rgba(0,0,0,.3)',
                content: '加载中...'
            });
            $.ajax({
                type: "POST",
                url: "/mobile/ajax_supplier/doOffShelve/",
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        $("#shangpin_" + id).remove()
                    } else {
                        showMsg(data.msg)
                    }
                    layer.closeAll();
                    return false
                }
            })
        }
    })
}

function doOnShelve(id) {
    layer.open({
        content: '确定要上架此商品吗？',
        title: '操作提示',
        btn: ['是的', '不要'],
        yes: function(index) {
            layer.open({
                type: 2,
                shade: 'background-color: rgba(0,0,0,.3)',
                content: '加载中...'
            });
            $.ajax({
                type: "POST",
                url: "/mobile/ajax_supplier/doOnShelve/",
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        $("#shangpin_" + id).remove()
                    } else {
                        showMsg(data.msg)
                    }
                    layer.closeAll();
                    return false
                }
            })
        }
    })
}

function doSaveLogo(id) {
    var supplier_logo = $("#supplier_logo_img").val();
    var supplier_poster = $("#supplier_poster_img").val();
            layer.open({
                type: 2,
                shade: 'background-color: rgba(0,0,0,.3)',
                content: '加载中...'
            });
            $.ajax({
                type: "POST",
                url: "/mobile/ajax_supplier/doSaveLogo/",
                data: {
                    id: id,
                    supplier_logo: supplier_logo,
                    supplier_poster: supplier_poster
                },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        showMsg(data.msg)
                    } else {
                        showMsg(data.msg)
                    }
                    return false
                }
            })
}

function getSupplierShowPat(id) {
    layer.open({
        type: 2,
        shade: 'background-color: rgba(0,0,0,.3)',
        content: '加载中...'
    });
    $.ajax({
        type: "POST",
        url: "/mobile/ajax_supplier/ajax_supplier_data/",
        dataType: 'json',
        data: {
            datatype: 'getSupplierPat',
			id:id,
        },
        success: function(data) {
            if (data.status == 1) {
                var html = '';
                $.each(data.list, function(index, obj) {
                    html += '<li style="margin-left:0px;';
                    if (index % 2 == 0) {
                        html += 'margin-right:2%;'
                    }
                    html += '">';
                    html += '	<div class="pict">';
                    html += '		<a href="' + LOCAL_PATH + '/mobile/supplier/showchat/' + obj.id + '">';
                    html += '			<img src="' + YYS_UPLOADS_PATH + '/' + obj.img + '" alt="" class="lazy"/>';
                    html += '		</a>';
                    html += '		<var>' + obj.notice + '</var>';
                    html += '	</div>';
                    html += '	<h3><span><i class="ico-i0"></i>' + obj.hit + '</span>';
                    html += '		<span><i class="ico-i1"></i>' + obj.comment + '</span>';
                    html += '		<span>' + $.myTime.UnixToDate(obj.time, false) + '</span></h3>';
                    html += '</li>'
                })
            } else {
                var html = '<li class="nodata"><span>没有数据记录</span></li>';
            }
            $(".paging ul").html(html);
            $("img.lazy").lazyload({
                threshold: 200,
                effect: "fadeIn"
            });
            layer.closeAll();
            return false
        }
    })
}

function doZan(id){
    $.ajax({
        type: "POST",
        url: "/mobile/ajax_supplier/ajax_supplier_data/",
        dataType: 'json',
        data: {
            datatype: 'doZan',
			id:id,
        },
        success: function(data) {
            if(data.status == 1) {
				$("#zan_"+id).html(data.zan);
				showTips(data.msg);
            }else{
				showMsg(data.msg);
            }
            return false
        }
    })
}