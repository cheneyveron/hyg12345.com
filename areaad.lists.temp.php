<?php defined('G_IN_ADMIN')or exit('Access Denied.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>区域广告首页</title>
	<link rel="stylesheet" href="<?php echo YYS_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
	<link rel="stylesheet" href="<?php echo YYS_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
	<link rel="stylesheet" href="<?php echo YYS_PLUGIN_PATH; ?>/calendar/calendar-blue.css" type="text/css"> 
	<script type="text/javascript" charset="utf-8" src="<?php echo YYS_PLUGIN_PATH; ?>/calendar/calendar.js"></script>
	<style>
		body{ background-color:#fff}
		.header-data{
			border: 1px solid #FFBE7A;
			zoom: 1;
			background: #FFFCED;
			padding: 8px 10px;
			line-height: 20px;
		}
		.table-list  tr {
			text-align:center;
		}
	</style>
</head>
<body>
<div class="header lr10">
	<?php echo $this->headerment();?>
</div>
<div class="bk10"></div>
<div class="header-data lr10">
<form action="#" method="post">
 添加时间: <input name="posttime1" type="text" id="posttime1" class="input-text posttime"  readonly="readonly" /> -  
			<input name="posttime2" type="text" id="posttime2" class="input-text posttime"  readonly="readonly" />
<script type="text/javascript">
		date = new Date();
		Calendar.setup({
					inputField     :    "posttime1",
					ifFormat       :    "%Y-%m-%d %H:%M:%S",
					showsTime      :    true,
					timeFormat     :    "24"
		});
		Calendar.setup({
					inputField     :    "posttime2",
					ifFormat       :    "%Y-%m-%d %H:%M:%S",
					showsTime      :    true,
					timeFormat     :    "24"
		});
				
</script>

<select name="sotype">
	<option value="title">区域广告标题</option>
	<option value="id">区域广告ID</option>
</select>
<input type="text" name="sosotext" class="input-text wid100"/>
<input class="button" type="submit" name="sososubmit" value="搜索">
</form>
</div>
<div class="bk10"></div>
<form action="#" method="post" name="myform">
<div class="table-list lr10">
	 <table width="100%" cellspacing="0">
     	<thead>
        		<tr>
					<th>排序</th>  
					<th>ID</th>  
					<th>区域</th>  
                    <th>区域广告标题</th>
					<th>广告图</th>
					<th>链接</th>
					<th>默认显示</th>
                    <th>添加时间</th>        
                    <th>管理</th>
				</tr>
        </thead>
        <tbody>				
        	<?php foreach($notice_list AS $v) { ?>
            <tr>
				<td align='center'>
					<input name='listorders[<?php echo $v['id']; ?>]' type='text' size='3' value='<?php echo $v['sort']; ?>' class='input-text-c'>
				</td>
                <td>
					<?php echo $v['id'];?>
				</td>
                <td>
					<?php echo $v['region'];?>
				</td>
                <td>
					<a target="_blank" href="/help/<?php echo $v['id'];?>">
						<?php echo _strcut($v['title'],0,25);?>
					</a>
				</td>
				<td><a target="_blank" href="<?php echo $v['url'];?>"><img src="<?php echo YYS_UPLOADS_PATH; ?>/<?php echo $v['img'];?>" width="80" height="60" /></a>
				</td>
				<td>
					<?php echo $v['url'];?>
				</td>
				<td>
					<?php echo $v['isDefault'];?>
				</td>
                <td>
					<?php echo date("Y-m-d H:i:s",$v['create_time']);?>
				</td>
                <td class="action">
                <a href="<?php echo G_ADMIN_PATH; ?>/areaad/edit/<?php echo $v['id'];?>">修改</a>
                <span class='span_fenge lr5'>|</span>    
				<a href="<?php echo G_ADMIN_PATH.'/areaad/del/'.$v['id'];?>" onclick="return confirm('确认删除这个区域广告吗？');">删除</a>

				</td>
            </tr>
            <?php } ?>
        </tbody>
     </table>
     </form>
   <div class="btn_paixu">
  	<div style="width:80px; text-align:center;">
          <input type="button" class="button" value=" 排序 "
        onclick="myform.action='<?php echo YYS_MODULE_PATH; ?>/content/article_listorder/dosubmit';myform.submit();"/>
    </div>
  </div>
<div id="pages"><ul><li>共 <?php echo $zongji; ?> 条</li><?php echo $fenye->show('one','li'); ?></ul></div>
</div>

</body>
</html>