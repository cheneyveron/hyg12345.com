<?php defined('G_IN_ADMIN')or exit('Access Denied.'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>修改会员信息</title>
<link rel="stylesheet" href="/statics/plugin/mstyle/css/style.css" type="text/css"><!--页面CSS-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/animate.min.css" type="text/css"><!--CSS3动画-->
<script type="text/javascript" src="/statics/plugin/mstyle/js/jquery.min.js"></script><!--jQuery库-->
<script src="<?php echo YYS_GLOBAL_STYLE; ?>/global/js/jquery-1.8.3.min.js"></script>
<script src="<?php echo YYS_PLUGIN_PATH; ?>/uploadify/api-uploadify.js" type="text/javascript"></script> 
</head>
<body>
	<div class="container">
		<div class="path">
			<i><a href="#"><img src="/statics/plugin/mstyle/img/ico_1.png" alt=""></a></i>
			<p>
				当前位置：<a href="#">会员信息 </a> > <a href="#">修改会员信息</a> >
			</p>
			<div class="push">
				<a href="<?php echo G_ADMIN_PATH; ?>/index/Tdefault">欢迎界面</a>
				<a href="javascript:void(0)" onclick="location=location">刷新框架</a>
				<a href="<?php echo G_ADMIN_PATH; ?>/cache/init">清理缓存</a>
			</div>
		</div><!-- path -->
		<form name="myform" action="" method="post" enctype="multipart/form-data">
		<div class="big-box">
			<div class="ba-member">
				<h2><a  href="#">新建会员信息</a><a class="afterLeft" href="/member/member/lists">会员信息列表</a> <span><?php echo _buy_time($huiyuan['time']); ?> 注册</span></h2>
				<ul>
					<li>
						<span>会员昵称</span>
						<input type="text" name="username" value="<?php echo $huiyuan['username'];?>"  placeholder="输入会员昵称">
					</li>
					<li>
						<span>邀请会员</span>
						<input type="text" name="yaoqing" value="<?php echo $huiyuan['yaoqing'];?>" placeholder="邀请会员对应的ID编号">
					</li>
					<li>
						<span>微信手机</span>
						<input type="text" name="mobile" value="<?php echo $huiyuan['mobile'];?>" placeholder="填写绑定微信手机号">
					</li>
					<li>
						<span>惠券钱包</span>
						<input type="text" name="money" value="<?php echo $huiyuan['money'];?>" placeholder="充值到账的惠券金额，只能消费">
					</li>
					<!--li>
						<div class="box">
							<span>是否开店</span>
							<select name="" id="">
								<option value="">否</option>
								<option value="">是</option>
							</select>
						</div>
						<div class="box">
							<span>是否冻结</span>
							<select name="" id="">
								<option value="">否</option>
								<option value="">是</option>
							</select>
						</div>
					</li-->
					<li>
						<span>福分积分</span>
						<input type="text" name="score" value="<?php echo $huiyuan['score'];?>" disabled placeholder="参与游戏获得的积分，可换算消费">
					</li>
					<li>
						<span>登陆密码</span>
						<input type="text" name="password" value="" placeholder="不填写默认原密码">
					</li>
					<li>
						<span>可提金额</span>
						<input type="text" name="envelopeCan" value="<?php echo $huiyuan['envelopeCan'];?>" disabled placeholder="开通店铺货款营业额，可提现微信"> 
					</li>
					<li class="ld">
						<span>手机验证</span>
						<label>
							<input type="radio" name="mobilecode" value="-1" <?php echo $huiyuan['mobilecode']=='-1'?'checked':'';?> class="input-text">未验证
						</label>
						<label>
							<input type="radio" name="mobilecode" value="1" <?php echo $huiyuan['mobilecode']=='1'?'checked':'';?> class="input-text">已验证
						</label>
						<label>
							<input type="checkbox"  value="1" name="inside" id="inside" <?php echo $huiyuan['inside']=='1'?'checked':'';?>>内部会员
						</label>
					</li>
					<li>
						<span>经验数值</span>
						<input disabled class="st" name="jingyan" value="<?php echo $huiyuan['jingyan'];?>" type="text" placeholder="惠券的经验数值">
						<input class="sr" type="text" placeholder="显示会员级别" disabled>
					</li>
					<li class="tb">
						<div class="pic"><img src="<?php echo YYS_UPLOADS_PATH.'/'.$huiyuan['img'];?>" alt=""></div>
						<input class="lj" type="text" id="imagetext" name="thumb" value="<?php echo $huiyuan['img']; ?>">
<input type="button" class="button" onClick="GetUploadify('<?php echo LOCAL_PATH; ?>','uploadify','头像上传','image','touimg',1,500000,'imagetext')" value="上传头像" />
					</li>
					<li>
						<span>邀请员数</span>
						<input disabled class="st" type="text" placeholder="邀请会员的数量">
						<input class="sr" type="text" placeholder="显示粉丝级别" disabled>
					</li>
				</ul>
			</div>
			<div class="save">
			<input type="submit" class="page-but" name="submit" value=" 保存 " >
&nbsp;&nbsp;&nbsp; &nbsp;
<input type="reset" class="page-but" name="reset" value="取消">
			</div>
		</div>
	</div><!-- container --></form>
</body>
<script>
function upImage(){
	return document.getElementById('imgfield').click();
}
</script>
<script src="/statics/plugin/mstyle/js/library-3.28.js"></script><!--自定义封装函数-->
<script src="/statics/plugin/mstyle/js/scrollanim.min.js"></script><!--动画效果库-->
</html>
