<?php 
defined('IN_YYS')or exit('Access Denied.');
System::DOWN_App_class('base','member','no');
System::DOWN_App_fun('my','index');
System::DOWN_App_fun('user','index');
System::DOWN_sys_fun('send');
System::DOWN_sys_fun('user');
class ajax_areaad extends base {
	
	public function __construct(){
		parent::__construct();
		$this->db = System::DOWN_sys_class('model');
	}
	
	/*
	*	获取坐标
	*/
	public function getUserGeolocation(){
		$lat = $_POST['lat'];
		$lng = $_POST['lng'];
		_setcookie('lat',$lat,'');
		_setcookie('lng',$lng,'');
	}
	
	/*
	*	获取区域广告
	*/
	public function getAreaAd(){
		$lat = _getcookie('lat');
		$lng = _getcookie('lng');
		
		$diffLat = $lat - 0.5;
		$addLat = $lat + 0.5;
		$diffLng = $lng - 0.5;
		$addLng = $lng + 0.5;
		
		$where = " `lat` >= '$diffLat' and `lat` <= '$addLat'";
		$where .= " and `lng` >= '$diffLng' and `lng` <= '$addLng'";
		
		$adList = $this->db->Ylist("select * from `@#_areaad` where $where order by sort desc,id desc limit 8");
		if(!$adList){
			$adList = $this->db->Ylist("select * from `@#_areaad` where `isDefault` = '1' order by sort desc,id desc limit 8");
		}
		if($adList){
			$status = 1;
		}else{
			$status = 0;
		}
		echo json_encode(array('status'=>$status,'list'=>$adList));
	}
	
	/*默认广告显示*/
	public function defaultAd(){
		$adList = $this->db->Ylist("select * from `@#_areaad` where `isDefault` = '1' order by sort desc,id desc limit 8");
		if($adList){
			$status = 1;
		}else{
			$status = 0;
		}
		echo json_encode(array('status'=>$status,'list'=>$adList));
	}
	
}