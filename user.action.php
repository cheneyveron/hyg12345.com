<?php 
error_reporting(E_ALL^E_NOTICE^E_WARNING);
defined('IN_YYS')or exit('Access Denied.');
System::DOWN_App_class('memberbase',null,'no');
System::DOWN_App_fun('user','index');
System::DOWN_App_fun('my','index');
System::DOWN_sys_fun('send');
class user extends memberbase {
	public function __construct(){	
		parent::__construct();
		$this->db = System::DOWN_sys_class("model");
		//weixin
		//wechat_login();
		
		if(!$this->userinfo){
			wechat_login();
		}		

		//include_once "weixin.php";
		//include_once "weixin.class.php";
	}

	public function cook_end(){
		_setcookie("uid","",time()-3600);
		_setcookie("ushell","",time()-3600);		
		header("location: ".LOCAL_PATH."/mobile/mobile/");
	}
	
	
	//返回登录页面
	public function login(){	
		$webname=$this->_yys['web_name'];
		$biaoti = "用户登录";
		$weer = $this->userinfo;
		if($weer){			
			header("Location:".LOCAL_PATH."/mobile/home/");exit;
		}

		include templates("mobile/user","login");
		
	}
	
	
	//返回登录页面2
	public function login2(){	
		 $webname=$this->_yys['web_name'];
		$weer = $this->userinfo;
		if($weer){			
			header("Location:".LOCAL_PATH."/mobile/home/");exit;
		}
		include templates("mobile/user","login2");
	}
	
	//返回注册页面
	public function register(){
		session_start();
		$webname=$this->_yys['web_name'];
		$biaoti = "新会员注册";
		include templates("mobile/user","register");
	}	
	
	//返回发送验证码页面
	public function mobilecode(){
	    $webname=$this->_yys['web_name'];
		$biaoti = "用户验证";
	    $mobilename=$this->segment(4);
		
		include templates("mobile/user","mobilecheck");
	}
	
	public function mobilecheck(){
	    $webname=$this->_yys['web_name'];
		$biaoti="验证手机";	
		$time=3000;
		$name=$this->segment(4);
		$huiyuan=$this->db->YOne("SELECT * FROM `@#_yonghu` WHERE `mobile` = '$name' LIMIT 1");
		if(!$huiyuan)_notemobile("参数不正确!");
		if($huiyuan['mobilecode']==1){
			_notemobile("该账号验证成功",LOCAL_PATH."/mobile/mobile");
		}
		if($huiyuan['mobilecode']==-1){
			$sendok = send_mobile_reg_code($name,$huiyuan['uid']);			
			if($sendok[0]!=1){
					_notemobile($sendok[1]);
			}
			header("location:".LOCAL_PATH."/mobile/user/mobilecheck/".$huiyuan['mobile']);
			exit;
		}		
		$enname=substr($name,0,3).'****'.substr($name,7,10);
		$time=120;
		include templates("mobile/user","mobilecheck");
	}
	
	
	public function buyDetail(){
		$biaoti = "参与记录";
		$webname=$this->_yys['web_name'];
		$huiyuan=$this->userinfo;
		$xiangmuid=intval($this->segment(4));    
		$xiangmulist=$this->db->Ylist("select *,a.time as timego,sum(gonumber) as gonumber from `@#_yonghu_yys_record` a left join `@#_shangpin` b on a.shopid=b.id where a.uid='$huiyuan[uid]' and b.id='$xiangmuid' group by a.id order by a.time" );
		if(!empty($xiangmulist)){
			if($xiangmulist[0]['q_end_time']!=''&&$xiangmulist[0]['q_showtime']!='Y'){
				//商品已揭晓
				$xiangmulist[0]['codeState']='已揭晓...';			 	   
				$xiangmulist[0]['class']='z-ImgbgC02';			 	   
			}elseif($xiangmulist[0]['shenyurenshu']==0){
				//商品购买次数已满
				$xiangmulist[0]['codeState']='已满员...';			
				$xiangmulist[0]['class']='z-ImgbgC01';		
			}else{
				//进行中
				$xiangmulist[0]['codeState']='进行中...';
				$xiangmulist[0]['class']='z-ImgbgC01';
			 
			}
			$bl=($xiangmulist[0]['canyurenshu']/$xiangmulist[0]['zongrenshu'])*100;
		}	 
		include templates("mobile/user","userbuyDetail");
	}
	
	function wxlogin(){
		$user = $this->userinfo;
	    $pro = $this->segment(4);

	    //file_put_contents('t.txt', "\n\r\r\n-----pro:".$pro,FILE_APPEND);
		if(!$user){
			$code = $this->create_code();
			if($pro){
				_setcookie("procode",$pro);
				$pu = $this->db->YOne("select * from `@#_activity_code` where `code`='$pro'");
				if(empty($pu)){
					$pu = $this->db->YOne("select * from `@#_yonghu` where `code`='$pro'");
				}
			}else{
				$pro=_getcookie("procode");
				$pu = $this->db->YOne("select * from `@#_activity_code` where `code`='$pro'");
			}
			$p_mobile = $pu['mobile'] ? $pu['mobile'] : '';
			$this->db->Query("insert into `@#_activity_code`(`code`,`status`,`pro`) values('$code',0,'{$p_mobile}')");
		}else{
			if(empty($user['code'])){
				$user['code'] = $this->create_code();
				$this->db->YOne("update `@#_yonghu` set code='{$user['code']}' where `uid`='{$user['uid']}'");
			}
		}

		if(!empty($user) && !empty($pro) && $pro==$user['code']){
			$mylink = '';
			include templates("mobile/index","activity_share");die;
		}

		session_start();
		$state  = md5(uniqid(rand(), TRUE));
		$_SESSION["wxState"]	=   $state;
		$redirect_uri = urlencode(LOCAL_PATH."/mobile/user/wx_callback/");
		$yaoqing = System::DOWN_App_config("user_fufen");
		$appid = $yaoqing['appid'];
		$wxurl = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=$appid&redirect_uri=$redirect_uri&response_type=code&scope=snsapi_userinfo&state=$state#wechat_redirect";
		header("Location: $wxurl");
	}


	function httpGet($url) {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_TIMEOUT,30);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$res = curl_exec($curl);
		curl_close($curl);

		return $res;
	}
	
     function wx_callback(){
         session_start();
		 if($_GET["state"] != $_SESSION["wxState"]){ _notemobile("登录验证失败!请重新关注公众号!!!",LOCAL_PATH."/mobile/user/login");  }

		 $code = $_GET["code"];

	     $procode = $this->segment(4);
	     //file_put_contents('t.txt', "\n\r\r\n-----procode:".$procode,FILE_APPEND);
		 $yaoqing = System::DOWN_App_config("user_fufen");
	
		// $appid = 'wx6cb56252f597e614';
		$appid = $yaoqing['appid'];
		$secret = $yaoqing['secret'];
		$dengluid = $yaoqing['dengluid'];
		$response = $this->httpGet("https://api.weixin.qq.com/sns/oauth2/access_token?appid=$appid&secret=$secret&code=$code&grant_type=authorization_code");
		$jsondecode = json_decode($response,true);

		$wx_openid =$jsondecode["openid"];
		$unionid=$jsondecode["unionid"];

		//var_dump($jsondecode);

		if(empty($wx_openid)){
         	_notemobile("绑定出错，请重新关注或联系管理员。");die;
         }

         $sql="select * FROM `@#_yonghu` WHERE openid ='".$wx_openid."' ORDER BY uid asc";
	     $huiyuan=$this->db->YOne($sql);

	     //var_dump($huiyuan);die;
	     if(!$huiyuan)
	     {
	     	 $access_token =$jsondecode["access_token"];
		 	 $response= $this->httpGet("https://api.weixin.qq.com/sns/userinfo?access_token=$access_token&openid=$wx_openid");
		 	 $jsondecode = json_decode($response,true);
		 	 $nickname = $jsondecode["nickname"];
			 $decode = $_SESSION['uu'];
			 $decode2=$_SESSION['yaoqing2'];
			 $decode3=$_SESSION['yaoqing3'];
             $fid = $_SESSION['fid'];
			 $session_id=session_id();
			 $ip = _huode_ip_dizhi();
			 $time = time();
			 $wximg =  $this->httpGet($jsondecode["headimgurl"]); 
		 	 $ttmm=$_SERVER['DOCUMENT_ROOT'].'/statics/uploads/photo/'.time().'.jpg';
			 file_put_contents($ttmm,$wximg); 
		     $userpass = md5("123456");
			 $go_user_img  ='photo/'.time().'.jpg';
		     $go_user_time = time();
			
			$sqls = "INSERT INTO `@#_yonghu` (`username`,`password`,`img`,`band`,`time`,`money`,`first`,code,yaoqing,yaoqing2,yaoqing3,yaoqing4,session_id,`user_ip`,`login_time`,`openid`,`fid`) VALUES ('$nickname','$userpass','$go_user_img','weixin','$go_user_time',0,1,'$procode','$decode','$decode2','$decode3','$decode4','$session_id','$ip','$time','$wx_openid','$fid')";
			$q1 = $this->db->Query($sqls);

		     $uid = $this->db->last_id();

			 $this->db->Query("INSERT INTO `@#_yonghu_band` (`b_uid`, `b_type`, `b_code`, `b_time`) VALUES ('$uid', 'weixin', '$wx_openid', '$go_user_time')");
			 $member = $this->db->YOne("select uid,password,mobile,email from `@#_yonghu` where `uid` = '$uid' LIMIT 1");
			
					
		    //限制登陆补丁
		    $se1 = _setcookie("uid",_encrypt($member['uid']),60*60*24*7);
		    $se2 = _setcookie("ushell",_encrypt(md5($member['uid'].$member['password'].$member['mobile'].$member['email'])),60*60*24*7);
			
			$regtype = System::DOWN_app_config("user_reg_type",'','member');
			if($regtype['reg_bind']){
				$callback_url =  LOCAL_PATH."/mobile/home/mobilebind";
			}else{
				$callback_url =  LOCAL_PATH."/mobile/home";
			}
			header("Location:$callback_url");
	     }
	     elseif($huiyuan['img']=='photo/member.jpg')
	     {
	     	 $access_token =$jsondecode["access_token"];
		 	 $response= $this->httpGet("https://api.weixin.qq.com/sns/userinfo?access_token=$access_token&openid=$wx_openid");
		 	 $jsondecode = json_decode($response,true);
		 	 $nickname = $jsondecode["nickname"];
			 $decode = $_SESSION['uu'];
			 $decode2=$_SESSION['yaoqing2'];
			 $decode3=$_SESSION['yaoqing3'];
             $fid= $_SESSION['fid'];
			 $session_id=session_id();
			 $ip = _huode_ip_dizhi();
			 $time = time();
			 $wximg =  $this->httpGet($jsondecode["headimgurl"]); 
		 	 $ttmm=$_SERVER['DOCUMENT_ROOT'].'/statics/uploads/photo/'.time().'.jpg';
			 file_put_contents($ttmm,$wximg); 
		     $userpass = md5("123456");
			 $go_user_img  ='photo/'.time().'.jpg';
		     $go_user_time = time();
		     $uid = $huiyuan["uid"];
			//限制登陆补丁
			
		 	$session_id=session_id();
			$ip = _huode_ip_dizhi();
			$time = time();
			$this->db->Query("UPDATE `@#_yonghu` SET user_ip='$ip',`login_time`='$time',session_id='$session_id',img='$go_user_img' where uid='$uid'");
			//限制登陆补丁
			$member = $this->db->YOne("select uid,password,mobile,email from `@#_yonghu` where `uid` = '$uid' LIMIT 1");
			
			$se1 = _setcookie("uid",_encrypt($member['uid']),60*60*24*7);
		    $se2 = _setcookie("ushell",_encrypt(md5($member['uid'].$member['password'].$member['mobile'].$member['email'])),60*60*24*7);
			
			$regtype = System::DOWN_app_config("user_reg_type",'','member');
			if($regtype['reg_bind']){
				$callback_url =  LOCAL_PATH."/mobile/home/mobilebind";
			}else{
				$callback_url =  LOCAL_PATH."/mobile/home";
			}
			
			header("Location:$callback_url");
	     }
	     else
	     {
	     	$uid = $huiyuan["uid"];
			//限制登陆补丁
			
		 	$session_id=session_id();
			$ip = _huode_ip_dizhi();
			$time = time();
			$this->db->Query("UPDATE `@#_yonghu` SET user_ip='$ip',`login_time`='$time',session_id='$session_id' where uid='$uid'");
			//限制登陆补丁
			$member = $this->db->YOne("select uid,password,mobile,email from `@#_yonghu` where `uid` = '$uid' LIMIT 1");
			
			$se1 = _setcookie("uid",_encrypt($member['uid']),60*60*24*7);
		    $se2 = _setcookie("ushell",_encrypt(md5($member['uid'].$member['password'].$member['mobile'].$member['email'])),60*60*24*7);
 
			$regtype = System::DOWN_app_config("user_reg_type",'','member');
			if($regtype['reg_bind']){
				$callback_url =  LOCAL_PATH."/mobile/home/mobilebind";
			}else{
				$callback_url =  LOCAL_PATH."/mobile/home";
			}
			header("Location:$callback_url");
			/*if(!$member['mobile']){
			    $callback_url =  LOCAL_PATH."/mobile/home/mobilebind";
			    header("Location:$callback_url");
            }else{
			  	$callback_url =  LOCAL_PATH."/mobile/home/mobilebind";
			  	header("Location:$callback_url");
			}*/
	     }






		
		
		//微信登陆通知start
		/*$weixin = new class_weixin();
		$iipp=$_SERVER["REMOTE_ADDR"];
		$tit=$this->_yys['web_name_two'];
		$template = array('touser' => $wx_openid,
                      'template_id' => $dengluid,
                      'url' => LOCAL_PATH."/mobile/home",
                      'topcolor' => "#7B68EE",
                      'data' => array('first'    => array('value' => "您好，欢迎登陆".$tit,
                                                         'color' => "#743A3A",
                                                        ),
										  'keyword1' => array('value' =>  date('Y-m-d h:i:s',time()),
															 'color' => "#FF0000",
															),
										   'keyword2'     => array('value' => _huode_ip_dizhi($iipp),
															 'color' => "#0000FF",
															),
										  'remark'     => array('value' => "\\n如非本人登陆,请修改密码！",
															 'color' => "#008000",
															),
										)
		);
	   $weixin->send_template_message($template);*/

   		//微信登陆通知end


	}


	//二维码威信登陆
	function wxloginer(){
		$user = $this->userinfo;
	    $pro = $this->segment(4);
	    file_put_contents('t.txt', "\n\r\r\n-----pro:".$pro,FILE_APPEND);
		if(!$user){
			$code = $this->create_code();
			if($pro){
				_setcookie("procode",$pro);
				$pu = $this->db->YOne("select * from `@#_activity_code` where `code`='$pro'");
				if(empty($pu)){
					$pu = $this->db->YOne("select * from `@#_yonghu` where `code`='$pro'");
				}
			}else{
				$pro=_getcookie("procode");
				$pu = $this->db->YOne("select * from `@#_activity_code` where `code`='$pro'");
			}
			$p_mobile = $pu['mobile'] ? $pu['mobile'] : '';
			$this->db->Query("insert into `@#_activity_code`(`code`,`status`,`pro`) values('$code',0,'{$p_mobile}')");
		}else{
			if(empty($user['code'])){
				$user['code'] = $this->create_code();
				$this->db->YOne("update `@#_yonghu` set code='{$user['code']}' where `uid`='{$user['uid']}'");
			}
		}

		if(!empty($user) && !empty($pro) && $pro==$user['code']){
			$mylink = '';
			include templates("mobile/index","activity_share");die;
		}

		session_start();
		$state  = md5(uniqid(rand(), TRUE));
		$_SESSION["wxState"]	=   $state;
		$redirect_uri = urlencode(LOCAL_PATH."/mobile/user/wx_callbacker/".$code."/");
		$yaoqing = System::DOWN_App_config("user_fufen");
		$appid = $yaoqing['appid'];
		$wxurl = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=$appid&redirect_uri=$redirect_uri&response_type=code&scope=snsapi_userinfo&state=$state#wechat_redirect";
		header("Location: $wxurl");
	}


     function wx_callbacker(){
         session_start();
		 if($_GET["state"] != $_SESSION["wxState"]){ _notemobile("登录验证失败!",LOCAL_PATH."/mobile/user/login");  }

		 $code = $_GET["code"];
	     $procode = $this->segment(4);
	     file_put_contents('t.txt', "\n\r\r\n-----procode:".$procode,FILE_APPEND);
		 $yaoqing = System::DOWN_App_config("user_fufen");
	
       // $appid = 'wx6cb56252f597e614';
		$appid = $yaoqing['appid'];
		$secret = $yaoqing['secret'];
	    $dengluid = $yaoqing['dengluid'];
		$response = file_get_contents("https://api.weixin.qq.com/sns/oauth2/access_token?appid=$appid&secret=$secret&code=$code&grant_type=authorization_code");
		

		$jsondecode = json_decode($response,true);
		$wx_openid =$jsondecode["openid"];
		// var_dump($jsondecode);
		// exit;
         if(empty($wx_openid)){
         	_notemobile("绑定出错，请联系管理员。");die;
         }
		 $access_token =$jsondecode["access_token"];
		 $response= file_get_contents("https://api.weixin.qq.com/sns/userinfo?access_token=$access_token&openid=$wx_openid");
		 $jsondecode = json_decode($response,true);
 		//微信登陆通知start


   		//微信登陆通知end

		 $nickname = $jsondecode["nickname"];
		 $go_user_info = $this->db->YOne("select * from `@#_yonghu_band` where `b_code` = '$wx_openid' and `b_type` = 'weixin' LIMIT 1");
		session_start();
		$decode = $_SESSION['uu'];
		$decode2=$_SESSION['yaoqing2'];
		$decode3=$_SESSION['yaoqing3'];
		$session_id=session_id();
		 if(!$go_user_info){
			 $wximg = file_get_contents($jsondecode["headimgurl"]); 
		 $ttmm=$_SERVER['DOCUMENT_ROOT'].'/statics/uploads/photo/'.time().'.jpg';
			file_put_contents($ttmm,$wximg); 
		    $userpass = md5("123456");
			$go_user_img  ='photo/'.time().'.jpg';
		    $go_user_time = time();
		    $q1 = $this->db->Query("INSERT INTO `@#_yonghu` (`username`,`password`,`img`,`band`,`time`,`money`,`first`,code,yaoqing,yaoqing2,yaoqing3,yaoqing4,session_id) VALUES ('$nickname','$userpass','$go_user_img','weixin','$go_user_time',0,1,'$procode','$decode','$decode2','$decode3','$decode4','$session_id')");

		    $uid = $this->db->last_id();

			$this->db->Query("INSERT INTO `@#_yonghu_band` (`b_uid`, `b_type`, `b_code`, `b_time`) VALUES ('$uid', 'weixin', '$wx_openid', '$go_user_time')");
			$member = $this->db->YOne("select uid,password,mobile,email from `@#_yonghu` where `uid` = '$uid' LIMIT 1");
			
					
		 	//限制登陆补丁
		    $se1 = _setcookie("uid",_encrypt($member['uid']),60*60*24*7);
		    $se2 = _setcookie("ushell",_encrypt(md5($member['uid'].$member['password'].$member['mobile'].$member['email'])),60*60*24*7);
			
			$callback_url =  LOCAL_PATH."/mobile/ajax/erweimas";
			header("Location:$callback_url");
		 }else{
			 
		    $uid = $go_user_info["b_uid"];
			//限制登陆补丁
					
		 
			$this->db->Query("UPDATE `@#_yonghu` SET session_id='$session_id' where uid='$uid'");
			//限制登陆补丁
			$member = $this->db->YOne("select uid,password,mobile,email from `@#_yonghu` where `uid` = '$uid' LIMIT 1");
			
			$se1 = _setcookie("uid",_encrypt($member['uid']),60*60*24*7);
		    $se2 = _setcookie("ushell",_encrypt(md5($member['uid'].$member['password'].$member['mobile'].$member['email'])),60*60*24*7);
 

   
			if(!$member['mobile']){
			    $callback_url =  LOCAL_PATH."/mobile/ajax/erweimas";
				
			    header("Location:$callback_url");
            }else{
			  	$callback_url =  LOCAL_PATH."/mobile/ajax/erweimas";
			  	header("Location:$callback_url");
			}
		 }

	}

	private function create_code($len = 6){
		$randpwd = '';
		for ($i = 0; $i < $len; $i++){
			$randpwd .= chr(mt_rand(33, 126));
		}
		$randpwd = base64_encode($randpwd);
		return $randpwd;
	}

	 //忘记密码

	 public function findpassword(){
		 $biaoti = "找回密码";
	  	$webname=$this->_yys['web_name'];
		include templates("mobile/user","findpassword");
	}

		/*/排行榜
	public function toumingbang(){
		
			//今日排行榜
		$startToday = mktime(0,0,0,date('m'),date('d'),date('Y'));
		$endToday = mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
		$sqlToday = "select (select sum(d.moneycount) from  yys_yonghu_yys_record  d where a.uid = d.uid and d.time between ".$startToday." and ".$endToday.") as money,a.uid,b.username,a.username as username1,count(DISTINCT d.shopid) as num,count(DISTINCT c.uid) as yq 
					from yys_yonghu_yys_record a left join yys_yonghu b on a.uid = b.uid
						left join yys_yonghu c on a.uid = c.yaoqing and c.time between ".$startToday." and ".$endToday."  
						left join yys_yonghu_yys_record d on a.id = d.id and d.huode != 0  
					where a.time between ".$startToday." and ".$endToday." 
					group by a.uid
					order by money desc limit 0,50";
		$listToday = $this->db->Ylist($sqlToday);
		$strToday = "";
		foreach($listToday as $key => $val){
		
			$tx = $this->db->YOne("select sum(b.money) as tx
					from yys_yonghu a left join yys_yonghu_recodes b on a.uid = b.uid and a.time between ".$startToday." and ".$endToday."
					where b.type=1 and b.time between ".$startToday." and ".$endToday." and a.yaoqing = ".$val['uid']);
			
			
			$strToday .= '<tr class="odd">';
			$strToday .= '<td><span>'.($key+1).'</span></td>';
			$strToday .= '<td><span><a style="color:blue;text-decoration: underline;" href="'.LOCAL_PATH.'/mobile/mobile/userindex/'.$val['uid'].'" target="_blank">'.($val['username']?$val['username']:$val['username1']).'</a></span></td>';
			$strToday .= '<td><span>'.$val['money'].'</span></td>';
			$strToday .= '<td><span>'.$val['num'].'</span></td>';
			$strToday .= '<td><span>'.$val['yq'].'</span></td>';
			$strToday .= '<td><span>'.($tx['tx']?$tx['tx']:0).'</span></td>';
			$strToday .= '</tr>';
		}
		
		//昨日排行榜
		$startLast = mktime(0,0,0,date('m'),date('d')-1,date('Y'));
		$endLast = mktime(0,0,0,date('m'),date('d'),date('Y'))-1;
		$sqlLast = "select (select sum(d.moneycount) from  yys_yonghu_yys_record  d where a.uid = d.uid and d.time between ".$startLast." and ".$endLast.") as money,a.uid,b.username,a.username as username1,count(DISTINCT d.shopid) as num ,count(DISTINCT c.uid) as yq 
					from yys_yonghu_yys_record a left join yys_yonghu b on a.uid = b.uid
						left join yys_yonghu c on a.uid = c.yaoqing and c.time between ".$startLast." and ".$endLast." 
						left join yys_yonghu_yys_record d on a.id = d.id and d.huode != 0   
					where a.time between ".$startLast." and ".$endLast." 
					group by a.uid
					order by money desc limit 0,50";
		$listLast = $this->db->Ylist($sqlLast);
		$strLast = "";
		foreach($listLast as $key => $val){
			$tx = $this->db->YOne("select sum(b.money) as tx 
					from yys_yonghu a left join yys_yonghu_recodes b on a.uid = b.uid and a.time between ".$startLast." and ".$endLast."
					where b.type=1 and b.time between ".$startLast." and ".$endLast." and a.yaoqing = ".$val['uid']);
			$strLast .= '<tr class="odd">';
			$strLast .= '<td><span>'.($key+1).'</span></td>';
			$strLast .= '<td><span><a style="color:blue;text-decoration: underline;" href="'.LOCAL_PATH.'/mobile/mobile/userindex/'.$val['uid'].'" target="_blank">'.($val['username']?$val['username']:$val['username1']).'</a></span></td>';
			$strLast .= '<td><span>'.$val['money'].'</span></td>';
			$strLast .= '<td><span>'.$val['num'].'</span></td>';
			$strLast .= '<td><span>'.$val['yq'].'</span></td>';
			$strLast .= '<td><span>'.($tx['tx']?$tx['tx']:0).'</span></td>';
			$strLast .= '</tr>';
		}
		//本周排行榜
		$startWeek = mktime(0, 0 , 0,date("m"),date("d")-date("w")+1,date("Y"));
		$endWeek = mktime(23,59,59,date("m"),date("d")-date("w")+7,date("Y"));
		$sqlWeek = "select (select sum(d.moneycount) from  yys_yonghu_yys_record  d where a.uid = d.uid and d.time between ".$startWeek." and ".$endWeek.") as money,a.uid,b.username,a.username as username1,count(DISTINCT d.shopid) as num ,count(DISTINCT c.uid) as yq 
					from yys_yonghu_yys_record a left join yys_yonghu b on a.uid = b.uid
						left join yys_yonghu c on a.uid = c.yaoqing and c.time between ".$startWeek." and ".$endWeek." 
						left join yys_yonghu_yys_record d on a.id = d.id and d.huode != 0   
					where a.time between ".$startWeek." and ".$endWeek." 
					group by a.uid
					order by money desc limit 0,50";;
		$listWeek = $this->db->Ylist($sqlWeek);
		$strWeek = "";
		foreach($listWeek as $key => $val){
			$tx = $this->db->YOne("select sum(b.money) as tx
					from yys_yonghu a left join yys_yonghu_recodes b on a.uid = b.uid and a.time between ".$startWeek." and ".$endWeek."
					where b.type=1 and b.time between ".$startWeek." and ".$endWeek." and yaoqing = ".$val['uid']);
			$strWeek .= '<tr class="odd">';
			$strWeek .= '<td><span>'.($key+1).'</span></td>';
			$strWeek .= '<td><span><a style="color:blue;text-decoration: underline;" href="'.LOCAL_PATH.'/mobile/mobile/userindex/'.$val['uid'].'" target="_blank">'.($val['username']?$val['username']:$val['username1']).'</a></span></td>';
			$strWeek .= '<td><span>'.$val['money'].'</span></td>';
			$strWeek .= '<td><span>'.$val['num'].'</span></td>';
			$strWeek .= '<td><span>'.$val['yq'].'</span></td>';
			$strWeek .= '<td><span>'.($tx['tx']?$tx['tx']:0).'</span></td>';
			$strWeek .= '</tr>';
		}
		include templates("mobile/user","toumingbang"); 
	}
	*/
	
	
	//会员设置子菜单	
	public function settings(){
		$huiyuan = $this->userinfo;
		$webname=$this->_yys['web_name'];
		$biaoti = "我的设置";
		$index="i4";
		
	 //获取等级
	  $huiyuandj=$this->db->Ylist("select * from `@#_yonghu_group` where `type` = '会员等级'");
	  $jingyan=$huiyuan['jingyan'];
	  if(!empty($huiyuandj)){
	     foreach($huiyuandj as $key=>$val){
		    if($jingyan>=$val['jingyan_start'] && $jingyan<=$val['jingyan_end']){
			   $huiyuan['level']=$val['name'];
			   $huiyuan['icon']=$val['icon'];
			}
		 }
	  }
		
		include templates("mobile/user","settings");
	}
	
	/*提现*/
	public function usercash(){
		$header = '零钱可提现到微信或支付宝';
		$biaoti = "货款提现";
		$huiyuan = $this->userinfo;
		$detailurl = '/mobile/user/cashlog/';
		include templates("mobile/user","usercash");
	}
	
	/*提现记录*/
	public function cashlog(){
		$header = '零钱可提现到微信或支付宝';
		$biaoti = "提现记录";
		$huiyuan = $this->userinfo;
		include templates("mobile/user","cashlog");
	}
	
	/*佣金入钱包*/
	public function moneybag(){
		$header = '请选择以下类项转入钱包';
		$biaoti = "货款转入钱包";
		$huiyuan = $this->userinfo;
		include templates("mobile/user","moneybag");
	}
	
	/*完善地址*/
	public function insertAddress(){
		$header = '完善收货地址';
		$huiyuan = $this->userinfo;
		$huiyuan_dizhi = $this->db->Ylist("select * from `@#_yonghu_dizhi` where uid='".$huiyuan['uid']."' limit 5");
		foreach($huiyuan_dizhi as $k=>$v){		
			$huiyuan_dizhi[$k] = _htmtguolv($v);
		}
		
		$orderSn = intval($this->segment(4));
		include templates("mobile/user","insertAddress");
	}
	
	/*收货评价*/
	public function goComment(){
		
	}
	
	/*获得卡券*/
	public function couponlist(){
		$biaoti = "我的众筹券/优惠券";
		include templates("mobile/user","couponlist");
	}
	
	//财务明细子菜单
	public function caiwu(){
		$huiyuan = $this->userinfo;
		$webname=$this->_yys['web_name'];
		$uid = $huiyuan['uid'];
		$biaoti = "财务明细";
		$index="i4";
		$wait = $this->db->YOne("select sum(`dayMoney`) as `countMoney` from `@#_zg_return_list` where `uid`='$uid' and `status` = '0'");
		$done = $this->db->YOne("select sum(`dayMoney`) as `countMoney` from `@#_zg_return_list` where `uid`='$uid' and `status` = '1'");
		$wait['countMoney'] = number_format($wait['countMoney'],2);
		$done['countMoney'] = number_format($done['countMoney'],2);
		include templates("mobile/user","caiwu");
	}
	/*共赢收益*/
	public function supplierGongying(){
		$biaoti = "共赢收益";
		$huiyuan = $this->userinfo;
		$webname=$this->_yys['web_name'];
		$uid = $huiyuan['uid'];
		$index="i4";
		$wait = $this->db->YOne("select sum(`dayMoney`) as `countMoney` from `@#_zg_return_list` where `uid`='$uid' and `status` = '0'");
		$done = $this->db->YOne("select sum(`dayMoney`) as `countMoney` from `@#_zg_return_list` where `uid`='$uid' and `status` = '1'");
		$wait['countMoney'] = number_format($wait['countMoney'],2);
		$done['countMoney'] = number_format($done['countMoney'],2);
		include templates("mobile/user","supplierGongying");
	}

	/*邀请收益/邀请收益*/
	public function supplierHuli(){
		$biaoti = "邀请收益";
		include templates("mobile/user","supplierHuli");
	}
	
	/*意见反馈*/
	public function feedback(){
		$biaoti = "意见反馈";
		include templates("mobile/user","feedback");
	}
	
	/*发送卡密*/
	public function sendCard(){
		$biaoti = "发送卡密";
		include templates("mobile/user","sendCard");
	}
}
