<?php defined('G_IN_ADMIN')or exit('Access Denied.'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>商户口碑列表</title>
<link rel="stylesheet" href="/statics/plugin/mstyle/css/style.css" type="text/css"><!--页面CSS-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/animate.min.css" type="text/css"><!--CSS3动画-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/timer.css" type="text/css"><!--CSS3动画-->
<script type="text/javascript" src="/statics/plugin/mstyle/js/jquery.min.js"></script><!--jQuery库-->
</head>
<body>
	<div class="container min-wid">
		<div class="path">
			<i><a href="#"><img src="/statics/plugin/mstyle/img/ico_1.png" alt=""></a></i>
			<p>
				当前位置：<a href="#">商户综合</a> > <a href="#">商户口碑列表</a> >
			</p>
			<div class="push">
				<a href="<?php echo YYS_MODULE_PATH; ?>/index/Tdefault">欢迎界面</a>
				<a href="javascript:void(0)" onclick="location=location">刷新框架</a>
				<a href="<?php echo YYS_MODULE_PATH; ?>/cache/init">清理缓存</a>
			</div>
		</div><!-- path -->
		<div class="ofmouth wid1">
			<div class="pict">
				<img src="/statics/plugin/mstyle/img/pic_02.png" alt="">
			</div>
			<div class="fl-box">
			<form method="GET" action="#">
			<?php 
				if($uid_url){
					echo '<var>商户编号</var>
					<input type="text" value="'.$thisYonghu["uid"].'" disabled>
					<var>店铺名称</var>
					<input class="lar" type="text" value="'.$thisYonghu["username"].'" disabled>';
				}else if($uid_req){
					echo '<var>商户编号</var>
					<input type="text" name="uid" value="'.$thisYonghu["uid"].'">
					<var>店铺名称</var>
					<input class="lar" type="text" name="username" value="'.$thisYonghu["username"].'" disabled>
					<button type="submit" name="uidsubmit" value="筛选">筛选</button>';
				}else{
					echo '<var>商户编号</var>
					<input type="text" name="uid" value="">
					<var>店铺名称</var>
					<input class="lar" type="text" name="username" value="" disabled>
					<button type="submit" name="uidsubmit" value="筛选">筛选</button>';
				}
				?>
				<div class="grate-date">
					评价日期
					<input id="in1" type="text" name="start_time" value='<?php if(isset($_REQUEST["start_time"])) echo $_REQUEST["start_time"]; else echo "2018-1-1"; ?>' class="date_picker">
					至
					<input id="in2" type="text" name="end_time" value='<?php if(isset($_REQUEST["end_time"])) echo $_REQUEST['end_time']; else echo '2018-2-1'; ?>'  class="date_picker">
				</div>
				<button type="submit" name="sososubmit" value="筛选">筛选</button>
				<button type="submit" name="chasubmit" value="筛选">查差评</button>
			</form>
			</div>
			<div class="fr-box">
			<?php if(isset($supplier)){ ?>
				<span><img src="/statics/uploads/<?php echo $supplier['icon']; ?>"></span>
				<span>商家信誉：<i><?php echo $supplier['credibility']; ?></i>分</span>
				<!--span>每页显示:
					<select name="" id="">
						<option value="">10</option>
						<option value="">20</option>
						<option value="">30</option>
					</select>
				</span-->
			<?php } ?>
			</div>
		</div>
<!-- identification -->
	<?php if($this->segment(4)!='xianshi' && $this->segment(4)!='maxqishu' && $this->segment(4)!='moneyasc' && $this->segment(4)!='money'): ?>
<form action="#" method="post" name="myform">

		<div class="ofmouth-table dt wid1">
			<ul>
				<li class="head">
					<span>编号</span>
					<span>会员头像</span>
					<span>评价会员</span>
					<span>被评商家</span>
					<span>相关订单</span>
					<span>口碑评论</span>
					<span>评价</span>
					<span>计分</span>
					<span>点赞量</span>
					<span>评价时间</span>
					<span>置顶</span>
					<span>删除</span>
				</li>
				<?php foreach($yyslist as $v) { 
					$uid = $v['uid'];
					$yonghu = $this->db->YOne("SELECT * FROM `yys_yonghu` WHERE `uid` = '$uid'");
					?>
				<li>
					<span><?php echo $v['id'];?></span>
					<span>
						<i>
							<img src="/statics/uploads/<?php echo $yonghu['img']; ?>" alt="">
						</i>
					</span>
					<span><?php echo $v['uid'];?></span>
					<span><?php echo $v['sid'];?></span>
					<span><?php echo $v['oid'];?></span>
					<span>
						<var><?php echo $v['content'];?></var>
					</span>
					<span>
					<?php
					if($v['praise']==1){
						echo "好评";
						}
					if($v['praise']==0){
						echo "中评";
					}
					if($v['praise']==-1){
						echo "差评";
					}
					?>
					</span>
					<span><?php echo $v['praise'];?></span>
					<span><?php echo $v['zan'];?></span>
					<span><?php echo date("Y-m-d H:i:s",$v['time']);?></span>
						<span><a href="<?php echo YYS_MODULE_PATH; ?>/supplier/comprehensive_comment/top/<?php echo $v['id']; ?>" class="ico-top"></a></span>
						<span><a href="<?php echo YYS_MODULE_PATH; ?>/supplier/comprehensive_comment/del/<?php echo $v['id']; ?>" class="ico-del"></a></span>
				</li>
 <?php } ?>
 <script>
function submitForm(e) {
    $('#li').submit();
};
 </script>

			</ul>
		</div>
<?php endif; ?>	
		<div class="pages">
			<span>
				<?php echo $fenye->show('new')['html']; ?>
			</span>
			<span>共 <?php echo ceil($zongji/$num); ?> 页</span>
		
			<span>到第<input type="text" id="page" value="">页</span>
			<span><a class="page-but" id="gopage" href="javascript:void(0);">确定</a></span>
		</div>
	</div><!-- container -->
</body>
<script src="/statics/plugin/mstyle/js/jquery.date_input.pack.js"></script>
<script src="/statics/plugin/mstyle/js/library-3.28.js"></script><!--自定义封装函数-->
<script src="/statics/plugin/mstyle/js/scrollanim.min.js"></script><!--动画效果库-->
<script>
	$(".ofgoods-table ul li span .ico-add").click(function(event) {
		$(".al-box").show();
	});
	$(".al-box .save a").click(function(event) {
		$(".al-box").hide();
	});
	$("#gopage").click(function(){
		var page = $("#page").val();
		var allpage = "<?php echo ceil($zongji/$num); ?>";
		if(page == '' || allpage < page){
			var page = 1;
		}
		location.href = "<?php echo $fenye->show('new')['url']; ?>"+page;
	});
</script>
</html>
