<?php defined('G_IN_ADMIN')or exit('Access Denied.'); ?><!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html;charset=utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=Edge"><title>编辑商品
</title><link rel="stylesheet" href="/statics/plugin/mstyle/css/style.css" type="text/css"><!--页面CSS--><script type="text/javascript" src="/statics/plugin/mstyle/js/jquery.min.on.js"></script><!--jQuery库--><script src="<?php echo YYS_GLOBAL_STYLE; ?>/global/js/jquery-1.8.3.min.js"></script><script src="<?php echo YYS_PLUGIN_PATH; ?>/uploadify/api-uploadify.js" type="text/javascript"></script>&nbsp;
<link rel="stylesheet" href="<?php echo YYS_PLUGIN_PATH; ?>/calendar/calendar-blue.css" type="text/css">
<script type="text/javascript" charset="utf-8" src="<?php echo YYS_PLUGIN_PATH; ?>/calendar/calendar.js"></script>
<script type="text/javascript">var editurl=Array();editurl['editurl']='<?php echo YYS_PLUGIN_PATH; ?>/ueditor/';editurl['imageupurl']='<?php echo G_ADMIN_PATH; ?>/ueditor/upimage/';editurl['imageManager']='<?php echo G_ADMIN_PATH; ?>/ueditor/imagemanager';</script>
<script type="text/javascript" charset="utf-8" src="<?php echo YYS_PLUGIN_PATH; ?>/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo YYS_PLUGIN_PATH; ?>/ueditor/ueditor.all.min.js"></script>
<style>
.bg {
	background: #fff url(<?php echo YYS_GLOBAL_STYLE; ?>/global/image/ruler.gif) repeat-x scroll 0 9px;
}
.color_window_td a {
	float: left;
	margin: 0px 10px;
}
</style>
</head>

<body>

<div class="container">
	<div class="path">
		<i><a href="#"><img src="/statics/plugin/mstyle/img/ico_1.png" alt=""></a></i>
		<p>当前位置：<a href="#">商品管理</a> &gt; <a href="#">编辑商品</a> &gt; </p>
		<div class="push">
			<a href="<?php echo YYS_MODULE_PATH; ?>/index/Tdefault">欢迎界面</a>
			<a href="javascript:void(0)" onclick="location=location">刷新框架</a>
			<a href="<?php echo YYS_MODULE_PATH; ?>/cache/init">清理缓存</a> </div>
	</div>
	<!-- path -->
	<form method="post" action="" onsubmit="return CheckForm()">
		<div class="card-list">
			<div class="card-box">
				<h2><a href="#">新建商品</a><a class="afterLeft" href="#">商品列表</a></h2>
				<div class="card-info">
					<ul>
						<li><span>关联商户</span><input type="text" id="supplierId" name="supplierId" value="<?php echo $shopinfo['supplierId']; ?>" placeholder="自动关联对应商户ID"><i> 
						*</i><var>请选择产品分类：							
								 </var> </li>
						
						<li><span>			</span>
						<select name="cateid" id="category" class="fl">
						<option value="-1">请选择一级分类</option>							
								<?php
									foreach($fenlei_lists as $key => $one){
										if($one['cateid'] == $shopinfo['cateid']){
											echo '<option selected value="'.$one['cateid'].'">'.$one['name'].'</option>';
										}else{
											echo '<option value="'.$one['cateid'].'">'.$one['name'].'</option>';
										}
									}
								?>              
							</select>				
						<select name="catesubid" id="category_two" class="fl">                    
								<option value="0">选择二级分类</option>
								<?php
									foreach($fenlei_list as $key => $one){
										if($one['cateid'] == $shopinfo['catesubid']){
											echo '<option selected value="'.$one['cateid'].'">'.$one['name'].'</option>';
										}else{
											echo '<option value="'.$one['cateid'].'">'.$one['name'].'</option>';
										}
									}
								?>  
							</select>  </li>

						<li><span>商品标题</span><input type="text" id="title" value="<?php echo $shopinfo['title']; ?>" name="title" onkeyup="return gbcount(this,100,'texttitle');" placeholder="填写商品标题，100个字符内。">
						</li>
						<li><span>商品副标</span><input type="text" id="title2" value="<?php echo $shopinfo['title2']; ?>" name="title2" onkeyup="return gbcount(this,100,'texttitle2');" placeholder="填写商品副标题，100个字符内。">
						</li>
						<li><span>关键热词</span><input type="text" value="<?php echo $shopinfo['keywords']; ?>" name="keywords" placeholder="填写商品关键词，多个用 ， 号分割开。">
						</li>
						<!--li><span>商品描述</span><textarea name="description" onKeyUp="gbcount(this,250,'textdescription');"  cols="30" rows="10" placeholder="可以分行填写商品描述，显示位于商品详情最下方。"><?php echo $shopinfo['description']; ?></textarea>						</li-->
						</li>
					</ul>
				</div>
				<div class="ea-price">
					<ul>
						<li><span>商品售价</span><input id="yunjiage" type="text" value="<?php echo $shopinfo['yunjiage']; ?>" name="yunjiage" placeholder="输入整数数值(元)" onkeyup="if(!/^\d+$/.test(this.value)) {this.value=this.value.replace(/[^\d]+/g,'');}"><i>*</i>
						<input type="hidden" id="money" value="<?php echo $shopinfo['money']; ?>" name="money" placeholder="显示市场价值">
						</li>
						<li><span>商品排序</span>
						<input type="text" id="order" value="<?php echo $shopinfo['order']; ?>" name="order" placeholder="越大越靠前显示">

						<i>*</i> </li>
						<li><span>已售数量</span>
						<input type="text" id="qishu" value="<?php echo $shopinfo['qishu']; ?>" name="qishu" disabled placeholder="显示商品销售数量">
						</li>
						<li><span>商品惠券</span>
						<input id="yun" id="supplierPercent" value="<?php echo $shopinfo['supplierPercent']; ?>" name="supplierPercent" type="text" placeholder="输入整数数值(元)" onkeyup="if(!/^\d+$/.test(this.value)) {this.value=this.value.replace(/[^\d]+/g,'');}">
						<i>*</i> </li>
						<li><span>商品库存</span>
						<input type="text" id="maxqishu" value="<?php echo $shopinfo['maxqishu']; ?>" name="maxqishu" placeholder="输入整数数值">
						<i>*</i> </li>
						<li><span>结算货款</span>
						<input id="reseult" disabled type="text" placeholder="显示惠券后结算金额">
						</li>
						<li class="wd"><span>商品属性</span>
						<label><input type="radio" name="isfreepost" value="1" <?php if ($shopinfo['isfreepost']==1) { echo "checked";} ?>="">包邮</label>
						<label><input type="radio" name="isfreepost" value="0" <?php if ($shopinfo['isfreepost']==0) { echo "checked";} ?>="">自提</label>				
						<!--label><input name="isSince" value="1" type="checkbox" <?php if($shopinfo['isSince']){echo "checked";} ?>="" />自提</label>
						<label><input name="isshop" value="1" type="checkbox" <?php if ($shopinfo['isshop']) { echo "checked";} ?>="" />店内</label-->
						<label><input name="isShelve" value="1" type="checkbox" <?php if ($shopinfo['isShelve']) {echo "checked"; } ?>="" />上架</label>
						<label><input name="isstop" value="1" type="checkbox" <?php if($shopinfo['isstop']){echo "checked";} ?>="" />停止</label>
						</li>
						<!--li class="sd1"><span>是否要发送密卡</span><select name="" id=""><option value="">否</option><option value="">是</option></select>						</li>						<li class="sd">创建时间：<var>2017-06-01 12:00:00</var></li-->
						</li>
						<li></li>
						<li class="ld"><span class="lh">商品主图</span>
						<div class="pic">
							<img src="<?php echo YYS_UPLOADS_PATH.'/'.$shopinfo['thumb']; ?>" alt=""></div>
						<input type="text" id="imagetext" name="thumb" value="<?php echo $shopinfo['thumb']; ?>">
						<a href="#">上传图片<input type="button" onclick="GetUploadify('<?php echo LOCAL_PATH; ?>','uploadify','缩略图上传','image','shopimg',1,500000,'imagetext')" value="上传主图" /></a>
						</li>
						<li class="ld"><span class="lh">详情图1</span>
						<div class="pic">
							<img src="<?php echo YYS_UPLOADS_PATH.'/'.$shopinfo['img1']; ?>" alt=""></div>
						<input type="text" id="img1" name="img1" value="<?php echo $shopinfo['img1']; ?>">
						<a href="#">上传详情图1<input type="button" onclick="GetUploadify('<?php echo LOCAL_PATH; ?>','uploadify','缩略图上传','image','shopimg',1,500000,'img1')" value="上传详情图1" /></a>
						</li>
						<li class="ld"><span class="lh">详情图2</span>
						<div class="pic">
							<img src="<?php echo YYS_UPLOADS_PATH.'/'.$shopinfo['img2']; ?>" alt=""></div>
						<input type="text" id="img2" name="img2" value="<?php echo $shopinfo['img2']; ?>">
						<a href="#">上传详情图2<input type="button" onclick="GetUploadify('<?php echo LOCAL_PATH; ?>','uploadify','缩略图上传','image','shopimg',1,500000,'img2')" value="上传详情图2" /></a>
						</li>
						<li class="ld"><span class="lh">详情图3</span>
						<div class="pic">
							<img src="<?php echo YYS_UPLOADS_PATH.'/'.$shopinfo['img3']; ?>" alt=""></div>
						<input type="text" id="img3" name="img3" value="<?php echo $shopinfo['img3']; ?>">
						<a href="#">上传详情图3<input type="button" onclick="GetUploadify('<?php echo LOCAL_PATH; ?>','uploadify','缩略图上传','image','shopimg',1,500000,'img3')" value="上传详情图3" /></a>
						</li>
						<li class="ld"><span class="lh">详情图4</span>
						<div class="pic">
							<img src="<?php echo YYS_UPLOADS_PATH.'/'.$shopinfo['img4']; ?>" alt=""></div>
						<input type="text" id="img4" name="img4" value="<?php echo $shopinfo['img4']; ?>">
						<a href="#">上传详情图4<input type="button" onclick="GetUploadify('<?php echo LOCAL_PATH; ?>','uploadify','缩略图上传','image','shopimg',1,500000,'img4')" value="上传详情图4" /></a>
						</li>
					</ul>
				</div>
			</div>
			<!-- ea-box -->
			<!--div class="fill-text">编辑器暂时不用，图文详情按4张图即可。<script name="content" id="myeditor" type="text/plain"  style="width:100%;height:387px;"><?php echo $shopinfo['content']; ?></script>			</div-->
			<div class="save">
				<input type="submit" class="page-but" name="dosubmit" value=" 保存 ">&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="reset" class="page-but" name="reset" value="取消">
			</div>
		</div>
	</form>
</div>
<!-- container -->
</form>

</body>
<script src="/statics/plugin/mstyle/js/public.js"></script>
<script src="/statics/plugin/mstyle/js/library-3.28.js"></script>
<script>$(".ea-price .pic-list p a input").change(function(event) {	var file = document.getElementById('file').files;	//console.log(file.length);	for(var i=0;i<file.length;i++){		//console.log(file.item(i));		//console.log(window.URL.createObjectURL(file.item(i)));	$(".ea-price .pic-list").append('<div class="pic-path"><div class="pict"><img src="'+window.URL.createObjectURL(file.item(i))+'" alt=""></div><input type="text" value="'+file.item(i).name+'"> <strong>删除</strong> </div>');	}});$(".ea-price .pic-list").on("click",".pic-path strong",function(event) {	$(this).parents(".pic-path").remove();});$(".ea-price ul li.ld a input").change(function(event) {	var e=getFileUrl("main-pic");	$(".ea-price .pic img").attr("src",e);	$(".ea-price ul li.ld input").val(getFileName($(this).val()));});$("#yun").keyup(function(event) {	var edt=parseInt($(this).val());	var edt1=parseInt($("#yunjiage").val());	$("#reseult").val(edt/edt1);});$("#yunjiage").keyup(function(event) {	var edt=parseInt($(this).val());	var edt1=parseInt($("#yun").val());	$("#reseult").val(edt1/edt);});</script>
<span id="title_colorpanel" style="position: absolute; left: 568px; top: 155px" class="colorpanel">
</span>
<script type="text/javascript"> 

	$("select[name='cateid']").change(function(){
		var cateid = $(this).val();
		$.ajax({
			type: "POST",
			url: "/admin/supplier/getSubFenlei",
			data: {cateid:cateid},
			dataType: "json",
			success: function(data){
				if(data.status == 1){
					var html = '';
					$.each(data.list,function (index,obj){
						console.log(obj.cateid);
						if(obj.cateid == "<?php echo $supplier['cateid'];?>"){
							html += '<option selected value="'+obj.cateid+'">'+obj.name+'</option>';
						}else{
							html += '<option value="'+obj.cateid+'">'+obj.name+'</option>';
						}
					});
					$('select[name="catesubid"]').html(html);
				}else{
					var html = '<option value="0">请选择行业服务二级</option>';
					$('select[name="catesubid"]').html(html);
				}
			}
		});
	});
	



   //实例化编辑器    var ue = UE.getEditor('myeditor');    ue.addListener('ready',function(){        this.focus()    });    function getContent() {        var arr = [];        arr.push( "使用editor.getContent()方法可以获得编辑器的内容" );        arr.push( "内容为：" );        arr.push(  UE.getEditor('myeditor').getContent() );        alert( arr.join( "\n" ) );    }    function hasContent() {        var arr = [];        arr.push( "使用editor.hasContents()方法判断编辑器里是否有内容" );        arr.push( "判断结果为：" );        arr.push(  UE.getEditor('myeditor').hasContents() );        alert( arr.join( "\n" ) );    }		var info=new Array();    function gbcount(message,maxlen,id){				if(!info[id]){			info[id]=document.getElementById(id);		}			        var lenE = message.value.length;        var lenC = 0;        var enter = message.value.match(/\r/g);        var CJK = message.value.match(/[^\x00-\xff]/g);//计算中文        if (CJK != null) lenC += CJK.length;        if (enter != null) lenC -= enter.length;				var lenZ=lenE+lenC;				if(lenZ > maxlen){			info[id].innerHTML=''+0+'';			return false;		}		info[id].innerHTML=''+(maxlen-lenZ)+'';    }	function set_title_color(color) {	$('#title2').css('color',color);	$('#title_style_color').val(color);}function set_title_bold(){	if($('#title_style_bold').val()=='bold'){		$('#title_style_bold').val('');			$('#title2').css('font-weight','');	}else{		$('#title2').css('font-weight','bold');		$('#title_style_bold').val('bold');	}}//API JS//window.parent.api_off_on_open('open');</script>

</html>

