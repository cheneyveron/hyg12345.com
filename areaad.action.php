<?php 
/*
*	Module:公共模块
*	Author:赤果果
*/
defined('IN_YYS')or exit('no');
System::DOWN_App_class('admin',G_ADMIN_DIR,'no');
System::DOWN_App_fun('global',G_ADMIN_DIR);
System::DOWN_sys_fun('user');
class areaad extends admin {
	private $db;
	public function __construct(){		
		parent::__construct();		
		$this->db=System::DOWN_sys_class('model');
		$this->ment=array();
		$this->categorys=$this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE 1 order by `parentid` ASC,`cateid` ASC",array('key'=>'cateid'));
		$this->models=$this->db->Ylist("SELECT * FROM `@#_moxing` WHERE 1",array('key'=>'modelid'));
		
		$this->ment=array(
			array("lists","区域广告列表",DOWN_M.'/'.DOWN_C."/lists"),
			array("insert","区域广告添加",DOWN_M.'/'.DOWN_C."/add"),
		);		
	}
	
	/*
	*	添加区域广告
	*/
	public function add(){	
		if(isset($_POST['dosubmit'])){
			$title =  htmlspecialchars($_POST['title']);
			$title_style_color = htmlspecialchars($_POST['title_style_color']);
			$title_style_bold = htmlspecialchars($_POST['title_style_bold']);
			$sub_text_des = htmlspecialchars($_POST['sub_text_des']);
			$sub_text_len = htmlspecialchars($_POST['sub_text_len']);
			$img = htmlspecialchars($_POST['img']);
			$create_time = strtotime($_POST['posttime']) ? strtotime($_POST['posttime']) : time();
			$sort = intval($_POST['sort']);
			$isDefault = htmlspecialchars($_POST['default']);
			$lat = htmlspecialchars($_POST['lat']);
			$lng = htmlspecialchars($_POST['lng']);
			$region = htmlspecialchars($_POST['region']);
			$url = $_POST['url'];
			
			if(empty($title)){_note("标题不能为空");}			
			$sql = "INSERT INTO `@#_areaad` (`title`,`url`,`title_style_color`, `title_style_bold`,`sub_text_len`, `sub_text_des`, `img`, `create_time`, `sort`, `isDefault`, `lat`, `lng`, `region`) VALUES ";
			$sql .= "('$title','$url', '$title_style_color', '$title_style_bold','$sub_text_len', '$sub_text_des', '$img', '$create_time', '$sort', '$isDefault', '$lat', '$lng', '$region')";
			if($this->db->Query($sql)){
				_note("区域广告添加成功");
			}else{
				_note("区域广告添加失败");
			}
			header("Cache-control: private");			
		}
		include $this->dwt(DOWN_M,'areaad.add');
	}

	/*
	*   删除
	*/
	public function del(){
		$id=intval($this->segment(4));
		$sql = "DELETE FROM `@#_areaad` WHERE `id` = '$id'";
		if($this->db->Query($sql)){
			_note("区域广告删除成功");
		}else{
			_note("区域广告删除失败");
		}
		header("Cache-control: private");	
	}
	
	/*
	*	编辑
	*/
	public function edit(){	
		$id = intval($this->segment(4));		
		$info = $this->db->YOne("SELECT * FROM `@#_areaad` where `id`='$id' LIMIT 1");
		
		if(isset($_POST['dosubmit'])){	
			$title =  htmlspecialchars($_POST['title']);
			$title_style_color = htmlspecialchars($_POST['title_style_color']);
			$title_style_bold = htmlspecialchars($_POST['title_style_bold']);
			$sub_text_len =  htmlspecialchars($_POST['sub_text_len']);
			$sub_text_des =  htmlspecialchars($_POST['sub_text_des']);
			$img = trim(htmlspecialchars($_POST['img']));		
			$url = htmlspecialchars($_POST['url']);
			$isDefault = htmlspecialchars($_POST['default']);
			$lat = htmlspecialchars($_POST['lat']);
			$lng = htmlspecialchars($_POST['lng']);
			$region = htmlspecialchars($_POST['region']);
			$biaoti_style='';
			if($biaoti_color){
				$biaoti_style.='color:'.$biaoti_color.';';
			}
			if($biaoti_bold){
				$biaoti_style.='font-weight:'.$biaoti_bold.';';
			}
					
			$content = editor_safe_replace(stripslashes($_POST['content']));
			$create_time = strtotime($_POST['posttime']) ? strtotime($_POST['posttime']) : time();
			$sort = intval($_POST['sort']);
			
			if(empty($title)){_note("标题不能为空");}
			
			$sql="UPDATE `@#_areaad` SET  	`title`= '$title',
											`title_style_color` = '$title_style_color',
											`title_style_bold` = '$title_style_bold', 
											`sub_text_len` = '$sub_text_len', 
											`sub_text_des` = '$sub_text_des', 
											`img` = '$img', 
											`url` = '$url',
											`create_time` = '$create_time',
											`sort` = '$sort',
											`isDefault` = '$isDefault',
											`lat` = '$lat',
											`lng` = '$lng',
											`region` = '$region'
											WHERE (`id`='$id')";
			$this->db->Query($sql);
			if($this->db->affected_rows()){
				_note("操作成功!");
			}else{
				_note("操作失败!");
			}
			header("Cache-control: private");
		}

		if(!$info)_note("参数错误");

		$info['picarr'] = unserialize($info['picarr']);		
		$info['create_time'] = date("Y-m-d H:i:s",$info['create_time']);
		if($info['title_style']){
			if(stripos($info['title_style'],"font-weight:")!==false){
				$biaoti_bold = 'bold';
			}else{
				$biaoti_bold = '';
			}
			if(stripos($info['title_style'],"color:")!==false){
				$biaoti_color = explode(';',$info['title_style']); 
				$biaoti_color = explode(':',$biaoti_color[0]); 
				$biaoti_color = $biaoti_color[1];
			}else{
				$biaoti_color = '';
			}				
		}else{
			$biaoti_color='';
			$biaoti_bold = '';
		}		
		
		include $this->dwt(DOWN_M,'areaad.edit');
	
	}	
	
	/*
	*	列表
	*/
	public function lists(){
		$list_where = '1';

		if(isset($_POST['sososubmit'])){			
			$posttime1 = !empty($_POST['posttime1']) ? strtotime($_POST['posttime1']) : NULL;
			$posttime2 = !empty($_POST['posttime2']) ? strtotime($_POST['posttime2']) : NULL;			
			$sotype = $_POST['sotype'];
			$title = $_POST['title'];
			$sosotext = $_POST['sosotext'];			
			if($posttime1 && $posttime2){
				if($posttime2 < $posttime1)_note("结束时间不能小于开始时间");
				$list_where = "`posttime` > '$posttime1' AND `posttime` < '$posttime2'";
			}
			if($posttime1 && empty($posttime2)){				
				$list_where = "`posttime` > '$posttime1'";
			}
			if($posttime2 && empty($posttime1)){				
				$list_where = "`posttime` < '$posttime2'";
			}
			if(empty($posttime1) && empty($posttime2)){				
				$list_where = false;
			}			
			
			if(!empty($sosotext)){			
				if($sotype == 'cateid'){
					$sosotext = intval($sosotext);
					if($list_where)
						$list_where .= "AND `cateid` = '$sosotext'";
					else
						$list_where = "`cateid` = '$sosotext'";
				}
				if($sotype == 'catename'){
					$sosotext = htmlspecialchars($sosotext);
					$info = $this->db->YOne("SELECT * FROM `@#_fenlei` where `name` = '$sosotext' LIMIT 1");
					
					if($list_where && $info)
						$list_where .= "AND `cateid` = '$info[cateid]'";
					elseif ($info)
						$list_where = "`cateid` = '$info[cateid]'";
					else
						$list_where = "1";
				}
				if($sotype == 'title'){
					$sosotext = htmlspecialchars($sosotext);
					$list_where = "`title` = '$sosotext'";
				}
				if($sotype == 'id'){
					$sosotext = intval($sosotext);
					$list_where = "`id` = '$sosotext'";
				}
			}else{
				if(!$list_where) $list_where='1';					
			}		
		}	
		
		$num=20;
		$zongji = $this->db->YCount("SELECT COUNT(*) FROM `@#_areaad` WHERE $list_where");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){
			$fenyenum=$_GET['p'];
		}else{
			$fenyenum=1;
		}	
		$fenye->config($zongji,$num,$fenyenum,"0");
		$notice_list = $this->db->YPage("SELECT * FROM `@#_areaad` WHERE $list_where order by `sort` DESC,`id` DESC",array("num"=>$num,"page"=>$fenyenum,"type"=>1,"cache"=>0));		
		include $this->dwt(DOWN_M,'areaad.lists');
	}
}