<?php 
defined('IN_YYS')or exit('Access Denied.');
System::DOWN_App_class('base','member','no');
System::DOWN_App_fun('my','index');
System::DOWN_App_fun('user','index');
System::DOWN_sys_fun('send');
System::DOWN_sys_fun('user');
class settings extends base {
	public function __construct(){ 
		parent::__construct();
		if(DOWN_A!='userphotoup' and DOWN_A!='singphotoup'){
			if(!$this->userinfo)_notemobile("请登录",LOCAL_PATH."/mobile/user/login",3);
		}
	}

	/*消费明细*/
	public function cost(){
		$header = '我的消费明细，每笔清清楚楚。';
		$biaoti="消费明细";
		include templates("mobile/settings","cost");
	}
	
	/*提现明细*/
	public function withdrawal(){
		$header = '我的消费明细，每笔清清楚楚。';
		$biaoti="提现明细";
		include templates("mobile/settings","withdrawal");
	}
	
	/*充值明细*/
	public function charge(){
		$header = '我的充值明细，每笔仔仔细细。';
		$biaoti="充值明细";
		include templates("mobile/settings","charge");
	}	
	
	/*佣金明细*/
	public function commission(){
		$header = '我的佣金明细，每笔仔仔细细。';
		$biaoti="佣金明细";
		include templates("mobile/settings","commission");
	}
	
	/*积分明细*/
	public function score(){
		$header = '我的充值明细，每笔仔仔细细。';
		$biaoti="积分明细";
		include templates("mobile/settings","score");
	}
	
	/*收货地址*/
	public function addressList(){
		$header = '轻松管理我的收货地址';
		$biaoti="收货地址";
		$provinceList = $this->db->Ylist("select * from `@#_address_info` where `province` = '0'");
		$orderId = $this->segment(4);
		$goto = htmlspecialchars($_GET['goto']);
		include templates("mobile/settings","addressList");
	}
	
	/*新建地址*/
	public function newAddress(){
		$header = '轻松管理我的收货地址';
		$biaoti="新建地址";
		$provinceList = $this->db->Ylist("select * from `@#_area` where `parentId` = '0'");
		$goto = htmlspecialchars($_GET['goto']);
		include templates("mobile/settings","address");
	}
	
	/*用户定位列表*/
	public function location(){
		$header = '轻松管理我的定位地址';
		$biaoti="用户定位列表";
		
		$huiyuan = $this->userinfo;
		$provinceList = $this->db->Ylist("select * from `@#_area` where `parentId` = '0'");
		$goto = htmlspecialchars($_GET['goto']);
		
		include templates("mobile/settings","location");
	}


	//定位设置
	public function set(){
		$p_key = $this->segment(4);
		$p_val = intval($this->segment(5));
		$huiyuan = $this->userinfo;
		if(empty($p_key) || empty($p_val)){
			_note("设置失败");
		}
		//var_dump($ss[sd_userid]);
		//exit;
		$query = true;
		switch($p_key){
				case 'location':
				$query = $this->db->Query("UPDATE `@#_yonghu` SET `locationId` = '$p_val' where `uid` = '$huiyuan[uid]'");
				$location=$this->db->YOne("SELECT * FROM `@#_yonghu_location` where `id` = '$p_val'");
				break;	
		}
		
		if($query){
	//	_note("为您跳转到该区域","/mobile/local/cate");
		header("location:/mobile/local/cate");
		}else{
			_note("设置失败，请重试或检查");
		}
		
		
	}

	/*银行卡号*/
	public function bankList(){
		$biaoti="银行卡号";
		$header = '轻松管理我的卡号账号<br/><div style="margin-top:-10px;">工作人员不会向您索取密码和验证信息，请注意安全！</div>';
		include templates("mobile/settings","bankList");
	}
	
	/*手机绑定*/
	public function mobileBind(){
		$biaoti="手机绑定";
		$header = '轻松管理我的卡号账号<br/><div style="margin-top:-10px;">工作人员不会向您索取密码和验证信息，请注意安全！</div>';
		include templates("mobile/settings","mobileBind");
	}

	/*修改密码*/
	public function password(){
		$biaoti="修改密码";
		$header = "请输入原密码，修改后妥善保管";
		$huiyuan = $this->userinfo;
		include templates("mobile/settings","password");
	}
	
	/*修改头像*/
	public function useravatar(){
		$biaoti="修改头像";
		$huiyuan = $this->userinfo;
		include templates("mobile/settings","useravatar");
	}
	
	/*帮助反馈 移动到根控制器，不需要权限打开 */  

	
}