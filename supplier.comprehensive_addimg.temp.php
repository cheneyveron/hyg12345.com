<?php defined('G_IN_ADMIN')or exit('Access Denied.'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>新建商户拍图</title>
<link rel="stylesheet" href="/statics/plugin/mstyle/css/style.css" type="text/css"><!--页面CSS-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/animate.min.css" type="text/css"><!--CSS3动画-->
<script type="text/javascript" src="/statics/plugin/mstyle/js/jquery.min.js"></script><!--jQuery库-->
<script src="<?php echo YYS_GLOBAL_STYLE; ?>/global/js/jquery-1.8.3.min.js"></script>
<script src="<?php echo YYS_PLUGIN_PATH; ?>/uploadify/api-uploadify.js" type="text/javascript"></script> 
</head>
<body>
	<div class="container">
		<div class="path">
			<i><a href="#"><img src="/statics/plugin/mstyle/img/ico_1.png" alt=""></a></i>
			<p>
				当前位置：<a href="#">商户综合</a> > <a href="#">新建商户拍图</a> >
		  </p>
			<div class="push">
				<a href="<?php echo YYS_MODULE_PATH; ?>/index/Tdefault">欢迎界面</a>
				<a href="javascript:void(0)" onclick="location=location">刷新框架</a>
				<a href="<?php echo YYS_MODULE_PATH; ?>/cache/init">清理缓存</a>
			</div>
		</div><!-- path -->
<form method="POST" action="#" enctype="multipart/form-data">
		<div class="p-card">
			<div class="p-box">
				<h2><a  href="#">新建商户拍图</a><a class="afterLeft" href="<?php echo YYS_MODULE_PATH; ?>/supplier/comprehensive_img/">商户拍图列表</a></h2>
				<ul>
					<li class="lar">
						<span>图片标题</span>
						<input name="title" class="inpu" type="text" placeholder="输入图片标题" value="<?php if($uid_url) echo $pat['notice']; ?>">
					</li>
					<li>
						<span>创建时间</span>
						<input name="time" type="text" value="<?php if($uid_url && $pat['time']) echo date("Y-m-d H:i:s",$pat['time']); else echo date("Y-m-d H:i:s",time()); ?>">
						<var>浏览量</var>
						<input name="hit" class="inpu1" type="text" value="<?php if($uid_url) echo $pat['hit']; ?>">
						<var>商户ID</var>
						<input name="sid" class="inpu1" type="text" value="<?php if($uid_url) echo $pat['sid']; ?>">
					</li>
					<li>
						<div class="pic"><img src="<?php echo YYS_UPLOADS_PATH.'/'.$pat['img']; ?>" alt=""></div>
						<input class="lj" type="text" id="img" name="img" value="<?php echo $pat['img']; ?>">
<input type="button" class="button" onClick="GetUploadify('<?php echo LOCAL_PATH; ?>','uploadify','头像上传','image','touimg',1,500000,'img')" value="上传头像" />
					</li>
				</ul>
			</div>
			<div class="save">
				<button class="page-but" name="savesubmit" type="submit">保存</a>
			</div>
		</div><!-- d-card -->
	</form>
	</div><!-- container -->
</body>
<script src="/statics/plugin/mstyle/js/library-3.28.js"></script><!--自定义封装函数-->
<script src="/statics/plugin/mstyle/js/scrollanim.min.js"></script><!--动画效果库-->
</html>
