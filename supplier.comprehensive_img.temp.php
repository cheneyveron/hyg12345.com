<?php defined('G_IN_ADMIN')or exit('Access Denied.'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>商户拍图列表</title>
<link rel="stylesheet" href="/statics/plugin/mstyle/css/style.css" type="text/css"><!--页面CSS-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/animate.min.css" type="text/css"><!--CSS3动画-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/timer.css" type="text/css"><!--CSS3动画-->
<script type="text/javascript" src="/statics/plugin/mstyle/js/jquery.min.js"></script><!--jQuery库-->
</head>
<body>
	<div class="container min-wid">
		<div class="path">
			<i><a href="#"><img src="/statics/plugin/mstyle/img/ico_1.png" alt=""></a></i>
			<p>
				当前位置：<a href="#">商户综合</a> > <a href="#">商户拍图列表</a> >
			</p>
			<div class="push">
				<a href="<?php echo YYS_MODULE_PATH; ?>/index/Tdefault">欢迎界面</a>
				<a href="javascript:void(0)" onclick="location=location">刷新框架</a>
				<a href="<?php echo YYS_MODULE_PATH; ?>/cache/init">清理缓存</a>
			</div>
		</div><!-- path -->
		<div class="apicture wid1">
			<div class="pict">
				<img src="/statics/plugin/mstyle/img/pic_02.png" alt="">
			</div>
			<div class="fl-box">
			<form method="GET" action="#">
			<?php 
				if($uid_url){
					echo '<var>商户编号</var>
					<input type="text" value="'.$supplier["id"].'" disabled>
					<var>店铺名称</var>
					<input class="lar" type="text" value="'.$supplier["name"].'" disabled>';
				}else if($uid_req){
					echo '<var>商户编号</var>
					<input type="text" name="uid" value="'.$supplier["id"].'">
					<var>店铺名称</var>
					<input class="lar" type="text" name="username" value="'.$supplier["name"].'" disabled>
					<button type="submit" name="uidsubmit" value="筛选">筛选</button>';
				}else{
					echo '<var>商户编号</var>
					<input type="text" name="uid" value="">
					<var>店铺名称</var>
					<input class="lar" type="text" name="username" value="" disabled>
					<button type="submit" name="uidsubmit" value="筛选">筛选</button>';
				}
				?>
				<div class="grate-date">
					邀请日期
					<input id="in1" type="text" name="start_time" value='<?php if(isset($_REQUEST["start_time"])) echo $_REQUEST["start_time"]; else echo "2018-1-1"; ?>' class="date_picker">
					至
					<input id="in2" type="text" name="end_time" value='<?php if(isset($_REQUEST["end_time"])) echo $_REQUEST['end_time']; else echo '2018-2-1'; ?>'  class="date_picker">
				</div>
				<button type="submit" name="sososubmit" value="筛选">筛选</button>
				<?php if($uids){ ?>
					<var class="v0">拍图功能</var>
					<select name="paitu">
						<option value="1" <?php if($supplier['isImg'] == 1) echo "selected"; ?>>开启</option>
						<option value="0" <?php if($supplier['isImg'] == 0) echo "selected"; ?>>关闭</option>
					</select>
					<button type="submit" name="paitusubmit" value="筛选">保存</button>
				<?php } ?>
				<a href="<?php echo YYS_MODULE_PATH; ?>/supplier/comprehensive_addimg">新建</a>
			</form>
			</div>
			<div class="fr-box">
				<span><img src="/statics/uploads/<?php echo $supplier["icon"]; ?>" alt=""></span>
			</div>
		</div>
		<div class="apicture-table dt wid1">
			<ul>
				<li class="head">
					<span>PTID</span>
					<span>图片标题</span>
					<span>浏览量</span>
					<span>创建时间</span>
					<span>拍图</span>
					<span>置顶</span>
					<span>编辑</span>
					<span>删除</span>
				</li>

				<?php foreach($yyslist as $v){ ?>
					<li>
						<span><?php echo $v['id']; ?></span>
						<span><?php echo $v['notice']; ?></span>
						<span><?php echo $v['hit']; ?></span>
						<span><?php echo date("Y-m-d H:i:s",$v['time']); ?></span>
						<span><i><a target="_blank" href="/statics/uploads/<?php echo $v['img']; ?>"><img style="max-height:36px;max-width:70px;" src="/statics/uploads/<?php echo $v['img']; ?>"></a></i></span>
						<span><a href="<?php echo YYS_MODULE_PATH; ?>/supplier/comprehensive_img/top/<?php echo $v['id']; ?>" class="ico-top"></a></span>
						<span><a href="<?php echo YYS_MODULE_PATH; ?>/supplier/comprehensive_addimg/<?php echo $v['id']; ?>" class="ico-edit"></a></span>
						<span><a href="<?php echo YYS_MODULE_PATH; ?>/supplier/comprehensive_img/del/<?php echo $v['id']; ?>" class="ico-del"></a></span>
					</li>
				<?php }	?>
			</ul>
		</div>
		<div class="pages">
			<span>
				<?php echo $fenye->show('new')['html']; ?>
			</span>
			<span>共 <?php echo ceil($zongji/$num); ?> 页</span>
		
			<span>到第<input type="text" id="page" value="">页</span>
			<span><a class="page-but" id="gopage" href="javascript:void(0);">确定</a></span>
		</div>
	</div><!-- container -->
</body>
<script src="/statics/plugin/mstyle/js/jquery.date_input.pack.js"></script>
<script src="/statics/plugin/mstyle/js/library-3.28.js"></script><!--自定义封装函数-->
<script src="/statics/plugin/mstyle/js/scrollanim.min.js"></script><!--动画效果库-->
<script>
	$("#gopage").click(function(){
		var page = $("#page").val();
		var allpage = "<?php echo ceil($zongji/$num); ?>";
		if(page == '' || allpage < page){
			var page = 1;
		}
		location.href = "<?php echo $fenye->show('new')['url']; ?>"+page;
	});
</script>
</html>
