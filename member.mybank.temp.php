<?php defined('G_IN_ADMIN')or exit('Access Denied.'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>会员钱包明细</title>
<link rel="stylesheet" href="/statics/plugin/mstyle/css/style.css" type="text/css"><!--页面CSS-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/animate.min.css" type="text/css"><!--CSS3动画-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/timer.css" type="text/css"><!--CSS3动画-->
<script type="text/javascript" src="/statics/plugin/mstyle/js/jquery.min.js"></script><!--jQuery库-->
</head>
<body>
	<div class="container min-wid">
		<div class="path">
			<i><a href="#"><img src="/statics/plugin/mstyle/img/ico_1.png" alt=""></a></i>
			<p>
				当前位置：<a href="#">财务管理</a> > <a href="#">会员钱包明细</a> >
			</p>
			<div class="push">
				<a href="<?php echo G_ADMIN_PATH; ?>/index/Tdefault">欢迎界面</a>
				<a href="javascript:void(0)" onclick="location=location">刷新框架</a>
				<a href="<?php echo G_ADMIN_PATH; ?>/cache/init">清理缓存</a>
			</div>
		</div><!-- path -->
		<div class="wallet wid1">
			<div class="pict">
				<img src="/statics/plugin/mstyle/img/pic_02.png" alt="">
			</div>
			<div class="fl-box">
			<form method="GET" action="#">
				<?php 
				if($uid_url){
					echo '<var>会员ID</var>
					<input type="text" value="'.$thisYonghu["uid"].'" disabled>
					<var>会员昵称</var>
					<input class="lar" type="text" value="'.$thisYonghu["username"].'" disabled>';
				}else if($uid_req){
					echo '<var>会员ID</var>
					<input type="text" name="uid" value="'.$thisYonghu["uid"].'">
					<var>会员昵称</var>
					<input class="lar" type="text" name="username" value="'.$thisYonghu["username"].'" disabled>
					<button type="submit" name="uidsubmit" value="筛选">筛选</button>';
				}else{
					echo '<var>会员ID</var>
					<input type="text" name="uid" value="">
					<var>会员昵称</var>
					<input class="lar" type="text" name="username" value="" disabled>
					<button type="submit" name="uidsubmit" value="筛选">筛选</button>';
				}
				?>
				<div class="grate-date">
					查询日期
					<input id="in1" type="text" name="start_time" value='<?php if(isset($_REQUEST["start_time"])) echo $_REQUEST["start_time"]; else echo "2018-1-1"; ?>' class="date_picker">
					至
					<input id="in2" type="text" name="end_time" value='<?php if(isset($_REQUEST["end_time"])) echo $_REQUEST['end_time']; else echo '2018-2-1'; ?>'  class="date_picker">
				</div>
				<span>类型:
					<select name="type" id="type">
						<option value="0">全部</option>
						<option value="1" <?php if($_REQUEST["type"]=='1') echo "selected" ?>>增加</option>
						<option value="-1" <?php if($_REQUEST["type"]=='-1') echo "selected" ?>>减少</option>
					</select>
				</span>
				<button type="submit" name="sososubmit" value="筛选">筛选</button>
			</form>
			</div>
			<div class="fr-box">
			<span>钱包余额<i><?php echo $qianbao; ?></i>元，可提货款<i><?php echo $keti; ?></i>元</span>
			<span>共<font color="#ff0000">+ </font><i><?php echo $zengjia; ?></i>元，<font color="#3399FF">- </font><i><?php echo $jianshao; ?></i>元</span>
			</div>
		</div><!-- nickname -->
		<div class="wallet-table dt wid1">
			<ul>
				<li class="head">
					<span>会员ID</span>
					<span>会员昵称</span>
					<span>来源类型</span>
					<span>往来金额</span>
					<span>发生时间</span>

				</li>
<?php 
		//foreach($recharge as $v){ 
		for($j=0;$j<count($recharge);$j++){
		?>
				<li>
					<span><?php echo $recharge[$j]['uid']; ?></span>
					<span><?php  echo $huiyuans[$j];?></span>
					<span><?php echo $recharge[$j]['content']; ?></span>
					<span>
					
<?php
if($recharge[$j]['type']==1){
echo '<font color="#ff0000">+ </font>';
}
?>
<?php
if($recharge[$j]['type']==-1){
echo '<font color="#3399FF">- </font>';
}
?>					
					
					<?php echo $recharge[$j]['money']; ?></span>
					<span><?php echo date("Y-m-d H:i:s",$recharge[$j]['time']); ?></span>
				</li><?php }  ?>
			</ul>
		</div>
		<div class="pages">
			<span>
				<?php echo $fenye->show('new')['html']; ?>
			</span>
			<span>共 <?php echo ceil($zongji/$num); ?> 页</span>
		
			<span>到第<input type="text" id="page" value="">页</span>
			<span><a class="page-but" id="gopage" href="javascript:void(0);">确定</a></span>
		</div>
	</div><!-- container -->
</body>
<script src="/statics/plugin/mstyle/js/jquery.date_input.pack.js"></script>
<script src="/statics/plugin/mstyle/js/library-3.28.js"></script><!--自定义封装函数-->
<script src="/statics/plugin/mstyle/js/scrollanim.min.js"></script><!--动画效果库-->
<script>
	$(".ofgoods-table ul li span .ico-add").click(function(event) {
		$(".al-box").show();
	});
	$(".al-box .save a").click(function(event) {
		$(".al-box").hide();
	});
	$("#gopage").click(function(){
		var page = $("#page").val();
		var allpage = "<?php echo ceil($zongji/$num); ?>";
		if(page == '' || allpage < page){
			var page = 1;
		}
		location.href = "<?php echo $fenye->show('new')['url']; ?>"+page;
	});
</script>
</html>
