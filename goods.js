/*
*	商品相关
*/

$(".qa-tags").find('li').click(function(){
	$(this).addClass('acti').siblings().removeClass('acti');
	var datatype = $(this).attr('datatype');
	var dataId = $(this).attr('dataId');
	ajax_index_item(datatype,dataId);
});


/*总参与记录*/
function ajax_index_item(datatype,dataId){
	layer.open({type: 2});
	$.ajax({
		type: "POST",
		url: "/mobile/ajax_item/ajax_index_item/",
		dataType:'json',
		data:{
			dataId:dataId,
			datatype:datatype
		},
		success: function(data){
			var html = '';
			if(data.status == 1){
				html = '<ul>';
				$.each(data.list,function (index,obj){
					html += '<li>';
					html += '	<div class="pic">';
					html += '		<img src="'+YYS_UPLOADS_PATH+'/'+obj.uphoto+'" alt="">';
					html += '	</div>';
					html += '	<div class="text">';
					html += '		<h2>'+obj.username+'</h2>';
					html += '		<p>惠券 <span>'+obj.gonumber+'</span> 人次<i>'+obj.time+'</i></p>';
					html += '	</div>';
					html += '</li>';
				});
				html += '</ul>';
				$(".qa-recording").html(html);
			}else if(data.status == 2){
				$(".qa-recording").html(data.html);
			}else if(data.status == 3){
				var html = '<div class="cash-coupon dt">';
					html += '	<ul>';
					html += '		<li>';
					html += '			<div class="pict" style="margin-top: 0.6rem;margin-bottom: 0.4rem;"><a title="'+data.shopinfo.title+'" href="' + LOCAL_PATH + '/mobile/mobile/item/'+data.shopinfo.id+'">';
					html += '				<img src="' + YYS_UPLOADS_PATH +'/'+data.shopinfo.thumb+'" alt="">';
					html += '			</a></div>';
					html += '			<div class="text">';
					html += '				<p>'+data.shopinfo.canyurenshu+'<var>'+data.shopinfo.zongrenshu+'</var></p>';
					html += '				<span><i style="width:'+data.shopinfo.percent+'%"></i></span>';
					html += '			</div>';
					html += '		</li>';
					html += '	</ul>';
					html += '	<div class="boxdow"><img src="' + YYS_TEMPLATES_CSS + '/images/pic_19.png" alt=""></div>';
					html += '</div>';
					html += '<div class="card-coupons">';
					html += '<p>1、幸运者获得购物卡券，只能在本店消费购物，全网不通用。</p>';
					html += '<p>2、下次购买商品时选择抵消你所获得的【惠券购物券】，商品金额必须大于卡券面值。</p>';
					html += '<p>3、卡券没有期限，消费时可以选择网购抵消或实体店抵消两种方式';
					html += '。实体店抵消方式请确认是否是本店卡券，消费后自动抵消。</p>';
					html += '<p>4、可以对本店的服务和商品评价，好评加1分，中评不加分，差评减1分。您的评价对本店很重要，会影响本店的口碑和搜索排序。</p>';
					html += '<p>5、谢谢你的好评，鼓励我们更好的提供服务和产品质量。</p>';
					html += '</div>'
					$(".qa-recording").html(html);
			}else{
				html += '<ul><li class="nolist">暂无参与记录</li></ul>';
				$(".qa-recording").html(html);
			}
			layer.closeAll()
		}
	}); 
}


/*所有商品*/
$(".all-pro").find('li').click(function(){
	$(this).addClass('cur2').siblings().removeClass('cur2');
	var datatype = $(this).attr('datatype');
	var dataId = $("#cateid").val();
	ajax_index_goods(datatype,dataId,'',Number(getCookie('lng')),Number(getCookie('lat')));
	if(datatype == 'priceUp'){
		$("#priceSelect").attr('datatype','priceDown');
	}else{
		$("#priceSelect").attr('datatype','priceUp');
	}
});

$("#glistCate").find("a").click(function(){
	$(this).addClass('acti').siblings().removeClass('acti');
	var datatype = 'brand';
	$("li[datatype='all']").removeClass('acti');
	var dataId = $(this).attr('dataId');
	$("#cateid").val(dataId);
	ajax_index_goods(datatype,dataId,'',Number(getCookie('lng')),Number(getCookie('lat')));
});

function setCookie(name,value)
{
	var Days = 1;
	var exp = new Date();
	exp.setTime(exp.getTime() + Days*24*60*60*1000);
	document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
}

function getCookie(name)
{
	var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
	if(arr=document.cookie.match(reg))
	return unescape(arr[2]);
	else
	return null;
}

function delCookie(name)
{
	var exp = new Date();
	exp.setTime(exp.getTime() - 1);
	var cval=getCookie(name);
	if(cval!=null)
	document.cookie= name + "="+cval+";expires="+exp.toGMTString();
}

function ajax_index_goods(datatype,dataId,keyword,lng,lat,fromdb="no"){
	layer.open({type:2,shade:'background-color: rgba(0,0,0,.3)',content:'玩命加载中...'});
	$("#type").val(datatype);
	$.ajax({
		type: "POST",
		url: "/mobile/ajax_goods/ajax_index_goods/",
		dataType:'json',
		data:{datatype:datatype,cateid:dataId,lng:lng,lat:lat,keyword:keyword,fromdb:fromdb},
		success: function(data){
			if(data.status == 1){
				var html = '';
				$.each(data.list,function (index,obj){
					html += '<li>';
					html += '	<div class="pict">';
					html += '      <a href="/mobile/mobile/goods/'+obj.id+'"><img src="'+YYS_UPLOADS_PATH+'/'+obj.thumb+'" alt=""/></a>';
					html += '   </div>';
					html += '   <div class="text">';
					html += '		<a href="/mobile/mobile/goods/'+obj.id+'"><p>'+obj.title+'</p></a>';
                    html += '        <p><span>'+obj.supplierArea+'</span>'+obj.supplierName+'</p>';
                    html += '        <p><span class="sp0">';
					if(obj.isfreepost == 1){
						html += '包邮';
					}else{
						html += '自提';
					}
					html += '			</span>付款：'+obj.qishu+'人</p>';
                    html += '        <p><span class="sp1">￥<i>'+obj.yunjiage+'</i></span>惠券：￥'+obj.supplierPercent+'</p>';
                    html += '    </div>';
                    html += '    <div class="shoucan">';
					if(obj.collection == 1){
						html += '		<a href="javascript:void(0);" class="acti" onclick="doCollection('+obj.id+',\'goods\',$(this));"><i class="ico-i3"></i></a></div>';
					}else{
						html += '		<a href="javascript:void(0);" onclick="doCollection('+obj.id+',\'goods\',$(this));"><i class="ico-i3"></i></a></div>';
					}
					html += ' </li>';
				});
			}else{
				var html = '';
					html += '<li>暂无商品';
					html += '</li>';
			}
			$(".right-box").find('ul').html(html);
			layer.closeAll()
		}
	}); 
}

/*所有商品结束*/

/*惠券记录*/
$(".gg-jilu").find('li').click(function(){
	var datatype = $(this).attr("datatype");
	$(this).addClass('acti').siblings().removeClass('acti');
	ajaxGetUserBuyList(datatype);
});

function ajaxGetUserBuyList(datatype){
	layer.open({type:2,shade:'background-color: rgba(0,0,0,.1)',content:'加载中...'});
	$.ajax({
		type: "POST",
		url: "/mobile/ajax_user/ajax_user_data/",
		dataType:'json',
		data:{datatype:datatype},
		success: function(data){
			if(data.status == 1){
				var html = '';
				$.each(data.list,function (index,obj){
					html += '<ul>';
					if(datatype == 'going'){
						html += '<li>';
						html += '	<div class="pict">';
						html += '		<a href="'+LOCAL_PATH+'/mobile/mobile/item/'+obj.shopid+'">'; 
						html += '			<img src="'+obj.thumb+'" alt="">';
						html += '		</a>';
						html += '		<div class="get">进行中</div>';
						html += '	</div>';
						html += '	<div class="text">';
						html += '		<p>(第'+obj.shopqishu+'期)'+obj.shopname+'</p>';
						html += '		<b>价值：￥'+obj.money+'</b>';
						html += '		<div class="box">';
						html += '			<strong>';
						html += '				<span>';
						html += '					<i style="width:'+((obj.canyurenshu/obj.zongrenshu)*100)+'%;"></i>';
						html += '				</span>';
						html += '				<var>';
						html += '					'+obj.canyurenshu+' <i>'+obj.zongrenshu+'</i>';
						html += '				</var>';
						html += '			</strong>';
						html += '			<a class="a1" href="'+LOCAL_PATH+'/mobile/supplier/index/'+obj.supplierId+'">';
						html += '				<img src="'+YYS_TEMPLATES_CSS+'/images/10140.png" alt="" />';
//						html += '			</a>';
//屏蔽收藏						html += '	 		<a class="keep" href="javascript:void(0);" onclick="doCollection('+obj.shopid+',\'item\');">';
//						html += '		 		<img src="'+YYS_TEMPLATES_CSS+'/images/ico_5.png" alt=""/>';
//						html += '			</a>';
						html += '		</div>';
						html += '	</div>';
						html += '</li>'
						$(".gg-product").removeClass('four');
					}else{
						html += '<li>';
						html += '	<div class="pict two">';
						html += ' 		<a href="'+LOCAL_PATH+'/mobile/mobile/item/'+obj.shopid+'">'; 
						html += '			<img src="'+obj.thumb+'" alt="">';
						html += '		</a>';
						html += ' 		<div class="get"> 已揭晓 </div>';
						html += '  	</div>';
						html += ' 	<div class="h1-info">';
						html += '    	<h2>获得者：<span>'+obj.username+'</span></h2>';
						html += '    	<p>众筹参与：'+obj.gonumber+'人次</p>';
						html += '   	<p>幸运众筹码： '+obj.huode+'</p>';
						html += '    	<p>揭晓时间： '+$.myTime.UnixToDate(obj.time,true)+'</p>';
						html += ' 	</div>';
						html += ' 	<div class="h1-avatar">';
						html += '       <img src="'+YYS_UPLOADS_PATH+'/'+obj.uphoto+'" alt=""/>';
						html += ' 	 </div>';
						html += '</li>';
						$(".gg-product").addClass('four');
					}
					html += '</ul>';
				});
			}else{
				var html = '<ul><li style="text-align:center;">';
					html += '还没有记录哦';
					html += '</li></ul>';
			}
			$(".gg-product").html(html);
			layer.closeAll();
		}
	}); 
}
/*惠券记录结束*/


/*最新揭晓*/
function newLottery(){
	layer.open({type: 2});
	$.ajax({
		type: "POST",
		url: "/mobile/ajax_item/getAjaxPage/",
		dataType:'json',
		data:{
			num : 10,
			page: 1,
			functionName : 'newLottery',
		},
		success: function(data){
			if(data.status == 1){
				var html = '';
				$.each(data.list,function (index,obj){
					html += '<li>';
					html += '	<a href="#">';
					html += '		<div class="pict">';
					html += '			<img src="'+YYS_UPLOADS_PATH+'/'+obj.thumb+'" alt="">';
					html += '		</div>';
					html += '		<div class="box1">';
					html += '			<div class="fl">';
					html += '				<b>获得者：<i>'+obj.username+'</i></b>';
					html += '				<p>众筹参与：'+obj.gonumber+'人次</p>';
					html += '				<p>幸运众筹码： '+obj.q_user_code+'</p>';
					html += '				<p>揭晓时间：'+obj.q_end_time+'</p>';
					html += '			</div>';
					html += '			<div class="fr">';
					html += '				<img src="'+YYS_UPLOADS_PATH+'/'+obj.img+'" alt="">';
					html += '			</div>';
					html += '		</div>';
					html += '	</a>';
					html += '</li>';
				});
				$(".new-list-pro").find('ul').html(html);
				layer.closeAll();
			}else{
				layer.closeAll();
				layer.open({
					content:data.msg
					,skin: 'msg'
					,time: 2
					,shade :true
				});
			}
		}
	});
}

function searchGoods(){
	var cateid = $("#cateid").val();
	var type = $("#type").val();
	var keyword = $("#keyword").val();
	if(!type){
		var type = 'all';
	}
	ajax_index_goods(type,cateid,keyword,Number(getCookie('lng')),Number(getCookie('lat')));
}
