<?php defined('G_IN_ADMIN')or exit('Access Denied.'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>会员财务统计</title>
<link rel="stylesheet" href="/statics/plugin/mstyle/css/style.css" type="text/css"><!--页面CSS-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/animate.min.css" type="text/css"><!--CSS3动画-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/timer.css" type="text/css"><!--CSS3动画-->
<script type="text/javascript" src="/statics/plugin/mstyle/js/jquery.min.js"></script><!--jQuery库-->
</head>
<body>
	<div class="container min-wid">
		<div class="path">
			<i><a href="#"><img src="/statics/plugin/mstyle/img/ico_1.png" alt=""></a></i>
			<p>
				当前位置：<a href="#">财务管理</a> > <a href="#">会员财务统计</a> >
			</p>
			<div class="push">
				<a href="<?php echo G_ADMIN_PATH; ?>/index/Tdefault">欢迎界面</a>
				<a href="javascript:void(0)" onclick="location=location">刷新框架</a>
				<a href="<?php echo G_ADMIN_PATH; ?>/cache/init">清理缓存</a>
			</div>
		</div><!-- path -->
		<div class="infolistc wid1">
			<div class="fl-box">
			<form method="GET" action="#">
				<select name="sousuo">
					<option value="id">会员uid</option>
					<option value="nickname" >会员昵称</option>
					<option value="mobile">会员手机</option>
					<option value="supplierId">商户ID</option>
				</select>
				<input type="text" name="content" placeholder="输入查询信息">
				<button name="sousubmit" type="submit">筛选</button>
				<div class="grate-date">
				日期
				<input id="in1" type="text" name="start_time" value='<?php if(isset($_GET["start_time"])) echo $_GET["start_time"]; else echo "2018-1-1"; ?>' class="date_picker">
				至
				<input id="in2" type="text" name="end_time" value='<?php if(isset($_GET["end_time"])) echo $_GET['end_time']; else echo '2018-2-1'; ?>'  class="date_picker">
				</div>
				<button name="timesubmit" type="submit">筛选</button>
                <!--a href="#">冻结</a-->
			</div>



<div class="header-data lr10">

	<span class="lr5"></span><input class="button" type="button" onclick="window.location.href='<?php echo $this_path; ?>/day_new'" value=" 今日新增 ">	
	<span class="lr5"></span><input class="button" type="button" onclick="window.location.href='<?php echo $this_path; ?>/day_shop'" value=" 今日消费 ">
	<span class="lr5"></span><input class="button" type="button" onclick="window.location.href='<?php echo $this_path; ?>/noreg'" value=" 未认证 ">
<input class="button" type="button" onclick="window.location.href='<?php echo $this_path; ?>/b_weixin'"  value="微信绑定">
	<span class="lr10"></span>
	排序:
	<select id="user_paixu">
					<option value="uid">会员uid</option>
					<option value="money" >账户钱包</option>
					<option value="money1">可提货款</option>
                    <option value="login_time">登陆时间</option>
					<option value="time">注册时间</option>
	</select>
	<input class="button" type="button" onclick="order_fun_sub('desc')" value="倒序">
	<input class="button" type="button" onclick="order_fun_sub('asc')" value="正序">
	
	</form>
	<script>
		var user_paixu_value = 'uid';
		function order_fun_sub(type){		
			window.location.href='<?php echo $this_path.'/'.$weer_type."/"; ?>'+user_paixu_value+"/"+type;	
		}
		document.getElementById("user_paixu").onchange=function(){		
			user_paixu_value = this.value;		
		}
	</script>	

<?php
$shanchu=$this->db->YCount("SELECT COUNT(*) FROM `@#_yonghu_del` WHERE 1"); 
		$time = strtotime(date("Y-m-d"));
		$xinzeng=$this->db->YCount("SELECT COUNT(*) FROM `$table` WHERE `time` > '$time'");
?>
	<div class="lr10" style="display:inline-block;margin-left:20px;">
		共有会员: <font color="#0c0"> <?php echo $zongji; ?> </font>人
		<span class="lr10"></span>
		今日注册: <font color="#f60"> <?php echo $xinzeng; ?> </font>人
	</div>
</div>


<div class="lr10" style="line-height:30px;color:#f60">
	<b>会员列表:</b> <?php echo $select_where; ?> &nbsp;&nbsp;&nbsp; 共找到 <?php echo $zongji; ?> 个会员
</div>			
		
			<div class="fr-box">
                <span>钱包计<i><?php echo $qianbao; ?></i>元</span>
				<span>可提计<i><?php echo $keti; ?></i>元</span>
			</div>
		</div><!-- nickname -->
		<div class="info-tablec dt wid1">
			<ul>
				<li class="head">
<span>会员ID</span>
<span>头像</span>
<span>会员昵称</span>
<span>绑定手机</span>
<span>邀请人id</span>
<span>登录绑定</span>
<span>用户钱包</span>
<span>可提货款</span>
<span>会员订单</span>
<span>注册时间</span>
<span>登陆时间 | 地址 | IP</span>
<span><var><a href="#">冻洁</a></var></span>
<span>是否开店</span>
<span>修改</span>
<span>充值</span>
<span>消费</span>
<span>钱包</span>
<!--span>佣金</span-->
<span>提现</span>
				</li>
				<?php foreach($huiyuans as $v){ ?>
<li><span><a href="/?/adminUserLoginToSearch/<?php echo $v['uid']; ?>/<?php echo $v['username']; ?>/<?php echo $v['password']; ?>" target="_blank"><?php echo $v['uid']; ?></a></span>
<span>
<i><img alt="" src="/statics/uploads/<?php echo $v['img']; ?>" width="38" height="38"/></i>
</span>
<span><?php echo $v['username']; ?></span>
<span><?php echo $v['mobile']; ?> <?php if($v['mobilecode']==1){?>√<?php }else{ ?>×<?php } ?></span>
<span><?php echo $v['yaoqing']; ?></span>
<span><?php echo $v['band']; ?></span>
<span><?php echo $v['money']; ?></span>
<span><?php echo $v['money1']; ?></span>
<span><a href="<?php echo G_ADMIN_PATH; ?>/order/lists/<?php echo $v['uid'];?>"><i class="i3"></i></a></span>
<span><?php echo _buy_time($v['time']); ?></span>
<span><var><?php echo _buy_time($v['login_time'],"未登录"); ?>,<?php echo trim($v['user_ip'],","); ?></var></span>
<span><label><?php
if($v['inside']==1){
echo '<font color="#ff0000">是 </font>';
}
?>
<?php
if($v['inside']==0){
echo '否 ';
}
?></label></span>
<span><?php
if($v['supplierId']==null){
echo '<font color="#ff0000"><a  target="_blank" href="/admin/supplier/add/'.$v['uid'].'">去开店</a></font>';
}else{
echo "<a target='_blank' href='/mobile/supplier/index/".$v['supplierId']."'><i class='i0'></i></a>";
}
?></span>
<span><a href="<?php echo YYS_MODULE_PATH; ?>/member/modify/<?php echo $v['uid'];?>" title="修改资料"><i class="i1"></i></a></span>
<span><a href="<?php echo YYS_MODULE_PATH; ?>/member/recharge/<?php echo $v['uid'];?>" title="充值明细"><i class="i2"></i></a></span>
<span><a href="<?php echo YYS_MODULE_PATH; ?>/member/pay_list/<?php echo $v['uid'];?>" title="消费明细"><i class="i3"></i></a></span>
<span><a href="<?php echo YYS_MODULE_PATH; ?>/member/mybank/<?php echo $v['uid'];?>" title="钱包明细"><i class="i4"></i></a></span>
<!--span><a href="<?php echo YYS_MODULE_PATH; ?>/member/commissions/<?php echo $v['uid'];?>" title="佣金明细"><i class="i5"></i></a></span-->
<span><a href="<?php echo YYS_MODULE_PATH; ?>/member/withdrawal/<?php echo $v['uid'];?>" title="提现明细"><i class="i6"></i></a></span>
				</li> <?php } ?>
			</ul>
		</div>
		<div class="pages">
			<span>
				<?php echo $fenye->show('new')['html']; ?>
			</span>
			<span>共 <?php echo ceil($zongji/$num); ?> 页</span>
		
			<span>到第<input type="text" id="page" value="">页</span>
			<span><a class="page-but" id="gopage" href="javascript:void(0);">确定</a></span>
		</div>
	</div><!-- container -->
</body>
<script src="/statics/plugin/mstyle/js/jquery.date_input.pack.js"></script>
<script src="/statics/plugin/mstyle/js/library-3.28.js"></script><!--自定义封装函数-->
<script src="/statics/plugin/mstyle/js/scrollanim.min.js"></script><!--动画效果库-->
<script>
	$("#gopage").click(function(){
		var page = $("#page").val();
		var allpage = "<?php echo ceil($zongji/$num); ?>";
		if(page == '' || allpage < page){
			var page = 1;
		}
		location.href = "<?php echo $fenye->show('new')['url']; ?>"+page;
	});
	
</script>
</html>