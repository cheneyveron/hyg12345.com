<?php

final class System {

	public static function DOWN_sys_class($class_name='',$module='sys',$new='yes'){
	
			static $classes = array();
			$path=self::DOWN_Class_file_name($class_name,$module);
			//var_dump($path);
			$key=md5($class_name.$path.$new);		
			if (isset($classes[$key])) {
				return $classes[$key];
			}
			if(file_exists($path)){
				include_once $path;
				if($new=='yes'){
					$classes[$key] = new $class_name;		
				}else{					
					$classes[$key]=true;
				}				
				return $classes[$key];			
			}else{
				_error('加载文件: '.$module." / ".$class_name,'抱歉，文件不存在哦');
			}
				
	}
	
	/***************************************************************/
	/***************************************************************/
	/***************************************************************/
	//调用模块应用类
	public static function DOWN_App_class($class_name='',$module='',$new='yes'){
			if(empty($module)){
				$module=DOWN_M;
			}
			return self::DOWN_sys_class($class_name,$module,$new);
	}

	/***************************************************************/
	/***************************************************************/
	/***************************************************************/	
	public static function DOWN_Class_file_name($class_name='',$module='sys'){		
		static $filename = array();
		if(isset($filename[$module.$class_name])) return $filename[$module.$class_name];
		if($module=='sys'){
		 	$filename[$module.$class_name]=YYS_MANAGE.'apps'.DIRECTORY_SEPARATOR.$class_name.'.class.php';
		}else if($module!='sys'){
			$filename[$module.$class_name]=YYS_MANAGE.'class'.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR."control".DIRECTORY_SEPARATOR.$class_name.'.class.php';
		}else{
			return $filename[$module.$class_name];
		}
		//var_dump($filename[$module.$class_name]);
		return $filename[$module.$class_name];
	}
		
	
	/***************************************************************/
	/***************************************************************/
	/***************************************************************/	
	public static function DOWN_sys_config($filename,$keys=''){
		static $configs = array();
		if(isset($configs[$filename])){
			if (empty($keys)) {
				return $configs[$filename];
			} else if (isset($configs[$filename][$keys])) {
				return $configs[$filename][$keys];
			}else{
				return $configs[$filename];
			}
		}	
		//fix by dabin
		//var_dump(YYS_CONFIGS);
		if (file_exists(YYS_CONFIGS.$filename.'.inc.php')){
				$configs[$filename]=include YYS_CONFIGS.$filename.'.inc.php';
				if(empty($keys)){
					return $configs[$filename];
				}else{
					return $configs[$filename][$keys];
				}
		}
		
		_error('load system config file: '.$filename,'抱歉，文件不存在哦');	
	}
		
	/***************************************************************/
	/***************************************************************/
	/***************************************************************/	
	public static function DOWN_App_config($filename,$keys='',$module=''){
		static $configs = array();	
		if(isset($configs[$filename])){
			if (empty($keys)) {
				return $configs[$filename];
			} else if (isset($configs[$filename][$keys])) {
				return $configs[$filename][$keys];
			}else{
				return $configs[$filename];
			}
		}
		if(empty($module))$module=DOWN_M;
		$path=YYS_MANAGE.'class'.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'control'.DIRECTORY_SEPARATOR.$filename.'.ini.php';	
		if (file_exists($path)){
				$configs[$filename]=include $path;
				if(empty($keys)){
					return $configs[$filename];
				}else{
					return $configs[$filename][$keys];
				}
		}	
		_error('load app config file: '.$module." / ".$filename,'抱歉，文件不存在哦');			
	}
	/***************************************************************/
	/***************************************************************/
	/***************************************************************/
	//函数文件名和所在模块
	public static function DOWN_sys_fun($fun_name){
		static $funcs = array();
		$path=YYS_MANAGE.'fo'.DIRECTORY_SEPARATOR.$fun_name.'.fo.php';	
		
		$key = md5($path);
		if (isset($funcs[$key])) return true;
		if (file_exists($path)){
			$funcs[$key] = true;
			return include $path;
		}else{
			$funcs[$key] = false;
			_error('load system function file: '.$fun_name,'抱歉，文件不存在哦');
		}
	
	}
	
	
	/***************************************************************/
	/***************************************************************/
	/***************************************************************/
	//函数文件名和所在模块
	public static function DOWN_App_fun($fun_name,$module=null){
		static $funcs = array();		
		if(empty($module)){
			$module=DOWN_M;
		}
		$path=YYS_MANAGE.'class'.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'control'.DIRECTORY_SEPARATOR.$fun_name.'.fun.php';
		
		$key = md5($path);
		if (isset($funcs[$key])) return true;
		if (file_exists($path)){
			$funcs[$key] = true;
			return include $path;
		}else{			
			_error('load app function file: '.$module." / ".$fun_name,'抱歉，文件不存在哦');
		}	
	}
	
	/***************************************************************/
	/***************************************************************/
	/***************************************************************/
	//数据模块操作类	
	public static function DOWN_App_model($model_name='',$module='',$new='yes'){			
		static $models=array();
		if(empty($module)){
				$module=DOWN_M;
		}
		$key=md5($module.$model_name.$new);
		if(isset($models[$key])){
			return $models[$key];
		}		
		$path=YYS_MANAGE.'class'.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'model'.DIRECTORY_SEPARATOR.$model_name.'.model.php';
		if (file_exists($path)){
			include $path;
			if($new=='yes'){			
				$models[$key]=new $model_name;				
			}else if($new=='no'){				
				$models[$key]=true;
			}
			return $models[$key];
		}
		_error('load app model file: '.$module." / ".$model_name,'抱歉，文件不存在哦');
		
	}
	/***************************************************************/
	
	public static function CApp(){
		return self::DOWN_sys_class('application');
	}
}

?>