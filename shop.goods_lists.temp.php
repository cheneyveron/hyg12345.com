<?php defined('G_IN_ADMIN')or exit('Access Denied.'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>商品列表</title>
<link rel="stylesheet" href="/statics/plugin/mstyle/css/style.css" type="text/css"><!--页面CSS-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/animate.min.css" type="text/css"><!--CSS3动画-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/timer.css" type="text/css"><!--CSS3动画-->
<script type="text/javascript" src="/statics/plugin/mstyle/js/jquery.min.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo YYS_PLUGIN_PATH; ?>/calendar/calendar.js"></script>
<script src="<?php echo YYS_GLOBAL_STYLE; ?>/global/js/jquery-1.8.3.min.js"></script><!--jQuery库-->
</head>
<body>
	<div class="container min-wid">
		<div class="path">
			<i><a href="#"><img src="/statics/plugin/mstyle/img/ico_1.png" alt=""></a></i>
			<p>
				当前位置：<a href="#">商品管理</a> > <a href="#">商品列表</a> >
			</p>
			<div class="push">
				<a href="<?php echo YYS_MODULE_PATH; ?>/index/Tdefault">欢迎界面</a>
				<a href="javascript:void(0)" onclick="location=location">刷新框架</a>
				<a href="<?php echo YYS_MODULE_PATH; ?>/cache/init">清理缓存</a>
			</div>
		</div><!-- path -->
		
		<div class="list-ofgoods wid1">
			<div class="fl-box">
			<form method="POST" action="#">
			<?php if($supplierId){ ?>
				<var>商户编号</var>
                <input type="text" disabled placeholder="<?php echo $supplierId;?>">
				<var>店名</var>
				<input type="text" disabled placeholder="<?php echo $supplier['name'];?>">
			<?php } ?>
				<var>商品类目</var>
				<select name="cateid">
				<option selected value="-1">未指定</option>
				<?php
				foreach($fenlei_lists as $key => $one){
					if($cate == $one['cateid']){
						echo '<option selected value="'.$one['cateid'].'">'.$one['name'].'</option>';
					}else if($one['cateid'] == $supplier['cateid']){
						echo '<option selected value="'.$one['cateid'].'">'.$one['name'].'</option>';
					}else{
						echo '<option value="'.$one['cateid'].'">'.$one['name'].'</option>';
					}
				}
				?>
				</select>
				<div class="grate-date">
					创建日期
					<input id="in1" type="text" name="start_time" value='<?php if(isset($_POST["start_time"])) echo $_POST["start_time"]; else echo "2018-1-1"; ?>' class="date_picker">
					至
					<input id="in2" type="text" name="end_time" value='<?php if(isset($_POST["end_time"])) echo $_POST['end_time']; else echo '2018-2-1'; ?>'  class="date_picker">
				</div>
				<button name="sososubmit" value="筛选">筛选</button>
				<a href="<?php echo G_ADMIN_PATH; ?>/shop/goods_lists/isfreepost">包邮</a>
				<a href="<?php echo G_ADMIN_PATH; ?>/shop/goods_lists/isfreepostzt">自提</a>
                <a href="<?php echo G_ADMIN_PATH; ?>/shop/goods_lists/isShelve2">在售</a>
                <a href="<?php echo G_ADMIN_PATH; ?>/shop/goods_lists/isstop">停用</a>
				<a href="<?php echo G_ADMIN_PATH; ?>/shop/goods_add/?supplierId=<?php echo $supplierId;?>">新建商品</a>
				</form>
			</div>
			<div class="fr-box">
				<!--span>每页显示:
					<select name="" id="">
						<option value="">10</option>
						<option value="">20</option>
						<option value="">30</option>
					</select>
				</span-->
			</div>
		</div><!-- identification -->
	<?php if($this->segment(4)!='xianshi' && $this->segment(4)!='maxqishu' && $this->segment(4)!='moneyasc' && $this->segment(4)!='money'): ?>
	
<form action="#" method="post" name="myform">

		<div class="ofgoods-table wid1">
			<ul>
				<li class="head">
					<span>商品ID</span>
					<span>商品标题</span>
					<span>所属类目</span>
					<span>商品售价</span>
					<span>商品惠券</span>
					<span>已售数量</span>
					<span>商品主图</span>
					<span>发货方式</span>
					<span>状态</span>
					<span>停止</span>
					<span><input type="button" class="button" value=" 排序 " onclick="myform.action='<?php echo YYS_MODULE_PATH; ?>/shop/goods_listorder/dosubmit';myform.submit();"/></span>
					<span>编辑</span>
					<span>查看</span>
				</li>
				<?php foreach($yyslist as $v) { ?>
				<li>
					<span><?php echo $v['id'];?></span>
					<span><?php echo _strcut($v['title'],30);?></span>
					<span><?php echo $this->categorys[$v['catesubid']]['name']; ?></span>
					<span><?php echo $v['yunjiage'];?></span>
					<span><?php echo $v['supplierPercent'];?></span>
					<span><?php echo $v['qishu'];?></span>
					<span>
						<i>
							<img src="/statics/uploads/<?php echo $v['thumb'];?>" alt="">
						</i>
					</span>
					<span><?php echo ($v['isfreepost']==1) ? '包邮' : '自提'; ?></span>
					<span>
					<?php
					if($v['isShelve']==1){
						echo "<a href='".YYS_MODULE_PATH."/shop/setshelve/".$v['id']."'>在售</a>";
					}else{
						echo "<a href='".YYS_MODULE_PATH."/shop/setshelve/".$v['id']."'>待上架</a>";	
					}
					?>
					</span>
					<span><?php echo ($v['isstop']==1) ? '停用' : '正常'; ?></span>
					<span><input name='listorders[<?php echo $v['id']; ?>]' type='text' size='3' value='<?php echo $v['order']; ?>' class='input-text-c'></span>
					<span><a class="ico-edit" href="<?php echo G_ADMIN_PATH; ?>/shop/goods_edit/<?php echo $v['id']; ?>"></a></span>
				<!--	[<a href="<?php echo G_ADMIN_PATH; ?>/shop/goods_del/<?php echo $v['id'];?>" onclick="return confirm('警告：是否真的删除该商品？（非测试运营千万别删除,可设置为下架）！');">删除商品</a>]-->
					<span><a target="_blank" class="ico-ease" href="/mobile/mobile/goods/<?php echo $v['id']; ?>"></a></span>
				</li>
 <?php } ?>
			</ul>
		</div>
<?php endif; ?>	
		<div class="pages">
			<span>
				<?php echo $fenye->show('new')['html']; ?>
			</span>
			<span>共 <?php echo ceil($zongji/$num); ?> 页</span>
		
			<span>到第<input type="text" id="page" value="">页</span>
			<span><a class="page-but" id="gopage" href="javascript:void(0);">确定</a></span>
		</div>
	</div><!-- container -->
</body>
<script src="/statics/plugin/mstyle/js/jquery.date_input.pack.js"></script>
<script src="/statics/plugin/mstyle/js/library-3.28.js"></script><!--自定义封装函数-->
<script src="/statics/plugin/mstyle/js/scrollanim.min.js"></script><!--动画效果库-->
<script>
	$(".ofgoods-table ul li span .ico-add").click(function(event) {
		$(".al-box").show();
	});
	$(".al-box .save a").click(function(event) {
		$(".al-box").hide();
	});
	$("#gopage").click(function(){
		var page = $("#page").val();
		var allpage = "<?php echo ceil($zongji/$num); ?>";
		if(page == '' || allpage < page){
			var page = 1;
		}
		location.href = "<?php echo $fenye->show('new')['url']; ?>"+page;
	});
</script>
</html>
