<?php 
defined('IN_YYS')or exit('Access Denied.');
System::DOWN_App_class('admin',G_ADMIN_DIR,'no');
System::DOWN_sys_fun('wechat');

class member extends admin {
	public $huiyuan_count_num = 0;
	public $huiyuan_del_num = 0;
	public $huiyuan_new_num = 0;
	public function __construct(){
		parent::__construct();
		$this->db=System::DOWN_sys_class("model");
		$this->ment=array(
			array("lists","会员列表",DOWN_M.'/'.DOWN_C."/lists"),
			array("lists","查找会员",DOWN_M.'/'.DOWN_C."/select"),
			array("insert","添加会员",DOWN_M.'/'.DOWN_C."/insert"),					
			array("insert","会员配置",DOWN_M.'/'.DOWN_C."/config"),
			array("insert","<b>会员福利配置</b>",DOWN_M.'/'.DOWN_C."/member_fufen"),
			array("insert","充值记录",DOWN_M.'/'.DOWN_C."/recharge"),
			array("insert","福分记录",DOWN_M.'/'.DOWN_C."/score"),
					
		);
		
		/*共有会员*/
			$this->member_count_num = $this->db->YNum("SELECT uid FROM `@#_yonghu` where 1");
		/*删除会员*/
			$this->member_del_num = $this->db->YNum("SELECT uid FROM `@#_yonghu_del` where 1");
		/*今日新增*/
			$time = strtotime(date("Y-m-d"));
			$this->member_new_num = $this->db->YNum("SELECT uid FROM `@#_yonghu` where `time` > '$time'");
	} 
	
	
	/*
		显示会员
		根据第四个参数显示不同类型会员
		@def 		默认会员
		@del 		删除会员
		@noreg 		未认证会员
		@b_qq 		QQ绑定会员
		@b_weibo 	微博绑定会员
		@b_taobao 	淘宝绑定会员
		@day_new	今日新增
		@day_shop	今日消费
		
					....
		第五个参数排序字段
		uid,money,time,jingyan,score
					....
		第六个参数排序类型
		desc,asc
					....		
	*/
	public function lists(){

		
		$weer_type = (!$this->segment(4)) ? 'def' : $this->segment(4);
		$weer_ziduan = (!$this->segment(5)) ? 'uid' : $this->segment(5);
		$weer_order = (!$this->segment(6)) ? 'desc' : $this->segment(6);
		
		
		$weer_type_arr = array("def"=>"默认会员","del"=>"删除会员","noreg"=>"未认证会员","day_new"=>"今日新增","day_shop"=>"今日消费","b_qq"=>"QQ绑定会员","b_weixin"=>"微信绑定","b_weibo"=>"微博绑定","b_taobao"=>"淘宝绑定会员");
		if(!isset($weer_type_arr[$weer_type])){
			$weer_type = "def";		
		}		
		if($weer_type == 'del'){$table = "@#_yonghu_del";}else{$table = "@#_yonghu";}
		
		$weer_ziduan_arr = array("uid"=>"会员ID","money"=>"账户金额","score"=>"账户福分","jingyan"=>"会员经验","time"=>"注册时间","login_time"=>"登陆时间","grade"=>"会员星级","envelopeCan"=>"红包余额");	
		if(!isset($weer_ziduan_arr[$weer_ziduan])){
			$weer_ziduan = "uid";
		
		}
		
		
		if($weer_order != "desc" && $weer_order != "asc"){			
			$weer_order = 'desc';
			$weer_order_cn = "倒序显示";
		}else{
			$weer_order_cn = "正序显示";
		}
		
		$sql_where = '' ;			
		switch($weer_type){
			case 'def':
				$sql_where = "(`emailcode` = '1' or `mobilecode` = '1') or `band` is not null";
			break; 
			case 'del':
				$sql_where = '1';	
			break; 
			case 'noreg':
				$sql_where = "`emailcode` <> '1' and `mobilecode` <> '1' and `band` is null";
			break;
			case 'b_qq':
				$sql_where = "`band` LIKE '%qq%'";
			break; 
			case 'b_weixin':
				$sql_where = "`band` LIKE '%weixin%'";
			break; 
			case 'b_mobile':
				$sql_where = "`mobilecode` LIKE '%1%'";
			break;
			case 'b_huiyuan':
				$sql_where = "`band` LIKE '%weixin%'";	
			break;
			case 'day_new':
				$day_time = strtotime(date("Y-m-d"));				
				$sql_where = "`time` > '$day_time'";		
			break;
			case 'day_shop':
				$day_time = strtotime(date("Y-m-d")).'.000';
				$uids = '';
				$conutc = $this->db->Ylist("SELECT uid FROM `@#_yonghu_yys_record` WHERE `time` > '$day_time'");				
				foreach($conutc as $c){
					$uids .= "'".$c['uid']."',";
				}
				$uids = trim($uids,",");
				if(!empty($uids)){
					$sql_where = "`uid` in($uids)";
				}else{
					$sql_where = "`uid` in('0')";
				}
				
			break;			
			default:	
				$sql_where = "`emailcode` = '1' or `mobilecode` = '1'";
			break;			
		}
		
		$this_path = LOCAL_PATH."/".DOWN_M."/".DOWN_C."/".DOWN_A;
		$select_where = "当前查看{$weer_type_arr[$weer_type]} - 使用{$weer_ziduan_arr[$weer_ziduan]} - {$weer_order_cn}";
		
		$num=15;
		$zongji=$this->db->YCount("SELECT COUNT(*) FROM `$table` WHERE $sql_where"); 
		$fenye=System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($zongji,$num,$fenyenum,"");		

		$huiyuans=$this->db->YPage("SELECT * FROM `$table` WHERE $sql_where order by `$weer_ziduan` $weer_order",array("num"=>$num,"page"=>$fenyenum,"type"=>1,"cache"=>0)); 
		foreach($huiyuans as $key => $one){
			$res = $this->db->YOne("select * from `@#_supplier` where `uid` = '$one[uid]'");
			if($res){
				$huiyuans[$key]['supplierId'] = $res['id'];
			}
		}
		include $this->dwt(DOWN_M,'member.lists');
		
	}
	//添加会员
	public function insert(){
		$mysql_model=System::DOWN_sys_class('model');
		$huiyuan_allgroup=$mysql_model->Ylist("select groupid,name from `@#_yonghu_group`");

		if(isset($_POST['submit'])){
			$weername=htmlspecialchars(trim($_POST['username']));
			if(empty($weername)){
				_note("用户名不能为空");
				exit;
			}
			$password=htmlspecialchars(trim($_POST['password']));
			if(empty($password)){
				_note("密码不能为空");
				exit;
			}else{
				$password=md5($password);
			}
			
			$img = htmlspecialchars($_POST['thumb']);
			$youjian=htmlspecialchars(trim($_POST['email']));
			$inside=htmlspecialchars(trim($_POST['inside']));
			$mobile=htmlspecialchars(trim($_POST['mobile']));
			
			$money=htmlspecialchars(trim($_POST['money']));
			$jingyan=htmlspecialchars(trim($_POST['jingyan']));
			$score=htmlspecialchars(trim($_POST['score']));
			$youjiancode=htmlspecialchars(trim($_POST['emailcode']));
			$mobilecode=htmlspecialchars(trim($_POST['mobilecode']));
			$qianming=htmlspecialchars(trim($_POST['qianming']));
			$huiyuangroup=htmlspecialchars(trim($_POST['membergroup']));
			$time = time();
			
			$info = $mysql_model->YOne("SELECT * FROM `@#_yonghu` WHERE `email` = '$youjian' or `mobile` = '$mobile'");

			$info1 = $mysql_model->YOne("SELECT * FROM `@#_yonghu` WHERE `email` = '$youjian'");
			$info2 = $mysql_model->YOne("SELECT * FROM `@#_yonghu` WHERE `mobile` = '$mobile'");
			
			
			if($info1&&$info2){
				_note("该会员已经存在！");
			}
			
			$sql="INSERT INTO `@#_yonghu` (username,img,email,inside,mobile,password,money,jingyan,score,emailcode,mobilecode,qianming,groupid,time) value ('$weername','$img','$youjian','$inside','$mobile','$password','$money','$jingyan','$score','$youjiancode','$mobilecode','$qianming','$huiyuangroup','$time')";
			$this->db->Query($sql);
			if($this->db->affected_rows()){
					_note("增加成功");
			}else{
					_note("增加失败");
			}
		}
		include $this->dwt(DOWN_M,'member_insert');
	}

	//修改会员基本资料
	public function modify(){
		$uid=intval($this->segment(4));
		$huiyuan=$this->db->YOne("select * from `@#_yonghu` where `uid`='$uid'");
		
		$huiyuan_group=$this->db->YOne("select name from `@#_yonghu_group` where `groupid`=".$huiyuan['groupid']."");
		$huiyuan_allgroup=$this->db->Ylist("select groupid,name from `@#_yonghu_group`");
		
		if(!empty($huiyuan['addgroup'])){		
			$huiyuan['addgroup'] = trim($huiyuan['addgroup'],',');
			$addgroup=explode(',',$huiyuan['addgroup']);			
			for($i=0;$i<count($addgroup);$i++){
				$quanzi=$this->db->YOne("select title from `@#_quan` where `id`=".$addgroup[$i]."");
				$quanziname[]=$quanzi['title'];
			}
		}
		
		
		if(isset($_POST['submit'])){
		
			$img = htmlspecialchars($_POST['thumb']);				
			$weername=htmlspecialchars(trim($_POST['username']));			
			$youjian=htmlspecialchars(trim($_POST['email']));
			$inside=htmlspecialchars(trim($_POST['inside']));
			
			$mobile=htmlspecialchars(trim($_POST['mobile']));
			$password=htmlspecialchars(trim($_POST['password']));
			$money1=htmlspecialchars(trim($_POST['money1']));
			if(empty($password)){
				$password=$huiyuan['password'];
			}else{
				$password=md5($password);
			}			
			$money = sprintf('%.2f', trim($_POST['money']));			
			$jingyan=htmlspecialchars(trim($_POST['jingyan']));
			$score=htmlspecialchars(trim($_POST['score']));
			$yaoqing=htmlspecialchars(trim($_POST['yaoqing']));
			$fid=htmlspecialchars(trim($_POST['yaoqing']));
			$youjiancode=htmlspecialchars(trim($_POST['emailcode']));
			$mobilecode=htmlspecialchars(trim($_POST['mobilecode']));
			$qianming=htmlspecialchars(trim($_POST['qianming']));
			$huiyuangroup=htmlspecialchars(trim($_POST['membergroup']));	
			
			$grade = intval($_POST['grade']);
			$risk = $_POST['risk'];
			
			if($money != $huiyuan['money']){		
				if($money > $huiyuan['money']){					
					$content_money = $money - $huiyuan['money'];
					$content_num  = '1';
				}else{				
					$content_money = $huiyuan['money'] - $money;
					$content_num  = '-1';
				}
				$time = time();
				$sql = $this->db->Query("INSERT INTO `@#_yonghu_zhanghao` (`uid`, `type`, `pay`, `content`, `money`, `time`) VALUES ('$huiyuan[uid]', '$content_num', '账户', '管理员修改金额', '$content_money', '$time')");				
			}
				
			
			$sql="UPDATE `@#_yonghu` SET `username`='$weername',`email`='$youjian',`inside`='$inside',`mobile`='$mobile',`password`='$password',`money`='$money',`jingyan`='$jingyan',`score`='$score',`yaoqing`='$yaoqing',`fid`='$fid',`emailcode`='$youjiancode',`mobilecode`='$mobilecode',`img`='$img',`groupid`='$huiyuangroup',`qianming`='$qianming',`money1`='$money1',`grade` = '$grade',`risk` = '$risk' WHERE `uid`='$uid'";
			if($this->db->Query($sql)){
					_note("修改成功");
			}else{
					_note("修改失败");
			}
		}
		
		include $this->dwt(DOWN_M,'member_modify');	
	}
	
	//恢复会员
	public function huifu(){
		$uid=intval($this->segment(4));
		$this->db->tijiao_start();		
		$q1 = $this->db->Query("insert into `@#_yonghu` select * from `@#_yonghu_del` where uid='$uid'");
		$q2 = $this->db->Query("delete from `@#_yonghu_del` where uid='$uid'");		
		if($q1 && $q2){
			$this->db->tijiao_commit();
			_note("恢复成功");
		}else{
			$this->db->tijiao_rollback();
			_note("恢复失败");
		}	
		
	}
	
	//删除会员
	public function del(){
		$uid=intval($this->segment(4));
		$this->db->tijiao_start();		
		$q1 = $this->db->Query("insert into `@#_yonghu_del` select * from `@#_yonghu` where uid='$uid'");
		$q2 = $this->db->Query("delete from `@#_yonghu` where uid='$uid'");		
		$q3 = $this->db->Query("delete from `@#_yonghu_band` where `b_uid`='$uid'");
		if($q1 && $q2){
			$this->db->tijiao_commit();
			_note("删除成功");
		}else{
			$this->db->tijiao_rollback();
			_note("删除失败");
		}			
	}
	
	public function del_true(){
		$uid=intval($this->segment(4));
		$q1 = $this->db->Query("delete from `@#_yonghu_del` where uid='$uid'");		
		if($q1){
			_note("删除成功");
		}else{
			_note("删除失败");
		}			
	}
	
	//查找会员
	public function select(){
		if(isset($_POST['submit'])){
			$sousuo=htmlspecialchars(trim($_POST['sousuo']));
			$content=htmlspecialchars(trim($_POST['content']));
		
			if(empty($sousuo) || empty($content)){
				_note("参数错误");
			}
			$huiyuans = array();
			if($sousuo=='id'){			
				$huiyuans[0]=$this->db->YOne("SELECT * FROM `@#_yonghu` WHERE `uid` = '$content'");				
			}
			if($sousuo=='nickname'){	
				$huiyuans=$this->db->Ylist("SELECT * FROM `@#_yonghu` WHERE `username` LIKE '%$content%'"); 
			}
			if($sousuo=='email'){				
				$huiyuans=$this->db->Ylist("SELECT * FROM `@#_yonghu` WHERE `email` LIKE '%$content%'");				
			}
			if($sousuo=='mobile'){
				$huiyuans=$this->db->Ylist("SELECT * FROM `@#_yonghu` WHERE `mobile` LIKE '%$content%'");			
			}			
			
		}
		
		include $this->dwt(DOWN_M,'member_select');
		
	}
	
	//会员组
		public function member_group(){
			$this->ment=array(
						array("member_group","会员组",DOWN_M.'/'.DOWN_C."/member_group"),
						array("member_add_group","添加会员组",DOWN_M.'/'.DOWN_C."/member_add_group"),
						
			);
			$huiyuans=$this->db->Ylist("select * from `@#_yonghu_group` where 1");
			include $this->dwt(DOWN_M,'member.group');		
		}
		
		//修改会员组
		public function group_modify(){
			$id=intval($this->segment(4));
			$huiyuans=$this->db->YOne("select * from `@#_yonghu_group` where `groupid`='$id'");
			if(isset($_POST['submit'])){
				$name=htmlspecialchars(trim($_POST['name']));
				$icon=htmlspecialchars(trim($_POST['icon']));
				$type=htmlspecialchars(trim($_POST['type']));
				$order=htmlspecialchars(trim($_POST['order']));
				$jingyan_start=htmlspecialchars(trim($_POST['jingyan_start']));
				$jingyan_end=htmlspecialchars(trim($_POST['jingyan_end'])); 
				if(empty($name)/*让经验起始可以从0开始 || empty($jingyan_start)  */|| empty($jingyan_end)){
					_note('会员组或者经验值不能为空');
				}elseif( $jingyan_start >= $jingyan_end){
					 _note('开始经验不能大于结束经验');
				}elseif($jingyan_end <= $jingyan_start){ 
					_note('结束经验不能小于开始经验');
				}
				$sql="UPDATE `@#_yonghu_group` SET `name`='$name',`icon`='$icon',`type`='$type',`order`='$order',`jingyan_start`='$jingyan_start', `jingyan_end`='$jingyan_end' WHERE `groupid`='$id'";
				$this->db->Query($sql);
				if($this->db->affected_rows()){
						_note("修改成功");
				}else{
						_note("修改失败");
				}
			}
			include $this->dwt(DOWN_M,'member.group_modify');		
		}
		
		
		//删除会员组
		public function group_del(){
			$id=intval($this->segment(4));
			$sql="DELETE FROM `@#_yonghu_group` WHERE `groupid`='$id'";
				$this->db->Query($sql);
				if($this->db->affected_rows()){
						_note("删除成功");
				}else{
						_note("删除失败");
				}
		}
		
		//增加会员组
		public function group_add(){
			if(isset($_POST['submit'])){
				$name=htmlspecialchars(trim($_POST['name']));
				$icon=htmlspecialchars(trim($_POST['icon']));
				$type=htmlspecialchars(trim($_POST['type']));
				$order=htmlspecialchars(trim($_POST['order']));
				$jingyan_start=htmlspecialchars(trim($_POST['jingyan_start']));
				$jingyan_end=htmlspecialchars(trim($_POST['jingyan_end']));
				if(empty($name) || empty($jingyan_start) || empty($jingyan_end)){
					_note('会员组或者经验值不能为空');
				}
				$sql="INSERT INTO `@#_yonghu_group` (`name`,`icon`,`type`,`order`,`jingyan_start`,`jingyan_end`) value ('$name','$icon','$type','$order','$jingyan_start','$jingyan_end')";
				$this->db->Query($sql);
				if($this->db->affected_rows()){
						_note("增加成功");
				}else{
						_note("增加失败");
				}
			}
			include $this->dwt(DOWN_M,'member.group_add');
		}
		
		//转账
		public function oplist(){
		
		if(isset($_POST['sososubmit'])){
			$posttime1=isset($_POST['posttime1'])?$_POST['posttime1']:'';
			$posttime2=isset($_POST['posttime2'])?$_POST['posttime2']:'';
			if(!empty($posttime1) && !empty($posttime2)){ //如果2个时间都不为空
				$posttime1=strtotime($posttime1);
				$posttime2=strtotime($posttime2);
				if($posttime1 > $posttime2){
					_note("前一个时间不能大于后一个时间");
				}
				$times= "`time`>='$posttime1' AND `time`<='$posttime2'";
			}
			else
			{
				$times="1";
			}
			
			$yonghu=isset($_POST['yonghu'])?$_POST['yonghu']:'';
			if(empty($yonghu) || $yonghu=='请选择用户类型'){
				$uid=' AND 1';
			}
			$yonghuzhi=isset($_POST['yonghuzhi'])?$_POST['yonghuzhi']:'';
			if($yonghu=='用户id'){
				if($yonghuzhi){
					$uid=" AND `uid`='$yonghuzhi'";
				}else{
					$uid=' AND 1';
				}
			}
			if($yonghu=='用户名称'){
				if($yonghuzhi){
					$user_uid=$this->db->YOne("select uid from `@#_yonghu` where `username`='$yonghuzhi'");
					if($user_uid){
						$uid=" AND `uid`='$user_uid[uid]'";
					}else{
						_note($yonghuzhi."用户不存在！");
						$uid=' AND 1';
					}
				}else{
					$uid=' AND 1';
				}
			}
			if($yonghu=='用户邮箱'){
				if($yonghuzhi){
					$user_uid=$this->db->YOne("select uid from `@#_yonghu` where `email`='$yonghuzhi'");
					if($user_uid){
						$uid=" AND `uid`='$user_uid[uid]'";
					}else{
						_note($yonghuzhi."用户不存在！");
						$uid=' AND 1';
					}
				}else{
					$uid=' AND 1';
				}
			}
			if($yonghu=='用户手机'){
				if($yonghuzhi){
					$user_uid=$this->db->YOne("select uid from `@#_yonghu` where `mobile`='$yonghuzhi'");
					if($user_uid){
						$uid=" AND `uid`='$user_uid[uid]'";
					}else{
						_note($yonghuzhi."用户不存在！");
						$uid=' AND 1';
					}
				}else{
					$uid=' AND 1';
				}
			}
			$wheres=$times.$uid;
		}
		$num=15;
		$summoeny=$this->db->YOne("SELECT sum(money) sum_money FROM `@#_yonghu_op_record`");
		if(empty($wheres)){
			$total=$this->db->YCount("SELECT count(*) FROM `@#_yonghu_op_record`"); 
			
		}else{
			$total=$this->db->YCount("SELECT count(*) FROM `@#_yonghu_op_record` WHERE $wheres"); 

		}
		$page=System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$pagenum=$_GET['p'];}else{$pagenum=1;}	
		$page->config($total,$num,$pagenum,"0");	
		if(empty($wheres)){
			$recharge=$this->db->YPage("SELECT * FROM `@#_yonghu_op_record`  order by `time` desc",array("num"=>$num,"page"=>$pagenum,"type"=>1,"cache"=>0)); 
		}else{
			$recharge=$this->db->YPage("SELECT * FROM `@#_yonghu_op_record` WHERE  $wheres order by `time` desc",array("num"=>$num,"page"=>$pagenum,"type"=>1,"cache"=>0)); 
		}
		$members=array();
		for($i=0;$i<count($recharge);$i++){
			$uid=$recharge[$i]['uid'];
			$member=$this->db->YOne("select * from `@#_yonghu` where `uid`='$uid'");
			$members[$i]=$member['username'];	
			if(empty($member['username'])){
				if(!empty($member['email'])){
					$members[$i]=$member['email'];
				}
				if(!empty($member['mobile'])){
					$members[$i]=$member['mobile'];
				}
			}
		}
		include $this->dwt(DOWN_M,'member.oplist');		
	}

		//会员配置
		public function config(){
			$regtype = System::DOWN_App_config("user_reg_type","",DOWN_M);			
			$nickname = $this->db->YOne("select * from `@#_linshi` where `key` = 'member_name_key' limit 1");			
			if(isset($_POST['submit'])){
				
				$nicknames = htmlspecialchars($_POST['nickname']);
				$nicknames = trim($nicknames,",");
				$nicknames = str_ireplace(" ",'',$nicknames);				
				
				
				$regtype['reg_email'] = isset($_POST['reg_email']) ? 1 : 0;
				$regtype['reg_mobile'] = isset($_POST['reg_mobile']) ? 1 : 0;
				$regtype['reg_num'] = isset($_POST['reg_num']) ? $_POST['reg_num'] : 0;
				$regtype['reg_bind'] = isset($_POST['reg_bind']) ? $_POST['reg_bind'] : 0;
				
				
				
				$html = "<?php return ".var_export($regtype,true)."; ?>";
				if(!is_writable(dirname(__FILE__).'/control/user_reg_type.ini.php')) exit('user_reg_type.ini.php 没有写入权限!');
				file_put_contents(dirname(__FILE__).'/control/user_reg_type.ini.php',$html);
				
				
				if($nickname){
					$this->db->Query("UPDATE `@#_linshi` SET `value` = '$nicknames' where `key` = 'member_name_key'");
				}else{
					$this->db->Query("INSERT INTO `@#_linshi` (`key`,`value`) value ('member_name_key','$nicknames')");
				}
				_note("操作成功");
			}			
			$nickname = $nickname['value'];
			include $this->dwt(DOWN_M,'member_config');
		}
		
		//福分配置
		public function member_fufen(){
			if(isset($_POST['submit'])){			
				$path =  dirname(__FILE__).'/control/user_fufen.ini.php';			
				if(!is_writable($path)) _note('Please chmod  user_fufen.ini.php  to 0777 !');
				$f_overziliao=intval(trim($_POST['f_overziliao']));
				$f_shoppay=intval(trim($_POST['f_shoppay']));
				$f_phonecode=intval(trim($_POST['f_phonecode']));
				$f_visituser=intval(trim($_POST['f_visituser']));
				//以上是福分，一下是经验值
				$z_overziliao=intval(trim($_POST['z_overziliao']));
				$z_shoppay=intval(trim($_POST['z_shoppay']));
				$z_phonecode=intval(trim($_POST['z_phonecode']));
				$z_visituser=intval(trim($_POST['z_visituser']));
				$fufen_yuan=intval(trim($_POST['fufen_yuan']));
				$fufen_yuansong=trim($_POST['fufen_yuansong']);
				/* $fufen_yongjin=floatval(trim($_POST['fufen_yongjin']));
				$fufen_yongjin2=floatval(trim($_POST['fufen_yongjin2']));
				$fufen_yongjin3=floatval(trim($_POST['fufen_yongjin3'])); */
				$fufen_yongjinqd=floatval(trim($_POST['fufen_yongjinqd']));
				$fufen_yongjinqd0=floatval(trim($_POST['fufen_yongjinqd0']));
				$fufen_yongjinqd1=floatval(trim($_POST['fufen_yongjinqd1']));
				$fufen_yongjinqd2=floatval(trim($_POST['fufen_yongjinqd2']));
	
				$fufen_yongjintx=floatval(trim($_POST['fufen_yongjintx']));

				//以下是邀请和佣金参数
				$renshuji1=floatval(trim($_POST['renshuji1']));
				$renshuji2=floatval(trim($_POST['renshuji2']));
				$renshuji3=floatval(trim($_POST['renshuji3']));
				$renshuji4=floatval(trim($_POST['renshuji4']));
				
				$yongjinc1j1=floatval(trim($_POST['yongjinc1j1']));
				$yongjinc1j2=floatval(trim($_POST['yongjinc1j2']));
				$yongjinc1j3=floatval(trim($_POST['yongjinc1j3']));
				$yongjinc1j4=floatval(trim($_POST['yongjinc1j4']));
				
				$yongjinc2j1=floatval(trim($_POST['yongjinc2j1']));
				$yongjinc2j2=floatval(trim($_POST['yongjinc2j2']));
				$yongjinc2j3=floatval(trim($_POST['yongjinc2j3']));
				$yongjinc2j4=floatval(trim($_POST['yongjinc2j4']));
				
				$yongjinc3j1=floatval(trim($_POST['yongjinc3j1']));
				$yongjinc3j2=floatval(trim($_POST['yongjinc3j2']));
				$yongjinc3j3=floatval(trim($_POST['yongjinc3j3']));
				$yongjinc3j4=floatval(trim($_POST['yongjinc3j4']));
				
				$yongjinc4j1=floatval(trim($_POST['yongjinc4j1']));
				$yongjinc4j2=floatval(trim($_POST['yongjinc4j2']));
				$yongjinc4j3=floatval(trim($_POST['yongjinc4j3']));
				$yongjinc4j4=floatval(trim($_POST['yongjinc4j4']));
				
				$yongjinc5j1=floatval(trim($_POST['yongjinc5j1']));
				$yongjinc5j2=floatval(trim($_POST['yongjinc5j2']));
				$yongjinc5j3=floatval(trim($_POST['yongjinc5j3']));
				$yongjinc5j4=floatval(trim($_POST['yongjinc5j4']));
				
				
				
				if($fufen_yuan<=0){
					_note('福分输入有错误');
				}
				if($fufen_yuansong<1){
					_note('冲值送余额需要设置为1以上');
				}
				$jieguo=$fufen_yuan%10;
				if($jieguo!=0){
					_note('福分输入有错误');
				}
			$html=<<<HTML
<?php 
return array (	
	'f_overziliao' => '$f_overziliao',
	'f_shoppay' => '$f_shoppay',
	'f_phonecode' => '$f_phonecode',
	'f_visituser' => '$f_visituser'	,
	'z_overziliao' => '$z_overziliao',
	'z_shoppay' => '$z_shoppay',
	'z_phonecode' => '$z_phonecode',
	'z_visituser' => '$z_visituser',
	'fufen_yuan' => '$fufen_yuan',
	'fufen_yuansong' => '$fufen_yuansong',
	'fufen_yongjinqd' => '$fufen_yongjinqd',
	'fufen_yongjinqd0' => '$fufen_yongjinqd0',
	'fufen_yongjinqd1' => '$fufen_yongjinqd1',
	'fufen_yongjinqd2' => '$fufen_yongjinqd2',
	'fufen_yongjintx' => '$fufen_yongjintx',
	'renshuji1' => '$renshuji1',
	'renshuji2' => '$renshuji2',
	'renshuji3' => '$renshuji3',
	'renshuji4' => '$renshuji4',
	'yongjinc1j1' => '$yongjinc1j1',
	'yongjinc1j2' => '$yongjinc1j2',
	'yongjinc1j3' => '$yongjinc1j3',
	'yongjinc1j4' => '$yongjinc1j4',
	'yongjinc2j1' => '$yongjinc2j1',
	'yongjinc2j2' => '$yongjinc2j2',
	'yongjinc2j3' => '$yongjinc2j3',
	'yongjinc2j4' => '$yongjinc2j4',
	'yongjinc3j1' => '$yongjinc3j1',
	'yongjinc3j2' => '$yongjinc3j2',
	'yongjinc3j3' => '$yongjinc3j3',
	'yongjinc3j4' => '$yongjinc3j4',
	'yongjinc4j1' => '$yongjinc4j1',
	'yongjinc4j2' => '$yongjinc4j2',
	'yongjinc4j3' => '$yongjinc4j3',
	'yongjinc4j4' => '$yongjinc4j4',
	'yongjinc5j1' => '$yongjinc5j1',
	'yongjinc5j2' => '$yongjinc5j2',
	'yongjinc5j3' => '$yongjinc5j3',
	'yongjinc5j4' => '$yongjinc5j4',
);
?>
HTML;

				file_put_contents($path,$html);
				_note("修改成功!");
			}
			
			$config = System::DOWN_App_config("user_fufen");		
			include $this->dwt(DOWN_M,'member_insertfufen');
		}	
		
//佣金明细
	public function commissions(){	
	$p_key = $this->segment(4);
	$configss = System::DOWN_App_config("user_fufen",'','member');//福分/经验/佣金
	//var_dump($configss);
		$num=15;
		$zongji=$this->db->YCount("SELECT COUNT(*) FROM `@#_yonghu_cashout1` WHERE 1"); 
		$fenye=System::DOWN_sys_class('page');//层次
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	

		$fenye->config($zongji,$num,$fenyenum,"");		

		$commissions=$this->db->YPage("SELECT * FROM `@#_yonghu_recodes`  WHERE 1 order by time desc",array("num"=>$num,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		//var_dump($commissions);
		//查询用户名
		if(!empty($commissions)){
		   foreach($commissions as $key=>$val){
		      $uid=$val['uid'];
			  $user[$key]=$this->db->YOne("SELECT * FROM `@#_yonghu` WHERE `uid`='$uid'  ");
		   }

         }

        $fufen = System::DOWN_app_config("user_fufen",'','member');
		include $this->dwt(DOWN_M,'member.commissions');
	}
	
/*商户提现审核列表*/
	public function withdrawal(){
		$wheres = '';
		//免审金额
		$AuditsWithdrawals = $this->db->YOne("select * from `yys_configs` where `name`='AuditsWithdrawals'");
		$mianshen = $AuditsWithdrawals['value'];
		//大额人工审核
		$largeWithdrawals = $this->db->YOne("select * from `yys_configs` where `name`='largeWithdrawals'");
		$dae = $largeWithdrawals['value'];

		$uids = '';
		if(isset($_REQUEST['mainshensubmit'])){
			$wheres .= " `money` <='$mianshen' AND ";
		}else if(isset($_REQUEST['daesubmit'])){
			$wheres .= " `money` >='$dae' AND ";
		}else if(isset($_REQUEST['unchecksubmit'])){
			$wheres .= " `auditstatus` = 0 and ";
		}else if(isset($_REQUEST['checkedsubmit'])){
			$wheres .= " `auditstatus` = 1 and ";
		}else if(isset($_REQUEST['failedsubmit'])){
			$wheres .= " `auditstatus` = 2 and ";
		}else if(isset($_REQUEST['sososubmit'])){
			$posttime1=isset($_REQUEST['start_time'])?$_REQUEST['start_time']:'';
			$posttime2=isset($_REQUEST['end_time'])?$_REQUEST['end_time']:'';
			if(empty($posttime1) || empty($posttime2)) _note("2个时间都不为能空！");
			if(!empty($posttime1) && !empty($posttime2)){ //如果2个时间都不为空
				$posttime1=strtotime($posttime1);
				$posttime2=strtotime($posttime2);
				if($posttime1 > $posttime2){
					_note("前一个时间不能大于后一个时间");
				}
				$wheres .= " `time`>='$posttime1' AND `time`<='$posttime2' and";
			}
		}else if(isset($_REQUEST['uidsubmit'])){
			$uid_req = $_REQUEST['uid'];
			$uids = $uid_req;
			$username = $_REQUEST['username'];
			if($username){
				$wheres .= " `username` = '$username' and ";
			}
		}

		
		$uid_url = $this->segment(4);
		if($uid_url){
			$uids = $uid_url;
		}
		if($uids){
			$wheres .= " `uid` = '$uids' and";
		}
		$thisYonghu = $this->db->YOne("SELECT * FROM `@#_yonghu` WHERE `uid` = '$uids'");
		
		$num=15;
		$zongji=$this->db->YCount("SELECT COUNT(*) FROM `@#_yonghu_cashout` WHERE 1"); 
		$fenye=System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($zongji,$num,$fenyenum,"");		
		//print_r($wheres);
		$commissions=$this->db->YPage("SELECT * FROM `@#_yonghu_cashout` WHERE $wheres 1 order by time desc",array("num"=>$num,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		//查询用户名
		if(!empty($commissions)){
			foreach($commissions as $key=>$val){
				$uid = $val['uid'];
				$user = $this->db->YOne("SELECT * FROM `@#_yonghu` WHERE `uid`='$uid'  ");
				$supplier = $this->db->YOne("select * from `@#_supplier` where `uid` = '$uid'");
				$commissions[$key]['supplierName'] = $supplier['name'];
				$commissions[$key]['mobile'] = $supplier['mobile'];
				$commissions[$key]['province'] = $supplier['province'];
				$commissions[$key]['city'] = $supplier['city'];
				$commissions[$key]['money1'] = $user['money1'];
				$commissions[$key]['supplier_logo'] = $supplier['supplier_logo'];
			}
		}
        $fufen = System::DOWN_app_config("user_fufen",'','member');
		include $this->dwt(DOWN_M,'member.withdrawal');
	}

//审核入账-手动打款方式
	public function withdrawal_review(){
	 $id=intval($this->segment(4));
	 
      
		 $audsta=$this->db->YOne("SELECT * FROM `@#_yonghu_cashout` WHERE `id`='$id'  ");
		 $is=$this->db->Query("UPDATE `@#_yonghu_cashout` SET `auditstatus`='1' where `id`=$id "); 
		 if($is==1){
		  //审核通过后将该数据插入到佣金记录表中
		  
		  $type=-3;
		  $content="商户提现";
		  
			$time=time();
		  $is=$this->db->Query("INSERT INTO `@#_yonghu_zhanghao1`(`uid`,`type`,`pay`,`content`,`money`,`time`)VALUES
			($audsta[uid],'$type','金币','$content','$audsta[money]','$time')");
		  
		  
		   _note("审核成功！");
		 }else{
			_note("审核失败！");	 
		 }
	}
//审核入账-自动打款方式-微信入账
	public function withdrawal_review_wx(){
		$id = $this->segment(4);
		if(!$id){
			_note('找不到ID');
		}else{
			$info = $this->db->YOne("select * from `@#_yonghu_cashout` where `id` = '$id'");
			if(!$info){
				_note('没有这个提现申请');
			}else{
				if($info['auditstatus'] != 0){
					_note('已审核或已拒绝的不可重复审核');
				}else{
					//写日志
					$time = time();
					if($info['payType'] == 'wechat'){
						$pay_type = '微信支付';
					}else{
						$pay_type = '支付宝支付';
					}

					$sql = "INSERT INTO `@#_yonghu_zhanghao` (`uid`,`type`,`pay`,`content`,`money`,`time`,`pay_type`) VALUES ('$info[uid]','-1','账户','商户提现','$info[money]','$time','$pay_type')";
					$res = $this->db->Query($sql);

					//打款
					if($res){
						if($info['payType'] == 'wechat'){
							//打款
							$desc = '商户提现';
							$userInfo = $this->db->YOne("select * from `@#_yonghu` where `uid` = '$info[uid]'");
							$amount = $info['money'] - $info['procefees'];
							$result = sendWxMoney($userInfo,$desc,$amount);
							$this->db->Query("UPDATE `@#_yonghu_cashout` SET `wxReply` = '".serialize($result)."' WHERE `id` = '$id'");
						}
						if($result['result_code'] == 'SUCCESS'){
							//改变
							$updateSql = "UPDATE `@#_yonghu_cashout` SET `auditTime` = '$time',`auditstatus` = '1' WHERE `id` = '$id'";
							$res = $this->db->Query($updateSql);
							if($res){
								_note('审核成功');
							}else{	
								_note('审核失败,请检查返回消息！');
							}
						}else{
							_note('审核失败,请检查返回消息！');
						}
					}else{
						_note('审核失败,请检查返回消息！');
					}
				}
			}
		}
	}
//商户提现-退回账户
	public function withdrawal_review_return(){
		$id = $this->segment(4);
		if(!$id){
			_note('找不到ID');
		}else{
			$info = $this->db->YOne("select * from `@#_yonghu_cashout` where `id` = '$id'");
			if(!$info){
				_note('没有这个提现申请');
			}else{
				if($info['auditstatus'] != 0){
					_note('已审核或已拒绝的不可重复审核');
				}else{
					$updateSql = "UPDATE `@#_yonghu_cashout` SET `auditTime` = '$time',`auditstatus` = '2' WHERE `id` = '$id'";
					$res = $this->db->Query($updateSql);
					if($res){
						$userSql = "UPDATE `@#_yonghu` SET `money1` = `money1` + '$info[money]' WHERE `uid` = '$info[uid]'";
						$res = $this->db->Query($userSql);
						if($res){
							_note('提现已退回到商户账户');
						}else{	
							_note('审核失败');
						}
					}else{
						_note('审核失败');
					}
				}
			}
		}
	}
/*商户提现明细记录  提现明细不需要开页面，使用列表筛选即可。*/

//商户提现-结束END	//财务统计开始Statistics

	public function statistics(){
		$sql_where = '';
		/*输入信息查找*/
		if(isset($_GET['sousubmit'])){
			$sousuo_tiaojian = $_GET['sousuo'];
			$content = htmlspecialchars(trim($_GET['content']));
			if($sousuo_tiaojian == 'id'){
				$sql_where .= " `uid` = '$content' and ";
			}else if($sousuo_tiaojian == 'nickname'){
				$sql_where .= " `username` LIKE '%$content%' and ";
			}else if($sousuo_tiaojian == 'mobile'){
				$sql_where .= " `mobile` LIKE '%$content%' and ";
			}else if($sousuo_tiaojian == 'supplierId'){
				$sql_where .= " `supplierId` = '%$content%' and ";
			}
		}

		/*注册时间查找*/
		if(isset($_GET['timesubmit'])){			
			$posttime1 = !empty($_GET['start_time']) ? strtotime($_GET['start_time']) : NULL;
			$posttime2 = !empty($_GET['end_time']) ? strtotime($_GET['end_time']) : NULL;
			if($posttime1 && $posttime2){
				if($posttime2 < $posttime1)_note("结束时间不能小于开始时间");
				$sql_where .= "`time` > '$posttime1' AND `time` < '$posttime2' and ";
			}
			if($posttime1 && empty($posttime2)){				
				$sql_where .= "`time` > '$posttime1' and ";
			}
			if($posttime2 && empty($posttime1)){				
				$sql_where .= "`time` < '$posttime2' and ";
			}
		}
		
		/*正序 倒序 排列*/
		$weer_type = (!$this->segment(4)) ? 'def' : $this->segment(4);
		//排序字段
		$weer_ziduan = (!$this->segment(5)) ? 'uid' : $this->segment(5);
		//正序 or 倒序
		$weer_order = (!$this->segment(6)) ? 'desc' : $this->segment(6);
		
		//字符串显示
		$table = "@#_yonghu";
		
		$weer_ziduan_arr = array("uid"=>"会员ID","money"=>"账户金额","money1"=>"可提货款","score"=>"账户福分","jingyan"=>"会员经验","time"=>"注册时间","login_time"=>"登陆时间","grade"=>"会员星级","envelopeCan"=>"红包余额");	
		if(!isset($weer_ziduan_arr[$weer_ziduan])){
			$weer_ziduan = "uid";
		}
		
		if($weer_order != "asc"){
			$weer_order = 'desc';
			$weer_order_cn = "倒序显示";
		}else{
			$weer_order_cn = "正序显示";
		}
		$weer_type_arr = array("def"=>"默认会员","del"=>"删除会员","noreg"=>"未认证会员","day_new"=>"今日新增","day_shop"=>"今日消费","b_qq"=>"QQ绑定会员","b_weixin"=>"微信绑定","b_weibo"=>"微博绑定","b_taobao"=>"淘宝绑定会员");
		if(!isset($weer_type_arr[$weer_type])){
			$weer_type = "def";		
		}
		
		$select_where = "当前查看{$weer_type_arr[$weer_type]} - 使用{$weer_ziduan_arr[$weer_ziduan]} - {$weer_order_cn}";

		switch($weer_type){
			case 'def':
				$sql_where .= " ((`emailcode` = '1' or `mobilecode` = '1') or `band` is not null) and ";
			break; 
			case 'noreg':
				$sql_where .= " `emailcode` <> '1' and `mobilecode` <> '1' and `band` is null and ";
			break;
			case 'b_qq':
				$sql_where .= " `band` LIKE '%qq%' and ";
			break; 
			case 'b_weixin':
				$sql_where .= " `band` LIKE '%weixin%' and ";
			break; 
			case 'b_weibo':
				$sql_where .= " `band` LIKE '%weibo%' and ";
			break;
			case 'b_taobao':
				$sql_where .= " `band` LIKE '%taobao%' and ";		
			break;
			case 'day_new':
				$day_time = strtotime(date("Y-m-d"));				
				$sql_where .= " `time` > '$day_time' and ";		
			break;
			case 'day_shop':
				$day_time = strtotime(date("Y-m-d")).'.000';
				$uids = '';
				$conutc = $this->db->Ylist("SELECT uid FROM `@#_yonghu_yys_record` WHERE `time` > '$day_time'");				
				foreach($conutc as $c){
					$uids .= "'".$c['uid']."',";
				}
				$uids = trim($uids,",");
				if(!empty($uids)){
					$sql_where .= "`uid` in($uids) and";
				}else{
					$sql_where .= "`uid` in('0') and";
				}
			break;			
			default:	
				$sql_where .= "`emailcode` = '1' or `mobilecode` = '1'";
			break;			
		}

		//排序
		if($weer_ziduan == 'uid'){
			$sql_order = " 1 order by `uid` $weer_order ";
		}else if($weer_ziduan == 'money'){
			$sql_order = " 1 order by `money` $weer_order ";
		}else if($weer_ziduan == 'money1'){
			$sql_order = " 1 order by `money1` $weer_order ";
		}else if($weer_ziduan == 'login_time'){
			$sql_order = " 1 order by `login_time` $weer_order ";
		}else if($weer_ziduan == 'time'){
			$sql_order = " 1 order by `time` $weer_order ";
		}
		//print_r($sql_where);
		
		$num=15;
		$this_path = LOCAL_PATH."/".DOWN_M."/".DOWN_C."/".DOWN_A;
		$qianbao = 0.0;
		$keti = 0.0;
		if($sousuo_tiaojian == 'supplierId'){
			$supplier = $this->db->YOne("SELECT * FROM `@#_supplier` WHERE `id` = '$content'");
			$supplierUid = $supplier['uid'];
			$sql = "SELECT * FROM `$table` WHERE `uid` = '$supplierUid'";
			$zongji = $this->db->YCount($sql);
			$huiyuan = $this->db->YOne("SELECT * FROM `$table` WHERE `uid` = '$supplierUid'");
			if($huiyuan){
				$qianbao = $huiyuan['money'];
			}else{
				$keti = $huiyuan['money1'];
			}
		}else{
			$zongji=$this->db->YCount("SELECT COUNT(*) FROM `$table` WHERE $sql_where $sql_order"); 
			$sql = "SELECT * FROM `$table` WHERE $sql_where $sql_order";
			$qianbao = $this->db->Query("SELECT SUM(`money`) FROM `$table` WHERE $sql_where 1");
			$row = $qianbao->fetch_row();
			$qianbao = $row['0'];
			$keti = $this->db->Query("SELECT SUM(`money1`) FROM `$table` WHERE $sql_where 1");
			$row = $keti->fetch_row();
			$keti = $row['0'];
		}
		$fenye=System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($zongji,$num,$fenyenum,"");
		//print_r($sql);
		$huiyuans=$this->db->YPage($sql,array("num"=>$num,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($huiyuans as $key => $one){
			$res = $this->db->YOne("select * from `@#_supplier` where `uid` = '$one[uid]'");
			if($res){
				$huiyuans[$key]['supplierId'] = $res['id'];
			}
		}
		
		include $this->dwt(DOWN_M,'member.statistics');
		
	}
//财务统计结束

//头像设置
	public function touxiang(){
	$shezhi=$this->db->Ylist("select * from `@#_yonghu` where `huiyuan`='1'");
	
	foreach($shezhi as $key => $v){
	$q1=$this->db->Query("UPDATE `@#_yonghu` SET `img`='photo/member ($key).jpg' where `uid`=$v[uid]");
	
	}
	
	if($q1){
					$this->db->tijiao_commit();
					_note("头像批量设置成功!");
			}else{
				$this->db->tijiao_rollback();
				_note("头像批量设置失败!");				
			}
			}
	
	//充值记录
	public function recharge(){
		$wheres = '';
		if(isset($_REQUEST['sososubmit'])){
			$posttime1=isset($_REQUEST['start_time'])?$_REQUEST['start_time']:'';
			$posttime2=isset($_REQUEST['end_time'])?$_REQUEST['end_time']:'';
			if(empty($posttime1) || empty($posttime2)) _note("2个时间都不为能空！");
			if(!empty($posttime1) && !empty($posttime2)){ //如果2个时间都不为空
				$posttime1=strtotime($posttime1);
				$posttime2=strtotime($posttime2);
				if($posttime1 > $posttime2){
					_note("前一个时间不能大于后一个时间");
				}
				$wheres .= " `time`>='$posttime1' AND `time`<='$posttime2' and";
			}
		}

		$uids = '';
		if(isset($_REQUEST['uidsubmit'])){
			$uid_req = $_REQUEST['uid'];
			$uids = $uid_req;
			$username = $_REQUEST['username'];
			if($username){
				$wheres .= " `username` = '$username' and ";
			}
		}
		
		$uid_url = $this->segment(4);
		if($uid_url){
			$uids = $uid_url;
		}
		if($uids){
			$wheres .= " `uid` = '$uids' and";
		}
		$thisYonghu = $this->db->YOne("SELECT * FROM `@#_yonghu` WHERE `uid` = '$uids'");

		$num=15;
		$zongji=$this->db->YCount("SELECT count(*) FROM `@#_yonghu_zhanghao` WHERE (`content`='充值' or `content`='管理员修改金额' or `content`='红包奖励'  or `content`='使用佣金充值到账户' or `content`='使用余额宝充值到账户') AND `type`='1' and $wheres 1"); 
		$summoeny=$this->db->YOne("SELECT sum(money) sum_money FROM `@#_yonghu_zhanghao` WHERE (`content`='充值' or `content`='管理员修改金额' or `content`='红包奖励'  or `content`='使用佣金充值到账户' or `content`='使用余额宝充值到账户') AND `type`='1' and $wheres 1"); 
		$fenye=System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		
		$fenye->config($zongji,$num,$fenyenum,$pageUrl);
		$recharge=$this->db->YPage("SELECT * FROM `@#_yonghu_zhanghao` WHERE `type`='1' and $wheres 1 order by `time` desc",array("num"=>$num,"page"=>$fenyenum,"type"=>1,"cache"=>0)); 

		$chongzhi = $this->db->Query("SELECT SUM(`money`) FROM `@#_yonghu_zhanghao` WHERE (`content`='充值' or `content`='管理员修改金额' or `content`='红包奖励'  or `content`='使用佣金充值到账户' or `content`='使用余额宝充值到账户') AND `type`='1' and $wheres 1");
		$row = $chongzhi->fetch_row();
		$chongzhi = $row[0] ? $row[0] : 0;
		$qianbao = $this->db->Query("SELECT SUM(`money`) FROM `@#_yonghu_zhanghao` WHERE `uid` in ( SELECT `uid` FROM `@#_yonghu_zhanghao` WHERE (`content`='充值' or `content`='管理员修改金额' or `content`='红包奖励'  or `content`='使用佣金充值到账户' or `content`='使用余额宝充值到账户') AND `type`='1' and $wheres 1)");
		$row = $qianbao->fetch_row();
		$qianbao = $row[0] ? $row[0] : 0;

		$huiyuans=array();
		for($i=0;$i<count($recharge);$i++){
			$uid=$recharge[$i]['uid'];
			$huiyuan=$this->db->YOne("select * from `@#_yonghu` where `uid`='$uid'");
			$huiyuans[$i]=$huiyuan['username'];	
			if(empty($huiyuan['username'])){
				if(!empty($huiyuan['email'])){
					$huiyuans[$i]=$huiyuan['email'];
				}
				if(!empty($huiyuan['mobile'])){
					$huiyuans[$i]=$huiyuan['mobile'];
				}
			}
		}
		include $this->dwt(DOWN_M,'member.recharge');		
	}
	
	//新增的福分充值记录
	public function score(){
		
		if(isset($_POST['sososubmit'])){
			$posttime1=isset($_POST['posttime1'])?$_POST['posttime1']:'';
			$posttime2=isset($_POST['posttime2'])?$_POST['posttime2']:'';
			if(empty($posttime1) || empty($posttime2)) _note("2个时间都不为能空！");
			if(!empty($posttime1) && !empty($posttime2)){ //如果2个时间都不为空
				$posttime1=strtotime($posttime1);
				$posttime2=strtotime($posttime2);
				if($posttime1 > $posttime2){
					_note("前一个时间不能大于后一个时间");
				}
				$times= "`time`>='$posttime1' AND `time`<='$posttime2'";
			}
			$chongzhi=isset($_POST['chongzhi'])?$_POST['chongzhi']:'';
			if(!empty($chongzhi) && $chongzhi!='请选择福分渠道来源'){
				$content=" AND `content`='$chongzhi'";
			}
			if(empty($chongzhi) || $chongzhi=='请选择充值来源') $content=" AND (`content`='完善昵称奖励' or `content`='每日签到' or `content`='邀请好友奖励' or `content`='晒单奖励' or `content`='手机认证完善奖励')";
			
			$yonghu=isset($_POST['yonghu'])?$_POST['yonghu']:'';
			if(empty($yonghu) || $yonghu=='请选择用户类型'){
				$uid=' AND 1';
			}
			$yonghuzhi=isset($_POST['yonghuzhi'])?$_POST['yonghuzhi']:'';
			if($yonghu=='用户id'){
				if($yonghuzhi){
					$uid=" AND `uid`='$yonghuzhi'";
				}else{
					$uid=' AND 1';
				}
			}
			if($yonghu=='用户名称'){
				if($yonghuzhi){
					$weer_uid=$this->db->YOne("select uid from `@#_yonghu` where `username`='$yonghuzhi'");
					if($weer_uid){
						$uid=" AND `uid`='$weer_uid[uid]'";
					}else{
						_note($yonghuzhi."用户不存在！");
						$uid=' AND 1';
					}
				}else{
					$uid=' AND 1';
				}
			}
			if($yonghu=='用户邮箱'){
				if($yonghuzhi){
					$weer_uid=$this->db->YOne("select uid from `@#_yonghu` where `email`='$yonghuzhi'");
					if($weer_uid){
						$uid=" AND `uid`='$weer_uid[uid]'";
					}else{
						_note($yonghuzhi."用户不存在！");
						$uid=' AND 1';
					}
				}else{
					$uid=' AND 1';
				}
			}
			if($yonghu=='用户手机'){
				if($yonghuzhi){
					$weer_uid=$this->db->YOne("select uid from `@#_yonghu` where `mobile`='$yonghuzhi'");
					if($weer_uid){
						$uid=" AND `uid`='$weer_uid[uid]'";
					}else{
						_note($yonghuzhi."用户不存在！");
						$uid=' AND 1';
					}
				}else{
					$uid=' AND 1';
				}
			}
			$wheres=$times.$content.$uid;
			$wheres .= " and `type` = '1'";
		}
		$num=15;
		if(empty($wheres)){
			$zongji=$this->db->YCount("SELECT count(*) FROM `@#_yonghu_zhanghao` WHERE (`content`='惠券了商品' or `content`='完善昵称奖励' or `content`='每日签到' or `content`='邀请好友奖励' or `content`='晒单奖励' or `content`='手机认证完善奖励') AND `type`='1'"); 
			$summoeny=$this->db->YOne("SELECT sum(money) sum_money FROM `@#_yonghu_zhanghao` WHERE (`content`='惠券了商品' or `content`='完善昵称奖励' or `content`='每日签到' or `content`='邀请好友奖励' or `content`='晒单奖励' or `content`='手机认证完善奖励') AND `type`='1'"); 
		}else{
			$zongji=$this->db->YCount("SELECT count(*) FROM `@#_yonghu_zhanghao` WHERE $wheres"); 
			$summoeny=$this->db->YOne("SELECT sum(money) sum_money FROM `@#_yonghu_zhanghao` WHERE $wheres"); 
		}
		$fenye=System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($zongji,$num,$fenyenum,"");	
		if(empty($wheres)){
			//下面这句是修改的，巅峰科技修正部分不入账显示在后台的问题QQ 320023815
			$score=$this->db->YPage("SELECT * FROM `@#_yonghu_zhanghao` WHERE (`content`='福分' or `content`='参与了商品' or `content`='商品' or `content`='完善昵称奖励' or `content`='每日签到' or `content`='邀请好友奖励' or `content`='参与了0人次商品' or `content`='手机认证完善奖励') AND `pay`='福分' order by `time` desc",array("num"=>$num,"page"=>$pagenum,"type"=>1,"cache"=>0)); 
		}else{
			$score=$this->db->YPage("SELECT * FROM `@#_yonghu_zhanghao` WHERE  $wheres order by `time` desc",array("num"=>$num,"page"=>$fenyenum,"type"=>1,"cache"=>0)); 
		}
		$huiyuans=array();
		for($i=0;$i<count($score);$i++){
			$uid=$score[$i]['uid'];
			$huiyuan=$this->db->YOne("select * from `@#_yonghu` where `uid`='$uid'");
			$huiyuans[$i]=$huiyuan['username'];	
			if(empty($huiyuan['username'])){
				if(!empty($huiyuan['email'])){
					$huiyuans[$i]=$huiyuan['email'];
				}
				if(!empty($huiyuan['mobile'])){
					$huiyuans[$i]=$huiyuan['mobile'];
				}
			}
		}
		include $this->dwt(DOWN_M,'member.score');		
	}
	
	
	//消费记录
	public function pay_list(){
		$wheres = '';
		if(isset($_REQUEST['sososubmit'])){
			$posttime1=isset($_REQUEST['start_time'])?$_REQUEST['start_time']:'';
			$posttime2=isset($_REQUEST['end_time'])?$_REQUEST['end_time']:'';
			if(empty($posttime1) || empty($posttime2)) _note("2个时间都不为能空！");
			if(!empty($posttime1) && !empty($posttime2)){ //如果2个时间都不为空
				$posttime1=strtotime($posttime1);
				$posttime2=strtotime($posttime2);
				if($posttime1 > $posttime2){
					_note("前一个时间不能大于后一个时间");
				}
				$wheres .= " `time`>='$posttime1' AND `time`<='$posttime2' and";
			}
		}

		$uids = '';
		if(isset($_REQUEST['uidsubmit'])){
			$uid_req = $_REQUEST['uid'];
			$uids = $uid_req;
			$username = $_REQUEST['username'];
			if($username){
				$wheres .= " `username` = '$username' and ";
			}
		}
		
		$uid_url = $this->segment(4);
		if($uid_url){
			$uids = $uid_url;
		}
		if($uids){
			$wheres .= " `uid` = '$uids' and";
		}
		$thisYonghu = $this->db->YOne("SELECT * FROM `@#_yonghu` WHERE `uid` = '$uids'");

		$num=15;
		$zongji=$this->db->YCount("SELECT count(*) FROM `@#_yonghu_yys_record` WHERE $wheres 1"); 
		$summoeny=$this->db->YOne("SELECT sum(moneycount) sum_money FROM `@#_yonghu_yys_record` WHERE $wheres 1"); 
		$fenye=System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($zongji,$num,$fenyenum,"");	
		$pay_list=$this->db->YPage("SELECT * FROM `@#_yonghu_yys_record` WHERE  $wheres 1 order by `time` desc",array("num"=>$num,"page"=>$fenyenum,"type"=>1,"cache"=>0)); 
	  
		$money_count = $this->db->Query("SELECT SUM(`moneycount`) FROM `@#_yonghu_yys_record` WHERE $wheres 1");
		$row = $money_count->fetch_row();
		$money_count = $row[0] ? $row[0] : 0;
		$huiyuans=array();
		for($i=0;$i<count($pay_list);$i++){
			$uid=$pay_list[$i]['uid'];
			$huiyuan=$this->db->YOne("select * from `@#_yonghu` where `uid`='$uid'");
			$huiyuans[$i]=$huiyuan['username'];	
			if(empty($huiyuan['username'])){
				if(!empty($huiyuan['email'])){
					$huiyuans[$i]=$huiyuan['email'];
				}
				if(!empty($huiyuan['mobile'])){
					$huiyuans[$i]=$huiyuan['mobile'];
				}
			}
		}
		include $this->dwt(DOWN_M,'member.pay_list');	
	}
	
	//钱包明细
	public function mybank(){
		$wheres = '';
		$type_where = '';
		$uids = '';
		if(isset($_REQUEST['sososubmit'])){
			$posttime1=isset($_REQUEST['start_time'])?$_REQUEST['start_time']:'';
			$posttime2=isset($_REQUEST['end_time'])?$_REQUEST['end_time']:'';
			if(empty($posttime1) || empty($posttime2)) _note("2个时间都不为能空！");
			if(!empty($posttime1) && !empty($posttime2)){ //如果2个时间都不为空
				$posttime1=strtotime($posttime1);
				$posttime2=strtotime($posttime2);
				if($posttime1 > $posttime2){
					_note("前一个时间不能大于后一个时间");
				}
				$wheres .= " `time`>='$posttime1' AND `time`<='$posttime2' and";
			}
			$type = intval($_REQUEST['type']);
			if($type == 1){
				$type_where .= " `type` = 1 and ";
			}else if($type == -1){
				$type_where .= " `type` = -1 and ";
			}
		}
		if(isset($_REQUEST['uidsubmit'])){
			$uid_req = $_REQUEST['uid'];
			$uids = $uid_req;
		}
		
		$uid_url = intval($this->segment(4));
		if($uid_url){
			$uids = $uid_url;
		}
		if($uids){
			$wheres .= " `uid` = '$uids' and";
		}
		$thisYonghu = $this->db->YOne("SELECT * FROM `@#_yonghu` WHERE `uid` = '$uids'");

		
		$num=15;
		$zongji=$this->db->YCount("SELECT count(*) FROM `@#_yonghu_zhanghao` WHERE (`content`='充值' or `content`='管理员修改金额' or `content`='红包奖励'  or `content`='使用佣金充值到账户' or `content`='参与了商品')  and $type_where $wheres 1"); 
		$summoeny=$this->db->YOne("SELECT sum(money) sum_money FROM `@#_yonghu_zhanghao` WHERE (`content`='充值' or `content`='管理员修改金额' or `content`='红包奖励'  or `content`='使用佣金充值到账户' or `content`='参与了商品') and $type_where $wheres 1"); 
		$fenye=System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	

		$qianbao = $this->db->Query("SELECT SUM(`money`) FROM `@#_yonghu` WHERE `uid` in (SELECT `uid` FROM `@#_yonghu_zhanghao` WHERE (`content`='充值' or `content`='管理员修改金额' or `content`='红包奖励'  or `content`='使用佣金充值到账户' or `content`='参与了商品')  and $type_where $wheres 1 ) ");
		$row = $qianbao->fetch_row();
		$qianbao = $row[0] ? $row[0] : 0;
		$keti = $this->db->Query("SELECT SUM(`money1`) FROM `@#_yonghu` WHERE `uid` in (SELECT `uid` FROM `@#_yonghu_zhanghao` WHERE (`content`='充值' or `content`='管理员修改金额' or `content`='红包奖励'  or `content`='使用佣金充值到账户' or `content`='参与了商品')  and $type_where $wheres 1 ) ");
		$row = $keti->fetch_row();
		$keti = $row[0] ? $row[0] : 0;
		$zengjia = $this->db->Query("SELECT SUM(`money`) FROM `@#_yonghu_zhanghao`  WHERE (`content`='充值' or `content`='管理员修改金额' or `content`='红包奖励'  or `content`='使用佣金充值到账户' or `content`='参与了商品')  and `type` = 1 and $wheres 1 ");
		$row = $zengjia->fetch_row();
		$zengjia = $row[0] ? $row[0] : 0;
		$jianshao = $this->db->Query("SELECT SUM(`money`) FROM `@#_yonghu_zhanghao` WHERE (`content`='充值' or `content`='管理员修改金额' or `content`='红包奖励'  or `content`='使用佣金充值到账户' or `content`='参与了商品')  and `type` = -1 and $wheres 1 ");
		$row = $jianshao->fetch_row();
		$jianshao = $row[0] ? $row[0] : 0;
		
		$fenye->config($zongji,$num,$fenyenum,$pageUrl);
		$recharge=$this->db->YPage("SELECT * FROM `@#_yonghu_zhanghao` WHERE (`content`='充值' or `content`='充值卡' or `content`='使用佣金充值到账户' or `content`='手机支付宝充值' or `content`='支付宝' or `content`='云支付充值' or `content`='参与了商品' or `content`='管理员修改金额' or `content`='红包奖励' or `content` LIKE '%收益%' or `content`='卡密充值')  and $type_where $wheres 1 order by `time` desc",array("num"=>$num,"page"=>$fenyenum,"cache"=>0)); 
		$huiyuans=array();
		for($i=0;$i<count($recharge);$i++){
			$uid=$recharge[$i]['uid'];
			$huiyuan=$this->db->YOne("select * from `@#_yonghu` where `uid`='$uid'");
			$huiyuans[$i]=$huiyuan['username'];	
			if(empty($huiyuan['username'])){
				if(!empty($huiyuan['email'])){
					$huiyuans[$i]=$huiyuan['email'];
				}
				if(!empty($huiyuan['mobile'])){
					$huiyuans[$i]=$huiyuan['mobile'];
				}
			}
		}
		include $this->dwt(DOWN_M,'member.mybank');		
	}
//新增的货款明细记录
	public function gold(){
		$wheres= '';
		if(isset($_POST['sososubmit'])){
			$posttime1=isset($_POST['start_time'])?$_POST['start_time']:'';
			$posttime2=isset($_POST['end_time'])?$_POST['end_time']:'';
			if(empty($posttime1) || empty($posttime2)) _note("2个时间都不为能空！");
			if(!empty($posttime1) && !empty($posttime2)){ //如果2个时间都不为空
				$posttime1=strtotime($posttime1);
				$posttime2=strtotime($posttime2);
				if($posttime1 > $posttime2){
					_note("前一个时间不能大于后一个时间");
				}
				$wheres= "`time`>='$posttime1' AND `time`<='$posttime2' and ";
			}
		}
		$supplierId = $this->segment(4);
		if($supplierId){
			$wheres .= " `uid` = '$supplierId' and ";
			$supplier = $this->db->YOne("SELECT * FROM `@#_supplier` WHERE `uid` = '$supplierId'");
		}else if(isset($_REQUEST['namesubmit'])){
			$id = intval($_REQUEST['id']);
			$name = htmlspecialchars($_REQUEST['name']);
			if($id){
				$supplier = $this->db->YOne("SELECT * FROM `@#_supplier` WHERE `id` = '$id'");
				$shanghuuid = $supplier['uid'];
				$wheres .= " `uid` = '$shanghuuid' and ";
			}else if($name){
				$suppliers = $this->db->Ylist("SELECT * FROM `@#_supplier` WHERE `name` LIKE '%".$name."%'");
				$shanghuuids = "";
				foreach($suppliers as $supplier){
					$shanghuuids = $shanghuuids."'".$supplier['uid']."',";
				}
    			$shanghuuids = rtrim($shanghuuids, ",");
				if($shanghuuids)
					$wheres .= " `uid` in ( $shanghuuids ) and ";
			}
		}
		$num=15;
		if(empty($wheres)){
					//搜索及分页
			$zongji=$this->db->YCount("SELECT count(*) FROM `@#_yonghu_zhanghao1` WHERE(`content`='商品' or `content`='金币提现' or `content`='金币' or `content` LIKE '%货款%' or `content`='金币调整')AND `type`='1'"); 
			$summoeny=$this->db->YOne("SELECT sum(money) sum_money FROM `@#_yonghu_zhanghao1` WHERE(`content`='商品' or `content`='金币提现' or `content`='金币' or `content` LIKE '%货款%' or `content`='金币调整')AND `type`='1'"); 
		}else{
			$zongji=$this->db->YCount("SELECT count(*) FROM `@#_yonghu_zhanghao1` WHERE $wheres 1"); 
			$summoeny=$this->db->YOne("SELECT sum(money) sum_money FROM `@#_yonghu_zhanghao1` WHERE $wheres 1"); 
		}
		$fenye=System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($zongji,$num,$fenyenum,"");	
		if(empty($wheres)){
			//默认列表显示
			$gold=$this->db->YPage("SELECT * FROM `@#_yonghu_zhanghao1` WHERE(`content`='金币提现' or `content`='金币转账到余额' or `content` LIKE '%货款%' or `content`='金币调整') AND `pay`='金币' order by `time` desc",array("num"=>$num,"page"=>$pagenum,"type"=>1,"cache"=>0)); 
		}else{
			$gold=$this->db->YPage("SELECT * FROM `@#_yonghu_zhanghao1` WHERE  $wheres 1 order by `time` desc",array("num"=>$num,"page"=>$fenyenum,"type"=>1,"cache"=>0)); 
		}
		$huiyuans=array();
		for($i=0;$i<count($gold);$i++){
			$uid=$gold[$i]['uid'];
			$huiyuan=$this->db->YOne("select * from `@#_yonghu` where `uid`='$uid'");
			$huiyuans[$i]=$huiyuan['username'];	
			if(empty($huiyuan['username'])){
				if(!empty($huiyuan['email'])){
					$huiyuans[$i]=$huiyuan['email'];
				}
				if(!empty($huiyuan['mobile'])){
					$huiyuans[$i]=$huiyuan['mobile'];
				}
			}
		}
		include $this->dwt(DOWN_M,'member.gold');		
	}
//货款明细结束	


//会员卡券明细
	public function mycard(){
		$wheres = '';
		$type_where = '';
		$uids = '';
		if(isset($_REQUEST['sososubmit'])){
			$posttime1=isset($_REQUEST['start_time'])?$_REQUEST['start_time']:'';
			$posttime2=isset($_REQUEST['end_time'])?$_REQUEST['end_time']:'';
			if(empty($posttime1) || empty($posttime2)) _note("2个时间都不为能空！");
			if(!empty($posttime1) && !empty($posttime2)){ //如果2个时间都不为空
				$posttime1=strtotime($posttime1);
				$posttime2=strtotime($posttime2);
				if($posttime1 > $posttime2){
					_note("前一个时间不能大于后一个时间");
				}
				$wheres .= " `create_time`>='$posttime1' AND `create_time`<='$posttime2' and";
			}
			$type = intval($_REQUEST['type']);
			if($type == 0){
				$type_where .= " `status` = '0' and ";
			}else if($type == 1){
				$type_where .= " `status` = '1' and ";
			}
		}
		if(isset($_REQUEST['uidsubmit'])){
			$uid_req = $_REQUEST['uid'];
			$uids = $uid_req;
		}
		
		$uid_url = intval($this->segment(4));
		if($uid_url){
			$uids = $uid_url;
		}
		if($uids){
			$wheres .= " `uid` = '$uids' and";
		}
		$thisYonghu = $this->db->YOne("SELECT * FROM `@#_yonghu` WHERE `uid` = '$uids'");

		
		$num=15;
		$zongji=$this->db->YCount("SELECT count(*) FROM `@#_yonghu_coupon` WHERE $type_where $wheres 1"); 
		$summoeny=$this->db->YOne("SELECT sum(money) sum_money FROM `@#_yonghu_coupon` WHERE $type_where $wheres 1"); 
		$fenye=System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	

		$qianbao = $this->db->Query("SELECT SUM(`money`) FROM `@#_yonghu` WHERE `uid` in (SELECT `uid` FROM `@#_yonghu_coupon` WHERE $type_where $wheres 1 ) ");
		$row = $qianbao->fetch_row();
		$qianbao = $row[0] ? $row[0] : 0;
		$keti = $this->db->Query("SELECT SUM(`money1`) FROM `@#_yonghu` WHERE `uid` in (SELECT `uid` FROM `@#_yonghu_coupon` WHERE $type_where $wheres 1 ) ");
		$row = $keti->fetch_row();
		$keti = $row[0] ? $row[0] : 0;
		$zengjia = $this->db->Query("SELECT SUM(`money`) FROM `@#_yonghu_coupon`  WHERE `status` = '0' and $wheres 1 ");
		$row = $zengjia->fetch_row();
		$zengjia = $row[0] ? $row[0] : 0;
		$jianshao = $this->db->Query("SELECT SUM(`money`) FROM `@#_yonghu_coupon` WHERE `status` = '1' and $wheres 1 ");
		$row = $jianshao->fetch_row();
		$jianshao = $row[0] ? $row[0] : 0;

		$fenye->config($zongji,$num,$fenyenum,$pageUrl);
		$recharge=$this->db->YPage("SELECT * FROM `@#_yonghu_coupon` WHERE  $type_where $wheres 1 order by `create_time` desc",array("num"=>$num,"page"=>$fenyenum,"cache"=>0)); 
		$huiyuans=array();
		for($i=0;$i<count($recharge);$i++){
			$uid=$recharge[$i]['uid'];
			$huiyuan=$this->db->YOne("select * from `@#_yonghu` where `uid`='$uid'");
			$huiyuans[$i]=$huiyuan['username'];	
			if(empty($huiyuan['username'])){
				if(!empty($huiyuan['email'])){
					$huiyuans[$i]=$huiyuan['email'];
				}
				if(!empty($huiyuan['mobile'])){
					$huiyuans[$i]=$huiyuan['mobile'];
				}
			}
		}
		include $this->dwt(DOWN_M,'member.mycard');		
	}

}

?>