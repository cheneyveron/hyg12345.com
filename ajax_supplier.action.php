<?php 
defined('IN_YYS')or exit('Access Denied.');
System::DOWN_App_class('base','member','no');
System::DOWN_App_fun('my','index');
System::DOWN_App_fun('user','index');
System::DOWN_sys_fun('send');
System::DOWN_sys_fun('user');
class ajax_supplier extends base {

	public function __construct(){
		parent::__construct();
		$this->db = System::DOWN_sys_class('model');
	}
	
	/*统一入口*/
	public function ajax_supplier_data(){
		$datatype = $_REQUEST['datatype'];
		$result = $this->$datatype();
		if($_REQUEST['lng']){
			_setcookie('lat',$_REQUEST['lat']);
			_setcookie('lng',$_REQUEST['lng']);
		}
		echo json_encode($result);
	}
	
	/*
	*	获取坐标
	*/
	public function getUserGeolocation(){
		$lat = $_POST['lat'];
		$lng = $_POST['lng'];
		_setcookie('lat',$lat,'');
		_setcookie('lng',$lng,'');
	}
	
	/*
	*	获取区域广告
	*/
	public function getAreaAd(){
		$lat = _getcookie('lat');
		$lng = _getcookie('lng');
		
		$diffLat = $lat - 0.5;
		$addLat = $lat + 0.5;
		$diffLng = $lng - 0.5;
		$addLng = $lng + 0.5;
		
		$where = " `lat` >= '$diffLat' and `lat` <= '$addLat'";
		$where .= " and `lng` >= '$diffLng' and `lng` <= '$addLng'";
		
		$adList = $this->db->Ylist("select * from `@#_areaad` where $where order by sort desc,id desc limit 8");
		if(!$adList){
			$adList = $this->db->Ylist("select * from `@#_areaad` where `isDefault` = '1' order by sort desc,id desc limit 8");
		}
		if($adList){
			$status = 0;
		}else{
			$status = 1;
		}
		echo json_encode(array('status'=>$status,'list'=>$adList));
	}
	
	/*默认广告显示*/
	public function defaultAd(){
		$adList = $this->db->Ylist("select * from `@#_areaad` where `isDefault` = '1' order by sort desc,id desc limit 8");
		if($adList){
			$status = 0;
		}else{
			$status = 1;
		}
		echo json_encode(array('status'=>$status,'list'=>$adList));
	}
	
	/*商铺申请第一步*/
	private function supplierSubmitOne(){
		$uid = $this->userinfo['uid'];
		$supplierData = convertUrlQuery($_POST['data']);
		$name = $supplierData['name'];
		$province = $supplierData['province'];
		$city = $supplierData['city'];
		$country = $supplierData['country'];
		$street = $supplierData['street'];
		$supplier_type = $supplierData['supplier_type'];
		$address = $supplierData['address'];
		$realname = $supplierData['realname'];
		$mobile = $supplierData['mobile'];
		/*$supplier_body = $supplierData['supplier_body'];*/
		$idcard = $supplierData['idcard'];
		$street = $supplierData['street'];
		$idimg = $supplierData['idimg'];
		$supplier_img = $supplierData['supplier_img'];
		$lat = $supplierData['lat'];
		$lng = $supplierData['lng'];
		$pid = $supplierData['pid'];
		$supplier_sub_type = $supplierData['supplier_sub_type'];

		if(!$name){
			$status = 0;
			$msg = '店铺名不可为空';
		}elseif(!$province){
			$status = 0;
			$msg = '请选择所在省份';
		}elseif(!$city){
			$status = 0;
			$msg = '请选择所在城市';
		}elseif(!$idimg){
			$status = 0;
			$msg = '请上传身份证图片';
		}elseif(!$supplier_img){
			$status = 0;
			$msg = '请上传店铺图片';
		}elseif(!$supplier_type){
			$status = 0;
			$msg = '请选择行业服务类型';
		}elseif(!$address){
			$status = 0;
			$msg = '请填写正确的营业地址';
		}elseif(!$realname){
			$status = 0;
			$msg = '请填写负责人真实姓名';
		}elseif(!$mobile || !preg_match("/^1[34578]\d{9}$/", $mobile)){
			$status = 0;
			$msg = '请填写正确的手机号码';
		/*}elseif(!$supplier_body){
			$status = 0;
			$msg = '请选择正确的店铺主体';*/
		}elseif(!$idcard || strlen($idcard) < 18 || !isCreditNo($idcard)){
			$status = 0;
			$msg = '请填写正确的证件号码';
		}elseif(!$lat || !$lng){
			$status = 0;
			$msg = '请选择店铺所在地理位置';
		}else{
			$supplierInfo = $this->db->Yone("select * from `@#_supplier` where `name` = '$name'");
			
			/*入库*/
			$supplier = array();
			$supplier['uid'] = $uid;
			$supplier['supplier_sn'] = strtoupper(substr(uniqid(),0,10));
			$supplier['name'] = $name;
			$supplier['province'] = $province;
			$supplier['city'] = $city;
			$supplier['country'] = $country;
			$supplier['street'] = $street;
			$supplier['cateid'] = $supplier_type;
			$supplier['catesubid'] = $supplier_sub_type;
			$supplier['address'] = $address;
			$supplier['realname'] = $realname;
			$supplier['mobile'] = $mobile;
			/*$supplier['supplier_body'] = $supplier_body;*/
			$supplier['idcard'] = $idcard;
			$supplier['step'] = 1;
			$supplier['idimg'] = $idimg;
			$supplier['supplier_img'] = $supplier_img;
			$supplier['lng'] = $lng;
			$supplier['lat'] = $lat;
			$supplier['create_time'] = time();
			if($pid){
				$supplier['pid'] = $pid;
			}
		
			if($supplierInfo['id']){
				$supplier['status'] = 0;
				$res = updateSet($supplier,'supplier',$supplierInfo['id']);
			}else{
				$res = insertInto($supplier,'supplier');
			}
			if($res){
				$status = 1;
				$msg = '提交成功，等待系统审核。';
			}else{
				$status = 0;
				$msg = '入库失败，请稍候再试！';
			}
		}
		$result['status'] = $status; 
		$result['msg'] = $msg; 
		return $result;
	}
	
	/*商铺申请第二步*/
	private function supplierSubmitTwo(){
		$uid = $this->userinfo['uid'];
		$supplierData = convertUrlQuery($_POST['data']);
		$notice = $supplierData['notice'];
		$goods_type = $supplierData['goods_type'];
		$service = $supplierData['service'];
		$appointment_mobile = $supplierData['appointment_mobile'];
		$supplier_logo = $supplierData['supplier_logo'];
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$uid'");
			
		if(!$supplier){
			$status = 0;
			$msg = '不存在的店铺';
		}elseif(!$notice){
			$status = 0;
			$msg = '店铺公告不可为空';
		}elseif(!$goods_type){
			$status = 0;
			$msg = '主营商品不可为空';
		}elseif(!$service){
			$status = 0;
			$msg = '主营服务不可为空';
		}elseif(!$appointment_mobile || !preg_match("/^1[34578]\d{9}$/", $appointment_mobile)){
			$status = 0;
			$msg = '预约电话不正确';
		/*}elseif(!$supplier_logo){
			$status = 0;
			$msg = '店标不可为空';*/
		}else{
			/*入库*/
			$supplierUpdateData = array();
			$supplierUpdateData['notice'] = $notice;
			$supplierUpdateData['goods_type'] = $goods_type;
			$supplierUpdateData['service'] = $service;
			$supplierUpdateData['appointment_mobile'] = $appointment_mobile;
			//$supplierUpdateData['supplier_logo'] = $supplier_logo;
			$supplierUpdateData['step'] = 2;

			$res = updateSet($supplierUpdateData,'supplier',$supplier['id']);
			if($res){
				$status = 1;
				$msg = '更新成功';
			}else{
				$status = 0;
				$msg = '更新失败，请稍候再试！';
			}
		}
		$result['status'] = $status; 
		$result['msg'] = $msg; 
		return $result;
	}
	
	
	/*上传海报、店铺图部分*/
	private function uploadPosters(){
		$uid = $this->userinfo['uid'];
		$postersData = convertUrlQuery($_POST['data']);
		$supplier = $this->db->Yone("select * from `@#_supplier` where `uid` = '$uid'");
		
		$res = false;
		foreach($postersData as $key => $one){
			if(preg_match('/sinwl_upload_/',$key)){
				$insertData = array();
				$insertData['supplier_id'] = $supplier['id'];
				$insertData['create_time'] = time();
				$insertData['img'] = $one;
				$insertData['type'] = 0;
			}else{
				$insertData = array();
				$insertData['supplier_id'] = $supplier['id'];
				$insertData['create_time'] = time();
				$insertData['img'] = $one;
				$insertData['type'] = 1;
			}
			$res = insertInto($insertData,'supplier_img');
		}
		
		if($res){
			if($supplier['step'] == 2){
				$this->db->query("update `@#_supplier` set `step` = '3' where `id` = '$supplier[id]'");
				$status = 1;
				$msg = '上传成功';
			}elseif($supplier['setp'] == 3){
				$status = 2;
				$msg = '上传成功';
			}
		}else{
			$status = 0;
			$msg = '上传失败';
		}
		
		$result['status'] = $status; 
		$result['msg'] = $msg; 
		return $result;
	}

	/*交易记录*/
	private function transactionRecord(){
		$uid = $this->userinfo['uid'];
		$count = $this->db->YCount("select * from `@#_yonghu_yys_record` where `supplierId` = '$uid'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_yys_record` where `supplierId` = '$uid' ORDER BY `id` ASC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		return $list;
	}
	
	/*卡券消费*/
	private function couponCost(){
		$uid = $this->userinfo['uid'];
		$count = $this->db->YCount("select * from `@#_yonghu_yys_record` where `supplierId` = '$uid'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_yys_record` where `supplierId` = '$uid' ORDER BY `id` ASC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		return $result;
	}
	
	/*本月卡券*/
	private function monthCoupon(){
		$uid = $this->userinfo['uid'];
		$count = $this->db->YCount("select * from `@#_yonghu_yys_record` where `supplierId` = '$uid'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_yys_record` where `supplierId` = '$uid' ORDER BY `id` ASC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		return $result;
	}
	
	/*发货管理*/
	private function noAddress(){
		$uid = $this->userinfo['uid'];
		$count = $this->db->YCount("select * from `@#_yonghu_yys_record` where `supplierId` = '$uid'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_yys_record` where `supplierId` = '$uid' ORDER BY `id` ASC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		return $result;
	}
	
	/*等待发货*/
	private function waitSend(){
		$uid = $this->userinfo['uid'];
		$count = $this->db->YCount("select * from `@#_yonghu_yys_record` where `supplierId` = '$uid'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_yys_record` where `supplierId` = '$uid' ORDER BY `id` ASC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		return $result;
	}
	
	/*完成订单*/
	private function complate(){
		$uid = $this->userinfo['uid'];
		$count = $this->db->YCount("select * from `@#_yonghu_yys_record` where `supplierId` = '$uid'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_yys_record` where `supplierId` = '$uid' ORDER BY `id` ASC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		return $result;
	}
	
	/*商品管理*/
	private function goodsManage(){
		$uid = $this->userinfo['uid'];
		$count = $this->db->YCount("select * from `@#_yonghu_yys_record` where `supplierId` = '$uid'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_yys_record` where `supplierId` = '$uid' ORDER BY `id` ASC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		return $result;
	}
	
	/*修改店铺信息*/
	private function modifySupplier(){
		$appointment_mobile = $_REQUEST['appointment_mobile'];
		$id = intval($_REQUEST['id']);
		$supplier = $this->db->YOne("select * from `@#_supplier` where `id` = '$id'");
		if(!$id || !$supplier){
			$status = 0;
			$msg = '非法操作,error:s0001';
		}elseif(!$appointment_mobile ||	!preg_match("/^1[34578]\d{9}$/", $appointment_mobile)){
			$status = 0;
			$msg = '请输入正确的电话号码';
		}else{
			$res = updateSet(array('appointment_mobile'=>$appointment_mobile),'supplier',$id);
			if($res){
				$status = 1;
				$msg = '修改成功！';
			}else{
				$status = 0;
				$msg = '修改失败，请稍候再试！';
			}
		}
		$result['status'] = $status; 
		$result['msg'] = $msg; 
		return $result;
	}
	
	/*卡券样式及选择*/
	private function getCouponStyle(){
		$cateId = intval($_REQUEST['id']);
		$where = "`is_effect` = '1'";
		
		if($cateId && $cateId != '9999'){
			$where .= " and `cate_id` = '$cateId'";
		}elseif($cateId == '9999'){
			$where .= " and `is_recommand` = '1'";
		}
		
		$count = $this->db->YCount("select * from `@#_supplier_coupon_style` where $where");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_supplier_coupon_style` where $where ORDER BY `sort` DESC,`id` DESC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		if($list){
			foreach($list as $key => $one){
				$list[$key]['id'] = sprintf('%04d',$one['id']);
			}
			$status = 1;
		}else{
			$status = 0;
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		return $result;
	}
	
	/*获取商户选择并入库*/
	public function insertSupplierSelect(){
		$data = $_REQUEST;
		if(!$data['money'] || $data['money'] <= 0){
			$status = 0;
			$msg = '请正确输入卡券面值';
		}elseif(!$data['couponCode']){
			$status = 0;
			$msg = '请选择卡券编号';
		}else{
			$uid = $this->userinfo['uid'];
			$supplier = $this->db->YOne("select * from `@#_supplier` where `uid` = '$uid'");
			if($supplier['status'] == 1){
				if($supplier['id']){
					$supplier_goods = $this->db->YOne("select * from `@#_shangpin` where `supplierId` = '$supplier[id]'");
					if($supplier_goods){
						$status = 0;
						$msg = '已经申请过了，请联系客服修改！';
						$url = '/mobile/home/';
					}else{
						$couponCode = sprintf('%05d',$data['couponCode']);
						$coupon = $this->db->Yone("select * from `@#_supplier_coupon_style` where `title` = '$couponCode'");
						if($coupon){
							$commission = $this->db->YOne("select * from `yys_configs` where `name`='commission'");
							$commission = $commission['value'];
							$money = intval($data['money']*$commission);
							$shangpin = array(
								'isCoupon' => 1,
								'title' => '众筹购物券'.$data['money'].'元（'.$supplier['name'].'）',
								'supplierId' => $supplier['id'],
								'thumb' => $coupon['thumb'],
								'couponId' => $coupon['id'],
								'yuanjia' => $data['money'],
								'money' => $money,//add Money
								'yunjiage' => 1,
								'maxqishu' => 65535,
								'zongrenshu' => $money,
								'shenyurenshu' => $money,
							);
							$res = insertInto($shangpin,'shangpin');
							if($res){
								//Update 云购码表
								$this->db->tijiao_start();
								System::DOWN_App_fun("content");
								$query_table = content_get_codes_table();
								if(!$query_table){
									$this->db->tijiao_rollback();
									$status = 0;
									$msg = '众筹码仓库不正确!';
									$url = '/mobile/home/';
								}
								content_huode_go_codes($money,3000,$res);
								$query_1 = $this->db->Query("UPDATE `@#_shangpin` SET `codes_table` = '$query_table',`sid` = '$res' where `id` = '$res'");
								if($query_1){
									$this->db->tijiao_commit();
								}else{
									$this->db->tijiao_rollback();
									$status = 0;
									$msg = '添加卡券失败，请稍后再试';
									$url = '/mobile/home/';
								}
								//end Update 云购码表
								updateSet(array('status'=>2),'supplier',$supplier['id']);
								$status = 1;
								$msg = '添加卡券成功，请等待后台审核！';
								$url = '/mobile/home/';
							}else{
								$status = 0;
								$msg = '添加卡券失败，请稍后再试';
								$url = '/mobile/home/';
							}
						}else{
							$status = 0;
							$msg = '不存在这个卡券样式';
						}
					}
				}else{
					$status = 0;
					$msg = '非法操作';
				}
			}else{
				$status = 0;
				$msg = '非法操作';
			}
		}
		$result['status'] = $status; 
		$result['msg'] = $msg; 
		$result['url'] = $url;
		echo json_encode($result);
	}
	
	
	private function getSupplierList(){
		$count = $this->db->YCount("select * from `@#_shangpin` where `shenyurenshu` > 0");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_shangpin` where `shenyurenshu` > 0 ORDER BY `money` DESC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		return $list;
	}
	
	public function setSupplierArea(){
		$id = intval($_REQUEST['id']);
		if(!$id){
			$status = 0;
			$msg = '不存在的店铺';
		}else{
			$supplier = $this->db->YOne("select * from `@#_supplier` where `id` = '$id'");
			if(!$supplier){
				$status = 0;
				$msg = '不存在的店铺';
			}else{
				$isCanSearch = intval($_REQUEST['isCanSearch']);
				$res = updateSet(array('isCanSearch'=>$isCanSearch),'supplier',$id);
				if($res){
					$status = 1;
					$msg = '设置成功';
				}else{
					$status = 0;
					$msg = '设置失败';
				}
			}
		}
		$result['status'] = $status; 
		$result['msg'] = $msg; 
		echo json_encode($result);
	}
	
	private function supplierComment(){
		$supplierId = intval($_REQUEST['supplierId']);
		$count = $this->db->YCount("select * from `@#_comment` where `sid` = '$supplierId'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_comment` where `sid` = '$supplierId' ORDER BY `top` DESC, `id` DESC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		return $result;
	}
	
	public function supplierService(){
		$id = intval($_REQUEST['id']);
		$result = array();
		if(!$id){
			$status = 0;
			$msg = '不存在的店铺';
		}else{
			$supplier = $this->db->YOne("select * from `@#_supplier` where `id` = '$id'");
			if(!$supplier){
				$status = 0;
				$msg = '不存在的店铺';
			}else{
				$address  = getCityName($supplier['province']).getCityName($supplier['city']).getCityName($supplier['country']);
				$address .= getCityName($supplier['street']).$supplier['address'];
				$supplier['address'] = $address;
				$supplier['supplier_type'] = getSupplierType($supplier['catesubid']);
				$supplier['street'] = getCityName($supplier['street']).$supplier['address'];
				$status = 1;
				$result['supplier'] = $supplier;
			}
		}
		$result['status'] = $status; 
		$result['msg'] = $msg; 
		echo json_encode($result);
		exit();
	}
	
	/*全部卡券*/
	private function getSupplierCoupon(){
		$uid = $this->userinfo['uid'];
		$supplier = $this->db->YOne("select * from `@#_supplier` where `uid` = '$uid'");
		$supplierId = $supplier['id'];
		$id = intval($_REQUEST['id']);
		if($id){
			$supplierId = $id;
		}
		$cate_id = intval($_REQUEST['cate_id']);
		$dataId = intval($_REQUEST['dataId']);
		$where = " `supplierId` = '$supplierId' and `isShelve` = '1' ";
		if($cate_id){
			$where .= " and `cateid` = '$cate_id' ";
		}
		if($dataId){
			$where .= " and `catesubid` = '$dataId' ";
		}
		$isfreepost = intval($_REQUEST['isfreepost']);
		if($isfreepost){
			$isfreepost = $isfreepost - 1;
			$where .= " and `isfreepost` = '$isfreepost' ";
		}
		$count = $this->db->YCount("select * from `@#_zg_shangpin` where $where");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");
		$list = $this->db->YPage("select * from `@#_zg_shangpin` where $where ORDER BY `order` DESC,`qishu` DESC,`time` DESC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			if($one['qishu'] == '-1'){
				$list[$key]['qishu'] = '9999';
			}
			$list[$key]['width'] = ($one['maxqishu']/$one['qishu'])*100;
		}
		
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		return $result;
	}
	
	/*public function getLocalSupplier(){
		$count = $this->db->YCount("select * from `@#_supplier` where `status` = '1'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_supplier` where `status` = '1' ORDER BY `id` DESC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		
		foreach($list as $key => $one){
			$list[$key]['address'] = getCityName($one['province']).getCityName($one['city']).getCityName($one['country']).getCityName($one['street']).$one['address'];
		}
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		echo json_encode($result);
	}*/
	
	public function getNoSpendList(){
		$uid = $this->userinfo['uid'];
		$supplier = $this->db->YOne("select * from `@#_supplier` where `uid` = '$uid'");
		$supplierId = $supplier['id'];
		$count = $this->db->YCount("select * from `@#_yonghu_coupon` where  `type` = '1' and `sid` = '$supplierId'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_coupon` where  `type` = '1' and `sid` = '$supplierId' ORDER BY `status` ASC ,`create_time` DESC,`id` DESC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$shangpin = $this->db->YOne("select * from `@#_shangpin` where `supplierId` = '$one[sid]'");
			$list[$key]['thumb'] = $shangpin['thumb'];
			$username = $this->db->YOne("select * from `@#_yonghu` where `uid` = '$one[uid]'");
			$list[$key]['username'] = $username['username'];
			$list[$key]['avatar'] = $username['img'];
			if($one['status'] == 1){
				$list[$key]['status'] = '已使用';
				$list[$key]['use_time'] = date('Y-m-d H:i:s',$one['use_time']);
			}else{
				$list[$key]['status'] = '未使用';
				$list[$key]['use_time'] = '未使用';
			}
		}
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		echo json_encode($result);
	}
	
	public function getSpendList(){
		$uid = $this->userinfo['uid'];
		$supplier = $this->db->YOne("select * from `@#_supplier` where `uid` = '$uid'");
		$supplierId = $supplier['id'];
		$count = $this->db->YCount("select * from `@#_yonghu_coupon` where `type` = '0' and `sid` = '$supplierId' and `status` = '1'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_coupon` where `type` = '0' and `sid` = '$supplierId' and `status` = '1' ORDER BY `status` ASC , `id` DESC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$shangpin = $this->db->YOne("select * from `@#_shangpin` where `supplierId` = '$one[sid]'");
			$list[$key]['thumb'] = $shangpin['thumb'];
			$list[$key]['qishu'] = $shangpin['qishu'];
			$list[$key]['title'] = $shangpin['title'];
			$list[$key]['q_end_time'] = $shangpin['q_end_time'] ? date('Y-m-d H:i:s',$shangpin['q_end_time']) : '未开奖';
			$list[$key]['q_user_code'] = $shangpin['q_user_code'];
			$username = $this->db->YOne("select * from `@#_yonghu` where `uid` = '$one[uid]'");
			$list[$key]['username'] = $username['username'];
			$list[$key]['avatar'] = $username['img'];
			$list[$key]['use_time'] = $one['use_time'] ? date('Y-m-d H:i:s',$one['use_time']) : '未使用';
			$list[$key]['goods_id'] = $shangpin['id'];
			if($one['status'] == 1){
				$list[$key]['status'] = '已使用';
			}else{
				$list[$key]['status'] = '未使用';
			}
		}
		
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		echo json_encode($result);	
	}
	
	public function getSupplierSpendList(){
		$uid = $this->userinfo['uid'];
		$supplier = $this->db->YOne("select * from `@#_supplier` where `uid` = '$uid'");
		$supplierId = $supplier['id'];
		$count = $this->db->YCount("select * from `@#_yonghu_coupon` where `sid` = '$supplierId'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");
		$list = $this->db->YPage("select * from `@#_yonghu_coupon` where `sid` = '$supplierId' ORDER BY `status` ASC,`shopid` DESC,`id` DESC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$order = $coupon = $username = '';
			$shangpin = $this->db->YOne("select * from `@#_shangpin` where `id` = '$one[shopid]'");
			$list[$key]['thumb'] = $shangpin['thumb'];
 			$list[$key]['qishu'] = $shangpin['qishu'];
			$list[$key]['title'] = $shangpin['title'];
			$list[$key]['create_time'] = date('Y-m-d H:i:s',$one['create_time']);
			$username = $this->db->YOne("select * from `@#_yonghu` where `uid` = '$one[uid]'");
			$list[$key]['username'] = $username['username'];
			$list[$key]['avatar'] = $username['img'];
			$list[$key]['use_time'] = $one['use_time'] ? date('Y-m-d H:i:s',$one['use_time']) : '未消费';
			$list[$key]['goods_id'] = $one['id'];
			$list[$key]['order_sn'] = $one['code'];
			if($one['status'] == 1){
				$list[$key]['status'] = '已消费';
			}else{
				$list[$key]['status'] = '未消费';
			}
		}

		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		echo json_encode($result);	
	}
	
	public function getNearSupplier(){
		$lng = $_REQUEST['lng'];
		$lat = $_REQUEST['lat'];
		$url = "http://api.map.baidu.com/geocoder/v2/?location=$lat,$lng&output=json&pois=0&ak=HrdIKAFcRojIa5MCysQwX4ux";
		$result = httpGet($url);
		$result = json_decode($result,true);
		
		$province = str_replace('省','',$result['result']['addressComponent']['province']);
		$district = str_replace('市','',$result['result']['addressComponent']['district']);
		//$city =  str_replace('区','',$result['result']['addressComponent']['city']);
		$city = $result['result']['addressComponent']['city'];
		
		/*通过城市查ID*/
		$provinceCode = $this->db->YOne("select * from `@#_area` where `name` like '%$province%' and `level` = '1'");
		$provinceCode = $provinceCode['code'];
		
		$cityCode = $this->db->YOne("select * from `@#_area` where `parentId` = '$provinceCode' and `name` like '%$city%' and `level` = '2'");
		$cityCode = $cityCode['code'];
		
		$districtCode = $this->db->YOne("select * from `@#_area` where `parentId` = '$cityCode' and`name` like '%$districtCode%' and `level` = '3'");
		$districtCode = $districtCode['code'];
		
		/*区代码*/
		$districtCode = $result['result']['addressComponent']['adcode'];
		
		/*通过地区编码查商铺*/
		$list = $this->db->YList("select * from `@#_supplier` where `province` = '$provinceCode' and `city` = '$cityCode' and country = '$districtCode' and `status` = '1' order by id desc");
		foreach($list as $key => $one){
			$list[$key]['address'] = getCityName($one['province']).getCityName($one['city']).getCityName($one['country']).getCityName($one['street']).$one['address'];
		}
		
		/*地区LIST*/
		$areaList = $this->db->YList("select * from `@#_area` where `parentId` = '$districtCode' order by id desc");
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		
		$result['areaList'] = $areaList;
		$result['status'] = $status; 
		$result['list'] = $list; 
		echo json_encode($result);
	}
	
	private function getSupplierPat(){
		$supplierId = intval($_REQUEST['id']);
		/*if(!$supplierId){
			$uid = $this->userinfo['uid'];
			$supplier = $this->db->YOne("select * from `@#_supplier` where `uid` = '$uid'");
			$supplierId = $supplier['id'];
		}*/
		$count = $this->db->YCount("select * from `@#_supplier_pat` where `sid` = '$supplierId'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_supplier_pat` where `sid` = '$supplierId' ORDER BY `sort` DESC ,`id` DESC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		foreach($list as $key => $one){
			$list[$key]['comment'] = getSupplierPatComment();
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		return $result;
	}
	
	/*获取本地商铺*/
	public function getLocalSupplier(){
		$lng = $_REQUEST['lng'];
		$lat = $_REQUEST['lat'];
		$fromdb = $_REQUEST['fromdb'];
		$cateid = intval($_REQUEST['cateid']);
		if($cateid){
			$where = " and (`catesubid` = '$cateid' or `cateid` = '$cateid')";
		}
		if($fromdb == "yes"){
			$uid = $this->userinfo['uid'];
			$locationId = $this->userinfo['locationId'];
			$location = $this->db->YOne("select * from `@#_yonghu_location` where `id` = '$locationId' and `uid` = '$uid'");
			$provinceCode = $location['sheng'];
			$cityCode = $location['shi'];
			$districtCode = $location['xian'];
			$district = $this->db->YOne("select * from `@#_area` where `code` = '$districtCode'");
		}else{
			$url = "http://api.map.baidu.com/geocoder/v2/?location=$lat,$lng&output=json&pois=0&ak=HrdIKAFcRojIa5MCysQwX4ux";
			$result = httpGet($url);
			$result = json_decode($result,true);
			$province = str_replace('省','',$result['result']['addressComponent']['province']);
			$district = str_replace('市','',$result['result']['addressComponent']['district']);
			//$city =  str_replace('区','',$result['result']['addressComponent']['city']);
			$city = str_replace('市','',$result['result']['addressComponent']['city']);//$result['result']['addressComponent']['city'];
			
			/*通过城市查ID*/
			$provinceCode = $this->db->YOne("select * from `@#_area` where `name` like '%$province%' and `level` = '1'");
			$provinceCode = $provinceCode['code'];
			
			$cityCode = $this->db->YOne("select * from `@#_area` where `parentId` = '$provinceCode' and `name` like '%$city%' and `level` = '2'");
			$cityCode = $cityCode['code'];
			
			$districtCode = $this->db->YOne("select * from `@#_area` where `parentId` = '$cityCode' and`name` like '%$districtCode%' and `level` = '3'");
			$districtCode = $districtCode['code'];
			
			/*区代码*/
			$districtCode = $result['result']['addressComponent']['adcode'];
			$district = $this->db->YOne("select * from `@#_area` where `code` = '$districtCode'");
		}


		//file_put_contents('supplier.txt',"select * from `@#_supplier` where `province` = '$provinceCode' and `city` = '$cityCode' and country = '$districtCode' and `status` = '3' $where ORDER BY `top` DESC,`credibility` DESC\r\n",FILE_APPEND);
		
		/*通过地区编码查商铺*/
		$list = $this->db->YList("select * from `@#_supplier` where `province` = '$provinceCode' and `city` = '$cityCode' and country = '$districtCode' and `status` = '3' and `isCanSearch` = '1' $where ORDER BY `top` DESC,`credibility` DESC");
		foreach($list as $key => $one){
			$list[$key]['address'] = '【'.getCityName($one['street']).'】'.$one['address'];
		}
		
	    //获取信誉等级
		$huiyuandjs=$this->db->Ylist("select * from `@#_yonghu_group` where `type` = '商户等级'");
		//print_r($huiyuandjs);
		//print_r($list);
		//$list[0]["level"] = array("a"=>"b");
	    if(!empty($huiyuandjs)){
			foreach($list as $key=>$val){
				$jingyan=$list[$key]['credibility'];
				foreach($huiyuandjs as $hk=>$hv){
					if($jingyan>=$hv['jingyan_start'] && $jingyan<=$hv['jingyan_end']){
						$list[$key]["level"] = $hv['name'];
						$list[$key]["icon"] = $hv['icon'];
						//array_push($list[$key],array("level"=>$hv['name'],"icon"=>$hv['icon']));
					}
				}
			}
	  	}
		//print_r($list);
		/*信誉等级结束*/				
		
		/*地区LIST*/
		$areaList = $this->db->YList("select * from `@#_area` where `parentId` = '$districtCode' order by id desc");
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result['districtCode'] = $districtCode;
		$result['districtName'] = $district['name'];
		$result['areaList'] = $areaList;
		$result['status'] = $status; 
		$result['list'] = $list; 
		echo json_encode($result);
	}
	
	public function getLocalSupplierForStreet(){
		$code = htmlspecialchars($_REQUEST['code']);
		$type = htmlspecialchars($_REQUEST['type']);
		$keyword = htmlspecialchars($_REQUEST['keyword']);
		$where = " `status` = '3'";
		if($code){
			if($type == 'all'){
				$code = substr($code,0,6);
			}
			$where .= " and (`country` = '$code' or `street` = '$code')  and `isCanSearch` = '1' ";
		}
		if($type == 'hot'){
			$order = " ORDER BY `credibility` DESC";
		}elseif($type == 'new'){
			$order = " ORDER BY `audit_time` DESC";
		}else{
			$order = " ORDER BY `top` DESC,`credibility` DESC";
		}
		

	
		if(_getcookie('cate_id')){
			$cate_id = _getcookie('cate_id');
			$where .= " and (`cateid` = '$cate_id' or `catesubid` = '$cate_id') ";
		}
		
		if($keyword){
			$where .= " and (`name` like '%$keyword%' or `goods_type` like '%$keyword%')";
		}
	
		$count = $this->db->YCount("select * from `@#_supplier` where $where ");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_supplier` where $where $order",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));

		
	    //获取信誉等级
		$huiyuandjs=$this->db->Ylist("select * from `@#_yonghu_group` where `type` = '商户等级'");
		//print_r($huiyuandjs);
		//print_r($list);
		//$list[0]["level"] = array("a"=>"b");
	    if(!empty($huiyuandjs)){
			foreach($list as $key=>$val){
				$jingyan=$list[$key]['credibility'];
				foreach($huiyuandjs as $hk=>$hv){
					if($jingyan>=$hv['jingyan_start'] && $jingyan<=$hv['jingyan_end']){
						$list[$key]["level"] = $hv['name'];
						$list[$key]["icon"] = $hv['icon'];
						//array_push($list[$key],array("level"=>$hv['name'],"icon"=>$hv['icon']));
					}
				}
			}
	  	}
		//print_r($list);
		/*信誉等级结束*/	
		
		
		foreach($list as $key => $one){
			$list[$key]['address'] = '【'.getCityName($one['street']).'】'.$one['address'];
		}
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		echo json_encode($result);
	}
	
	public function delSupplierPat(){
		$id = intval($_REQUEST['id']);
		$supplierPat = $this->db->YOne("select * from `@#_supplier_pat` where `id` = '$id'");
		if(!$supplierPat){
			$status = 0;
			$msg = '不存在这个拍图';
		}else{
			$res = deleteInfo($id,'supplier_pat');
			if($res){
				$status = 1;
				$msg = '删除成功';
			}else{
				$status = 0;
				$msg = '删除失败';
			}
		}
		$result['status'] = $status; 
		$result['msg'] = $msg; 
		echo json_encode($result);
		exit();
	}
	
	public function sendSupplierPatToUp(){
		$id = intval($_REQUEST['id']);
		$supplierPat = $this->db->YOne("select * from `@#_supplier_pat` where `id` = '$id'");
		if(!$supplierPat){
			$status = 0;
			$msg = '不存在这个拍图';
		}else{
			$res = $this->db->query("UPDATE `@#_supplier_pat` SET `sort` = '0' where `sort` = '1'");
			if($res){
				updateSet(array('sort'=>1),'supplier_pat',$id);
				$status = 1;
				$msg = '置顶成功';
			}else{
				$status = 0;
				$msg = '置顶失败';
			}
		}
		$result['status'] = $status; 
		$result['msg'] = $msg; 
		echo json_encode($result);
		exit();
	}
	
	public function getSubFenlei(){
		$cateid = intval($_REQUEST['cateid']);
		$fenlei = $this->db->YList("select * from `@#_fenlei` where `type` = '1' and `parentid` = '$cateid' order by `order` desc ");
		if($fenlei){
			echo json_encode(array('list'=>$fenlei,'status'=>1));
		}else{
			echo json_encode(array('list'=>null,'status'=>0));
		}
		exit();
	}
	
	private function getGoodsShelves(){
		$uid = $this->userinfo['uid'];
		$isShelve = intval($_REQUEST['isShelve']);
		$dataId = intval($_REQUEST['dataId']);
		$where = " zg.`isShelve` = '$isShelve'";
		if($dataId){
			$where .= " and zg.`catesubid` = '$dataId' ";
		}
		$isfreepost = intval($_REQUEST['isfreepost']);
		if($isfreepost){
			$isfreepost = $isfreepost - 1;
			$where .= " and zg.`isfreepost` = '$isfreepost' ";
		}
		$supplier = $this->db->YOne("select * from `@#_supplier` where `uid` = '$uid'");
		$supplierId = $supplier['id'];
		$count = $this->db->YCount("select zg.* from `@#_zg_shangpin` as zg left join `@#_supplier` as su on zg.supplierId = su.id where $where and zg.`supplierId` = '$supplierId'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,100000,$fenyenum,"0");	
		$list = $this->db->YPage("select zg.* from `@#_zg_shangpin` as zg left join `@#_supplier` as su on zg.supplierId = su.id where $where and zg.`supplierId` = '$supplierId' ORDER BY zg.`order` DESC ,su.`credibility` DESC ,zg.`qishu` DESC,zg.`time` DESC",array("num"=>100000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		return $result;
	}
	
	/*private function getGoodsShelves(){
		$uid = $this->userinfo['uid'];
		$isShelve = intval($_REQUEST['isShelve']);
		$dataId = intval($_REQUEST['dataId']);
		$where = " `isShelve` = '$isShelve'";
		if($dataId){
			$where .= " and `catesubid` = '$dataId' ";
		}
		$isfreepost = intval($_REQUEST['isfreepost']);
		if($isfreepost){
			$isfreepost = $isfreepost - 1;
			$where .= " and `isfreepost` = '$isfreepost' ";
		}
		$supplier = $this->db->YOne("select * from `@#_supplier` where `uid` = '$uid'");
		$supplierId = $supplier['id'];
		$count = $this->db->YCount("select * from `@#_zg_shangpin` as zg left join `@#_supplier` as su on zg.supplierId onn su.id  where $where and `supplierId` = '$supplierId'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");	
		$list = $this->db->YPage("select * from `@#_zg_shangpin` where $where and `supplierId` = '$supplierId' ORDER BY `sort` DESC ,`id` DESC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		return $result;
	}*/
	
	
	private function getSupplierCouponDetail(){
		$supplierId = intval($_REQUEST['id']);
		$coupon = $this->db->YOne("select * from `@#_shangpin` where `supplierId` = '$supplierId' and `shenyurenshu` > 0 and `canyurenshu` < `zongrenshu`");
		if($coupon){
			$status = 1;
		}else{
			$status = 0;
		}
		$coupon['percent'] = $coupon['canyurenshu'] / $coupon['zongrenshu'] * 100;
		$result['status'] = $status; 
		$result['list'] = $coupon; 
		return $result;	
	}
	
	public function setSupplierGoodsNewTypes(){
		$id = intval($_REQUEST['id']);
		$goods = $this->db->YOne("select * from `@#_zg_shangpin` where `id` = '$id'");
		$uid = $this->userinfo['uid'];
		$supplier = $this->db->YOne("select * from `@#_supplier` where `uid` = '$uid'");
		$supplierId = $supplier['id'];
		if(!$id || $goods['supplierId'] != $supplierId){
			$status = 0;
			$msg = '不存在的商品或此商品不属于你';
		}else{
			$sort = $_REQUEST['sort'];
			$type = $_REQUEST['type'];

			$data = array();
			foreach($sort as $key => $one){
				$data[$key] = array(
					'sort' => $one,
					'type' => $type[$key]
				);
			}
			$newTypes = serialize($data);
			$res = updateSet(array('newTypes'=>$newTypes),'zg_shangpin',$id);
			if($res){
				$status = 1;
				$msg = '添加规格成功';
			}else{
				$status = 0;
				$msg = '添加规格失败，请稍后再试';
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg));
	}
	
	public function setSupplierGoodsNewColor(){
		$id = intval($_REQUEST['id']);
		$goods = $this->db->YOne("select * from `@#_zg_shangpin` where `id` = '$id'");
		$uid = $this->userinfo['uid'];
		$supplier = $this->db->YOne("select * from `@#_supplier` where `uid` = '$uid'");
		$supplierId = $supplier['id'];
		if(!$id || $goods['supplierId'] != $supplierId){
			$status = 0;
			$msg = '不存在的商品或此商品不属于你';
		}else{
			$sort = $_REQUEST['sort'];
			$color = $_REQUEST['color'];

			$data = array();
			foreach($sort as $key => $one){
				$data[$key] = array(
					'sort' => $one,
					'color' => $color[$key]
				);
			}
			$newColor = serialize($data);
			$res = updateSet(array('color'=>$newColor),'zg_shangpin',$id);
			if($res){
				$status = 1;
				$msg = '添加颜色成功';
			}else{
				$status = 0;
				$msg = '添加颜色失败，请稍后再试';
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg));
	}
	
	public function doOnShelve(){
		$id = intval($_REQUEST['id']);
		$uid = $this->userinfo['uid'];
		$supplier = $this->db->YOne("select * from `@#_supplier` where `uid` = '$uid'");
		$supplierId = $supplier['id'];
		$shangpin = $this->db->YOne("select * from `@#_zg_shangpin` where `id` = '$id'");
		if(!$id || !$shangpin || $shangpin['supplierId'] != $supplierId){
			$status = 0;
			$msg = '不存在的商品或此商品不属于你';
		}else{
			if($shangpin['isShelve'] == 1){
				$status = 0;
				$msg = '此商品已经上架了';
			}else{
				$res = updateSet(array('isShelve'=>1),'zg_shangpin',$id);
				if($res){
					$status = 1;
					$msg = '上架成功';
				}else{
					$status = 0;
					$msg = '上架失败，请稍后再试';
				}
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg));
	}
	
	public function doOffShelve(){
		$id = intval($_REQUEST['id']);
		$uid = $this->userinfo['uid'];
		$supplier = $this->db->YOne("select * from `@#_supplier` where `uid` = '$uid'");
		$supplierId = $supplier['id'];
		$shangpin = $this->db->YOne("select * from `@#_zg_shangpin` where `id` = '$id'");
		if(!$id || !$shangpin || $shangpin['supplierId'] != $supplierId){
			$status = 0;
			$msg = '不存在的商品或此商品不属于你';
		}else{
			if($shangpin['isShelve'] == 0){
				$status = 0;
				$msg = '此商品已经下架了';
			}else{
				$res = updateSet(array('isShelve'=>0),'zg_shangpin',$id);
				if($res){
					$status = 1;
					$msg = '下架成功';
				}else{
					$status = 0;
					$msg = '下架失败，请稍后再试';
				}
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg));
	}
	
	public function delSupplierGoods(){
		$id = intval($_REQUEST['id']);
		$uid = $this->userinfo['uid'];
		$supplier = $this->db->YOne("select * from `@#_supplier` where `uid` = '$uid'");
		$supplierId = $supplier['id'];
		$shangpin = $this->db->YOne("select * from `@#_zg_shangpin` where `id` = '$id'");
		if(!$id || !$shangpin || $shangpin['supplierId'] != $supplierId){
			$status = 0;
			$msg = '不存在的商品或此商品不属于你';
		}else{
			$res = deleteInfo($id,'zg_shangpin');
			if($res){
				$status = 1;
				$msg = '删除成功';
			}else{
				$status = 0;
				$msg = '删除失败，请稍后再试';
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg));
	}
	
	public function doSaveLogo(){
		$id = intval($_REQUEST['id']);
		$uid = $this->userinfo['uid'];
		$supplier = $this->db->YOne("select * from `@#_supplier` where `id` = '$id' and `uid` = '$uid'");
		if(!$id || !$supplier){
			$status = 0;
			$msg = '不存在的商铺';
		}else{
			$supplier_logo = $_REQUEST['supplier_logo'];
			$supplier_poster = $_REQUEST['supplier_poster'];
			$res = updateSet(array('supplier_logo'=>$supplier_logo,'supplier_poster'=>$supplier_poster),'supplier',$id);
			if($res){
				$status = 1;
				$msg = '保存成功';
			}else{
				$status = 0;
				$msg = '保存失败，请稍后再试';
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg));
	}
	
	private function getSupplierComment(){
		$pid = intval($_REQUEST['pid']);
		$where = "`pid` = '$pid'";
		$count = $this->db->YCount("select * from `@#_supplier_pat_comment` where $where");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_supplier_pat_comment` where $where ORDER BY `time` DESC,`id` DESC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		foreach($list as $key => $one){
			$list[$key]['username'] = getUserInfo($one['uid'],'uid','username');
			$list[$key]['avatar'] = getUserInfo($one['uid'],'uid','img');
			$list[$key]['time'] = _buy_time($one['time']);
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		return $result;
	}
	
	public function addSupplierPatComment(){
		$content = $_REQUEST['content'];
		$pid = intval($_REQUEST['pid']);
		$uid = $this->userinfo['uid'];
		if(!$uid){
			$status = 2;
			$msg = '请先登录';
		}elseif(!$content){
			$status = 0;
			$msg = '评论内容不可为空';
		}elseif(mb_strlen($content) < 6){
			$status = 0;
			$msg = '评论内容至少6个字符以上';
		}else{
			$supplier_pat = $this->db->YOne("select sid from `@#_supplier_pat` where `id` = '$pid'");
			$supplier = $this->db->Yone("select uid from `@#_supplier` where `id` = '$supplier_pat[sid]'");
			if($uid == $supplier['uid']){
				$type = 1;
			}else{
				$type = 0;
			}
			$supplierPatComment = array(
				'uid' => $uid,
				'content' => htmlspecialchars($content),
				'time' => time(),
				'pid' => $pid,
				'type' => $type,
			);
			$res = insertInto($supplierPatComment,'supplier_pat_comment');
			if($res){
				$status = 1;
				$msg = '发布成功';
			}else{
				$status = 0;
				$msg = '发布失败';
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg));
	}
	
	
	private function getSupplierOrder(){
		$id = intval($_REQUEST['id']);
		$uid = $this->userinfo['uid'];
		$supplier = $this->db->YOne("select * from `@#_supplier` where `uid` = '$uid'");
		$status = intval($_REQUEST['status']);
		if($status != 9){
			$where = " and o.`status` = '$status'";
		}
		$time = time();
		$count = $this->db->YCount("select i.* from `@#_order_list` as o left join `@#_order_item` as i on o.`order_sn` = i.`order_sn` where o.`is_cancel` = '0' and o.`is_zhongchou` = '0' and o.`is_delete` = '0' and i.`supplier_id` = '$supplier[id]'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$lists = $this->db->YPage("select i.*,o.address_id,o.total_money,o.status,o.order_sn,o.express_id from `@#_order_list` as o left join `@#_order_item` as i on o.`order_sn` = i.`order_sn` where o.`is_cancel` = '0' and o.`is_delete` = '0' and o.`is_zhongchou` = '0' and i.`supplier_id` = '$supplier[id]' $where ORDER BY o.`id` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));		
		
		foreach($lists as $key => $one){
			$one['goods_name'] = html_entity_decode($one['goods_name']);
			if($one['express']){
				$one['express'] = '包邮';
			}else{
				$one['express'] = '自提';
			}
			$list[$one['order_sn']]['goods'][] = $one;
			$address = $this->db->YOne("select * from `@#_yonghu_dizhi` where `id` = '$one[address_id]'");
			$province = $this->db->YOne("select * from `@#_area` where `code` = '$address[sheng]'");
			$city = $this->db->YOne("select * from `@#_area` where `code` = '$address[shi]'");
			$distinct = $this->db->YOne("select * from `@#_area` where `code` = '$address[xian]'");
			$street = $this->db->YOne("select * from `@#_area` where `code` = '$address[jiedao]'");
			$address['address'] = $province['name'].$city['name'].$distinct['name'].$street['name'].$address['address'];
			if(!$address['shouhuoren']){
				$address['shouhuoren'] = '未填写';
			}
			if(!$address['mobile']){
				$address['mobile'] = '未填写';
			}
			if(!$address['address']){
				$address['address'] = '未填写';
			}
			$list[$one['order_sn']]['address'] = $address;
			$list[$one['order_sn']]['order']['total_money'] = $one['total_money'];	
			$list[$one['order_sn']]['order']['order_sn'] = $one['order_sn'];
			if($one['status'] == 0){
				$list[$one['order_sn']]['order']['order_status'] = '待买家付款';
			}elseif($one['status'] == 1){
				$list[$one['order_sn']]['order']['order_status'] = '待发货';
			}elseif($one['status'] == 2){
				$list[$one['order_sn']]['order']['order_status'] = '已发货，待确认';
			}elseif($one['status'] == 3){
				$list[$one['order_sn']]['order']['order_status'] = '交易成功，待评价';
			}elseif($one['status'] == 4){
				$list[$one['order_sn']]['order']['order_status'] = '已评价';
			}elseif($one['status'] == 5){
				$list[$one['order_sn']]['order']['order_status'] = '订单已取消';
			}
			$list[$one['order_sn']]['order']['status'] = $one['status'];
			
			if($one['express_id']){
				$express = $this->db->YOne("select * from `@#_express_list` where id = '$one[express_id]'");
				$express_company = $this->db->YOne("select * from `@#_express_company` where `id` = '$express[express_id]'");
				$express['express_company_name'] = $express_company['name'];
				$list[$one['order_sn']]['express'] = $express;
			}
			
			$express_list = $this->db->YList("select * from `@#_express_company`");
			$list[$one['order_sn']]['express_list'] = $express_list;
		}
		
		// echo "<Pre>";
		// print_r($list);
		// die();
		
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		$result = array(
			'status' => $status,
			'list' => $list
		);
		return $result;
	}

	public function ajaxSupplierSendOrder(){
		$code = $_REQUEST['code'];
		$express = intval($_REQUEST['express']);
		$express_sn = $_REQUEST['express_sn'];
		$list = $this->db->YOne("select * from `@#_order_list` where `order_sn` = '$code'");
		if(!$code || !$list){
			$status = 0;
			$msg = '不存在的订单';
		}elseif(!$express_sn){
			$status = 0;
			$msg = '请输入快递单号';
		}else{
			$express_company = $this->db->YOne("select * from `@#_express_company` where `id` = '$express'");
			if($express_company){
				$express_list_data = array();
				$express_list_data['order_sn'] = $code;
				$express_list_data['express_id'] = $express;
				$express_list_data['express_sn'] = $express_sn;
				$express_list_data['create_time'] = time();
				$res = insertInto($express_list_data,'express_list');
				if($res){
					$re = updateSet(array('express_id'=>$res,'status'=>2),'order_list',$code,'order_sn');
					if($re){
						$status = 1;
						$msg = '发货成功';
					}else{
						$status = 0;
						$msg = '发货失败，请稍后再试';
					}
				}else{
					$status = 0;
					$msg = '发货失败，请稍后再试';
				}
			}else{
				$status = 0;
				$msg = '不存在的快递公司';
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg,'express_sn'=>$express_sn,'express_company'=>$express_company['name']));
		exit();
	}
	
	public function doZan(){
		$id = intval($_REQUEST['id']);
		$comment = $this->db->YOne("select * from `@#_comment` where `id` = '$id'");
		if(!$comment){
			$status = 0;
			$msg = '不存在的评论';
		}else{
			$zan = $comment['zan'] + 1;
			$res = updateSet(array('zan'=>$zan),'comment',$id);
			if($res){
				$status = 1;
				$msg = '谢谢点赞';
			}else{
				$status = 0;
				$msg = '点赞失败';
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg,'zan'=>$zan));
		exit();
	}
	
	private function goodsComment(){
		$id = intval($_REQUEST['id']);
		$where = "`pid` = '$pid'";
		$count = $this->db->YCount("select * from `@#_supplier_pat_comment` where $where");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_supplier_pat_comment` where $where ORDER BY `time` DESC,`id` DESC",array("num"=>10000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		if($list){
			$status = 1;
		}else{
			$status = 0;
		}
		foreach($list as $key => $one){
			$list[$key]['username'] = getUserInfo($one['uid'],'uid','username');
			$list[$key]['avatar'] = getUserInfo($one['uid'],'uid','img');
			$list[$key]['time'] = _buy_time($one['time']);
		}
		$result['status'] = $status; 
		$result['list'] = $list; 
		return $result;
	}
	
	/*private function getSupplierOrder(){
		$id = intval($_REQUEST['id']);
		$uid = $this->userinfo['uid'];
		$supplier = $this->db->YOne("select * from `@#_supplier` where `uid` = '$uid'");
		$status = intval($_REQUEST['status']);
		if($status != 9){
			$where = " and o.`status` = '$status'";
		}
		$time = time();
		$count = $this->db->YCount("select * from `@#_order_list` as o left join `@#_order_item` as i on o.`order_sn` = i.`order_sn` where i.`supplier_id` = '$supplier[id]'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_order_list` as o left join `@#_order_item` as i on o.`order_sn` = i.`order_sn` where i.`supplier_id` = '$supplier[id]' $where ORDER BY o.`id` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));		
		/*梳理商户*
		$supplierArray = array();
		foreach($list as $key => $one){
			if(!in_array($one['code'],$supplierArray)){
				$supplierArray[] = $one['code'];
			}
		}
		
		$new_data = array();
		foreach($list as $key => $one){
			/*构造*
			$new_data[$one['code']]['supplierName'] = getSupplierInfo($one['supplierId'],'name');
			if($one['status'] == 0){
				$new_data[$one['code']]['order_status'] = '待买家付款';
			}elseif($one['status'] == 1){
				$new_data[$one['code']]['order_status'] = '待卖家发货';
			}elseif($one['status'] == 2){
				$new_data[$one['code']]['order_status'] = '卖家已发货';
			}elseif($one['status'] == 3){
				$new_data[$one['code']]['order_status'] = '交易成功';
			}
			
			/*if($one['pay_status'] == 0){
				$new_data[$one['code']]['pay_msg'] = '待买家付款';
			}elseif($one['pay_status'] == 1){
				$new_data[$one['code']]['pay_msg'] = '待卖家发货';
			}elseif($one['pay_status'] == 2){
				$new_data[$one['code']]['pay_msg'] = '卖家已发货';
			}elseif($one['pay_status'] == 3){
				$new_data[$one['code']]['pay_msg'] = '交易成功';
			}*
			$new_data[$one['code']]['express'] = '';
			if($one['express_id']){
				$express = $this->db->YOne("select * from `@#_express_list` where id = '$one[express_id]'");
				$express_company = $this->db->YOne("select * from `@#_express_company` where `id` = '$express[express_id]'");
				$express['express_company_name'] = $express_company['name'];
				$new_data[$one['code']]['express'] = $express;
			}

			if($one['is_delete'] == 1){
				$new_data[$one['code']]['order_delete'] = '订单已取消';
			}else{
				$new_data[$one['code']]['order_delete'] = '取消订单';
			}
			$new_data[$one['code']]['is_delete'] = $one['is_delete'];
			if($one['isfreepost']){
				$one['express'] = '包邮';
			}else{
				$one['express'] = '自有';
			}
			$new_data[$one['code']]['sid'] = $one['id'];
			$new_data[$one['code']]['code'] = $one['code'];
			$new_data[$one['code']]['totalMoney'] += $one['totalMoney'];
			$new_data[$one['code']]['goods'][] = $one;
			/*构造收货地址*
			$address = $this->db->YOne("select * from `@#_yonghu_dizhi` where `id` = '$one[addressId]'");
			$sheng = $this->db->YOne("select * from `@#_area` where `code` = '$address[sheng]'");
			$shi = $this->db->YOne("select * from `@#_area` where `code` = '$address[shi]'");
			$xian = $this->db->YOne("select * from `@#_area` where `code` = '$address[xian]'");
			$jiedao = $this->db->YOne("select * from `@#_area` where `code` = '$address[jiedao]'");
			$address['address'] = $sheng['name'].$shi['name'].$xian['name'].$jiedao['name'].$address['address'];
			$new_data[$one['code']]['address'] = $address;
			
			/*如果是已发货，构造物流*
			if($one['status'] == 1){
			
			}
			
			/*快递列表*
			$express_list = $this->db->YList("select * from `@#_express_company`");
			$new_data[$one['code']]['express_list'] = $express_list;
		}
		if($new_data){
			$status = 1;
		}else{
			$status = 0;
		}
		$result = array(
			'status' => $status,
			'list' => $new_data
		);
		return $result;
	}*/
	
}