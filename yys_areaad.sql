﻿# Host: localhost  (Version: 5.5.53)
# Date: 2018-01-23 15:05:04
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "yys_areaad"
#

DROP TABLE IF EXISTS `yys_areaad`;
CREATE TABLE `yys_areaad` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL COMMENT '链接',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '广告标题',
  `create_time` int(10) NOT NULL COMMENT '添加时间',
  `lat` varchar(255) DEFAULT NULL COMMENT '座标1',
  `lng` varchar(255) DEFAULT NULL COMMENT '座标2',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `title_style_color` varchar(255) DEFAULT NULL,
  `title_style_bold` varchar(255) DEFAULT NULL,
  `sub_text_des` varchar(255) DEFAULT NULL,
  `sub_text_len` varchar(255) DEFAULT NULL,
  `isDefault` enum('0','1') DEFAULT '1' COMMENT '是否为默认地区',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='区域广告位';
