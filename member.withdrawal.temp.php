<?php defined('G_IN_ADMIN')or exit('Access Denied.'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>会员提现审核</title>
<link rel="stylesheet" href="/statics/plugin/mstyle/css/style.css" type="text/css"><!--页面CSS-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/animate.min.css" type="text/css"><!--CSS3动画-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/timer.css" type="text/css"><!--CSS3动画-->
<script type="text/javascript" src="/statics/plugin/mstyle/js/jquery.min.js"></script><!--jQuery库-->
</head>
<body>
	<div class="container" style="width:1650px;">
		<div class="path">
			<i><a href="#"><img src="/statics/plugin/mstyle/img/ico_1.png" alt=""></a></i>
			<p>
				当前位置：<a href="#">财务管理</a> > <a href="#">商户提现审核</a> >
			</p>
			<div class="push">
				<a href="<?php echo G_ADMIN_PATH; ?>/index/Tdefault">欢迎界面</a>
				<a href="javascript:void(0)" onclick="location=location">刷新框架</a>
				<a href="<?php echo G_ADMIN_PATH; ?>/cache/init">清理缓存</a>
			</div>
		</div><!-- path -->
		<div class="checking wid1">
			<div class="fl-box">
			<form method="GET" action="#">
			<?php 
				if($uid_url){
					echo '<var>会员ID</var>
					<input type="text" value="'.$thisYonghu["uid"].'" disabled>
					<var>会员昵称</var>
					<input class="lar" type="text" value="'.$thisYonghu["username"].'" disabled>';
				}else if($uid_req){
					echo '<var>会员ID</var>
					<input type="text" name="uid" value="'.$thisYonghu["uid"].'">
					<var>会员昵称</var>
					<input class="lar" type="text" name="username" value="'.$thisYonghu["username"].'" disabled>
					<button type="submit" name="uidsubmit" value="筛选">筛选</button>';
				}else{
					echo '<var>会员ID</var>
					<input type="text" name="uid" value="">
					<var>会员昵称</var>
					<input class="lar" type="text" name="username" value="" disabled>
					<button type="submit" name="uidsubmit" value="筛选">筛选</button>';
				}
				?>
				<div class="grate-date">
					查询日期
					<input id="in1" type="text" name="start_time" value='<?php if(isset($_REQUEST["start_time"])) echo $_REQUEST["start_time"]; else echo "2018-1-1"; ?>' class="date_picker">
					至
					<input id="in2" type="text" name="end_time" value='<?php if(isset($_REQUEST["end_time"])) echo $_REQUEST['end_time']; else echo '2018-2-1'; ?>'  class="date_picker">
				</div>
				<button type="submit" name="sososubmit" value="筛选">筛选</button>
				&nbsp;&nbsp;
				<button style="width:100px;" type="submit" name="mainshensubmit" value="筛选"><?php echo $mianshen; ?>元以下免审</button>
				<button style="width:100px;" type="submit" name="daesubmit" value="筛选"><?php echo $dae; ?>元以上大额</button>
				<button type="submit" name="unchecksubmit" value="筛选">未审核</button>
				<button type="submit" name="checkedsubmit" value="筛选">已审核</button>
				<button type="submit" name="failedsubmit" value="筛选">未通过</button>
			</form>
			</div>
			<div class="fr-box">
				<span>查询<i>10000</i>元</span>
				<!--span>每页:
				注释：
				免审核起点金额配置
				$AuditsWithdrawals = $db->YOne("select * from `yys_configs` where `name`='AuditsWithdrawals'");
				大额人工审核
				$largeWithdrawals = $db->YOne("select * from `yys_configs` where `name`='largeWithdrawals'");
				
					<select name="" id="">
						<option value="">10</option>
						<option value="">20</option>
						<option value="">30</option>
					</select>
				</span-->
			</div>
		</div><!-- nickname -->
		<div class="checking-table dt wid1">
			<ul>
				<li class="head">
					<span>商户ID</span>
					<span>店标</span>
					<span>店铺名称</span>
                    <span>所属城市</span>
					<span>绑定手机</span>
					<span>申请时间</span>
					<span>账户金额</span>
					<span>类型</span>
					<span>提现金额</span>
					<span>手续费/元</span>
					<span>微信回复</span>
					<span>审核状态</span>
					<span>管理提现申请</span>
					<span>提现</span>
					<span>钱包</span>
				</li>
				<?php foreach($commissions as $key=>$v){ ?>
				<li>
					<span><?php echo $v['id']; ?></span>
					<span>
						<i><img src="<?php echo YYS_UPLOADS_PATH.'/'.$v['supplier_logo'];?>" alt=""></i>
					</span>
					<span><?php echo $v['supplierName']; ?></span>
                    <span><?php echo getCityName($v['province']).getCityName($v['city']);?></span>
					<span><?php echo $v['mobile'];?></span>
					<span><?php echo date("Y-m-d H:i:s",$v['time']);?></span>
					<span><?php echo $v['money1'];?></span>
					<span>
									<?php
					if($v['payType'] == 'wechat'){
						echo "微信"; 
					}elseif($v['payType'] == 'alipay'){
						echo "支付宝";
					}
				?>
					</span>
					<span><?php echo $v['money']; ?></span>
					<span><?php echo $v['procefees']; ?></span>
					<span><var><?php
					$return_msg = unserialize($v['wxReply']);
					echo $return_msg['return_msg']; ?></var></span>
					<span><?php if($v['auditstatus']==1){echo "审核通过";}else if($v['auditstatus']==2){echo "已退回";}else{echo "未审核";} ?></span>
					<?php 
					if($v['auditstatus'] == 0){
				?>
					<span><a href="/member/member/withdrawal_review/<?php echo $v['id'];?>">手打款</a>| <a href="/member/member/withdrawal_review_wx/<?php echo $v['id'];?>">微信打</a> | <a href="/member/member/withdrawal_review_return/<?php echo $v['id'];?>">驳回</a></span>
				<?php 
					}elseif($v['auditstatus'] == 1){
				?>
					<span style='color:#0000FF'>已审核/打款</span>
				<?php
					}elseif($v['auditstatus'] == 2){
						echo "<span style='color:#FF0000'>已退回</span>";
					}
				?></span>
					<span><a href="/member/member/withdrawal/<?php echo $v['id']; ?>"><i class="i1"></i></a></span>
					<span><a href="/member/member/recharge/<?php echo $v['uid']; ?>"><i class="i2"></i></a></span>
				</li><?php } ?>
			</ul>
		</div>
		<div class="pages">
			<span>
				<?php echo $fenye->show('new')['html']; ?>
			</span>
			<span>共 <?php echo ceil($zongji/$num); ?> 页</span>
		
			<span>到第<input type="text" id="page" value="">页</span>
			<span><a class="page-but" id="gopage" href="javascript:void(0);">确定</a></span>
		</div>
	</div><!-- container -->
</body>
<script src="/statics/plugin/mstyle/js/jquery.date_input.pack.js"></script>
<script src="/statics/plugin/mstyle/js/library-3.28.js"></script><!--自定义封装函数-->
<script src="/statics/plugin/mstyle/js/scrollanim.min.js"></script><!--动画效果库-->
<script>
</script>
</html>
