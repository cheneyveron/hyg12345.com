/*获取地址*/
function getUserAddress(page){
	layer.open({type:2,shade:'background-color: rgba(0,0,0,.1)',content:'加载中...'});
	$.ajax({
		type: "POST",
		url: "/mobile/ajax_user/ajax_user_data/",
		dataType:'json',
		data:{
			page:page,
			datatype:'getUserAddressList',
		},
		success: function(data){
			var html = '';
			if(data.status == 1){
				html = '<ul>';
				$.each(data.list,function (index,obj){
					html += '<li id="addressId_'+obj.id+'">';
					html += '	<div class="fl-box">';
					html += '		<p>姓名：'+obj.shouhuoren+'<var>手机：'+obj.mobile+'</var></p>';
					html += '		<p><span>收货地址：</span><em>'+obj.sheng+obj.shi+obj.xian+obj.jiedao+'</em></p>';
					html += '	</div>';
					html += '	<div class="fr-box">';
					html += '		<input type="radio" name="addressId" value="'+obj.id+'"/>';
					html += '		<a href="javascript:void(0);" onclick="deleteUserAddress('+obj.id+');" class="ico-del del"></a>';
					html += '	</div>';
					html += '</li>';
				});
				html += '</ul>';
				$(".Management-address").html(html);
			}else{
				html += '<ul><li class="nolist">暂无参与记录</li></ul>';
				$(".qa-recording").html(html);
			}
			layer.closeAll()
		}
	}); 
}


/*删除地址*/
function deleteUserAddress(id){
	layer.open({type:2,shade:'background-color: rgba(0,0,0,.1)',content:'加载中...'});
	$.ajax({
		type: "POST",
		url: "/mobile/ajax_user/deleteUserAddress/",
		dataType:'json',
		data:{
			id:id
		},
		success: function(data){
			showTips(data.msg);
			if(data.status == 1){
				$("#addressId_"+id).remove();
			}
			layer.closeAll()
		}
	}); 
}

/*地址联动*/
function ajaxGetArea(code,level){
	var code = code.val();
	$.ajax({
		type: "POST",
		url: "/mobile/ajax_user/ajax_add_data/",
		dataType:'json',
		data:{code:code,level:level},
		success: function(data){
			if(data.status == 1){
				var html = '<option value="0">请选择</option>';
				$.each(data.list,function (index,obj){
					html += '<option value="'+obj.code+'">'+obj.name+'</option>';
				});

				if(level == '2'){
					$("select[name='city']").html(html);
					$("select[name='country']").html(html);
					$("select[name='area']").html(html);
					$("select[name='street']").html(html);
				}else if(level == '3'){
					$("select[name='country']").html(html);
					$("select[name='area']").html(html);
					$("select[name='street']").html(html);
				}else if(level == '4'){
					$("select[name='area']").html(html);
					$("select[name='street']").html(html);
				}else if(level == '5'){
					$("select[name='area']").html(html);
				}
			}
		}
	}); 
}

function submitAddress(type){
	var province = $("#province").val();
	var city = $("#city").val();
	var country = $("#country").val();
	var street = $("#street").val();
	var address = $("#address").val();
	var shouhuoren = $("#shouhuoren").val();
	var mobile = $("#mobile").val();
	var isDefault = $("#isDefault").val();
	
	if(province == 0 || province == ''){
		var msg = '省不可为空';
	}else if(city == 0 || city == ''){
		var msg = '城市不可为空';
	}else if(country == 0 || country == ''){
		var msg = '区县不可为空';
	}else if(street == 0 || street == ''){
		var msg = '街道不可为空';
	}else if(address == 0 || address == ''){
		var msg = '详细地址不可为空';
	}else if(shouhuoren == 0 || shouhuoren == ''){
		var msg = '收货人不可为空';
	}else if(!checkPhone(mobile)|| mobile == ''){
		var msg = '手机号码格式不正确';
	}else{
		$.ajax({
			type: "POST",
			url: "/mobile/ajax_user/saveUserAddress/",
			dataType:'json',
			data:{
				province : province,
				city : city,
				country : country,
				street : street,
				address : address,
				shouhuoren : shouhuoren,
				mobile : mobile,
				isDefault : isDefault,
			},
			success: function(data){
				if(data.status == 1){
					if(type == 'Cart'){
						showMsg(data.msg,'温馨提示',"/mobile/settings/addressList");
					}else{
						showMsg(data.msg,'温馨提示',"/mobile/cart/pay");
					}
				}else{
					showMsg(data.msg);
				}
				return false;
			}
		}); 
	}
	showTips(msg);
	return false;
}

/*充值*/
function ajaxGetUserData_1(datatype,date){
	layer.open({type:2,shade:'background-color: rgba(0,0,0,.1)',content:'加载中...'});
	$.ajax({
		type: "POST",
		url: "/mobile/ajax_user/ajax_user_data/",
		dataType:'json',
		data:{
			datatype:datatype,
			date:date
		},
		success: function(data){
			if(data.status == 1){
				var html = '';
				$.each(data.list,function (index,obj){
					html += '<li>';
					html += '	<h3>'+obj.pay_type+'</h3>';
					html += '	<span>'+obj.time+'</span>';
					html += '	<span class="fr">'+obj.money+'</span>';
					html += '</li>';
				});
			}else{
				var html = '<li style1="text-align:center;">';
					html += '当前还没有记录';
					html += '</li>';
			}
			
			if(datatype == 'charge'){
				$(".moneyShow").html(data.money);
				$(".monthShow").html(data.month);
			}
			$(".Recharge").find('ul').html(html);
			layer.closeAll();
		}
	}); 
}

/*写入地*/
$(".insertOrderAddress").unbind('click').click(function(){
	layer.open({type:2,shade:'background-color: rgba(0,0,0,.1)',content:'加载中...'});
	var gotoact = $("#gotoact").val();
	if(gotoact == 'Cart'){
		var addressId = $("input[name='addressId']:checked").val();
		if(addressId == '' || !addressId){
			showMsg('请选择收货地址');
		}else{
			$.ajax({  
				url:LOCAL_PATH+'/mobile/ajax_cart/setUserAddress',
				data:{
					addressId:addressId,
				},  
				type:'post',
				cache:false,
				dataType:'json',  
				success:function(data) {  
					if(data.status == 1){
						showMsg(data.msg,'温馨提示','/mobile/cart/pay');
					}else{
						showTips(data.msg);
					}
					return false;
				},  
				error:function(){
					showTips('内部错误，稍后重试！');
					return false;
				}
			});
		}
	}else{
		var orderId = $("#orderId").val();
		var addressId = $("input[name='addressId']:checked").val();
		if(orderId == '' || !orderId){
			showMsg('没有订单哦');
		}else if(addressId == '' || !addressId){
			showMsg('请选择收货地址');
		}else{
			$.ajax({  
				url:LOCAL_PATH+'/mobile/ajax_user/setAddress',
				data:{
					addressId:addressId,
					orderSn:orderId
				},  
				type:'post',
				cache:false,
				dataType:'json',  
				success:function(data) {  
					if(data.status == 1){
						showMsg(data.msg,'温馨提示','/mobile/home/orderlist');
					}else{
						showTips(data.msg);
					}
					return false;
				},  
				error:function(){
					showTips('内部错误，稍后重试！');
					return false;
				}
			});
		}
	}
});

/*提现*/

function tx(){
	layer.open({type:2,shade:'background-color: rgba(0,0,0,.1)',content:'加载中...'});
	var money = $("#money").val();
	if(money == '' || money == 0){
		$("#money").focus();
		showMsg('请输入要提现的金额！');
		return false;
	}else{
		$.ajax({
			type: "POST",
			url: "/mobile/ajax_user/doCash/",
			dataType:'json',
			data:{
				money:money,
			},
			success: function(data){
				if(data.status == 1){
					var url = '/mobile/settings/withdrawal';
					showMsg(data.msg,url);
				}else if(data.status == 2){
					showMsg(data.msg);
				}else{
					showMsg(data.msg);
				}
				return false;
			}
		}); 
	}
}

function txMoney(money){
	$("#money").val(money);
}

function maxTxMoney(maxMoney){
	var money = $("#money").val();
	if(money*1 > maxMoney*1){
		$("#money").val(maxMoney);
	}
}


$(".fenxiang1").find('li').click(function(){
	var datatype = $(this).attr("datatype");
	$(this).addClass('cur2').siblings().removeClass('cur2');
	ajaxGetUserData(datatype);
});

function ajaxGetUserData(datatype,date){
	console.log(datatype);
	layer.open({type:2,shade:'background-color: rgba(0,0,0,.1)',content:'加载中...'});
	$.ajax({
		type: "POST",
		url: "/mobile/ajax_user/ajax_user_data/",
		dataType:'json',
		data:{date:date,datatype:datatype},
		success: function(data){
			if(data.status == 1){
				var html = '';
				if(datatype == 'cost'){
					$.each(data.list,function (index,obj){
						html += '<li>';
						html += '	<h3>';
						html += '		<div class="pict">';
						html += '			<img src="'+YYS_UPLOADS_PATH+'/'+obj.thumb+'" alt=""/>';
						html += '		</div>';
						html += '	</h3>';
						html += '	<span>'+obj.time+'</span>';
						html += '	<span class="fr">-'+obj.money+'</span>';
						html += '</li>';
					});
				}else if (datatype == 'score'){
					$.each(data.list,function (index,obj){
						html += '<li>';
						html += '	<h3 style="font-size:14px;">'+obj.content+'</h3>';
						html += '	<span>'+obj.time+'</span>';
						html += '	<span class="fr">'+obj.money+'</span>';
						html += '</li>';
					});
				}else if (datatype == 'withdrawl'){
					$.each(data.list,function (index,obj){
						html += '<li>';
						html += '	<span>'+obj.time+'</span>';
						html += '	<h3>'+obj.status+'</h3>';
						html += '	<span class="fr">'+obj.money+'</span>';
						html += '</li>';
					});
				}else{
					$.each(data.list,function (index,obj){
						html += '<li>';
						html += '	<span>'+obj.time+'</span>';
						html += '	<h3>微信</h3>';
						html += '	<span class="fr">'+obj.money+'</span>';
						html += '</li>';
					});
				}
			}else{
				var html = '<li style="text-align:center;">';
					html += '当前还没有记录';
					html += '</li>';
			}
			
			if(datatype == 'charge' || datatype == 'cost' || datatype == 'score' || datatype == 'withdrawl'){
				$(".moneyShow").html(data.money);
				$(".monthShow").html(data.month);
			}
			
			$(".withdrawal1").find('ul').html(html);
			layer.closeAll();
		}
	}); 
}

/*修改密码*/
function modifyPassword(){
	var password = $("#password").val();
	var newpassword = $("#newpassword").val();
	var repassword = $("#repassword").val();
	
	if(!password || password == ''){
		showMsg('请输入您的旧密码');
	}else if(!newpassword.match(/[a-zA-Z0-9]{6,}/)){
		showMsg('请输入8-20位字母，数字或符号');
	}else if(newpassword != repassword){
		showMsg('两次输入密码不一致');
	}else{
		$.ajax({
			type: "POST",
			url: "/mobile/ajax_user/modifyPassword/",
			dataType:'json',
			data:{
				password:password,
				newpassword:newpassword,
				repassword:repassword,
			},
			success: function(data){
				if(data.status == 1){
					showMsg(data.msg,'温馨提示',data.url);
				}else{
					showMsg(data.msg);
				}
				return false;
			}
		}); 
	}
}

/*修改头像*/
function useravatar(){
	var username = $("#username").val();
	var sex = $("#sex").val();
	var img = $("#img").val();
	if(username == '' || !username){
		showTips('请输入正确的昵称');
	}else if(img == '' || !img){
		showTips('请上传您的头像');
	}else{
		$.ajax({
			type: "POST",
			url: "/mobile/ajax_user/modifyAvatar/",
			dataType:'json',
			data:{
				username:username,
				sex:sex,
				img:img,
			},
			success: function(data){
				if(data.status == 1){
					showMsg(data.msg,'温馨提示',data.url);
				}else{
					showMsg(data.msg);
				}
				return false;
			}
		}); 
	}
	return false;
}

