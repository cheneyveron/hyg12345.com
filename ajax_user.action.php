<?php 
defined('IN_YYS')or exit('Access Denied.');
System::DOWN_App_class('base','member','no');
System::DOWN_App_fun('my','index');
System::DOWN_App_fun('user','index');
System::DOWN_sys_fun('send');
System::DOWN_sys_fun('user');
class ajax_user extends base {
	
	public function __construct(){
		parent::__construct();
		$this->db = System::DOWN_sys_class('model');
	}
	
	/*统一入口*/
	public function ajax_user_data(){
		$datatype = $_REQUEST['datatype'];
		$result = $this->$datatype();
		if($result){
			$result['list'] = $result;
			$result['status'] = 1;
		}else{
			$result['list'] = $result;
			$result['status'] = 0;
		}
		$result['money'] = $this->money ? number_format($this->money,2) : 0;
		$result['month'] = $this->month;
		echo json_encode($result);
	}

	/*本月充值*/
	private function charge(){
		$userInfo = $this->userinfo;
		$date = $_REQUEST['date'];
		$uid = $userInfo['uid'];
		
		$where = ' 1=1 ';
		$where .= " and `uid` = '$uid'";
		
		if($date){
			$startTime = strtotime('20'.$date);
			$endTime = date('Y-m',$startTime + 86400 * 30);
			$where .= " and `time` >= '$startTime' ";
			$where .= " and `time` <= '".$endTime."'";
		}else{
			$startTime = strtotime(date('Y-m'));
			$where .= " and `time` >= '$startTime' ";
			$where .= " and `time` <= '".time()."'";
		}	

		$count = $this->db->YCount("select * from `@#_yonghu_addmoney_record` where $where ");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_addmoney_record` where $where ORDER BY `time` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		$money = 0;
		foreach($list as $key => $one){
			$list[$key]['time'] = date('Y-m-d H:i:s',$one['time']);
			$money += $one['money'];
			$pay_type += $one['pay_type'];
		}
		$this->month = date('Y年m月',$startTime);
		$this->money = $money;
		return $list;
	}
	
	/*三月内充值*/
	private function three_month(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$nowTime = time();
		$time = strtotime("-3 months");
		$count = $this->db->YCount("select * from `@#_yonghu_addmoney_record` where `uid` = '$uid' and `time` >= '$time' and `time` <= '$nowTime'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_addmoney_record` where `uid` = '$uid' and `time` >= '$time' and `time` <= '$nowTime' ORDER BY `time` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$list[$key]['time'] = date('Y-m-d H:i:s',$one['time']);
		}
		return $list;
	}
	
	/*三月前充值*/
	private function three_month_go(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$time = strtotime("-3 months");
		$count = $this->db->YCount("select * from `@#_yonghu_addmoney_record` where `uid` = '$uid' and `time` <= '$time'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_addmoney_record` where `uid` = '$uid' and `time` <= '$time' ORDER BY `time` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$list[$key]['time'] = date('Y-m-d H:i:s',$one['time']);
		}
		return $list;
	}
	
	/*当月积分*/
	private function score(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$startTime = strtotime(date('Y-m'));
		$time = time();
		$count = $this->db->YCount("select * from `@#_yonghu_zhanghao` where `pay` = '福分' and `uid` = '$uid' and `time` >= '$startTime' and `time` <= '$time'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_zhanghao` where `pay` = '福分' and `uid` = '$uid' and `time` >= '$startTime' and `time` <= '$time' ORDER BY `time` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		
		$money = 0;
		foreach($list as $key => $one){
			$list[$key]['time'] = date('Y-m-d H:i:s',$one['time']);
			if($one['type'] == 1){
				$list[$key]['money'] = $one['money'];
			}else{
				$list[$key]['money'] = '-'.$one['money'];
			}
			$money += $one['money'];
		}
		$this->month = date('Y年m月');
		$this->money = $money;
		
		return $list;
	}
	
	/*三月内积分*/
	private function threeMonthScore(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$nowTime = time();
		$time = strtotime("-3 months");
		$count = $this->db->YCount("select * from `@#_yonghu_zhanghao` where `pay` = '福分' and `uid` = '$uid' and `time` >= '$time' and `time` <= '$nowTime'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_zhanghao` where `pay` = '福分' and `uid` = '$uid' and `time` >= '$time' and `time` <= '$nowTime' ORDER BY `time` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$list[$key]['time'] = date('Y-m-d H:i:s',$one['time']);
		}
		return $list;
	}
	
	/*三月前积分*/
	private function threeMonthAgoScore(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$time = strtotime("-3 months");
		$count = $this->db->YCount("select * from `@#_yonghu_zhanghao` where `pay` = '福分' and `uid` = '$uid' and `time` <= '$time'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_zhanghao` where `pay` = '福分' and `uid` = '$uid' and `time` <= '$time' ORDER BY `time` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$list[$key]['time'] = date('Y-m-d H:i:s',$one['time']);
		}
		return $list;
	}	
	
	/*当月消费*/
	private function cost(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$time = $_REQUEST['date'];
		if($time){
			$startTime = strtotime('20'.$time);
			$endTime = $startTime + 86400 * 30;
			$where = "`time` >= '$startTime' and `time` <= '$endTime'";
		}else{
			$where = " 1=1";
		}

		$count = $this->db->YCount("select * from `@#_yonghu_zhanghao` where `pay` = '账户' and `type` = '-1' and `uid` = '$uid' and $where");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_zhanghao` where `pay` = '账户' and `type` = '-1' and `uid` = '$uid' and $where ORDER BY `time` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		$money = 0;
		foreach($list as $key => $one){
			$order_info = $this->db->YOne("select * from `@#_order_list` where `order_sn` = '$one[order_sn]'");
			$supplier = $this->db->YOne("select * from `@#_supplier` where `id` = '$order_info[supplier_id]'");
			$list[$key]['thumb'] = $supplier['supplier_logo'];
			$list[$key]['time'] = date('Y-m-d H:i:s',$one['time']);
			$money += $one['money'];
		}
		$this->month = date('Y年m月');
		$this->money = $money;
		return $list;
	}
	
	/*三月内积分*/
	private function threeMonthCost(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$nowTime = time();
		$time = strtotime("-3 months");
		$count = $this->db->YCount("select * from `@#_yonghu_zhanghao` where `pay` = '账户' and `type` = '-1' and `uid` = '$uid' and `time` >= '$time' and `time` <= '$nowTime'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_zhanghao` where `pay` = '账户' and `type` = '-1' and `uid` = '$uid' and `time` >= '$time' and `time` <= '$nowTime' ORDER BY `time` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$list[$key]['time'] = date('Y-m-d H:i:s',$one['time']);
		}
		return $list;
	}
	
	/*三月前积分*/
	private function threeMonthAgoCost(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$time = strtotime("-3 months");
		$count = $this->db->YCount("select * from `@#_yonghu_zhanghao` where `pay` = '账户' and `type` = '-1' and `uid` = '$uid' and `time` <= '$time'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_zhanghao` where `pay` = '账户' and `type` = '-1' and `uid` = '$uid' and `time` <= '$time' ORDER BY `time` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$list[$key]['time'] = date('Y-m-d H:i:s',$one['time']);
		}
		return $list;
	}	
	
	
	/*惠券进行中*/
	private function going(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$startTime = strtotime(date('Y-m'));
		$time = time();
		$count = $this->db->YCount("select * from `@#_shangpin` as sp left join `@#_yonghu_yys_record` as yyr on sp.`id` = yyr.`shopid` where yyr.`uid` = '$uid' and sp.`q_uid` IS NULL GROUP BY yyr.`shopid` ");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,100000,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_shangpin` as sp left join `@#_yonghu_yys_record` as yyr on sp.`id` = yyr.`shopid` where yyr.`uid` = '$uid' and sp.`q_uid` IS NULL GROUP BY yyr.`shopid` ORDER BY yyr.`time` DESC",array("num"=>100000,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		
		foreach($list as $key => $one){
			$shopInfo = $this->db->YOne("select * from `@#_shangpin` where `id` = '$one[shopid]'");
			$list[$key]['thumb'] = YYS_UPLOADS_PATH.'/'.$shopInfo['thumb'];
			$list[$key]['width'] = ($shopInfo['canyurenshu'] / $shopInfo['zongrenshu']) * 100;
			$list[$key]['canyurenshu'] = $shopInfo['canyurenshu'];
			$list[$key]['zongrenshu'] = $shopInfo['zongrenshu'];
			$list[$key]['shenyurenshu'] = $shopInfo['shenyurenshu'];
			$list[$key]['supplierId'] = $shopInfo['supplierId'];
		}
		return $list;
	}
	
	/*惠券已揭晓*/
	private function monthPrize(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$startTime = strtotime(date('Y-m'));
		$time = time();
		$count = $this->db->YCount("select r.* from `@#_yonghu_yys_record` as r left join `@#_shangpin` as s on r.shopid = s.id where r.`uid` = '$uid' and r.`huode` != '0' and s.`q_end_time` <= '$time'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select r.* from `@#_yonghu_yys_record` as r left join `@#_shangpin` as s on r.shopid = s.id where r.`uid` = '$uid' and r.`huode` != '0' and s.`q_end_time` <= '$time' ORDER BY r.`time` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		
		foreach($list as $key => $one){
			$shopInfo = $this->db->YOne("select thumb from `@#_shangpin` where `id` = '$one[shopid]'");
			$list[$key]['thumb'] = YYS_UPLOADS_PATH.'/'.$shopInfo['thumb'];
			$img = getUserInfo($one['uid'],'uid','img');
			$list[$key]['img'] = $img['img'];
		}
		return $list;
	}
	
	/*三个月内已揭晓*/
	private function threeMonthPrize(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$nowTime = time();
		$time = strtotime("-3 months");
		$count = $this->db->YCount("select * from `@#_yonghu_yys_record` as r left join `@#_shangpin` as s on r.`shopid` = s.`id` where s.`q_end_time` < '$nowTime' and s.`q_uid` IS NOT NULL and r.`uid` = '$uid' and FIND_IN_SET('已付款',r.`status`) and r.`time` >= '$time' and r.`time` <= '$nowTime'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_yys_record` as r left join `@#_shangpin` as s on r.`shopid` = s.`id` where s.`q_end_time` < '$nowTime' and s.`q_uid` IS NOT NULL and r.`uid` = '$uid' and FIND_IN_SET('已付款',r.`status`) and r.`time` >= '$time' and r.`time` <= '$nowTime' ORDER BY r.`time` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));

		foreach($list as $key => $one){
			$shopInfo = $this->db->YOne("select thumb from `@#_shangpin` where `id` = '$one[shopid]'");
			$list[$key]['thumb'] = YYS_UPLOADS_PATH.'/'.$shopInfo['thumb'];
		}
		return $list;
	}
	
	/*三个月前已揭晓*/
	private function threeMonthAgoPrize(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$time = strtotime("-3 months");
		$nowTime = time();
		$count = $this->db->YCount("select * from `@#_yonghu_yys_record` as r left join `@#_shangpin` as s on r.`shopid` = s.`id` where s.`q_end_time` < '$nowTime' and s.`q_uid` IS NOT NULL and r.`uid` = '$uid' and FIND_IN_SET('已付款',r.`status`) and r.`time` <= '$time'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_yys_record` as r left join `@#_shangpin` as s on r.`shopid` = s.`id` where s.`q_end_time` < '$nowTime' and s.`q_uid` IS NOT NULL and r.`uid` = '$uid' and FIND_IN_SET('已付款',r.`status`) and r.`time` <= '$time' ORDER BY r.`time` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$shopInfo = $this->db->YOne("select thumb from `@#_shangpin` where `id` = '$one[shopid]'");
			$list[$key]['thumb'] = YYS_UPLOADS_PATH.'/'.$shopInfo['thumb'];
		}
		return $list;
	}
	
	/*分页加载*/
	public function getAjaxPage(){
		$functionName = $_REQUEST['functionName'];
		if($functionName){
			$page = intval($_POST['page']);
			if(!$page || $page <= 0){
				$page = 1;
			}
			$num = intval($_POST['num']);
			if(!$num || $num <= 0){
				$num = 10;
			}
			$return['list'] = $this->$functionName($page,$num);
			$return['status'] = 1;
		}else{
			$return = array(
				'status' => 0,
				'msg' => '内部错误',
			);
		}
		echo json_encode($return);
	}
	
	/*从JD采集地址数据，这里需要JD的API*/
	public function getAddressInfo(){
		$url = 'http://easybuy.jd.com//address/getProvinces.action';
		$res = file_get_contents($url);
		print_r($res);
	}
	
	/*获取用户银行列表*/
	private function bankList(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$startTime = strtotime(date('Y-m'));
		$time = time();
		$count = $this->db->YCount("select * from `@#_yonghu_yys_record` where `uid` = '$uid' and `huode` != '0'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_yys_record` where `uid` = '$uid' and `huode` != '0' ORDER BY `time` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		
		foreach($list as $key => $one){
			$shopInfo = $this->db->YOne("select thumb from `@#_shangpin` where `id` = '$one[shopid]'");
			$list[$key]['thumb'] = YYS_UPLOADS_PATH.'/'.$shopInfo['thumb'];
		}
		return $list;
	}
	
	
	/*我要提现*/
	public function doCash(){
		$userInfo = $this->userinfo;
		if(!$userInfo){
			$return['status'] = 2;
		}else{
			$money = $_REQUEST['money'];
			$userInfo = getUserInfo($userInfo['uid']);
			$withdrawStart = $this->db->YOne("select * from `yys_configs` where `name`='withdrawStart'");
			$withdrawStart = $withdrawStart['value'];
			if($money < $withdrawStart){
				$return['status'] = 0;
				$return['msg'] = '最低提现金额为：'.$withdrawStart.'元';
			}elseif($userInfo['money1'] < $money){
				$return['status'] = 0;
				$return['msg'] = '可提金额不足';
			}else{
				$goodsFee = $this->db->YOne("select * from `yys_configs` where `name`='goodsFee'");
				$goodsFee = $goodsFee['value'];
				$yonghu_cashout = array(
					'uid' => $userInfo['uid'],
					'remoney' => $userInfo['money'] - $money,
					'money' => $money * (1 - $goodsFee),
					'procefees' => $goodsFee * $money,
					'time' => time(),
				);
				$res = insertInto($yonghu_cashout,'yonghu_cashout');
				if($res){
					/*先扣除防止重复提交*/
					$re = updateSet(array('money1'=>$userInfo['money1'] - $money),'yonghu',$userInfo['uid'],'uid');
					if($re){
						$return['status'] = 1;
						$return['msg'] = '提现申请成功，请等待客服审核！';
					}else{
						$return['status'] = 0;
						$return['msg'] = '提现申请失败';
					}
				}else{
					$return['status'] = 0;
					$return['msg'] = '提现申请失败';
				}
			}
		}
		echo json_encode($return);
		exit();
	}
	
	/*提现*/
	private function withdrawl(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$date = $_REQUEST['date'];
		$where = " `uid` = '$uid' ";
		if($date){
			$nowTime = time();
			$time = strtotime("-3 months");
			$where = '';
		}

		$count = $this->db->YCount("select * from `@#_yonghu_cashout` where $where");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_cashout` where $where ORDER BY `time` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		$money = 0;
		foreach($list as $key => $one){
			$list[$key]['time'] = date('Y-m-d H:i:s',$one['time']);
			$money += $one['money'];
			if($one['auditstatus'] == 0){
				$list[$key]['status_msg'] = '待审核';
			}elseif($one['auditstatus'] == 1){
				$list[$key]['status_msg'] = '已提现';
			}else{
				$list[$key]['status_msg'] = '已退回';
			}
		}
		$this->month = date('Y年m月');
		$this->money = $money;
		return $list;
	}
	
	
	/*设置收货地址*/
	public function setAddress(){
		$addressId = intval($_REQUEST['addressId']);
		$orderSn = $_REQUEST['orderSn'];
		$userInfo = $this->userinfo;
		if(!$userInfo){
			$status = 2;
			$msg = '请先登录';
		}else{
			if(!$addressId){
				$status = 0;
				$msg = '请选择正确的地址信息';
			}else{
				$addressInfo = $this->db->Yone("select * from `@#_yonghu_dizhi` where `id` = '$addressId' and `uid` = '$huiyuan[uid]'");
				if(!$addressInfo){
					if($orderSn){
						$orderInfo = $this->db->Yone("select * from `@#_yonghu_yys_record` where `id` = '$orderSn'");
						if(!$orderInfo){
							$status = 0;
							$msg = '不存在这个订单信息，请核实！如有疑问请联系客服！';
						}else{
							$res = updateSet(array('addressId' => $addressId,'status' => '已付款,未发货,未完成,已提交地址'),'yonghu_yys_record',$orderSn);
							if($res){
								$status = 1;
								$msg = '设置成功，返回列表';
							}else{
								$status = 0;
								$msg = '选择失败，请稍后再试！';
							}
						}
					}else{
						$status = 0;
						$msg = '不存在这个订单信息，请核实！如有疑问请联系客服！error:0001';
					}
				}else{
					$status = 0;
					$msg = '不存在这个地址信息或地址信息不属于您！error:0002';
				}
			}
		}
		$return = array(
			'status' => $status,
			'msg' => $msg
		);
		echo json_encode($return);
	}
	
	/*获取用户地址列表*/
	private function getUserAddressList(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$startTime = strtotime(date('Y-m'));
		$time = time();
		$count = $this->db->YCount("select * from `@#_yonghu_dizhi` where `uid` = '$uid'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_dizhi` where `uid` = '$uid' ORDER BY `id` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$list[$key]['sheng'] = getCityName($one['sheng']);
			$list[$key]['shi'] = getCityName($one['shi']);
			$list[$key]['xian'] = getCityName($one['xian']);
			$list[$key]['jiedao'] = getCityName($one['jiedao']);
		}
		return $list;
	}
	
	/*删除地址*/
	public function deleteUserAddress(){
		$id = intval($_REQUEST['id']);
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$address = $this->db->YOne("select * from `@#_yonghu_dizhi` where `id` = '$id' and `uid` = '$uid'");
		if(!$id || !$address){
			$status = 0;
			$msg = '不存在这个地址信息或地址信息不属于您！error001';
		}else{
			$res = deleteInfo($id,'yonghu_dizhi');
			if($res){
				$status = 1;
				$msg = '删除成功';
			}else{
				$status = 0;
				$msg = '不存在这个地址信息或地址信息不属于您！error002';
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg));
	}
	
	/*获取地址信息*/
	public function ajax_add_data(){
		$code = $_POST['code'];
		$level = $_POST['level'];
		
		if(!$code){
			$return['status'] = 0;
		}else{
			if($level == 2){
				$where = " `parentId` = '$code' and `level` = '2'";
			}elseif($level == 3){
				$where = " `parentId` = '$code' and `level` = '3'";
			}elseif($level == 4){
				$where = " `parentId` = '$code' and `level` = '4'";
			}
			$list = $this->db->YList("select * from `@#_area` where $where order by id desc");
		}
		$return['list'] = $list; 
		if($list){
			$return['status'] = 1;
		}else{
			$return['status'] = 0;
		}
		echo json_encode($return);
	}
	
	/*保存地址*/
	public function saveUserAddress(){
		$sheng = $_REQUEST['province'];
		$shi = $_REQUEST['city'];
		$xian = $_REQUEST['country'];
		$jiedao = $_REQUEST['street'];
		$address = $_REQUEST['address'];
		$shouhuoren = $_REQUEST['shouhuoren'];
		$mobile = $_REQUEST['mobile'];
		$isDefault = intval($_REQUEST['isDefault']) ? 'Y' : 'N';
		if(!$sheng){
			$status = 0;
			$msg = '省不可为空';
		}elseif(!$shi){
			$status = 0;
			$msg = '城市不可为空';
		}elseif(!$xian){
			$status = 0;
			$msg = '区县不可为空';
		}elseif(!$jiedao){
			$status = 0;
			$msg = '街道不可为空';
		}elseif(!$address){
			$status = 0;
			$msg = '详细地址不可为空';
		}elseif(!$shouhuoren){
			$status = 0;
			$msg = '收货人不可为空';
		}elseif(!$mobile || !checkMobile($mobile)){
			$status = 0;
			$msg = '手机号码格式不正确';
		}else{
			$data = array(
				'sheng' => $sheng,
				'shi' => $shi,
				'xian' => $xian,
				'jiedao' => $jiedao,
				'address' => $address,
				'shouhuoren' => $shouhuoren,
				'mobile' => $mobile,
				'uid' => $this->userinfo['uid'],
				'default' => $isDefault,
			);
			$res = insertInto($data,'yonghu_dizhi');
			if($res){
				$status = 1;
				$msg = '保存成功';
			}else{
				$status = 0;
				$msg = '保存失败，请稍后再试！';
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg));
	}
/*增加用户定位列表开始20180202*/
	/*设置定位地址*/
	public function setLocation(){
		$locationId = intval($_REQUEST['locationId']);
		$orderSn = $_REQUEST['orderSn'];
		$userInfo = $this->userinfo;
		if(!$userInfo){
			$status = 2;
			$msg = '请先登录';
		}else{
			if(!$locationId){
				$status = 0;
				$msg = '请选择正确的定位信息';
			}else{
				$addressInfo = $this->db->Yone("select * from `@#_yonghu_location` where `id` = '$locationId' and `uid` = '$huiyuan[uid]'");
				if(!$addressInfo){
					if($orderSn){
						$orderInfo = $this->db->Yone("select * from `@#_yonghu_yys_record` where `id` = '$orderSn'");
						if(!$orderInfo){
							$status = 0;
							$msg = '不存在这个订单信息，请核实！如有疑问请联系客服！';
						}else{
							$res = updateSet(array('locationId' => $locationId,'status' => '已付款,未发货,未完成,已提交定位'),'yonghu_yys_record',$orderSn);
							if($res){
								$status = 1;
								$msg = '设置成功，返回列表';
							}else{
								$status = 0;
								$msg = '选择失败，请稍后再试！';
							}
						}
					}else{
						$status = 0;
						$msg = '不存在这个订单信息，请核实！如有疑问请联系客服！error:0001';
					}
				}else{
					$status = 0;
					$msg = '不存在这个定位信息或定位信息不属于您！error:0002';
				}
			}
		}
		$return = array(
			'status' => $status,
			'msg' => $msg
		);
		echo json_encode($return);
	}
	
	/*获取用户定位定位列表*/
	private function getUserLocationList(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$startTime = strtotime(date('Y-m'));
		$time = time();
		$count = $this->db->YCount("select * from `@#_yonghu_location` where `uid` = '$uid'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu_location` where `uid` = '$uid' ORDER BY `id` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$list[$key]['sheng'] = getCityName($one['sheng']);
			$list[$key]['shi'] = getCityName($one['shi']);
			$list[$key]['xian'] = getCityName($one['xian']);
			$list[$key]['jiedao'] = getCityName($one['jiedao']);
		}
		return $list;
	}
	
	/*删除定位*/
	public function deleteUserLocation(){
		$id = intval($_REQUEST['id']);
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$address = $this->db->YOne("select * from `@#_yonghu_location` where `id` = '$id' and `uid` = '$uid'");
		if(!$id || !$address){
			$status = 0;
			$msg = '不存在这个定位信息或定位信息不属于您！error001';
		}else{
			$res = deleteInfo($id,'yonghu_location');
			if($res){
				$status = 1;
				$msg = '删除成功';
			}else{
				$status = 0;
				$msg = '不存在这个定位信息或定位信息不属于您！error002';
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg));
	}
	
	
	/*保存定位*/
	public function saveUserLocation(){
		$sheng = $_REQUEST['province'];
		$shi = $_REQUEST['city'];
		$xian = $_REQUEST['country'];
		$location = $_REQUEST['location'];
		$lng = $_REQUEST['lng'];
		$lat = $_REQUEST['lat'];
		$isDefault = intval($_REQUEST['isDefault']) ? 'Y' : 'N';
		if(!$location){
			$status = 0;
			$msg = '请选择三级区域定位';
		}elseif(!$lng){
			$status = 0;
			$msg = '请点定位后再保存';/*
		}elseif(!$xian){
			$status = 0;
			$msg = '区县不可为空';
			
		}elseif(!$jiedao){
			$status = 0;
			$msg = '街道不可为空';
		}elseif(!$address){
			$status = 0;
			$msg = '详细定位不可为空';
		}elseif(!$lng){
			$status = 0;
			$msg = '收货人不可为空';
		}elseif(!$lat || !checklat($lat)){
			$status = 0;
			$msg = '手机号码格式不正确';*/
		}else{
			$data = array(
				'sheng' => $sheng,
				'time' => time(),
				'xian' => $xian,
				'location' => $location,
				'lng' => $lng,
				'lat' => $lat,
				'uid' => $this->userinfo['uid'],
				'default' => $isDefault,
			);
			$res = insertInto($data,'yonghu_location');
			if($res){
				$status = 1;
				$msg = '保存成功';
			}else{
				$status = 0;
				$msg = '保存失败，请稍后再试！';
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg));
	}

/*增加用户定位列表END*/
	/*获取当前用户邀请列表*/
	private function getUserInviteList(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		$startTime = strtotime(date('Y-m'));
		$time = time();
		$count = $this->db->YCount("select * from `@#_yonghu` where `yaoqing` = '$uid'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select * from `@#_yonghu` where `yaoqing` = '$uid' ORDER BY `uid` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$isWin = isWin($one['uid']);
			$list[$key]['isWin'] = $isWin ? '是' : '否';
		}
		return $list;
	}
	
	/*修改密码*/
	public function modifyPassword(){
		$userInfo = $this->userinfo;
		$uid = $userInfo['uid'];
		if(!$uid){
			$status = 1;
			$msg = '请先登录';
			$url = LOCAL_PATH.'/mobile/user/login';
		}else{
			$password = $_REQUEST['password'];
			$repassword = $_REQUEST['repassword'];
			$newpassword = $_REQUEST['newpassword'];

			if(!$password || (md5($password) != $userInfo['password']) ){
				$status = 0;
				$msg = '旧密码不正确';
			}elseif($repassword != $newpassword){
				$status = 0;
				$msg = '两次密码输入不一致';
			}else{
				$res = updateSet(array('password'=>md5($newpassword)),'yonghu',$uid,'uid');
				if($res){
					$status = 1;
					$msg = '修改成功';
					$url = LOCAL_PATH.'/mobile/home';
				}else{
					$status = 0;
					$msg = '修改失败，请稍后再试！';
				}
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg,'url'=>$url));
	}
	
	public function modifyAvatar(){
		
		$userinfo = $this->userinfo;
		$uid = $userinfo['uid'];
		if(!$uid){
			$status = 1;
			$msg = '请先登录';
			$url = LOCAL_PATH.'/mobile/user/login';
		}else{
			$username = $_REQUEST['username'];
			$sex = $_REQUEST['sex'];
			$img = $_REQUEST['img'];
			if(!$username){
				$status = 0;
				$msg = '请输入您的昵称';
			}elseif(!$img){
				$status = 0;
				$msg = '请上传您的头像';
			}else{
				$res = updateSet(array('username'=>$username,'sex'=>$sex,'img'=>$img),'yonghu',$uid,'uid');
				if($res){
					$status = 1;
					$msg = '恭喜您，修改成功！';
					$url = LOCAL_PATH.'/mobile/home';
				}else{
					$status = 0;
					$msg = '修改失败';
				}
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg,'url'=>$url));
	}
	
	public function supplierGongying(){
		$userinfo = $this->userinfo;
		$uid = $userinfo['uid'];
		$where = " `uid` = '$uid' and `type` = '1' and `pay` = '系统' and `pay_type` = '2'";
		$lists = $this->db->YList("select * from `@#_yonghu_zhanghao` where $where order by id desc");
		$total_money = 0;
		if($lists){
			$return['status'] = 1;
			foreach($lists as $k => $v){
				$lists[$k]['time'] = date("Y-m-d H:i:s",$v['time']);
				$total_money += $v['money'];
			}
		}else{
			$return['status'] = 0;
		}
		//print_r($lists);
		$return['list'] = $lists; 
		$return['totalmoney'] = $total_money; 
		echo json_encode($return);
		exit();
	}
	
	public function supplierSettlement(){
		$userinfo = $this->userinfo;
		$uid = $userinfo['uid'];
		$where = " uid = '$uid' and type = '1'";
		$list = $this->db->YList("select * from `@#_yonghu_zhanghao1` where $where order by id desc");
		foreach($list as $key => $one){
			$list[$key]['goods_name'] = html_entity_decode(html_entity_decode($one['goods_name']));
			$list[$key]['create_time'] = date('Y-m-d H:i:s',$one['time']);
		}
		$return['list'] = $list; 
		if($list){
			$return['status'] = 1;
		}else{
			$return['status'] = 0;
		}
		echo json_encode($return);
		exit();
	}
	
	public function supplierHuli(){
		$userinfo = $this->userinfo;
		$uid = $userinfo['uid'];
		$where = " `uid` = '$uid' and `type` = '1' and `pay` = '系统' and `pay_type` = '3'";
		$lists = $this->db->YList("select * from `@#_yonghu_zhanghao` where $where order by id desc");
		$total_money = 0;
		if($lists){
			$return['status'] = 1;
			foreach($lists as $k => $v){
				$sid = $v['sid'];
				$supplier = $this->db->YOne("select * from `@#_supplier` where `id` = '$sid'");
				$lists[$k]['thumb'] = $supplier['supplier_logo'];
				$lists[$k]['time'] = date("Y-m-d H:i:s",$v['time']);
				$total_money += $v['money'];
			}
		}else{
			$return['status'] = 0;
		}
		//print_r($lists);
		$return['list'] = $lists; 
		$return['totalmoney'] = $total_money; 
		echo json_encode($return);
		exit();
	}
	
	public function doExchange(){
		$userInfo = $this->userinfo;
		if(!$userInfo){
			$return['status'] = 2;
		}else{
			$money = $_REQUEST['money'];
			$userInfo = getUserInfo($userInfo['uid']);
			if($userInfo['money1'] < $money){
				$return['status'] = 0;
				$return['msg'] = '可转金额不足';
			}else{
				$yonghu_zhanghao = array(
					'uid' => $userInfo['uid'],
					'type' => 1,
					'pay' => '账户',
					'content' => '货款转入钱包',
					'money' => $money,
					'time' => time(),
				);
				$res = insertInto($yonghu_zhanghao,'yonghu_zhanghao');
				if($res){
					/*先扣除防止重复提交*/
					$re = updateSet(array('money1'=>$userInfo['money1'] - $money,'money' => $userInfo['money'] + $money),'yonghu',$userInfo['uid'],'uid');
					if($re){
						$return['status'] = 1;
						$return['msg'] = '转入成功';
					}else{
						$return['status'] = 0;
						$return['msg'] = '转入失败';
					}
				}else{
					$return['status'] = 0;
					$return['msg'] = '转入失败';
				}
			}
		}
		echo json_encode($return);
		exit();
	}
}