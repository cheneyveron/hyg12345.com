<?php 
defined('IN_YYS')or exit('no');
System::DOWN_App_class('admin',G_ADMIN_DIR,'no');
System::DOWN_App_fun('global',G_ADMIN_DIR);
System::DOWN_sys_fun('user');
class content extends admin {
	private $db;
	public function __construct(){		
		parent::__construct();		
		$this->db=System::DOWN_sys_class('model');
		$this->ment=array();
		$this->categorys=$this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE 1 order by `parentid` ASC,`cateid` ASC",array('key'=>'cateid'));
		$this->models=$this->db->Ylist("SELECT * FROM `@#_moxing` WHERE 1",array('key'=>'modelid'));
	}
	//添加新文章
	public function article_add(){	
		if(isset($_POST['dosubmit'])){
						
			$cateid = intval($_POST['cateid']);		
			$biaoti =  htmlspecialchars($_POST['title']);
			$biaoti_color = htmlspecialchars($_POST['title_style_color']);
			$biaoti_bold = htmlspecialchars($_POST['title_style_bold']);
			
			$biaoti_style='';
			if($biaoti_color){
				$biaoti_style.='color:'.$biaoti_color.';';
			}
			if($biaoti_bold){
				$biaoti_style.='font-weight:'.$biaoti_bold.';';
			}
			
			$guanjianzi = htmlspecialchars($_POST['keywords']);
			$miaoshu = htmlspecialchars($_POST['description']);

			$content = editor_safe_replace(stripslashes($_POST['content']));
			
			$author = isset($_POST['zuoze']) ? htmlspecialchars($_POST['zuoze']) : '';	
			$thumb = isset($_POST['thumb']) ? htmlspecialchars($_POST['thumb']) : '';						
			$picarr = isset($_POST['uppicarr']) ? serialize($_POST['uppicarr']) : serialize(array());			
			
			$posttime = strtotime($_POST['posttime']) ? strtotime($_POST['posttime']) : time();
			$hit = intval($_POST['hit']);
			
			$position_arr = isset($_POST['position']) ? $_POST['position'] : false;
						
			if(empty($biaoti)){_note("标题不能为空");}
			if(!$cateid){_note("栏目不能为空");}				
			$sql="INSERT INTO `@#_wenzhang` (`cateid`, `author`, `title`,`title_style`, `thumb`, `picarr`, `keywords`, `description`, `content`, `hit`, `order`, `posttime`) VALUES ('$cateid', '$author', '$biaoti','$biaoti_style', '$thumb', '$picarr', '$guanjianzi', '$miaoshu', '$content', '$hit', '1', '$posttime')";
			if($this->db->Query($sql)){
				if($position_arr){
					$posinfo=array();
					$posinfo['id'] = $this->db->last_id();
					$posinfo['title'] = $biaoti;	
					$posinfo['title_style'] = $biaoti_style;						
					$posinfo['thumb'] = $thumb;								
					$position = System::DOWN_App_model("position",'api');
					$position->pos_insert($position_arr,$posinfo);
				}
					_note("文章添加成功");
				
			}else{
					_note("文章添加失败");
			}
		
			header("Cache-control: private");			
		}
			
		$cateid=intval($this->segment(4));		
		$fenleis=$this->categorys;
		$tree=System::DOWN_sys_class('tree');
		$tree->icon = array('│ ','├─ ','└─ ');
		$tree->nbsp = '&nbsp;';
		$fenleishtml="<option value='\$cateid'>\$spacer\$name</option>";
		$tree->init($fenleis);	
		$fenleishtml=$tree->get_tree(0,$fenleishtml);
		$fenleishtml='<option value="0">≡ 请选择栏目 ≡</option>'.$fenleishtml;
		if($cateid){			
			$cateinfo=$this->db->YOne("SELECT * FROM `@#_fenlei` WHERE `cateid` = '$cateid' LIMIT 1");
			if(!$cateinfo)_note("参数不正确,没有这个栏目",G_ADMIN_PATH.'/'.DOWN_C.'/addarticle');
			$fenleishtml.='<option value="'.$cateinfo['cateid'].'" selected="true">'.$cateinfo['name'].'</option>';			
		}		
		include $this->dwt(DOWN_M,'article.insert');
	}
	
	//文章编辑
	public function article_edit(){	
		$id=intval($this->segment(4));		
		$info=$this->db->YOne("SELECT * FROM `@#_wenzhang` where `id`='$id' LIMIT 1");
		
		if(isset($_POST['dosubmit'])){
			$cateid = intval($_POST['cateid']);		
			$biaoti =  htmlspecialchars($_POST['title']);
			$biaoti_color = htmlspecialchars($_POST['title_style_color']);
			$biaoti_bold = htmlspecialchars($_POST['title_style_bold']);
			
			$biaoti_style='';
			if($biaoti_color){
				$biaoti_style.='color:'.$biaoti_color.';';
			}
			if($biaoti_bold){
				$biaoti_style.='font-weight:'.$biaoti_bold.';';
			}
			
			$guanjianzi = htmlspecialchars($_POST['keywords']);
			$miaoshu = htmlspecialchars($_POST['description']);			
			$content = editor_safe_replace(stripslashes($_POST['content']));
			
			$author = isset($_POST['zuoze']) ? htmlspecialchars($_POST['zuoze']) : '';	
			$thumb = isset($_POST['thumb']) ? htmlspecialchars($_POST['thumb']) : '';						
			$picarr = isset($_POST['uppicarr']) ? serialize($_POST['uppicarr']) : serialize(array());			
			
			$posttime = strtotime($_POST['posttime']) ? strtotime($_POST['posttime']) : time();
			$hit = intval($_POST['hit']);
			
			$position_arr = isset($_POST['position']) ? $_POST['position'] : false;
						
			if(empty($biaoti)){_note("标题不能为空");}
			if(!$cateid){_note("栏目不能为空");}		
			
			$sql="UPDATE `@#_wenzhang` SET `cateid`='$cateid', 
										  `author`='$author', 
										  `title`='$biaoti',
										  `title_style` = '$biaoti_style', 
										  `keywords`='$guanjianzi', 
										  `description`='$miaoshu',
										  `thumb`='$thumb',
										  `picarr`='$picarr',										  
										  `content`='$content', 
										  `posttime`='$posttime',
										  `hit` = '$hit'
										  WHERE (`id`='$id')";
			$this->db->Query($sql);
			if($position_arr){
					$posinfo=array();
					$posinfo['id'] = $info['id'];
					$posinfo['title'] = $biaoti;	
					$posinfo['title_style'] = $biaoti_style;						
					$posinfo['thumb'] = $thumb;								
					$position = System::DOWN_App_model("position",'api');
					$position->pos_insert($position_arr,$posinfo);
			}
				
			if($this->db->affected_rows()){
				_note("操作成功!");
			}else{
				_note("操作失败!");
			}
			header("Cache-control: private");
		}

		
			
		if(!$info)_note("参数错误");
		$cateinfo=$this->db->YOne("SELECT * FROM `@#_fenlei` WHERE `cateid` = '$info[cateid]' LIMIT 1");		
		
		$fenleis=$this->categorys;
		$tree=System::DOWN_sys_class('tree');
		$tree->icon = array('│ ','├─ ','└─ ');
		$tree->nbsp = '&nbsp;';
		$fenleishtml="<option value='\$cateid'>\$spacer\$name</option>";
		$tree->init($fenleis);	
		$fenleishtml=$tree->get_tree(0,$fenleishtml);		
		$fenleishtml.='<option value="'.$cateinfo['cateid'].'" selected="true">'.$cateinfo['name'].'</option>';
		
		$this->ment=array(
						array("lists","内容管理",DOWN_M.'/'.DOWN_C."/article_list"),
						array("insert","添加文章",DOWN_M.'/'.DOWN_C."/article_add"),
		);
		
		$info['picarr']=unserialize($info['picarr']);		
		$info['posttime'] = date("Y-m-d H:i:s",$info['posttime']);
		if($info['title_style']){		
			if(stripos($info['title_style'],"font-weight:")!==false){
				$biaoti_bold = 'bold';
			}else{
				$biaoti_bold = '';
			}
			if(stripos($info['title_style'],"color:")!==false){
				$biaoti_color = explode(';',$info['title_style']); 
				$biaoti_color = explode(':',$biaoti_color[0]); 
				$biaoti_color = $biaoti_color[1];
			}else{
				$biaoti_color = '';
			}				
		}else{
			$biaoti_color='';
			$biaoti_bold = '';
		}		
		
		include $this->dwt(DOWN_M,'article.insert');
	
	}	
	//文章列表
	public function article_list(){
		
		$this->ment=array(
						array("lists","文章管理",DOWN_M.'/'.DOWN_C."/article_list"),
						array("insert","添加文章",DOWN_M.'/'.DOWN_C."/article_add"),
		);		
		
		$cateid=intval($this->segment(4));
		$list_where = '';
		if(!$cateid){
			$list_where = "1";
		}else{
			$list_where = "`cateid` = '$cateid'";
		}
		if(isset($_POST['sososubmit'])){			
			$posttime1 = !empty($_POST['posttime1']) ? strtotime($_POST['posttime1']) : NULL;
			$posttime2 = !empty($_POST['posttime2']) ? strtotime($_POST['posttime2']) : NULL;			
			$sotype = $_POST['sotype'];
			$title = $_POST['title'];
			$sosotext = $_POST['sosotext'];			
			if($posttime1 && $posttime2){
				if($posttime2 < $posttime1)_note("结束时间不能小于开始时间");
				$list_where = "`posttime` > '$posttime1' AND `posttime` < '$posttime2'";
			}
			if($posttime1 && empty($posttime2)){				
				$list_where = "`posttime` > '$posttime1'";
			}
			if($posttime2 && empty($posttime1)){				
				$list_where = "`posttime` < '$posttime2'";
			}
			if(empty($posttime1) && empty($posttime2)){				
				$list_where = false;
			}			
			
			if(!empty($sosotext)){			
				if($sotype == 'cateid'){
					$sosotext = intval($sosotext);
					if($list_where)
						$list_where .= "AND `cateid` = '$sosotext'";
					else
						$list_where = "`cateid` = '$sosotext'";
				}
				if($sotype == 'catename'){
					$sosotext = htmlspecialchars($sosotext);
					$info = $this->db->YOne("SELECT * FROM `@#_fenlei` where `name` = '$sosotext' LIMIT 1");
					
					if($list_where && $info)
						$list_where .= "AND `cateid` = '$info[cateid]'";
					elseif ($info)
						$list_where = "`cateid` = '$info[cateid]'";
					else
						$list_where = "1";
				}
				if($sotype == 'title'){
					$sosotext = htmlspecialchars($sosotext);
					$list_where = "`title` = '$sosotext'";
				}
				if($sotype == 'id'){
					$sosotext = intval($sosotext);
					$list_where = "`id` = '$sosotext'";
				}
			}else{
				if(!$list_where) $list_where='1';					
			}		
		}	
		$num=20;
		$zongji=$this->db->YCount("SELECT COUNT(*) FROM `@#_wenzhang` WHERE $list_where");
		$fenye=System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($zongji,$num,$fenyenum,"0");
		$wenzhanglist=$this->db->YPage("SELECT * FROM `@#_wenzhang` WHERE $list_where order by `order` DESC",array("num"=>$num,"page"=>$fenyenum,"type"=>1,"cache"=>0));		
		include $this->dwt(DOWN_M,'article.lists');
	}
	
	//模型
	public function model(){
		$models=$this->models;	
		include $this->dwt(DOWN_M,'content.model');
	}
	
	public function lists(){
		$fenleis=$this->categorys;
		$models=$this->models;
		$tree=System::DOWN_sys_class('tree');
		$tree->icon = array('│ ','├─ ','└─ ');
		$tree->nbsp = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		foreach($fenleis as $v){
			$v['typename']=cattype($v['model']);		
			if($v['model']==-1 || $v['model']==-2){
				$v['seecontent']=G_ADMIN_PATH.'/category/editcate/';
				$v['addcontent']=G_ADMIN_PATH.'/category/editcate/';
			}			
			if($v['model']>0){
				$v['seecontent']=G_ADMIN_PATH.'/'.DOWN_C.'/get/';	
				$v['addcontent']=G_ADMIN_PATH.'/'.DOWN_C.'/add/';
				$v['model']=$models[$v['model']]['name'];
			}else{
				$v['model']='';
			}
			$fenleis[$v['cateid']]=$v;
		}
		$html=<<<HTML
			<tr>
			<td align='center'>\$cateid</td>
            <td align='left'>\$spacer\$name</th>
            <td align='center'>\$typename</td>
            <td align='center'>\$model</td>
            <td align='center'></td>
			<td align='center'>               
				<a href='\$addcontent\$cateid\'>添加内容</a><span class='span_fenge lr5'>|</span>
				 <a href='\$seecontent\$cateid'>查看内容</a><span class='span_fenge lr5'></span>   
            </td>
          </tr>
HTML;

		$tree->init($fenleis);
		$html=$tree->get_tree(0,$html);	
		include $this->dwt(DOWN_M,'content.list');
	}
	
	//商品列表	
	public function goods_list(){
		$this->ment=array(
			array("lists","商品管理",DOWN_M.'/'.DOWN_C."/goods_list"),
			array("add","添加商品",DOWN_M.'/'.DOWN_C."/goods_add"),
			array("renqi","人气商品",DOWN_M.'/'.DOWN_C."/goods_list/renqi"),
			array("xsjx","限时惠券商品",DOWN_M.'/'.DOWN_C."/goods_list/xianshi"),
			array("qishu","期数倒序",DOWN_M.'/'.DOWN_C."/goods_list/qishu"),
			array("danjia","单价倒序",DOWN_M.'/'.DOWN_C."/goods_list/danjia"),
			array("money","商品价格倒序",DOWN_M.'/'.DOWN_C."/goods_list/money"),
			array("money","已惠券",DOWN_M.'/'.DOWN_C."/goods_list/jiexiaook"),
			array("money","<font color='#f00'>期数已满商品</font>",DOWN_M.'/'.DOWN_C."/goods_list/maxqishu"),
		);
	    $cateid=$this->segment(4);
		$list_where = '';
		if($cateid){
			if($cateid=='jiexiaook'){
				$list_where = "`q_uid` is not null and `uid` = 0 ";
			}
			/*商户开店 -未制卡状态*/
			if($cateid=='all'){
				$list_where = "`uid` = '0' ";
			}
			/*商户开店 -未制卡状态*/
			if($cateid=='secret'){
				$list_where = "`secret` = '0' ";
			}
			/*商户开店 -正常卡状态*/
			if($cateid=='manage_card'){
				$shanghuid=$this->segment(5);
				$list_where = "`secret` = '1' and `supplierId` = '$shanghuid'";
			}
			if($cateid=='xianshi'){
				$list_where = "`xsjx_time` != '0' and `uid` = 0 ";
			}
			if($cateid=='qishu'){
				$list_where = "1  and `uid` = 0 order by `qishu` DESC";
				$this->ment[4][1]="期数正序";
				$this->ment[4][2]=DOWN_M.'/'.DOWN_C."/goods_list/qishuasc";
			}
			if($cateid=='qishuasc'){
				$list_where = "1  and `uid` = 0 order by `qishu` ASC";
				$this->ment[4][1]="期数倒序";
				$this->ment[4][2]=DOWN_M.'/'.DOWN_C."/goods_list/qishu";
			}
			if($cateid=='danjia'){
				$list_where = "1  and `uid` = 0 order by `yunjiage` DESC";
				$this->ment[5][1]="单价正序";
				$this->ment[5][2]=DOWN_M.'/'.DOWN_C."/goods_list/danjiaasc";
			}
			if($cateid=='danjiaasc'){
				$list_where = "1  and `uid` = 0 order by `yunjiage` ASC";
				$this->ment[5][1]="单价倒序";
				$this->ment[5][2]=DOWN_M.'/'.DOWN_C."/goods_list/danjia";
			}
			if($cateid=='money'){
				$list_where = "1  and `uid` = 0 order by `money` DESC";
				$this->ment[6][1]="商品价格正序";
				$this->ment[6][2]=DOWN_M.'/'.DOWN_C."/goods_list/moneyasc";
			}
			if($cateid=='moneyasc'){
				$list_where = "1  and `uid` = 0 order by `money` ASC";
				$this->ment[6][1]="商品价格倒序";
				$this->ment[6][2]=DOWN_M.'/'.DOWN_C."/goods_list/money";
			}
			if($cateid==''){
				$list_where = "`q_uid` is null  and `uid` = 0  order by `id` DESC";
			}
			if(intval($cateid)){
				$list_where = "`cateid` = '$cateid' ";
			}			
		}else{
			$list_where = "`q_uid` is null order by `id` DESC";
		}

		
		if(isset($_POST['sososubmit'])){			
			$posttime1 = !empty($_POST['posttime1']) ? strtotime($_POST['posttime1']) : NULL;
			$posttime2 = !empty($_POST['posttime2']) ? strtotime($_POST['posttime2']) : NULL;			
			$sotype = $_POST['sotype'];
			$sosotext = $_POST['sosotext'];			
			if($posttime1 && $posttime2){
				if($posttime2 < $posttime1)_note("结束时间不能小于开始时间");
				$list_where = "`time` > '$posttime1' AND `time` < '$posttime2' and `uid` = 0 ";
			}
			if($posttime1 && empty($posttime2)){				
				$list_where = "`time` > '$posttime1' and `uid` = 0 ";
			}
			if($posttime2 && empty($posttime1)){				
				$list_where = "`time` < '$posttime2' and `uid` = 0 ";
			}
			if(empty($posttime1) && empty($posttime2)){
				$list_where = false;
			}
			
			if(!empty($sosotext)){			
				if($sotype == 'cateid'){
					$sosotext = intval($sosotext);
					if($list_where)
						$list_where .= " `cateid` = '$sosotext' and `uid` = 0 ";
					else
						$list_where = "`cateid` = '$sosotext' and `uid` = 0 ";
				}
				if($sotype == 'brandid'){
					$sosotext = intval($sosotext);
					if($list_where)
						$list_where .= " `brandid` = '$sosotext' and `uid` = 0 ";
					else
						$list_where = "`brandid` = '$sosotext' and `uid` = 0 ";
				}
				
				if($sotype == 'brandname'){
					$sosotext = htmlspecialchars($sosotext);
					$info = $this->db->YOne("SELECT * FROM `@#_pinpai` where `name` LIKE '%$sosotext%' LIMIT 1");
					
					if($list_where && $info)
						$list_where .= " `brandid` = '$info[id]' and `uid` = 0 ";
					elseif ($info)
						$list_where = "`brandid` = '$info[id]' and `uid` = 0 ";
					else
						$list_where = "1 and `uid` = 0 ";
				}
				
				
				if($sotype == 'catename'){
					$sosotext = htmlspecialchars($sosotext);
					$info = $this->db->YOne("SELECT * FROM `@#_fenlei` where `name` LIKE '%$sosotext%' LIMIT 1");
					
					if($list_where && $info)
						$list_where .= " `cateid` = '$info[cateid]' and `uid` = 0 ";
					elseif ($info)
						$list_where = "`cateid` = '$info[cateid]' and `uid` = 0 ";
					else
						$list_where = "1 and `uid` = 0 ";
				}
				if($sotype == 'title'){
					$sosotext = htmlspecialchars($sosotext);
					$list_where .= "`title` like '%$sosotext%' and `q_uid` IS NULL ";
				}
				if($sotype == 'id'){
					$sosotext = intval($sosotext);
					$list_where .= "`id` = '$sosotext' and `uid` = 0 ";
				}
			}else{
				if(!$list_where) $list_where='1 and `uid` = 0 ';
			}		
		}		
//		$list_where .= " and (`canyurenshu` < `zongrenshu` or `zongrenshu` = '0') ";
		$num=20;
		$zongji=$this->db->YCount("SELECT COUNT(*) FROM `@#_shangpin` WHERE $list_where");
		$fenye=System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($zongji,$num,$fenyenum,"0");
		$yyslist=$this->db->YPage("SELECT * FROM `@#_shangpin` WHERE $list_where",array("num"=>$num,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		include $this->dwt(DOWN_M,'shop.lists');
	}
	
	/* 单个商品的购买详细 */
	public function goods_go_one(){
		$yonghuid = intval($this->segment(4));
		$ginfo = $this->db->YOne("select * from `@#_shangpin` where `id` = '$yonghuid' limit 1");
		if(!$ginfo)_note("没有找到这个商品");
		
		$zongji=$this->db->YCount("select * from `@#_yonghu_yys_record` where `shopid` = '$yonghuid' order by `id` DESC");
		$fenye=System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($zongji,20,$fenyenum,"0");		
		$go_list = $this->db->YPage("select * from `@#_yonghu_yys_record` where `shopid` = '$yonghuid' order by `id` DESC",array("num"=>20,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		include $this->dwt(DOWN_M,'shop.go_list');		
	}
	/* 手动惠券	*/
	public function goods_one_ok(){
	
		$yonghuid = intval($this->segment(4));
		$ginfo = $this->db->YOne("select * from `@#_shangpin` where `id` = '$yonghuid' limit 1");
		if(!$ginfo)_note("没有找到这个商品");
		$jinri_time = time();		
		if($ginfo['xsjx_time']!='0')_note("限时惠券商品不能手动惠券");
		if($ginfo['shenyurenshu']!='0')_note("该商品还有剩余人数,不能手动惠券！");
		if($ginfo['shenyurenshu']=='0' && (empty($ginfo['q_uid']) || $ginfo['q_uid']=='')){		
			System::DOWN_App_fun("pay","pay");
			$this->db->tijiao_start();
			$ok = pay_insert_shop($ginfo);
			
			if(!$ok){
				$this->db->tijiao_rollback();
				_note("惠券失败!");	
			}else{
				$this->db->tijiao_commit();		
				_note("惠券成功!");
			}
		}
	}
	
	//商品回收站
	public function goods_del_list(){
		$this->ment=array(
			array("lists","返回商品列表",DOWN_M.'/'.DOWN_C."/goods_list"),
			array("add","添加商品",DOWN_M.'/'.DOWN_C."/goods_add"),
		);
		
		$num=20;
		$zongji=$this->db->YCount("SELECT COUNT(*) FROM `@#_shangpin_del` WHERE 1"); 
		$fenye=System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($zongji,$num,$fenyenum,"0");
		$yyslist=$this->db->YPage("SELECT * FROM `@#_shangpin_del` WHERE 1",array("num"=>$num,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		
		
		include $this->dwt(DOWN_M,'shop.del');
	}
	
	/*编辑商品  此处不得修改金额否侧摇券模式码库对应不起来会报错。*/
	public function goods_edit(){
		$this->db->tijiao_start();
		$shopid=intval($this->segment(4));		
		$shopinfo=$this->db->YOne("SELECT * FROM `@#_shangpin` WHERE `id` = '$shopid' and `qishu` order by `qishu` DESC LIMIT 1 for update");	
		/*调用商户ID*/
		$supplierId = $shopinfo['supplierId'];
		$supplier=$this->db->YOne("SELECT * FROM `@#_supplier` WHERE `id` = '$supplierId' LIMIT 1 for update");	
		
		if($shopinfo['q_end_time'])_note("该商品已经惠券,不能修改!",YYS_MODULE_PATH.'/content/goods_list');
		if(!$shopinfo)_note("参数不正确");	
		
		if(isset($_POST['dosubmit'])){		
		
			$cateid = intval($_POST['cateid']);
			$pinpaiid = intval($_POST['brand']);
			$biaoti = htmlspecialchars($_POST['title']);
			$biaoti_color = htmlspecialchars($_POST['title_style_color']);
			$biaoti_bold = htmlspecialchars($_POST['title_style_bold']);
			$biaoti2 = htmlspecialchars($_POST['title2']);
			$guanjianzi = htmlspecialchars($_POST['keywords']);
			$miaoshu = htmlspecialchars($_POST['description']);
$yunjiage = htmlspecialchars($_POST['yunjiage']);
$yuanjia = htmlspecialchars($_POST['yuanjia']);
$money = htmlspecialchars($_POST['money']);
$supplierId = htmlspecialchars($_POST['supplierId']);
$status = htmlspecialchars($_POST['status']);
			$content = editor_safe_replace(stripslashes($_POST['content']));			
			$thumb = trim(htmlspecialchars($_POST['thumb']));		
			$maxqishu = intval($_POST['maxqishu']) ? intval($_POST['maxqishu']) : 1;	
			$shangpinss_key_pos = isset($_POST['goods_key']['pos']) ? 1 : 0;
			$shangpinss_key_renqi = isset($_POST['goods_key']['renqi']) ? 1 : 0;		
			$shangpinss_key_renqi1 = isset($_POST['goods_key']['renqi1']) ? 1 : 0;
			$shangpinss_key_secret = isset($_POST['goods_key']['secret']) ? 1 : 0;
			//fxi by dabin
			$goods_key_leixing = $_POST['goods_key']['leixing'];
			
			//roce
			$cardId1 = mysql_escape_string($_POST['cardId1']);
			$cardId2 = mysql_escape_string($_POST['cardId2']);
                        $cardId = mysql_escape_string($_POST['cardId']);
                        $cardPwd = mysql_escape_string($_POST['cardPwd']);


$quyu_begin = intval($_POST['quyu_begin']) ? intval($_POST['quyu_begin']) : null;
			$quyu_end = intval($_POST['quyu_end']) ? intval($_POST['quyu_end']) : null;
			if(!$quyu_begin || !$quyu_end){
				$max_quyu = max($quyu_begin,$quyu_end);
				$quyu_begin = $quyu_begin==$max_quyu ? $quyu_end : $quyu_begin;
				$quyu_end = $max_quyu;
			}

			$firstYongjin = $_POST['firstYongjin'];
			$secondYongjin = $_POST['secondYongjin'];
			$thirdYongjin = $_POST['thirdYongjin'];
			
//			if(!$cateid)_note("请选择栏目");
//			if(!$pinpaiid)_note("请选择品牌");
			if(!$biaoti)_note("标题不能为空");
			if(!$thumb)_note("缩略图不能为空");
			
			$biaoti_style='';
			if($biaoti_color){
				$biaoti_style.='color:'.$biaoti_color.';';
			}
			if($biaoti_bold){
				$biaoti_style.='font-weight:'.$biaoti_bold.';';
			}
			if(isset($_POST['uppicarr'])){
				$picarr = serialize($_POST['uppicarr']);
			}else{
				$picarr = serialize(array());
			}			
			if($_POST['xsjx_time'] != ''){			
				$xsjx_time = strtotime($_POST['xsjx_time']) ? strtotime($_POST['xsjx_time']) : time();
				$xsjx_time_h = intval($_POST['xsjx_time_h']) ? $_POST['xsjx_time_h'] : 36000;
				$xsjx_time += $xsjx_time_h;
			}else{
				$xsjx_time = '0';
			}
			
			if($maxqishu > 65535){
				_note("最大期数不能超过65535期");
			}	
			if($maxqishu < $shopinfo['qishu']){
				_note("最期数不能小于当前期数！");
			}	
			
			$sql="UPDATE `@#_shangpin` SET `cateid` = '$cateid',
										   `brandid` = '$pinpaiid',
										   `title` = '$biaoti',
										   `title_style` = '$biaoti_style',
										   `title2` = '$biaoti2',
										   `keywords`='$guanjianzi',
										   `yunjiage`='$yunjiage',
										   `money`='$money',
										   `supplierId`='$supplierId',
										   `description`='$miaoshu',
										   `thumb` = '$thumb',
										   `picarr` = '$picarr',
										   `content` = '$content',										  
										   `maxqishu` = '$maxqishu',
										   `renqi` = '$shangpinss_key_renqi',
										   `renqi1` = '$shangpinss_key_renqi1',
										   `secret` = '$shangpinss_key_secret',
										   `leixing` = '$goods_key_leixing',
										   `xsjx_time` = '$xsjx_time',
										  `quyu_begin` = '$quyu_begin' ,
											`quyu_end` = '$quyu_end' ,
										   `ka`='".htmlspecialchars($_POST['kahao'])."',
										   `mi`='".htmlspecialchars($_POST['mima'])."',
										   `pos` = '$shangpinss_key_pos',
										   `cardId1` = '$cardId1',
											`yuanjia` = '$yuanjia',
										   `cardId` = '$cardId',
										   `cardPwd` = '$cardPwd',
										   `firstYongjin` = '$firstYongjin',
										   `secondYongjin` = '$secondYongjin',
										   `thirdYongjin` = '$thirdYongjin'
											WHERE `id`='$shopid'
			";				
			$s_sid = $shopinfo['sid'];
			$this->db->Query("UPDATE `@#_shangpin` SET `maxqishu` = '$maxqishu' where `sid` = '$s_sid'");	
/*增加同步更新商户为正式开店*/
			$supplierId = $shopinfo['supplierId'];
			$this->db->Query("UPDATE `@#_supplier` SET `status` = '$status' where `id` = '$supplierId'");	
			
			
			if($this->db->Query($sql)){			
				$this->db->tijiao_commit();	
				_note("开卡成功!");
			}else{	
				$this->db->tijiao_rollback();
				_note("修改失败!");
			}			
		}	
		$this->ment=array(
						array("lists","商品管理",DOWN_M.'/'.DOWN_C."/goods_list"),
						array("insert","添加商品",DOWN_M.'/'.DOWN_C."/goods_add"),
		);			
		
		$cateinfo=$this->db->YOne("SELECT * FROM `@#_fenlei` WHERE `cateid` = '$shopinfo[cateid]' LIMIT 1");
		$pinpaiList=$this->db->Ylist("SELECT * FROM `@#_pinpai` where 1",array("key"=>'id'));
		
		$fenleis=$this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE `model` = '1' order by `parentid` ASC,`cateid` ASC",array('key'=>'cateid'));
		$tree=System::DOWN_sys_class('tree');
		$tree->icon = array('│ ','├─ ','└─ ');
		$tree->nbsp = '&nbsp;';
		$fenleishtml="<option value='\$cateid'>\$spacer\$name</option>";
		$tree->init($fenleis);	
		$fenleishtml=$tree->get_tree(0,$fenleishtml);		
		$fenleishtml.='<option value="'.$cateinfo['cateid'].'" selected="true">'.$cateinfo['name'].'</option>';
		
		if($shopinfo['title_style']){		
			if(stripos($shopinfo['title_style'],"font-weight:")!==false){
				$biaoti_bold = 'bold';
			}else{
				$biaoti_bold = '';
			}
			if(stripos($shopinfo['title_style'],"color:")!==false){
				$biaoti_color = explode(';',$shopinfo['title_style']); 
				$biaoti_color = explode(':',$biaoti_color[0]); 
				$biaoti_color = $biaoti_color[1];
			}else{
				$biaoti_color = '';
			}				
		}else{
			$biaoti_color='';
			$biaoti_bold = '';
		}	
		
		$shopinfo['picarr'] = unserialize($shopinfo['picarr']);
				
		if($shopinfo['xsjx_time']){
			$shopinfo['xsjx_time_1'] = date("Y-m-d",$shopinfo['xsjx_time']);
			$shopinfo['xsjx_time_h'] = $shopinfo['xsjx_time'] - strtotime($shopinfo['xsjx_time_1']);
		    $shopinfo['xsjx_time'] = $shopinfo['xsjx_time_1'];
		}else{
			$shopinfo['xsjx_time']='';
			$shopinfo['xsjx_time_h']=79200;
		}	
		
	
		include $this->dwt(DOWN_M,'shop.edit');
	}
	
	
	//添加卡券
	public function goods_add(){		
	
		/*
		$this->db->tijiao_start();
		$shopid=intval($this->segment(4));		
		$shopinfo=$this->db->YOne("SELECT * FROM `@#_shangpin` WHERE `id` = '$shopid' and `qishu` order by `qishu` DESC LIMIT 1 for update");	
		$supplierId = $shopinfo['supplierId'];
		$supplier=$this->db->YOne("SELECT * FROM `@#_supplier` WHERE `id` = '$supplierId' LIMIT 1 for update");	
		
		if($shopinfo['q_end_time'])_note("该商品已经惠券,不能修改!",YYS_MODULE_PATH.'/content/goods_list');
		if(!$shopinfo)_note("参数不正确");	
		$coupon = $this->db->YOne("select * from `@#_supplier_coupon_style` where `id` = '$shopinfo[couponId]'");
		$shopinfo['thumb'] = $coupon['thumb'];
		*/
		
		if(isset($_POST['dosubmit'])){		
			$cateid = intval($_POST['cateid']);
			$pinpaiid = intval($_POST['brand']);
			$biaoti = _htmtguolv($_POST['title']);
			$biaoti_color = htmlspecialchars($_POST['title_style_color']);
			$biaoti_bold = htmlspecialchars($_POST['title_style_bold']);
			$biaoti2 = _htmtguolv($_POST['title2']);
						
			$guanjianzi = htmlspecialchars($_POST['keywords']);
			$miaoshu = htmlspecialchars($_POST['description']);
			$content = editor_safe_replace(stripslashes($_POST['content']));
			$money = intval($_POST['money']);
			$yunjiage = intval($_POST['yunjiage']);
			$yuanjia = intval($_POST['yuanjia']);
			$thumb = htmlspecialchars($_POST['thumb']);		
			$maxqishu = intval($_POST['maxqishu']);			
			$canyurenshu = 0;		
			$shangpinss_key_pos = isset($_POST['goods_key']['pos']) ? 1 : 0;
			$shangpinss_key_renqi = isset($_POST['goods_key']['renqi']) ? 1 : 0;
			$shangpinss_key_renqi1 = isset($_POST['goods_key']['renqi1']) ? 1 : 0;
			$firstYongjin = $_POST['firstYongjin'];
			$secondYongjin = $_POST['secondYongjin'];
			$thirdYongjin = $_POST['thirdYongjin'];
			$secret = floatval($_POST['secret']); 
			$supplierId = floatval($_POST['supplierId']);
			
            if($_POST['zzfb'] > 0){
                $id = $_POST['zzfb'];
                $detail=$this->db->YOne("SELECT * FROM `@#_sqfb` WHERE id = $id and stuats = 2 and goods_id = 0");
                if(!$detail)
                {
                    _note("请检查是否审核通过或是已存在商品",G_ADMIN_PATH.'/zzfb/addlist');
                }
                $uid = $_POST['uid'];
            }
            else{
                $uid = 0;
            }
			$shangpin = $this->db->Yone("select * from `@#_shangpin` where `supplierId` = '$supplierId'");
			if($shangpin){
				_note("已存在卡券",G_ADMIN_PATH.'/content/goods_list/secret');
			}
			
			//fix by dabin
			$goods_key_leixing = $_POST['goods_key']['leixing'];
			
			//roce:获取卡密卡号
			$cardId1 = mysql_escape_string($_POST['cardId1']);
			$cardId2 = mysql_escape_string($_POST['cardId2']);
			$cardId = mysql_escape_string($_POST['cardId']);
			$cardPwd = mysql_escape_string($_POST['cardPwd']);

			$quyu_begin = intval($_POST['quyu_begin']) ? intval($_POST['quyu_begin']) : null;
			$quyu_end = intval($_POST['quyu_end']) ? intval($_POST['quyu_end']) : null;
			if(!$quyu_begin || !$quyu_end){
				$max_quyu = max($quyu_begin,$quyu_end);
				$quyu_begin = $quyu_begin==$max_quyu ? $quyu_end : $quyu_begin;
				$quyu_end = $max_quyu;
			}
//			if(!$cateid)_note("请选择栏目");
//			if(!$pinpaiid)_note("请选择品牌");
			if(!$biaoti)_note("标题不能为空");
			if(!$thumb)_note("缩略图不能为空");
			
			
			$biaoti_style='';
			if($biaoti_color){
				$biaoti_style.='color:'.$biaoti_color.';';
			}
			if($biaoti_bold){
				$biaoti_style.='font-weight:'.$biaoti_bold.';';
			}
			if(isset($_POST['uppicarr'])){
				$picarr = serialize($_POST['uppicarr']);
			}else{
				$picarr = serialize(array());
			}
		
		
		
			
			if($_POST['xsjx_time'] != ''){			
				$xsjx_time = strtotime($_POST['xsjx_time']) ? strtotime($_POST['xsjx_time']) : time();
				$xsjx_time_h = intval($_POST['xsjx_time_h']) ? $_POST['xsjx_time_h'] : 36000;
				$xsjx_time += $xsjx_time_h;	
			}else{
				$xsjx_time = '0';		
			}	
		
		
			if($maxqishu > 65535){
				_note("最大期数不能超过65535期");
			}			
							
								
			if($money < $yunjiage) _note("商品价格不能小于购买价格");					
			$zongrenshu = ceil($money/$yunjiage);
			$codes_len = ceil($zongrenshu/3000);
			$shenyurenshu = $zongrenshu-$canyurenshu;
			if($zongrenshu==0 || ($zongrenshu-$canyurenshu)==0){
				_note("价格不正确");
			}
					
			$time=time();	//卡券添加时间		
			$this->db->tijiao_start();
					
			$query_1 = $this->db->Query("INSERT INTO `@#_shangpin` (`supplierId`,`secret`,`quyu_begin`,`quyu_end`,`ka`,`mi`,`cateid`, `brandid`, `title`, `title_style`, `title2`, `keywords`, `description`, `money`, `yunjiage`, `zongrenshu`, `canyurenshu`,`shenyurenshu`, `qishu`,`maxqishu`,`thumb`, `picarr`, `content`,`xsjx_time`,`renqi`,`renqi1`,`pos`, `time`, `cardId`, `cardId1`, `cardPwd`, `leixing`,`yuanjia`,`uid`,`firstYongjin`,`secondYongjin`,`thirdYongjin`) VALUES ('$supplierId','1','$quyu_begin','$quyu_end','".htmlspecialchars($_POST['kahao'])."','".htmlspecialchars($_POST['mima'])."','$cateid', '$pinpaiid', '$biaoti', '$biaoti_style', '$biaoti2', '$guanjianzi', '$miaoshu', '$money', '$yunjiage', '$zongrenshu', '$canyurenshu','$shenyurenshu', '1','$maxqishu', '$thumb', '$picarr', '$content','$xsjx_time','$shangpinss_key_renqi','$shangpinss_key_renqi1', '$shangpinss_key_pos','$time','$cardId','$cardId1','$cardPwd','$goods_key_leixing','$yuanjia','$uid','$firstYongjin','$secondYongjin','$thirdYongjin')");
			$shopid = $this->db->last_id();
			System::DOWN_App_fun("content");	
			
			$query_table = content_get_codes_table();
			if(!$query_table){
				$this->db->tijiao_rollback();
				_note("众筹码仓库不正确!");
			}
			$query_2 = content_huode_go_codes($zongrenshu,3000,$shopid);
			$query_3 = $this->db->Query("UPDATE `@#_shangpin` SET `codes_table` = '$query_table',`sid` = '$shopid',`def_renshu` = '$canyurenshu' where `id` = '$shopid'");
			
					
			if($query_1 && $query_2 && $query_3){
				$this->db->tijiao_commit();
                if($_POST['zzfb'] > 0){
                    $id = $_POST['zzfb'];
                    $this->db->Query("UPDATE `@#_sqfb` set goods_id = $shopid WHERE id = $id");

                    include_once((dirname(dirname(dirname(dirname(__FILE__)))).'/phpqrcode/phpqrcode.php'));
                    QRcode::png(LOCAL_PATH."/mobile/mobile/item/".$shopid.'.html','zizhu/'.$shopid.'.png','L',8);

                    _note("自助卡券添加成功并已生成链接!",G_ADMIN_PATH.'/zzfb/goodslist');
                }
		_note("卡券添加成功!",G_ADMIN_PATH.'/content/goods_list/secret');
			}else{		
			
				$this->db->tijiao_rollback();
				_note("卡券添加失败!");
			}	
			
			header("Cache-control: private");
		}
		
		
		$temp=intval($this->segment(4));
        if($temp == 'zzfb'){
            $id=intval($this->segment(5));
        }
        else{
            $cateid=intval($this->segment(4));
        }
		$fenleis=$this->db->Ylist("SELECT * FROM `@#_fenlei` WHERE `model` = '1' order by `parentid` ASC,`cateid` ASC",array('key'=>'cateid'));
		$tree=System::DOWN_sys_class('tree');
		$tree->icon = array('│ ','├─ ','└─ ');
		$tree->nbsp = '&nbsp;';
		$fenleishtml="<option value='\$cateid'>\$spacer\$name</option>";
		$tree->init($fenleis);	
		$fenleishtml=$tree->get_tree(0,$fenleishtml);
		$fenleishtml='<option value="0">≡ 请选择栏目 ≡</option>'.$fenleishtml;
		if($cateid){			
			$cateinfo=$this->db->YOne("SELECT * FROM `@#_fenlei` WHERE `cateid` = '$cateid' LIMIT 1");
			if(!$cateinfo)_note("参数不正确,没有这个栏目",G_ADMIN_PATH.'/'.DOWN_C.'/addarticle');
			$fenleishtml.='<option value="'.$cateinfo['cateid'].'" selected="true">'.$cateinfo['name'].'</option>';
			$pinpaiList=$this->db->Ylist("SELECT * FROM `@#_pinpai` where `cateid`='$cateid'",array("key"=>"id"));
		}else{
			$pinpaiList=$this->db->Ylist("SELECT * FROM `@#_pinpai` where 1",array("key"=>"id"));
		}	
		
		$this->ment=array(
			array("lists","商品管理",DOWN_M.'/'.DOWN_C."/goods_list"),
			array("insert","添加商品",DOWN_M.'/'.DOWN_C."/goods_add"),
		);
        if($id){
            $detail=$this->db->YOne("SELECT * FROM `@#_sqfb` WHERE id = $id and stuats = 2 and goods_id = 0");
            if(!$detail)
            {
                _note("请检查是否审核通过或是已存在商品",G_ADMIN_PATH.'/zzfb/addlist');
            }
        }
		include $this->dwt(DOWN_M,'shop.insert');
			
	}
	
	
	//商品设置
	public function goods_set(){
		$p_key = $this->segment(4);
		$p_val = intval($this->segment(5));
		
		if(empty($p_key) || empty($p_val)){
			_note("设置失败");
		}
		$ss = $this->db->YOne("SELECT * FROM `@#_shai` where `sd_shopid` = '$p_val'");
		//var_dump($ss[sd_userid]);
		//exit;
		$query = true;
		switch($p_key){
			case 'renqi':
				$query = $this->db->Query("UPDATE `@#_shangpin` SET `renqi` = '1' where `id` = '$p_val'");				
				break;	
				
			case 'pos':
				$query = $this->db->Query("UPDATE `@#_shangpin` SET `pos` = '1' where `id` = '$p_val'");				
				break;
				case 'renqi1':
				$query = $this->db->Query("UPDATE `@#_shangpin` SET `renqi1` = '1' where `id` = '$p_val'");				
				break;	
				case 'shenhe':
				$query = $this->db->Query("UPDATE `@#_shai` SET `shenhe` = '1' where `sd_shopid` = '$p_val'");	
				$this->db->Query("UPDATE `@#_yonghu` SET `score` = score+'1000' where `uid` = '$ss[sd_userid]'");
				break;	
				case 'shenhe1':
				$query = $this->db->Query("UPDATE `@#_shai` SET `shenhe` = '0' where `sd_shopid` = '$p_val'");		
				case 'shenhe2':
				$query = $this->db->Query("UPDATE `@#_shai` SET `shenhe` = '2' where `sd_shopid` = '$p_val'");					
				break;	
				case 'fahuo':
				$query = $this->db->Query("UPDATE `@#_shangpin` SET `fahuo` = '0' where `id` = '$p_val'");				
				break;
				case 'fahuo1':
				$query = $this->db->Query("UPDATE `@#_shangpin` SET `fahuo` = '1' where `id` = '$p_val'");				
				break;
				case 'huiyuan':
				$query = $this->db->Query("UPDATE `@#_yonghu` SET `huiyuan` = '1' where `uid` = '$p_val'");
				break;	
				case 'huiyuan1':
				$query = $this->db->Query("UPDATE `@#_yonghu` SET `huiyuan` = '0' where `uid` = '$p_val'");
				break;	
		}
		
		if($query){
			_note("设置成功");
		}else{
			_note("设置失败");
		}
		
		
	}
	
	//ajax 删除文章
	public function article_del(){
		$id=intval($this->segment(4));		
		$this->db->Query("DELETE FROM `@#_wenzhang` WHERE (`id`='$id') LIMIT 1");
			if($this->db->affected_rows()){			
				_note("文章删除成功",LOCAL_PATH."/admin/content/article_list");
			}else{
				echo "no";
			}	
	}
	
	//ajax 删除商品
	public function goods_del(){
		$this->db=System::DOWN_App_model('admin_model',G_ADMIN_DIR);
		if($uid && $ashell){
			$CheckId = _encrypt($uid,'DECODE');
			$CheckAshell =  _encrypt($ashell,'DECODE');
		}else{			
			$CheckId=_encrypt(_getcookie("AID"),'DECODE');
			$CheckAshell=_encrypt(_getcookie("ASHELL"),'DECODE');
		}
		$info=$this->db->YOne("SELECT * FROM `@#_manage` WHERE `uid` = '$CheckId'");
		//老的权限删除
	/*
		if($info[xianzhi]=='1'){
				_note("测试帐号无修改权限!<br>旺旺号:澳安达");
			}
			*/
		$shopid=intval($this->segment(4));		
		$info = $this->db->YOne("SELECT codes_table FROM `@#_shangpin` WHERE `id` = '$shopid'");
		$table = $info['codes_table'];
		
		$this->db->tijiao_start();	
		//$q1 = $this->db->Query("INSERT INTO `@#_shangpin_del` select * from `@#_shangpin` where `id` = '$shopid'");
		$q1 = $this->db->Query("DELETE FROM `@#_{$table}` WHERE `s_id` = '$shopid'");
		$q2 = $this->db->Query("DELETE FROM `@#_shangpin` WHERE `id` = '$shopid' LIMIT 1");	
		$q3 = $this->db->Query("DELETE FROM `@#_yonghu_yys_record` WHERE `shopid` = '$shopid'");	
		if($q1 && $q2 && $q3){					
			$this->db->tijiao_commit();
			_note("商品删除成功",LOCAL_PATH."/admin/content/goods_list");
		}else{
			$this->db->tijiao_rollback();
			echo "no";
		}
		exit;
	}	
	
	
	// 撤销删除
	public function goods_del_key(){
		$shopid=intval($this->segment(4));
		$key=$this->segment(5);
		//撤销	
		if($key=='yes'){			
			$this->db->tijiao_start();
			$q1 = $this->db->Query("INSERT INTO `@#_shangpin` select * from `@#_shangpin_del` where `id` = '$shopid' LIMIT 1");
			$q2 = $this->db->Query("DELETE FROM `@#_shangpin_del` WHERE `id` = '$shopid' LIMIT 1");
			if(!$q1 || !$q2){
				$this->db->tijiao_rollback();
				_note("操作失败");			
			}else{
				$this->db->tijiao_commit();
				_note("操作成功");
			}
		}
		//从数据库删除
		if($key=='no'){
			$this->db->Query("DELETE FROM `@#_shangpin_del` WHERE `id` = '$shopid' LIMIT 1");
			_note("操作成功");
		}
	}	
	
	//清空回收站
	public function goods_del_all(){
		$this->db->Query("TRUNCATE TABLE  `@#_shangpin_del`");
		_note("清空成功");
	}
	
	
	/*
	*	商品排序
	*/	
	public function goods_listorder(){		
		if($this->segment(4)=='dosubmit'){
			foreach($_POST['listorders'] as $id => $listorder){
				$id = intval($id);
				$listorder = intval($listorder);
				$this->db->Query("UPDATE `@#_shangpin` SET `order` = '$listorder' where `id` = '$id'");		
			}				
			_note("排序更新成功");
		}else{
			_note("请排序");
		}		
	}//
	
	/*
	*	文章排序
	*/	
	public function article_listorder(){		
		if($this->segment(4)=='dosubmit'){
			foreach($_POST['listorders'] as $id => $listorder){
				$id = intval($id);
				$listorder = intval($listorder);
				$this->db->Query("UPDATE `@#_wenzhang` SET `order` = '$listorder' where `id` = '$id'");		
			}				
			_note("排序更新成功");
		}else{
			_note("请排序");
		}		
	}
	//
		
	/*
	*	商品最大期数修改
	*	ajax
	*/	
	public function goods_max_qishu(){
		if($this->segment(4)!='dosubmit'){
			echo json_encode(array("meg"=>"not key","err"=>"-1"));			
			exit;
		}		
		
		if(!isset($_POST['gid']) || !isset($_POST['qishu']) || !isset($_POST['money']) || !isset($_POST['onemoney'])){
			echo json_encode(array("meg"=>"参数不正确!","err"=>"-1"));
			exit;
		}
				
		$yonghuid = abs(intval($_POST['gid']));
		$qishu = abs(intval($_POST['qishu']));
		$money = abs(intval($_POST['money']));
		$onemoney = abs(intval($_POST['onemoney']));
		
		
		if(!$yonghuid || !$qishu || !$money || !$onemoney){
			echo json_encode(array("meg"=>"参数不正确!","err"=>"-1"));
			exit;
		}
		if($money < $onemoney){
			echo json_encode(array("meg"=>" 总价不能小于惠券价!","err"=>"-1"));
			exit;
		}
		
		$info = $this->db->YOne("SELECT * FROM `@#_shangpin` where `id` = '$yonghuid' and `q_end_time` is not null");
		if(!$info || ($info['qishu']!=$info['maxqishu'])){
			echo json_encode(array("meg"=>"没有该商品或还有剩余期数!","err"=>"-1"));
			exit;
		}
		if($qishu <= $info['qishu']){
			echo json_encode(array("meg"=>"期数不正确!","err"=>"-1"));
			exit;
		}		
		$ret = $this->db->Query("UPDATE `@#_shangpin` SET `maxqishu` = '$qishu' where `sid` = '$info[sid]'");
		if(!$ret){
			echo json_encode(array("meg"=>"期数更新失败!","err"=>"-1"));
			exit;
		}
		$info['maxqishu'] = $qishu;
		$info['money'] = $money;
		$info['yunjiage'] = $onemoney;
		$info['zongrenshu'] = ceil($money/$onemoney);		
		System::DOWN_App_fun("content");
		$ret = content_add_shop_install($info);
		if($ret){
			echo json_encode(array("meg"=>"新建商品成功!","err"=>"1"));
			exit;
		}else{
			echo json_encode(array("meg"=>"更新失败失败!","err"=>"-1"));
			exit;		
		}		
	
		
	}//
	
	/**
	*	重置商品价格
	**/
	public function goods_set_money(){		

		$this->ment=array(
						array("lists","商品管理",DOWN_M.'/'.DOWN_C."/goods_list"),
						array("add","添加商品",DOWN_M.'/'.DOWN_C."/goods_add"),
						array("renqi","人气商品",DOWN_M.'/'.DOWN_C."/goods_list/renqi"),
						array("xsjx","限时惠券商品",DOWN_M.'/'.DOWN_C."/goods_list/xianshi"),
						array("qishu","期数倒序",DOWN_M.'/'.DOWN_C."/goods_list/qishu"),
						array("danjia","单价倒序",DOWN_M.'/'.DOWN_C."/goods_list/danjia"),
						array("money","商品价格倒序",DOWN_M.'/'.DOWN_C."/goods_list/money"),
						array("money","已惠券",DOWN_M.'/'.DOWN_C."/goods_list/jiexiaook"),
						array("money","<font color='#f00'>期数已满商品</font>",DOWN_M.'/'.DOWN_C."/goods_list/maxqishu"),
		);	
		$this->db->tijiao_start();	
		$yonghuid = abs(intval($this->segment(4)));
		$shopinfo = $this->db->YOne("SELECT * FROM `@#_shangpin` where `id` = '$yonghuid' for update");		
		if(!$shopinfo || !empty($shopinfo['q_uid'])){
			_note("参数不正确!");exit;
		}
		
		if(isset($_POST['money']) || isset($_POST['yunjiage'])){
			$new_money = abs(intval($_POST['money']));
			$new_one_m = abs(intval($_POST['yunjiage']));
			
	
			if($new_one_m > $new_money){
				_note("单人次购买价格不能大于商品总价格!");
			}
			if(!$new_one_m || !$new_money){
				_note("价格填写错误!");
			}
			if(($new_one_m == $shopinfo['yunjiage']) && ($new_money == $shopinfo['money'])){
				_note("价格没有改变!");
			}
			
			System::DOWN_App_fun("content");
						
			$table = $shopinfo['codes_table'];
			$this->db->tijiao_start();	
			$q1 = $this->db->Query("DELETE FROM `@#_yonghu_yys_record` WHERE `shopid` = '$yonghuid'");
			$q2 = $this->db->Query("DELETE FROM `@#_{$table}` WHERE `s_id` = '$yonghuid'");			
			$zongrenshu = ceil($new_money/$new_one_m);		
			
			$q3 = content_huode_go_codes($zongrenshu,3000,$yonghuid);
			$q4 = $this->db->Query("UPDATE `@#_shangpin` SET 
			`canyurenshu` = '0',
			`zongrenshu` = '$zongrenshu', 
			`money` = '$new_money', 
			`yunjiage` = '$new_one_m', 
			`shenyurenshu` = `zongrenshu`
			where `id` = '$yonghuid'");
			
			
			
			if($q1 && $q2 && $q3 && $q4){
					$this->db->tijiao_commit();
					_note("更新成功!");
			}else{
				$this->db->tijiao_rollback();
				_note("更新失败!");				
			}
		}
				
		include $this->dwt(DOWN_M,'shop.set_money');
	}
	
	
}
?>