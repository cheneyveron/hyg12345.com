<?php defined('G_IN_ADMIN')or exit('Access Denied.'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>商户综合信息</title>
<link rel="stylesheet" href="/statics/plugin/mstyle/css/style.css" type="text/css"><!--页面CSS-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/animate.min.css" type="text/css"><!--CSS3动画-->
<script type="text/javascript" src="/statics/plugin/mstyle/js/jquery.min.js"></script><!--jQuery库-->
<script type="text/javascript" src="http://api.map.baidu.com/api?key=&v=1.1&services=true"></script>
			<script type="text/javascript" charset="utf-8" src="<?php echo YYS_PLUGIN_PATH; ?>/calendar/calendar.js"></script>
			<script src="<?php echo YYS_GLOBAL_STYLE; ?>/global/js/jquery-1.8.3.min.js"></script>
			<script src="<?php echo YYS_PLUGIN_PATH; ?>/uploadify/api-uploadify.js" type="text/javascript"></script> 
			<script type="text/javascript">
			var editurl=Array();
			editurl['editurl']='<?php echo YYS_PLUGIN_PATH; ?>/ueditor/';
			editurl['imageupurl']='<?php echo G_ADMIN_PATH; ?>/ueditor/upimage/';
			editurl['imageManager']='<?php echo G_ADMIN_PATH; ?>/ueditor/imagemanager';
			</script>
			<script type="text/javascript" charset="utf-8" src="<?php echo YYS_PLUGIN_PATH; ?>/ueditor/ueditor.config.js"></script>
			<script type="text/javascript" charset="utf-8" src="<?php echo YYS_PLUGIN_PATH; ?>/ueditor/ueditor.all.min.js"></script>
						<style>

				.bg{background:#fff url(<?php echo YYS_GLOBAL_STYLE; ?>/global/image/ruler.gif) repeat-x scroll 0 9px }
				.header-title{
					text-indent:0px;
				}
				.header-title h3{
					display:-webkit-inline-box;
				}

			</style>
</head>
<body>
	<div class="container min-wid">
		<div class="path">
			<i><a href="#"><img src="/statics/plugin/mstyle/img/ico_1.png" alt=""></a></i>
			<p>
				当前位置：<a href="#">商户管理</a> > <a href="#">商户综合信息</a> >
		  </p>
			<div class="push">
				<a href="<?php echo YYS_MODULE_PATH; ?>/index/Tdefault">欢迎界面</a>
				<a href="javascript:void(0)" onclick="location=location">刷新框架</a>
				<a href="<?php echo YYS_MODULE_PATH; ?>/cache/init">清理缓存</a>
			</div>
		</div><!-- path -->
		<form method="post" action="/admin/supplier/manage_show/">
		<div class="rehensive wid1">
			<div class="fl-box h758">
				<h2><a  href="#">商户综合信息</a><a class="afterLeft" href="#">商户综合信息</a></h2>
				<div class="layout-fl">
					<ul>
						<li>
							<span>关联会员</span>
							<input type="text" value="<?php echo $supplier['uid']; ?>" name="uid" id="uid" placeholder="关联对应会员编号">
							<i>*</i>
						</li>
						<li class="ml25">
							<span>邀请号码</span>
							<input type="text" value="<?php echo $supplier['pid']; ?>" name="pid" id="pid" placeholder="邀请人对应店铺编号">
							<i>*</i>
						</li>
						<li>
							<span>店铺名称</span>
							<input type="text" value="<?php echo $supplier['name']; ?>" name="name" id="name"  placeholder="请填写店铺名称">
							<i>*</i>
						</li>
						<li class="ml25">
							<span>店铺编码</span>
							<input type="text"  value="<?php echo $supplier['id']; ?>" name="supplier_sn" id="supplier_sn"  disabled placeholder="自动生成唯一店铺编号">
							<i>*</i>
						</li>
						<li>
							<span>省市</span>
							<select name="province" id="province" class="fl">
<option value="0">省市名称</option>
<?php
	foreach($provinceList as $key => $one){
		$select = '';
		if($one['code'] == $supplier['province']){
			$select = "selected";
		}
		echo '<option '.$select.' value="'.$one['code'].'">'.$one['name'].'</option>';
	}
?>             
							</select>
							<i>*</i>
						</li>
						<li class="ml25">
							<span>负责人</span>
							<input type="text" value="<?php echo $supplier['realname']; ?>" name="realname" id="realname" placeholder="输入店铺负责人真实姓名">
							<i>*</i>
						</li>
						<li>
							<span>城市</span>
							<select class="fl" name="city" id="city">
<option value="">地级市</option>
<?php
	foreach($cityList as $key => $one){
		$select = '';
		if($one['code'] == $supplier['city']){
			$select = "selected";
		}
		echo '<option '.$select.' value="'.$one['code'].'">'.$one['name'].'</option>';
	}
?>             
							</select> 
							<i>*</i>
						</li>
						<li class="ml25">
							<span>手机号码</span>
							<input type="text" value="<?php echo $supplier['mobile']; ?>" name="mobile" id="mobile" placeholder="输入负责人手机号码">
							<i>*</i>
						</li>
						<li>
							<span>区县</span>
							<select class="fl" name="country" id="country">
								<option value="0">区或县级市</option>
<?php
	foreach($countryList as $key => $one){
		$select = '';
		if($one['code'] == $supplier['country']){
			$select = "selected";
		}
		echo '<option '.$select.' value="'.$one['code'].'">'.$one['name'].'</option>';
	}
?>             
							</select> 
							<i>*</i>
						</li>
						<li class="ml25">
							<span>身份证号</span>
							<input type="text" value="<?php echo $supplier['idcard']; ?>" name="idcard" id="idcard" maxlength="18" placeholder="输入负责人身份证号码">
							<i>*</i>
						</li>
						<li>
							<span>街道</span>
							<select class="fl" name="street" id="street" >
								<option value="0">街道或镇区</option>
								<?php
									foreach($streetList as $key => $one){
										$select = '';
										if($one['code'] == $supplier['street']){
											$select = "selected";
										}
										echo '<option '.$select.' value="'.$one['code'].'">'.$one['name'].'</option>';
									}
								?>             
							</select> 
							<i>*</i>
						</li>
						<li class="ml25">
							<span>主业选择</span>
							<select name="cateid" id="category" class="fl">
								<option value="-1">请选择行业服务一级</option>							
								<?php
									foreach($fenlei_lists as $key => $one){
										if($one['cateid'] == $supplier['cateid']){
											echo '<option selected value="'.$one['cateid'].'">'.$one['name'].'</option>';
										}else{
											echo '<option value="'.$one['cateid'].'">'.$one['name'].'</option>';
										}
									}
								?>              
							</select> 
							<i>*</i>
							<span>副业选择</span>
							<select name="catesubid" id="category_two" class="fl">                    
								<option value="0">请选择行业服务二级</option>
								<?php
									foreach($fenlei_list as $key => $one){
										if($one['cateid'] == $supplier['catesubid']){
											echo '<option selected value="'.$one['cateid'].'">'.$one['name'].'</option>';
										}else{
											echo '<option value="'.$one['cateid'].'">'.$one['name'].'</option>';
										}
									}
								?>  
							</select> 
						</li>
						<li class="lar">
							<span>营业地址</span>
							<input class="inp1" style="width:500px;" type="text" value="<?php echo $supplier['address']; ?>" name="address" id="address" placeholder="请填写营业店铺地址。">
							<i>*</i>
							<a style="width: 68px;
height: 28px;
text-align: center;
background: #eeeeee;
border: 1px solid #c9c9c9;
font-size: 14px;
color: #313131;
display: inline-block;
line-height: 26px;
margin: 0;" href="javascript:position()" onclick="position()">地图定位</a>
							<!-- <label>内部店铺 <input type="checkbox" name="" id=""></label>
							<label>是否停业 <input type="checkbox" name="" id=""></label> -->
						</li>
						<li class="lar">
							<span>审核回复</span>
							<input type="text" id="refuse" value="<?php echo $supplier['refuse']; ?>" name="refuse"  placeholder="请输入审核回复内容，规范不用回复。">
							<div class="box fr">
								审核选项
								<select name="status" id="status">
									<option value="0" <?php if($supplier['status'] == 0) echo "selected";?>>申请开店</option>
									<option value="1" <?php if($supplier['status'] == 1) echo "selected";?>>审核开店</option>
									<option value="2" <?php if($supplier['status'] == 2) echo "selected";?>>准备开店</option>
									<option value="3" <?php if($supplier['status'] == 3) echo "selected";?>>正式开店</option>
									<option value="4" <?php if($supplier['status'] == 4) echo "selected";?>>申请驳回</option>
									<option value="5" <?php if($supplier['status'] == 5) echo "selected";?>>店铺停业</option>
									<option value="6" <?php if($supplier['status'] == 6) echo "selected";?>>账户冻结</option>
								</select>
								<i>*</i>
							</div>
						</li>
						<li class="lar">
							<div class="box1 fl">
								<span>身份证照</span>
								<input type="text" type="text" id="idimg" value="<?php echo $supplier['idimg']; ?>" name="idimg" placeholder="点击上传身份证照图片">
								<input style="width:57px;" type="button" class="button" onClick="GetUploadify('<?php echo LOCAL_PATH; ?>','uploadify','身份证图上传','image','supplier',1,500000,'idimg')" value="上传图片"/>
							</div>
							<div class="box1 fl">
								<span>商店图证</span>
								<input type="text" id="supplier_img" value="<?php echo $supplier['supplier_img']; ?>" name="supplier_img" placeholder="点击上传身份证照图片">
								<input style="width:57px;" type="button" class="button" onClick="GetUploadify('<?php echo LOCAL_PATH; ?>','uploadify','店铺图上传','image','supplier',1,500000,'supplier_img')" value="上传图片"/>
							</div>
						</li>

						<li class="lar">
							<div class="box2 fl">
								<span>证件缩图</span>
								<div class="pict">
									<img src="<?php echo YYS_UPLOADS_PATH.'/'.$supplier['idimg']; ?>" alt="">
								</div>
							</div>
							<div class="box2 fr">
								<span>店景缩图</span>
								<div class="pict">
									<img src="<?php echo YYS_UPLOADS_PATH.'/'.$supplier['supplier_img']; ?>" alt="">
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
				<div class="shop-noti">
					<ul>
						<li>
							<span>店铺公告</span>
							<textarea name="notice" id="notice" placeholder="请输入店铺公告。"><?php echo $supplier['notice']; ?></textarea>
						</li>
						<li>
							<span>主营商品</span>
							<textarea name="goods_type" id="goods_type" placeholder="请输入主营商品。"><?php echo $supplier['goods_type']; ?></textarea>
						</li>
						<li>
							<span>营业服务</span>
							<textarea name="service" id="service" placeholder="请输入营业服务。"><?php echo $supplier['service']; ?></textarea>
						</li>
						<li>
							<div class="fl-dian-num">
								<dl>
									<dd>
										<span>预约手机</span>
										<input type="text" name="appointment_mobile" id="appointment_mobile" value="<?php echo $supplier['appointment_mobile']; ?>" placeholder="请输入预约手机。">
									</dd>
									<dd>
										<span>邀店铺数</span>
										<input class="inp1" type="text" disabled placeholder="邀请店铺的数量">
										<a href="#" class="a1">显示封户级别</a>
									</dd>
									<dd>
										<span>评价计分</span>
										<input class="inp1" type="text" value="<?php echo $supplier['credibility']; ?>" disabled placeholder="店铺评价的计分">
										<a href="#" class="a1">显示评价级别</a>
									</dd>
									<dd>
										<div class="pict">
											<img  src="<?php echo YYS_UPLOADS_PATH.'/'.$supplier['supplier_logo']; ?>" alt=""/>
										</div>
										<input type="text" id="supplier_logo" value="<?php echo $supplier['supplier_logo']; ?>" name="supplier_logo" placeholder="点击上传LOGO图片" style="width:202px"/>
										<input style="width:57px;" type="button" class="button" onClick="GetUploadify('<?php echo LOCAL_PATH; ?>','uploadify','店铺logo','image','supplier',1,500000,'supplier_logo')" value="上传图片"/>
									</dd>
								</dl>
							</div>
							<div class="fr-dian-pic">
								<dl>
									<dd>

							
								<span>店招海报</span>
								<input type="text" id="supplier_poster" value="<?php echo $supplier['supplier_poster']; ?>" name="supplier_poster" placeholder="点击上传店招海报图片">
								<input style="width:57px;" type="button" class="button" onClick="GetUploadify('<?php echo LOCAL_PATH; ?>','uploadify','店招海报上传','image','supplier',1,500000,'supplier_poster')" value="上传图片"/>
							
										
										
									</dd>
									<dd class="mb0">
										<span>店招缩图</span>
										<div class="pict">
											<img src="<?php echo YYS_UPLOADS_PATH.'/'.$supplier['supplier_poster']; ?>" alt="">
										</div>
									</dd>
								</dl>
							</div>
						</li>
						<li class="mt0">
							<span>店铺选项</span>

							<label><input name="isCanSearch" id="isCanSearch" value="1" type="checkbox" <?php if($supplier['isCanSearch']){echo "checked";} ?>/>区域</label>
							<label><input name="isImg" id="isImg" value="1" type="checkbox" <?php if($supplier['isImg']){echo "checked";} ?>/>拍图</label>
							<label><input name="isCard" id="isCard" value="1" type="checkbox" <?php if($supplier['isCard']){echo "checked";} ?>/>卡券</label>
							<label><input name="isBenefits" id="isBenefits" value="1" type="checkbox" <?php if($supplier['isBenefits']){echo "checked";} ?>/>共赢</label>
							<label><input name="isMutual" id="isMutual" value="1" type="checkbox" <?php if($supplier['isMutual']){echo "checked";} ?>/>邀请</label>
							<label><input name="inside" id="inside" value="1" type="checkbox" <?php if($supplier['inside']){echo "checked";} ?>/>内部店</label>
							<!--label><input type="checkbox" checked="checked" name="1" id="">是否停业</label-->
						</li>
						<li>
							<span>内部备注</span>
							<input type="text" id="remarks" value="<?php echo $supplier['remarks']; ?>" name="remarks" placeholder="请输入商户的审核状态或停业原因等，用于内部管理对商户的注释文本，便于了解商户情况。">
						</li>
					</ul>
					<div class="save">
						<div class="coordinate">
							<p class="fl">
								地理坐标
								<input type="text" id="lat" name="lat" value="<?php echo $supplier['lat']; ?>" style="width:220px;"/>
								<input type="text" id="lng" name="lng" value="<?php echo $supplier['lng']; ?>"/>
							</p>
						</div>
						<input type="submit"	//window.parent.api_off_on_open('open');
	window.map = new BMap.Map("dituContent");//在百度地图容器中创建一个地图
        var point = new BMap.Point(116.331398,39.897445);//定义一个中心点坐标
        window.map.centerAndZoom(point,12);//设定地图的中心点和坐标并将地图显示在地图容器中
		window.map.addEventListener("click",function(e){
			$("#lat").val(e.point.lat);
			$("#lng").val(e.point.lng);
		});
    
        window.map.enableDragging();//启用地图拖拽事件，默认启用(可不写)
        window.map.enableScrollWheelZoom();//启用地图滚轮放大缩小
        window.map.enableDoubleClickZoom();//启用鼠标双击放大，默认启用(可不写)
        window.map.enableKeyboard();//启用键盘上下左右键移动地图

        //向地图中添加缩放控件
		var ctrl_nav = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});
		window.map.addControl(ctrl_nav);
			//向地图中添加缩略图控件
		var ctrl_ove = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:1});
		window.map.addControl(ctrl_ove);
			//向地图中添加比例尺控件
		var ctrl_sca = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});
		window.map.addControl(ctrl_sca);

		window.map.addEventListener("dragend", function(){
			var center = window.map.getCenter();
			//alert("地图中心点变更为：" + center.lng + ", " + center.lat);
			$(".chant-shop .layout-fr .coordinate .fl input").val(center.lng + ", " + center.lat);
		});
			 class="page-but" name="dosubmit"  value=" 保存 " >
						&nbsp;&nbsp;&nbsp; &nbsp;
						<input type="reset" class="page-but" name="reset" value="取消">
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<div class="rehen wid1">
			<div id="dituContent"></div>
		</div>
		<input type="hidden" value="<?php echo $supplier['id'];?>" name="id"/>
	</div><!-- container --></form>
</body>
<script src="/statics/plugin/mstyle/js/library-3.28.js"></script><!--自定义封装函数-->
<script src="/statics/plugin/mstyle/js/scrollanim.min.js"></script><!--动画效果库-->
<script type="text/javascript">
    var showop=true;
	$(".chant-shop .layout-fl .lar .fl.box2 .pict").click(function(event) {
		//$(".chant-shop .layout-fl .lar .box2 .pict").css({"width":"250px","height":"180px"});
		if(showop){
			$(this).css({"z-index":"2"}).animate({"z-Index":"1","height":"+=220px","width":"+=360px"},400);
			showop=false;
		}
		else{
			showop=true;
			$(this).animate({"height":"-=220px","width":"-=360px"},400,function(){
				$(this).css({"z-index":"0"});
			});
		}
	});
</script>
<span id="title_colorpanel" style="position:absolute; left:568px; top:115px" class="colorpanel"></span>
<script type="text/javascript">
	/*地区联动*/

	$("select[name='province']").change(function(){

		var code = $(this).val();

		ajaxGetArea(code,'2');

	});

	

	$("select[name='city']").change(function(){

		var code = $(this).val();

		ajaxGetArea(code,'3');

	});

	

	$("select[name='country']").change(function(){

		var code = $(this).val();

		ajaxGetArea(code,'4');

	});

	

	$("select[name='street']").change(function(){

		var code = $(this).val();

		ajaxGetArea(code,'5');

	});



	function ajaxGetArea(code,level){

		$.ajax({

			type: "POST",

			url: "/mobile/ajax_user/ajax_add_data/",

			dataType:'json',

			data:{code:code,level:level},

			success: function(data){

				if(data.status == 1){

					var html = '<option value="0">请选择</option>';

					$.each(data.list,function (index,obj){

						html += '<option value="'+obj.code+'">'+obj.name+'</option>';

					});



					if(level == '2'){

						$("select[name='city']").html(html);

						$("select[name='country']").html(html);

						$("select[name='area']").html(html);

						$("select[name='street']").html(html);

					}else if(level == '3'){

						$("select[name='country']").html(html);

						$("select[name='area']").html(html);

						$("select[name='street']").html(html);

					}else if(level == '4'){

						$("select[name='area']").html(html);

						$("select[name='street']").html(html);

					}else if(level == '5'){

						$("select[name='area']").html(html);

					}

				}

			}

		}); 

	}







	$("#status").change(function(){	

		var status = $(this).val();

		if(status == '2'){

			$("#refuse").show();

		}else{

			$("#refuse").hide();

		}

	});
	$("select[name='cateid']").change(function(){
		var cateid = $(this).val();
		$.ajax({
			type: "POST",
			url: "/admin/supplier/getSubFenlei",
			data: {cateid:cateid},
			dataType: "json",
			success: function(data){
				if(data.status == 1){
					var html = '';
					$.each(data.list,function (index,obj){
						html += '<option value="'+obj.cateid+'">'+obj.name+'</option>';
					});
					$('select[name="catesubid"]').html(html);
				}else{
					var html = '<option value="0">请选择行业服务二级</option>';
					$('select[name="catesubid"]').html(html);
				}
			}
		});
	});
	


	var lng = "<?php echo $supplier['lng'];?>";

	var lat = "<?php echo $supplier['lat'];?>";

	var map = new BMap.Map("allmap");

	map.enableScrollWheelZoom();

	var point = new BMap.Point(lng,lat);

	map.centerAndZoom(point,30);

	var marker = new BMap.Marker(point);

	map.addOverlay(marker);

	marker.setAnimation(BMAP_ANIMATION_BOUNCE);

	

	map.addEventListener("click",function(e){

		$("#lat").val(e.point.lat);

		$("#lng").val(e.point.lng);

		map.clearOverlays();

		var point = new BMap.Point(e.point.lng,e.point.lat);

		var marker = new BMap.Marker(point);

		map.addOverlay(marker);

		marker.setAnimation(BMAP_ANIMATION_BOUNCE);

	});





	date = new Date();

	Calendar.setup({

		inputField     :    "create_time",

		ifFormat       :    "%Y-%m-%d %H:%M:%S",

		showsTime      :    true,

		timeFormat     :    "24"

	});

		

    //实例化编辑器

    var ue = UE.getEditor('myeditor');

    ue.addListener('ready',function(){

        this.focus()

    });

	$("select[name='cateid']").change(function(){
		var cateid = $(this).val();
		$.ajax({
			type: "POST",
			url: "/admin/supplier/getSubFenlei",
			data: {cateid:cateid},
			dataType: "json",
			success: function(data){
				if(data.status == 1){
					var html = '';
					$.each(data.list,function (index,obj){
						console.log(obj.cateid);
						if(obj.cateid == "<?php echo $supplier['cateid'];?>"){
							html += '<option selected value="'+obj.cateid+'">'+obj.name+'</option>';
						}else{
							html += '<option value="'+obj.cateid+'">'+obj.name+'</option>';
						}
					});
					$('select[name="catesubid"]').html(html);
				}else{
					var html = '<option value="0">请选择行业服务二级</option>';
					$('select[name="catesubid"]').html(html);
				}
			}
		});
	});
	

	var info=new Array();

    function gbcount(message,maxlen,id){

		

		if(!info[id]){

			info[id]=document.getElementById(id);

		}			

        var lenE = message.value.length;

        var lenC = 0;

        var enter = message.value.match(/\r/g);

        var CJK = message.value.match(/[^\x00-\xff]/g);//计算中文

        if (CJK != null) lenC += CJK.length;

        if (enter != null) lenC -= enter.length;		

		var lenZ=lenE+lenC;		

		if(lenZ > maxlen){

			info[id].innerHTML=''+0+'';

			return false;

		}

		info[id].innerHTML=''+(maxlen-lenZ)+'';

    }

	

	function set_title_color(color) {

		$('#title').css('color',color);

		$('#title_style_color').val(color);

	}



	function set_title_bold(){

		if($('#title_style_bold').val()=='bold'){

			$('#title_style_bold').val('');	

			$('#title').css('font-weight','');

		}else{

			$('#title').css('font-weight','bold');

			$('#title_style_bold').val('bold');

		}

	}

	//API JS

	//window.parent.api_off_on_open('open');

</script>
<script>	
	//window.parent.api_off_on_open('open');
	window.map = new BMap.Map("dituContent");//在百度地图容器中创建一个地图
        var point = new BMap.Point(116.331398,39.897445);//定义一个中心点坐标
        window.map.centerAndZoom(point,12);//设定地图的中心点和坐标并将地图显示在地图容器中
		window.map.addEventListener("click",function(e){
			$("#lat").val(e.point.lat);
			$("#lng").val(e.point.lng);
		});
    
        window.map.enableDragging();//启用地图拖拽事件，默认启用(可不写)
        window.map.enableScrollWheelZoom();//启用地图滚轮放大缩小
        window.map.enableDoubleClickZoom();//启用鼠标双击放大，默认启用(可不写)
        window.map.enableKeyboard();//启用键盘上下左右键移动地图

        //向地图中添加缩放控件
		var ctrl_nav = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});
		window.map.addControl(ctrl_nav);
			//向地图中添加缩略图控件
		var ctrl_ove = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:1});
		window.map.addControl(ctrl_ove);
			//向地图中添加比例尺控件
		var ctrl_sca = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});
		window.map.addControl(ctrl_sca);

		window.map.addEventListener("dragend", function(){
			var center = window.map.getCenter();
			//alert("地图中心点变更为：" + center.lng + ", " + center.lat);
			$(".chant-shop .layout-fr .coordinate .fl input").val(center.lng + ", " + center.lat);
		});
			
	function position(){
        var province = $("select[name='province']").find('option:selected').text();
        var city = $("select[name='city']").find('option:selected').text();
        var country = $("select[name='country']").find('option:selected').text();
        var street = $("select[name='street']").find('option:selected').text();
        var address = $("#address").val();
        if (!address || address == '') {
            alert('请先填写具体地址')
        } else {
			var myGeo = new BMap.Geocoder();
			console.log(province + city + country + street + address);
            myGeo.getPoint(province + city + country + street + address, function(point) {
                if (point) {
                    window.map.clearOverlays();
                    window.map.centerAndZoom(point, 16);
                    window.map.addOverlay(new BMap.Marker(point));
                    $("#lat").val(point.lat);
                    $("#lng").val(point.lng)
                } else {
                    alert('没有找到这个地址，请手动选择')
                }
            }, city)
        }
	}
	position();
</script>
</html>
