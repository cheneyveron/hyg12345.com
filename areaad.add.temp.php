<?php defined('G_IN_ADMIN')or exit('Access Denied.'); ?>
<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>后台首页</title>
			<link rel="stylesheet" href="<?php echo YYS_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
			<link rel="stylesheet" href="<?php echo YYS_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
			<link rel="stylesheet" href="<?php echo YYS_PLUGIN_PATH; ?>/calendar/calendar-blue.css" type="text/css"> 
			<script type="text/javascript" charset="utf-8" src="<?php echo YYS_PLUGIN_PATH; ?>/calendar/calendar.js"></script>
			<script src="<?php echo YYS_GLOBAL_STYLE; ?>/global/js/jquery-1.8.3.min.js"></script>
			<script src="<?php echo YYS_PLUGIN_PATH; ?>/uploadify/api-uploadify.js" type="text/javascript"></script> 
			<script type="text/javascript">
			var editurl=Array();
			editurl['editurl']='<?php echo YYS_PLUGIN_PATH; ?>/ueditor/';
			editurl['imageupurl']='<?php echo G_ADMIN_PATH; ?>/ueditor/upimage/';
			editurl['imageManager']='<?php echo G_ADMIN_PATH; ?>/ueditor/imagemanager';
			</script>
			<script type="text/javascript" src="http://api.map.baidu.com/api?key=&v=1.1&services=true"></script>
			<script type="text/javascript" charset="utf-8" src="<?php echo YYS_PLUGIN_PATH; ?>/ueditor/ueditor.config.js"></script>
			<script type="text/javascript" charset="utf-8" src="<?php echo YYS_PLUGIN_PATH; ?>/ueditor/ueditor.all.min.js"></script>
			<style>
				.bg{background:#fff url(<?php echo YYS_GLOBAL_STYLE; ?>/global/image/ruler.gif) repeat-x scroll 0 9px }
				.header-title{
					text-indent:0px;
				}
				.header-title h3{
					display:-webkit-inline-box;
				}
			</style>
		</head>
		<body>
			<div class="header-title lr10">
				<?php echo $this->headerment();?>
				<b>添加区域广告</b>
			</div>
			<div class="bk10"></div>
			<div class="table_form lr10">
			<form method="post" action="">
				<table width="100%"  cellspacing="0" cellpadding="0">
					<tr>
						<td align="right" width="120">
							<font color="red">*</font>区域广告标题：
						</td>
						<td>
							<input  type="text"  name="title" id="title" onKeyUp="return gbcount(this,100,'texttitle');"  class="input-text wid400 bg"/>
							<input type="hidden" name="title_style_color" id="title_style_color"/>
							<input type="hidden" name="title_style_bold" id="title_style_bold"/>
							<script src="<?php echo YYS_GLOBAL_STYLE; ?>/global/js/colorpicker.js"></script>
							<img src="<?php echo YYS_GLOBAL_STYLE; ?>/global/image/colour.png" width="15" height="16" onClick="colorpicker('title_colorpanel','set_title_color');" style="cursor:hand"/>
							<img src="<?php echo YYS_GLOBAL_STYLE; ?>/global/image/bold.png" onClick="set_title_bold();" style="cursor:hand"/>
							<span style="margin-left:10px">还能输入<b id="texttitle">100</b>个字符</span>
						</td>
					</tr>
					<tr>
						<td width="200" align="right">URL：</td>
						<td>
							<input type="text" name="url" value="" class="input-text wid400"/>
							<span style="margin-left:10px">格式：http://www.xxx.com</span>
						</td>
					</tr>
					<tr>
						<td align="right" style="width:120px">
							<font color="red">*</font>广告图：
						</td>
						<td>
							<img src="<?php echo YYS_UPLOADS_PATH; ?>/photo/goods.jpg" style="border:1px solid #eee; padding:1px; width:50px; height:50px;">
							<input type="text" id="imagetext" name="img" value="photo/goods.jpg" class="input-text wid300">
							<input type="button" class="button" onClick="GetUploadify('<?php echo LOCAL_PATH; ?>','uploadify','广告图上传','image','shopimg',1,500000,'imagetext','zhutu')" value="上传图片"/>
						</td>
					</tr>
					<tr>
						<td align="right">发布时间：</td>
						<td>           
							<input name="posttime" type="text" id="posttime" value="<?php echo date("Y-m-d H:i:s",$info['create_time']); ?>" class="input-text posttime date_picker"  readonly="readonly" />
							<script type="text/javascript">
							date = new Date();
							Calendar.setup({
								inputField     :    "posttime",
								ifFormat       :    "%Y-%m-%d %H:%M:%S",
								showsTime      :    true,
								timeFormat     :    "24"
							});
							</script></td>             
					</tr> 
					<tr>
						<td align="right">是否默认：</td>
						<td><input type="radio" name="default" value="1"/>是
						&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="default" value="0"/>否</td>
						<td>
						</td>             
					</tr>
					<tr>
						<td align="right">排序：</td>
						<td><input type="text" name="sort" class="input-text wid50" value="0"/></td>
						<td>
						</td>             
					</tr>
					<tr>
						<td align="right">位置（在下方地图中选择）：</td>
						<td>经度：<input type="text" name="lat" id="lat" class="input-text wid100" value=""/>
						纬度：<input type="text" name="lng" id="lng" class="input-text wid100" value=""/>
						所属区域：<input type="text" name="region" id="region" class="input-text wid200" value=""/>
						<a href="javascript:posistion()" onclick="posistion()" class="button">地图定位</a>
						</td>
					</tr>
					<tr height="60px">
						<td align="right"></td>
						<td><input type="submit" name="dosubmit" class="button" value="添加公告" /></td>
					</tr>
				</table>
			</form>
		</div>
						<div id="dituContent" style="width:100%;height:500px;"></div>
<span id="title_colorpanel" style="position:absolute; left:568px; top:115px;z-index:999;" class="colorpanel"></span>
<script type="text/javascript">
    //实例化编辑器
    var ue = UE.getEditor('myeditor');
    ue.addListener('ready',function(){
        this.focus()
    });
	
	var info=new Array();
    function gbcount(message,maxlen,id){
		
		if(!info[id]){
			info[id]=document.getElementById(id);
		}			
        var lenE = message.value.length;
        var lenC = 0;
        var enter = message.value.match(/\r/g);
        var CJK = message.value.match(/[^\x00-\xff]/g);//计算中文
        if (CJK != null) lenC += CJK.length;
        if (enter != null) lenC -= enter.length;		
		var lenZ=lenE+lenC;		
		if(lenZ > maxlen){
			info[id].innerHTML=''+0+'';
			return false;
		}
		info[id].innerHTML=''+(maxlen-lenZ)+'';
    }
	
	function set_title_color(color) {
		$('#title').css('color',color);
		$('#title_style_color').val(color);
	}

	function set_title_bold(){
		if($('#title_style_bold').val()=='bold'){
			$('#title_style_bold').val('');	
			$('#title').css('font-weight','');
		}else{
			$('#title').css('font-weight','bold');
			$('#title_style_bold').val('bold');
		}
	}

	//API JS
	//window.parent.api_off_on_open('open');
</script>
<script>

	//window.parent.api_off_on_open('open');
	window.map = new BMap.Map("dituContent");//在百度地图容器中创建一个地图
        var point = new BMap.Point(116.331398,39.897445);//定义一个中心点坐标
        window.map.centerAndZoom(point,12);//设定地图的中心点和坐标并将地图显示在地图容器中
		window.geoc = new BMap.Geocoder();    
		window.map.addEventListener("click",function(e){
			$("#lat").val(e.point.lat);
			$("#lng").val(e.point.lng);
			var pt = e.point;
			window.geoc.getLocation(pt, function(rs){
				var addComp = rs.addressComponents;
				$("#region").val(addComp.province + addComp.city + addComp.district + addComp.street);
			});
		});
    
        window.map.enableDragging();//启用地图拖拽事件，默认启用(可不写)
        window.map.enableScrollWheelZoom();//启用地图滚轮放大缩小
        window.map.enableDoubleClickZoom();//启用鼠标双击放大，默认启用(可不写)
        window.map.enableKeyboard();//启用键盘上下左右键移动地图

        //向地图中添加缩放控件
		var ctrl_nav = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});
		window.map.addControl(ctrl_nav);
			//向地图中添加缩略图控件
		var ctrl_ove = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:1});
		window.map.addControl(ctrl_ove);
			//向地图中添加比例尺控件
		var ctrl_sca = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});
		window.map.addControl(ctrl_sca);

		window.map.addEventListener("dragend", function(){
			var center = window.map.getCenter();
			//alert("地图中心点变更为：" + center.lng + ", " + center.lat);
			$(".chant-shop .layout-fr .coordinate .fl input").val(center.lng + ", " + center.lat);
		});
	function posistion(){
        var address = $("#region").val();
        if (!address || address == '') {
            alert('请先填写具体地址')
        } else {
			var myGeo = new BMap.Geocoder();
            myGeo.getPoint(address, function(point) {
                if (point) {
                    window.map.clearOverlays();
                    window.map.centerAndZoom(point, 16);
                    window.map.addOverlay(new BMap.Marker(point));
                    $("#lat").val(point.lat);
                    $("#lng").val(point.lng)
                } else {
                    alert('没有找到这个地址，请手动选择')
                }
            }, "北京")
        }
	}
</script>
</body>
</html> 