<?php defined('G_IN_ADMIN')or exit('Access Denied.'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>商户正式开店</title>
<link rel="stylesheet" href="/statics/plugin/mstyle/css/style.css" type="text/css"><!--页面CSS-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/animate.min.css" type="text/css"><!--CSS3动画-->
<link rel="stylesheet" href="/statics/plugin/mstyle/css/timer.css" type="text/css"><!--CSS3动画-->
<script type="text/javascript" src="/statics/plugin/mstyle/js/jquery.min.js"></script><!--jQuery库-->
</head>
<body>
	<div class="container min-wid">
		<div class="path">
			<i><a href="#"><img src="/statics/plugin/mstyle/img/ico_1.png" alt=""></a></i>
			<p>
				当前位置：<a href="#">商户管理</a> > <a href="#">商户正式开店</a> >
			</p>
			<div class="push">
				<a href="<?php echo YYS_MODULE_PATH; ?>/index/Tdefault">欢迎界面</a>
				<a href="javascript:void(0)" onclick="location=location">刷新框架</a>
				<a href="<?php echo YYS_MODULE_PATH; ?>/cache/init">清理缓存</a>
			</div>
		</div><!-- path -->
		<form action="#" method="get">
		<div class="formal wid1">
			<div class="fl-box">
			<div class="grate-date">
				开店日期
				<input id="in1" type="text" name="start_time" value='<?php if(isset($_POST["start_time"])) echo $_POST["start_time"]; else echo "2018-1-1"; ?>' class="date_picker">
				至
				<input id="in2" type="text" name="end_time" value='<?php if(isset($_POST["end_time"])) echo $_POST['end_time']; else echo '2018-2-1'; ?>'  class="date_picker">
			</div>
			<button type="submit" name="timesubmit" value="筛选">筛选</button>
				<var>省市</var>
				<select name="province" id="province">
					<option value="0">省市名称</option>
					<?php
						foreach($provinceList as $key => $one){
							$select = '';
							if($one['code'] == $supplier['province']){
								$select = "selected";
							}
							echo '<option '.$select.' value="'.$one['code'].'">'.$one['name'].'</option>';
						}
					?>
				</select>
				<var>城市</var>
				<select name="city" id="city">
					<option value="0">地级市</option>
				</select> 
				<var>区县</var>
				<select name="country" id="country">
					<option value="0">区或县级市</option>
				</select>
				<button type="submit" name="areasubmit" value="筛选">筛选</button>
				<select name="sotype">
					<option value="name">商户名称</option>
					<option value="id">商户编号</option>
					<option value="mobile">手机号码</option>
				</select>
				<input type="text" name="sosotext" placeholder="输入关键词查询">
				<button type="submit" name="namesubmit">筛选</button>

			</div>
			<div class="fr-box">
			</div>
		</div>
		</form>

		<div class="formal-table wid1">
			<ul>
				<li class="head">
					<span>店铺ID</span>
					<span>店铺名称</span>
					<span>区县街道</span>
					<span>具体地址</span>
					<span>行业服务</span>
					<span>负责人</span>
					<span>店招主图</span>
					<span>评价级别</span>
					<span>店铺状态</span>
					<span>区域搜索</span>
					<span>编辑</span>
					<span>卡券</span>
					<span>共赢</span>
					<span>邀请</span>
					<span>邀店铺</span>
					<span>店商品</span>
					<span>货款</span>
					<span>拍图</span>
					<span>口碑</span>
					<!--span>变更</span-->
				</li>
				<?php foreach($notice_list AS $v) { ?>
				<li>
					<span><?php echo $v['id'];?></span>
					<span><a target="_blank" href="/mobile/supplier/index/<?php echo $v['id']; ?>"><?php echo _strcut($v['name'],0,25);?></a></span>
					<span><?php 
						$address  = getCityName($v['country']);
						$address .= getCityName($v['street']);
						echo $address;
					?></span>
					<span><?php echo $v['address']; ?></span>
					<span><?php echo $this->categorys[$v['cateid']]['name']; ?></span>
					<span><?php echo $v['realname']; ?></span>
					<span>
						<i>
							<img src="/statics/uploads/<?php echo $v['supplier_poster']; ?>" alt="">
						</i>
					</span>
					<span>
						<i>
							<img src="/statics/uploads/<?php echo $v['icon']; ?>" alt="">
						</i>
					</span>
					<span><select name="status" id="status">
<option value="0" <?php if($v['status'] == 0) echo "selected";?>>申请开店</option>
<option value="1" <?php if($v['status'] == 1) echo "selected";?>>审核开店</option>
<option value="2" <?php if($v['status'] == 2) echo "selected";?>>准备开店</option>
<option value="3" <?php if($v['status'] == 3) echo "selected";?>>正式开店</option>
<option value="4" <?php if($v['status'] == 4) echo "selected";?>>申请驳回</option>
<option value="5" <?php if($v['status'] == 5) echo "selected";?>>店铺停业</option>
<option value="6" <?php if($v['status'] == 6) echo "selected";?>>账户冻结</option>
</select></span>
<span><?php echo ($v['isCanSearch']=='1') ? '开启' : '关闭'; ?></span>
<span><a class="ico-edit" href="<?php echo YYS_MODULE_PATH; ?>/supplier/manage_show/<?php echo $v['id']; ?>"></a></span>
<span><a class="ico-svip" href="<?php echo YYS_MODULE_PATH; ?>/content/goods_list/manage_card/<?php echo $v['id']; ?>"></a></span>
<span><a class="ico-qian" href="<?php echo YYS_MODULE_PATH; ?>/supplier/manage_benefits/<?php echo $v['id']; ?>#"></a></span>
<span><a class="ico-ying" href="<?php echo YYS_MODULE_PATH; ?>/supplier/manage_mutual/<?php echo $v['id']; ?>"></a></span>
<span><a class="ico-dian" href="<?php echo YYS_MODULE_PATH; ?>/supplier/manage_invite_b/<?php echo $v['id']; ?>"></a></span>
<span><a class="ico-yuan" href="<?php echo YYS_MODULE_PATH; ?>/shop/goods_lists/0/<?php echo $v['id']; ?>"></a></span>
<span><a class="ico-hk" href="/member/member/gold/<?php echo $v['uid']; ?>"></a></span>
<span><a class="ico-pt" href="<?php echo YYS_MODULE_PATH; ?>/supplier/comprehensive_img/<?php echo $v['id']; ?>"></a></span>
<span><a class="ico-kb" href="<?php echo YYS_MODULE_PATH; ?>/supplier/comprehensive_comment/<?php echo $v['id']; ?>"></a></span>
<!--span><a class="ico-gm" href="<?php echo YYS_MODULE_PATH; ?>/supplier/comprehensive_change/<?php echo $v['id']; ?>#"></a></span-->
				</li>
				<?php } ?>

			</ul>
		</div>
		<div class="pages">
			<span>
				<?php echo $fenye->show('new')['html']; ?>
			</span>
			<span>共 <?php echo ceil($zongji/$num); ?> 页</span>
		
			<span>到第<input type="text" id="page" value="">页</span>
			<span><a class="page-but" id="gopage" href="javascript:void(0);">确定</a></span>
		</div>
	</div><!-- container -->
</body>
<script src="/statics/plugin/mstyle/js/jquery.date_input.pack.js"></script>
<script src="/statics/plugin/mstyle/js/library-3.28.js"></script><!--自定义封装函数-->
<script src="/statics/plugin/mstyle/js/scrollanim.min.js"></script><!--动画效果库-->
<script>
	$("#gopage").click(function(){
		var page = $("#page").val();
		var allpage = "<?php echo ceil($zongji/$num); ?>";
		if(page == '' || allpage < page){
			var page = 1;
		}
		location.href = "<?php echo $fenye->show('new')['url']; ?>"+page;
	});
	
	/*地区联动*/
	$("select[name='province']").change(function(){
		var code = $(this).val();
		ajaxGetArea(code,'2');
	});
	
	$("select[name='city']").change(function(){
		var code = $(this).val();
		ajaxGetArea(code,'3');
	});
	
	$("select[name='country']").change(function(){
		var code = $(this).val();
		ajaxGetArea(code,'4');
	});
	
	$("select[name='street']").change(function(){
		var code = $(this).val();
		ajaxGetArea(code,'5');
	});

	function ajaxGetArea(code,level){
		$.ajax({
			type: "POST",
			url: "/mobile/ajax_user/ajax_add_data/",
			dataType:'json',
			data:{code:code,level:level},
			success: function(data){
				if(data.status == 1){
					var html = '<option value="0">请选择</option>';
					$.each(data.list,function (index,obj){
						html += '<option value="'+obj.code+'">'+obj.name+'</option>';
					});

					if(level == '2'){
						$("select[name='city']").html(html);
						$("select[name='country']").html(html);
						$("select[name='area']").html(html);
						$("select[name='street']").html(html);
					}else if(level == '3'){
						$("select[name='country']").html(html);
						$("select[name='area']").html(html);
						$("select[name='street']").html(html);
					}else if(level == '4'){
						$("select[name='area']").html(html);
						$("select[name='street']").html(html);
					}else if(level == '5'){
						$("select[name='area']").html(html);
					}
				}
			}
		}); 
	}
</script>
</html>
