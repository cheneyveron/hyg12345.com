<?php 
defined('IN_YYS')or exit('Access Denied.');
System::DOWN_App_class('base','member','no');
System::DOWN_App_fun('my','index');
System::DOWN_App_fun('user','index');
System::DOWN_sys_fun('send');
System::DOWN_sys_fun('user');
class ajax_coupon extends base {
	
	public function __construct(){
		parent::__construct();
		$this->db = System::DOWN_sys_class('model');
	}
	
	public function ajax_index_order(){
		$datatype = $_REQUEST['datatype'];
		$result = $this->$datatype();
		if($result){
			$return['status'] = 1;
		}else{
			$return['status'] = 0;
		}
		$return['list'] = $result;
		echo json_encode($return);
	}
	
	private function getUserCostConpon(){
		$uid = $this->userinfo['uid'];
		$count = $this->db->YCount("select s.*,y.id as gid from `@#_shangpin` as s left join `@#_yonghu_yys_record` as y on s.id = y.shopid where y.`huode` = s.`q_user_code` and s.`q_uid` = '$uid' and y.addressId = '0' and s.isCoupon = '1'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select s.*,y.code,y.status,y.id as gid from `@#_shangpin` as s left join `@#_yonghu_yys_record` as y on s.id = y.shopid where y.`huode` = s.`q_user_code` and s.`q_uid` = '$uid' and y.addressId = '0' and s.isCoupon = '1' ORDER BY y.`id` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));		
		foreach($list as $key => $one){
			$order = $this->db->Yone("select * from `@#_yonghu_yys_record` where `huode` = '$one[q_user_code]' and `uid` = '$uid' and `shopid` = '$one[id]'");
			$list[$key]['order_id'] = $order['id'];
			$list[$key]['time'] = date('Y-m-d H:i:s',$one['q_end_time']);
			
			$supplier = getSupplierInfo($one['supplierId']);
			$address  = getCityName($supplier['country']);
			$address .= getCityName($supplier['street']).$supplier['address'];
			$list[$key]['address'] = $address;
			$status = explode(',',$one['status']);
			/*
			*	1为未发货，2为已发货
			*/
			if($one['addressId'] && in_array('已发货',$status)){
				$list[$key]['express'] = '2';
			}elseif($one['addressId'] && in_array('未发货',$status)){
				$list[$key]['express'] = '1';
			}elseif(in_array('未提交地址',$status)){
				$list[$key]['express'] = '0';
			}
		}	
		return $list;
	}
	
	private function getUserCommentConpon(){
		$uid = $this->userinfo['uid'];
		$count = $this->db->YCount("select s.*,y.id as gid from `@#_shangpin` as s left join `@#_yonghu_yys_record` as y on s.id = y.shopid where y.`huode` = s.`q_user_code` and s.`q_uid` = '$uid' and y.addressId = '0' and s.isCoupon = '1'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");		
		$list = $this->db->YPage("select s.*,y.code,y.status,y.id as gid from `@#_shangpin` as s left join `@#_yonghu_yys_record` as y on s.id = y.shopid where y.`huode` = s.`q_user_code` and s.`q_uid` = '$uid' and y.addressId = '0' and s.isCoupon = '1' ORDER BY y.`id` DESC",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));		
		foreach($list as $key => $one){
			$order = $this->db->Yone("select * from `@#_yonghu_yys_record` where `huode` = '$one[q_user_code]' and `uid` = '$uid' and `shopid` = '$one[id]'");
			$list[$key]['order_id'] = $order['id'];
			$list[$key]['time'] = date('Y-m-d H:i:s',$one['q_end_time']);
			
			$supplier = getSupplierInfo($one['supplierId']);
			$address  = getCityName($supplier['province']).getCityName($supplier['city']).getCityName($supplier['country']);
			$address .= getCityName($supplier['street']).$supplier['address'];
			$list[$key]['address'] = $address;
			$status = explode(',',$one['status']);
			/*
			*	1为未发货，2为已发货
			*/
			if($one['addressId'] && in_array('已发货',$status)){
				$list[$key]['express'] = '2';
			}elseif($one['addressId'] && in_array('未发货',$status)){
				$list[$key]['express'] = '1';
			}elseif(in_array('未提交地址',$status)){
				$list[$key]['express'] = '0';
			}
		}	
		$list = '';
		return $list;
	}	
	
	private function getUserAllConpon(){
		$type = $_REQUEST['type'];
		$uid = $this->userinfo['uid'];
		$count = $this->db->YCount("select * from `@#_yonghu_coupon` where `type` = '$type' and `uid` = '$uid'");
		$fenye = System::DOWN_sys_class('page');
		if(isset($_GET['p'])){$fenyenum=$_GET['p'];}else{$fenyenum=1;}	
		$fenye->config($count,10,$fenyenum,"0");
		$list = $this->db->YPage("select * from `@#_yonghu_coupon` where `type` = '$type' and `uid` = '$uid' order by status asc, id desc",array("num"=>10,"page"=>$fenyenum,"type"=>1,"cache"=>0));
		foreach($list as $key => $one){
			$coupon = $this->db->YOne("select * from `@#_shangpin` where `supplierId` = '$one[sid]'");
			$list[$key]['thumb'] = $coupon['thumb'];
			
			$supplier = $this->db->YOne("select * from `@#_supplier` where `id` = '$one[sid]'");
			$list[$key]['supplierName'] = $supplier['name'];
			$list[$key]['supplierAddress'] = getCityName($supplier['country']).getCityName($supplier['street']);
			if($one['status'] == 1){
				$list[$key]['status'] = '已消费';
			}else{
				$list[$key]['status'] = '未消费';
			}
			
			$list[$key]['create_time'] = date('Y-m-d H:i:s',$one['create_time']);
		}
		
		return $list;
	}	
	
	/*卡券核销*/
	public function costCoupon(){
		$supplierId = $this->userinfo['supplierId'];
		$code = $_REQUEST['code'];
		$secret = $_REQUEST['secret'];
		if(!$code || !$secret){
			$status = 0;
			$msg = '不存在的卡券';
		}else{
			$coupon = $this->db->YOne("select * from `@#_shangpin` where `code` = '$code' and `secret` = '$secret'");
			if(!$coupon){
				$status = 0;
				$msg = '不存在的卡券';
			}elseif($coupon['supplierId'] != $supplierId){
				$status = 0;
				$msg = '请勿操作其他商户卡券';
			}elseif($coupon['isCost'] == 1){
				$status = 0;
				$msg = '该卡券已被核销';
			}else{
				$this->db->tijiao_start();
				$coupon_log = array(
					'sid' => $coupon['id'],
					'supplierId' =>$supplierId,
					'costTime' => time(),
					'costIp' => _huode_ip(),
				);
				$res = insertInto($coupon_log,'coupon_log');
				if($res){
					$shangpin = array(
						'isCost' => 1,
						'costTime' => time(),
						'costSupplier' => $supplierId,
						'costIp' => _huode_ip(),
					);
					$res = updateSet($shangpin,'shangpin',$coupon['id']);
					if($res){
						/*进行结算*/
						$supplierSettlement = $this->supplierSettlement($coupon['id']);
						if($supplierSettlement){
							$status = 1;
							$msg = '核销成功';
							$this->db->tijiao_commit();						
						}else{
							$this->db->tijiao_rollback();	
							$status = 0;
							$msg = '结算失败，请稍后再试';
						}
					}else{
						$this->db->tijiao_rollback();	
						$status = 0;
						$msg = '核销失败，请稍后再试';
					}
				}else{
					$this->db->tijiao_rollback();	
					$status = 0;
					$msg = '核销失败，请稍后再试';
				}
			}
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg));
	}
	
	/*商户结算*/
	private function supplierSettlement(){

		return true;
	}

}